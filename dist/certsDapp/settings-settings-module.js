(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-settings-module"],{

/***/ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js":
/*!****************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/observable/throw.js ***!
  \****************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");

rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].throw = rxjs__WEBPACK_IMPORTED_MODULE_0__["throwError"];
rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].throwError = rxjs__WEBPACK_IMPORTED_MODULE_0__["throwError"];
//# sourceMappingURL=throw.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/map.js":
/*!************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/map.js ***!
  \************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_map__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/map */ "./node_modules/rxjs-compat/_esm5/operator/map.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.map = _operator_map__WEBPACK_IMPORTED_MODULE_1__["map"];
//# sourceMappingURL=map.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js":
/*!****************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/timeout.js ***!
  \****************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_timeout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/timeout */ "./node_modules/rxjs-compat/_esm5/operator/timeout.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.timeout = _operator_timeout__WEBPACK_IMPORTED_MODULE_1__["timeout"];
//# sourceMappingURL=timeout.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js":
/*!********************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js ***!
  \********************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _operator_timeoutWith__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/operator/timeoutWith.js");


rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"].prototype.timeoutWith = _operator_timeoutWith__WEBPACK_IMPORTED_MODULE_1__["timeoutWith"];
//# sourceMappingURL=timeoutWith.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js":
/*!******************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

//# sourceMappingURL=toPromise.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/map.js":
/*!********************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/map.js ***!
  \********************************************************/
/*! exports provided: map */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "map", function() { return map; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

function map(project, thisArg) {
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(project, thisArg)(this);
}
//# sourceMappingURL=map.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/timeout.js":
/*!************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/timeout.js ***!
  \************************************************************/
/*! exports provided: timeout */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timeout", function() { return timeout; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");


function timeout(due, scheduler) {
    if (scheduler === void 0) { scheduler = rxjs__WEBPACK_IMPORTED_MODULE_0__["asyncScheduler"]; }
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["timeout"])(due, scheduler)(this);
}
//# sourceMappingURL=timeout.js.map

/***/ }),

/***/ "./node_modules/rxjs-compat/_esm5/operator/timeoutWith.js":
/*!****************************************************************!*\
  !*** ./node_modules/rxjs-compat/_esm5/operator/timeoutWith.js ***!
  \****************************************************************/
/*! exports provided: timeoutWith */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timeoutWith", function() { return timeoutWith; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");


function timeoutWith(due, withObservable, scheduler) {
    if (scheduler === void 0) { scheduler = rxjs__WEBPACK_IMPORTED_MODULE_0__["asyncScheduler"]; }
    return Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["timeoutWith"])(due, withObservable, scheduler)(this);
}
//# sourceMappingURL=timeoutWith.js.map

/***/ }),

/***/ "./src/app/constants.ts":
/*!******************************!*\
  !*** ./src/app/constants.ts ***!
  \******************************/
/*! exports provided: Constants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Constants", function() { return Constants; });
var Constants = /** @class */ (function () {
    function Constants() {
    }
    Object.defineProperty(Constants, "HOME_URL", {
        get: function () { return 'http://54.157.252.226:9305/api/dapps/048c1869547f60db8a6cd93b06a786abe5f50c4567198f4df0376462f9a20c5e/'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Constants, "HOST_URL", {
        get: function () { return 'http://54.157.252.226:9305/api/dapps/'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Constants, "SWAGGERAPI", {
        get: function () { return 'http://54.254.174.74:8080/api/v1/'; },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(Constants, "SERVERURL", {
        get: function () { return 'http://52.201.227.220:8080/'; },
        enumerable: true,
        configurable: true
    });
    ;
    return Constants;
}());



/***/ }),

/***/ "./src/app/layout/settings/settings-routing.module.ts":
/*!************************************************************!*\
  !*** ./src/app/layout/settings/settings-routing.module.ts ***!
  \************************************************************/
/*! exports provided: SettingsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsRoutingModule", function() { return SettingsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _settings_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./settings.component */ "./src/app/layout/settings/settings.component.ts");




var routes = [{
        path: '', component: _settings_component__WEBPACK_IMPORTED_MODULE_3__["SettingsComponent"]
    }];
var SettingsRoutingModule = /** @class */ (function () {
    function SettingsRoutingModule() {
    }
    SettingsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], SettingsRoutingModule);
    return SettingsRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/settings/settings.component.html":
/*!*********************************************************!*\
  !*** ./src/app/layout/settings/settings.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"height:800px !important\"><br>\n  <br>\n  <div [@.disabled]=\"true\" id=\"wrapper\" style=\"height:100% !important;\">\n    <mat-tab-group disableUnderline={true} dynamicHeight=\"true\" id=\"tab-group\" style=\"height:100% !important;\">\n      <mat-tab label=\"CERTIFICATE FIELDS\">\n\n        <div class=\"col-xl-6 col-lg-6 col-md-6 col-12 left fieldsform PT10 MT10\"\n          style=\"margin-left:2%; height:100% !important\">\n          <form method=\"post\" style=\"height: 100% !important\" action=\"\" [formGroup]=\"fieldForm\"\n            (ngSubmit)=\"setfields()\">\n\n            <div class=\"form-group\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" formControlName=\"studname\" name=\"name\" placeholder=\"Name\" required disabled>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-group\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" formControlName=\"enrollment\" name=\"enrollment no.\"  placeholder=\"Enrollment no.\" required disabled>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-group\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" formControlName=\"degree\" name=\"Degree\" placeholder=\"Degree\" required disabled>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-group\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" formControlName=\"topic\" name=\"Topic\" placeholder=\"Topic\" required disabled>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-group\">\n              <div class=\"input-group\">\n                <input type=\"text\" class=\"form-control\" formControlName=\"year\" name=\"Year\" placeholder=\"Year\" required disabled>\n              </div>\n            </div>\n            <div class=\"form-group\">\n                <div class=\"input-group\">\n                    <input type=\"text\" class=\"form-control\"  class=\"form-control name_list\"  placeholder=\"Enter New Field\" required >\n                    <button  type=\"button\" name=\"add\"  class=\"btn btn-success\" (click)=\"addfieldinput()\">+</button>\n                </div>\n              </div>\n    \n        <br>\n        <br>\n        <div class=\"form-group \">\n          <input type=\"submit\" style=\"left:50%;\" class=\"btn submitbutton\" value=\"Done\">\n        </div>\n        </form>\n\n      </div>\n        <div class=\"col-md-2\">\n          <div class=\"certificate\">\n            <br><br><br><br><br><br>\n            <h3 style=\"text-align:center\"><b> ANDHRA UNIVERSITY</b></h3>\n            <br><br>\n            <p style=\"margin-left: 8%;\">This is to Certify that&nbsp;&nbsp;<span id=\"\"><input type=\"text\"\n                  class=\"underlineinput1\" value=\"\"> </span> </p>\n            <p style=\"margin-left: 8%;\">is awarded with degree of&nbsp;&nbsp; <span id=\"\"><input type=\"text\"\n                  class=\"underlineinput2\" value=\"\"> </span></p>\n            <p style=\"margin-left: 8%;\"><span id=\"\"><input type=\"text\" class=\"underlineinput3\" value=\"\"> </span></p>\n            <p style=\"margin-left: 8%;\">in<span id=\"\"> <input type=\"text\" class=\"underlineinput4\" value=\"\"> </span> </p>\n            <p style=\"margin-left: 8%;\">on having passed the examinations in<span id=\"\"> <input type=\"text\"\n                  class=\"underlineinput5\" value=\"\"> </span> </p>\n\n            <p style=\"margin-left: 8%;\">Enrollment No:<span id=\"\"> <input type=\"text\" class=\"underlineinput6\" value=\"\">\n              </span></p>\n            <p style=\"margin-left: 8%;\">Date:<span id=\"\"> <input type=\"text\" class=\"underlineinput7\" value=\"\">\n              </span></p>\n\n\n          </div>\n        </div>\n      </mat-tab>\n  <mat-tab label=\"MAPPING\">\n    <div class=\"col-xl-10 col-lg-6 col-md-6 col-12 left fieldsform1 PT10 MT10\" style=\"margin-left:2%\">\n      <div class=\"form-row\">\n        <div class=\"form-group col-md-10\">\n          <form method=\"post\" action=\"\" [formGroup]=\"depForm\" (ngSubmit)=\"assignAuthorizers()\">\n            <div class=\"form-row\">\n              <div class=\"form-group col-md-6\">\n                <h5>Department Name</h5>\n                <div class=\"input-group\">\n                  <input type=\"text\" class=\"form-control\" name=\"branch\" formControlName=\"branch\"\n                    placeholder=\"Enter Department name\" required>\n                </div>\n              </div>\n              <div class=\"form-group  col-md-2\">\n                <h5>Levels</h5>\n               \n                <div class=\"input-group\" >\n                    <input type=\"text\" class=\"form-control\" \n                    placeholder=\"3\" required disabled>\n                  <!-- <select class=\"form-control\" formControlName=\"levels\"required>\n                    <option>1</option>\n                    <option>2</option>\n                    <option>3</option>\n                    <option>4</option>\n                    <option>5</option>\n                    <option>6</option>\n                  </select>  -->\n                </div>\n              </div>\n            </div>\n            <br>\n            <h5>Enter Details</h5>\n            <div class=\"form-row\">\n              <div class=\"form-group col-md-6\">\n                <div class=\"input-group\">\n                  <h5 style=\"margin-top:15px\">level1:</h5>&nbsp;&nbsp;&nbsp;\n                  <select type=\"text\" class=\"form-control\" formControlName=\"auth1\"  id=\"level1\" name=\"level1\" (change)=trimauth($event.target) placeholder=\"Authorizer1@yopmail.com\"\n                    required>\n                  <option>none</option>\n                  <option *ngFor=\"let auth of authorizers\">{{auth.email}}</option>\n                </select>\n\n                </div>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-row\">\n\n              <div class=\"form-group col-md-6\">\n                <div class=\"input-group\">\n                  <h5 style=\"margin-top:15px\">level2:</h5>&nbsp;&nbsp;&nbsp;\n\n                  <select type=\"text\" class=\"form-control\" name=\"level2\" formControlName=\"auth2\" placeholder=\"\"\n                    required>\n                    <option>none</option>\n                  <option *ngFor=\"let auth of authorizers\">{{auth.email}}</option>\n\n                    </select>\n                </div>\n              </div>\n            </div>\n            <br>\n            <div class=\"form-row\">\n              <div class=\"form-group col-md-6\">\n                <div class=\"input-group\">\n                  <h5 style=\"margin-top:15px\">level3:</h5>&nbsp;&nbsp;&nbsp;\n                   <select type=\"text\" class=\"form-control\" name=\"level3\" formControlName=\"auth3\" name=\"auth1\"\n                    placeholder=\"Authorizer3@yopmail.com\" required>\n                    <option>none</option>\n                  <option *ngFor=\"let auth of authorizers\">{{auth.email}}</option>\n\n                    </select>\n                </div>\n              </div>\n\n            </div>\n            <br>\n            <br>\n            <br>\n            <!-- <div class=\"form-group \">\n              <input type=\"submit\" class=\"btn submitbutton\" style=\"left:28%;\" value=\"Submit\" (click)=\"openModal()\">\n            </div> -->\n          </form><br><br>\n\n          <!--Adding Table -->\n          <!-- <div id=\"tab1\" class=\"tab-pane active\">\n              <table class=\"table table-hover superadmintables\" style=\"border:1px solid #ccc;\">\n                <thead>\n                  <tr>\n                    <th scope=\"col\">Department Name</th>\n                    <th scope=\"col\">Levels</th>\n                    <th scope=\"col\"></th>\n                   \n                  </tr>\n                </thead>\n                <tbody>\n\n                  <tr *ngFor=\"\">\n                  \n                    <td>{{issuer['departmentname']}}</td>\n                    <td>{{issuer['levels']}}</td>\n                     \n                  </tr>\n                </tbody>\n              </table>\n              </div> -->\n \n        </div>\n        <div class=\"from-group col-md-2\">\n      <br><br>\n          <p class=\"rectangle\">Level 1 <br>abc@yopmail.com</p>\n          <p class=\"rectangle\">Level 2 <br>abc@yopmail.com</p>\n          <p class=\"rectangle\">Level 3<br>abc@yopmail.com</p>\n          <br><br>\n          \n\n        </div>\n      </div>\n\n    </div>\n\n  </mat-tab>\n\n  </mat-tab-group>\n\n</div>\n</div>"

/***/ }),

/***/ "./src/app/layout/settings/settings.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/layout/settings/settings.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  font-size: 14px;\n  font-family: Muli; }\n\n.mainlist a {\n  font-family: Muli;\n  font-size: 20px;\n  font-weight: 800;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #223e60; }\n\n.nav-tabs {\n  border-bottom: 0px solid #ddd; }\n\n.fieldsform input[type=\"text\"] {\n  width: 407px;\n  height: 42px;\n  border-radius: 4px;\n  background-color: var(--white-grey); }\n\n.fieldsform1 input[type=\"text\"] {\n  width: 435px;\n  height: 45px;\n  border-radius: 4px;\n  background-color: var(--white); }\n\n.fieldsform1 h5 {\n  /* width: 131px;\n\n    height: 19px; */\n  font-family: Muli;\n  font-size: 18px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  /* text-align: left; */\n  color: #cbcbcb; }\n\n.submitbutton {\n  width: 185px;\n  height: 45px;\n  border-radius: 10px;\n  background-color: #f87d42;\n  font-family: Muli;\n  font-size: 17px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white);\n  float: none;\n  position: absolute;\n  /* top: 50%; */\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n\nselect.form-control:not([size]):not([multiple]) {\n  height: 45px;\n  width: 159px;\n  border-radius: 4px;\n  background-color: var(--white); }\n\n.form-control {\n  display: block;\n  width: 100%;\n  padding: 0.375rem 0.75rem;\n  font-size: 16px;\n  line-height: 1.5;\n  color: #495057;\n  background-color: #fff;\n  background-clip: padding-box;\n  border: 1px solid #ced4da;\n  border-radius: 0.25rem;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out; }\n\n.initiatecircle {\n  width: 29.4px;\n  height: 29.4px;\n  background-color: #4879ff; }\n\n.rectangle {\n  width: 200px;\n  height: 55px;\n  border-radius: 5px;\n  background-color: #fdcc00;\n  color: #ffffff;\n  text-align: center;\n  font-size: 15px; }\n\n.issued {\n  width: 200px;\n  height: 55px;\n  border-radius: 5px;\n  background-color: #ffffff;\n  border: 3px solid green;\n  color: green;\n  text-align: center;\n  font-size: 40px;\n  -webkit-transform: rotate(-6deg);\n          transform: rotate(-6deg); }\n\n.blur {\n  color: transparent;\n  text-shadow: 0 0 5px rgba(31, 226, 106, 0.5); }\n\n.PT10 {\n  padding-top: 10ps !important; }\n\n.MT10 {\n  margin-top: 30px !important; }\n\n.MB10 {\n  margin-bottom: 10px !important; }\n\n#wrapper {\n  padding: 16px;\n  min-height: 100% !important;\n  height: 100% !important;\n  box-sizing: border-box !important;\n  /*new*/ }\n\n#tab-group {\n  height: 100% !important; }\n\n#tab-group mat-tab-body {\n  flex-grow: 1 !important; }\n\n.certificate {\n  width: 410px;\n  height: 563px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n  border: 0px solid #000;\n  background-image: url('Certbgfix.png');\n  margin-left: 40%;\n  font-family: Muli;\n  border-radius: 10px; }\n\n.underlineinput1 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 200px;\n  font-size: 18px; }\n\n.underlineinput2 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 170px;\n  font-size: 18px; }\n\n.underlineinput3 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 333px;\n  font-size: 18px; }\n\n.underlineinput4 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 320px;\n  font-size: 18px; }\n\n.underlineinput5 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 110px;\n  font-size: 18px; }\n\n.underlineinput6 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 100px;\n  font-size: 18px; }\n\n.underlineinput7 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 100px;\n  font-size: 18px; }\n\n.underlineunversity {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 200px;\n  margin-left: 25%;\n  font-size: 18px; }\n\n.underlinename {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 200px;\n  margin-left: 25%;\n  font-size: 18px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9sYXlvdXQvc2V0dGluZ3Mvc2V0dGluZ3MuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7O0FBRXJCO0VBRUksaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixnQkFBZ0I7RUFFaEIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixpQkFBaUI7RUFFakIsc0JBQXNCO0VBRXJCLGdCQUFnQjtFQUVoQixjQUFjLEVBQUE7O0FBR25CO0VBRUksNkJBQTZCLEVBQUE7O0FBSWpDO0VBQ0ksWUFBWTtFQUVaLFlBQVk7RUFFWixrQkFBa0I7RUFFbEIsbUNBQW1DLEVBQUE7O0FBTXZDO0VBQ0ksWUFBWTtFQUVaLFlBQVk7RUFFWixrQkFBa0I7RUFFbEIsOEJBQThCLEVBQUE7O0FBR2xDO0VBQ0k7O21CQ3RCZTtFRDBCZixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGdCQUFnQjtFQUVoQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsc0JBQUE7RUFFQSxjQUFjLEVBQUE7O0FBSWxCO0VBQ0ksWUFBWTtFQUVaLFlBQVk7RUFFWixtQkFBbUI7RUFFbkIseUJBQXlCO0VBQ3JCLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsY0FBQTtFQUVBLHdDQUFnQztVQUFoQyxnQ0FBZ0MsRUFBQTs7QUFlcEM7RUFDSSxZQUFZO0VBQ1osWUFBWTtFQUlaLGtCQUFrQjtFQUVsQiw4QkFBOEIsRUFBQTs7QUFLbEM7RUFDSSxjQUFjO0VBQ2QsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsNEJBQTRCO0VBQzVCLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIsd0VBQXdFLEVBQUE7O0FBRzVFO0VBQ1EsYUFBYTtFQUNiLGNBQWM7RUFDZCx5QkFBeUIsRUFBQTs7QUFFakM7RUFDSSxZQUFZO0VBQ1osWUFBWTtFQUNaLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3hCLHVCQUF1QjtFQUN4QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixnQ0FBd0I7VUFBeEIsd0JBQXdCLEVBQUE7O0FBRTFCO0VBQ0Usa0JBQWtCO0VBQ2xCLDRDQUE0QyxFQUFBOztBQUUvQztFQUNJLDRCQUE0QixFQUFBOztBQUdoQztFQUNJLDJCQUEyQixFQUFBOztBQUcvQjtFQUNJLDhCQUE4QixFQUFBOztBQUVsQztFQUNHLGFBQWE7RUFDYiwyQkFBMkI7RUFDM0IsdUJBQXVCO0VBQ3ZCLGlDQUFpQztFQUFDLE1BQUEsRUFBTzs7QUFFM0M7RUFDRSx1QkFBdUIsRUFBQTs7QUFFekI7RUFDRSx1QkFBdUIsRUFBQTs7QUFFekI7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLDJDQUEyQztFQUMzQyxzQkFBcUI7RUFDckIsc0NBQTBEO0VBQzFELGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0ksU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxlQUFlLEVBQUE7O0FBRW5CO0VBQ0UsU0FBUTtFQUNSLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsOEJBQThCO0VBQzlCLFlBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXJ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xufVxuLm1haW5saXN0IGF7XG4gIFxuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBcbiAgICBmb250LXNpemU6IDIwcHg7XG4gIFxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIFxuICAgIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICBcbiAgICAgdGV4dC1hbGlnbjogbGVmdDsgXG4gIFxuICAgICBjb2xvcjogIzIyM2U2MDtcbn0gIFxuXG4ubmF2LXRhYnMge1xuXG4gICAgYm9yZGVyLWJvdHRvbTogMHB4IHNvbGlkICNkZGQ7XG4gICAgXG4gICAgfVxuXG4uZmllbGRzZm9ybSBpbnB1dFt0eXBlPVwidGV4dFwiXXtcbiAgICB3aWR0aDogNDA3cHg7XG5cbiAgICBoZWlnaHQ6IDQycHg7XG4gIFxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUtZ3JleSk7XG4gICAgXG4gICAgXG59XG5cblxuLmZpZWxkc2Zvcm0xIGlucHV0W3R5cGU9XCJ0ZXh0XCJde1xuICAgIHdpZHRoOiA0MzVweDtcblxuICAgIGhlaWdodDogNDVweDtcbiAgXG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG59XG5cbi5maWVsZHNmb3JtMSBoNXtcbiAgICAvKiB3aWR0aDogMTMxcHg7XG5cbiAgICBoZWlnaHQ6IDE5cHg7ICovXG4gIFxuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBcbiAgICBmb250LXNpemU6IDE4cHg7XG4gIFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIFxuICAgIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICBcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICBcbiAgICAvKiB0ZXh0LWFsaWduOiBsZWZ0OyAqL1xuICBcbiAgICBjb2xvcjogI2NiY2JjYjtcbiAgICBcbn1cblxuLnN1Ym1pdGJ1dHRvbntcbiAgICB3aWR0aDogMTg1cHg7XG5cbiAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y4N2Q0MjtcbiAgICAgICAgZm9udC1mYW1pbHk6IE11bGk7XG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgICAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgICAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuMjM7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgICAgICAgZmxvYXQ6IG5vbmU7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgLyogdG9wOiA1MCU7ICovXG4gICAgICBcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG5cbiAgICB9XG5cblxuICAgIC8vIC5sZXZlbHN7XG4gICAgLy8gICAgIHdpZHRoOiAxNTlweDtcblxuICAgICAgIFxuICAgICAgXG4gICAgLy8gICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgIFxuICAgIC8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gICAgLy8gfVxuXG4gICAgc2VsZWN0LmZvcm0tY29udHJvbDpub3QoW3NpemVdKTpub3QoW211bHRpcGxlXSkge1xuICAgICAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgICAgIHdpZHRoOiAxNTlweDtcblxuICAgICAgIFxuICAgICAgXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgIFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gICAgfVxuXG5cblxuICAgIC5mb3JtLWNvbnRyb2wge1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgICAgICAgY29sb3I6ICM0OTUwNTc7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgICAgIGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNjZWQ0ZGE7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDAuMjVyZW07XG4gICAgICAgIHRyYW5zaXRpb246IGJvcmRlci1jb2xvciAwLjE1cyBlYXNlLWluLW91dCwgYm94LXNoYWRvdyAwLjE1cyBlYXNlLWluLW91dDtcbiAgICB9XG5cbiAgICAuaW5pdGlhdGVjaXJjbGUge1xuICAgICAgICAgICAgd2lkdGg6IDI5LjRweDtcbiAgICAgICAgICAgIGhlaWdodDogMjkuNHB4O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4NzlmZjtcbiAgICB9XG4gICAgLnJlY3RhbmdsZSB7XG4gICAgICAgIHdpZHRoOiAyMDBweDtcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZGNjMDA7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgIH1cbiAgICAgIC5pc3N1ZWQge1xuICAgICAgICB3aWR0aDogMjAwcHg7XG4gICAgICAgIGhlaWdodDogNTVweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xuICAgICAgICAgYm9yZGVyOiAzcHggc29saWQgZ3JlZW47XG4gICAgICAgIGNvbG9yOiBncmVlbjtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDQwcHg7XG4gICAgICAgIHRyYW5zZm9ybTogcm90YXRlKC02ZGVnKTtcbiAgICAgIH1cbiAgICAgIC5ibHVyIHtcbiAgICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xuICAgICAgICB0ZXh0LXNoYWRvdzogMCAwIDVweCByZ2JhKDMxLCAyMjYsIDEwNiwgMC41KTtcbiAgICAgfVxuICAgICAuUFQxMHtcbiAgICAgICAgIHBhZGRpbmctdG9wOiAxMHBzICFpbXBvcnRhbnQ7XG4gICAgIH1cblxuICAgICAuTVQxMHtcbiAgICAgICAgIG1hcmdpbi10b3A6IDMwcHggIWltcG9ydGFudDtcbiAgICAgfVxuXG4gICAgIC5NQjEwe1xuICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xuICAgICB9XG4gICAgICN3cmFwcGVye1xuICAgICAgICBwYWRkaW5nOiAxNnB4OyBcbiAgICAgICAgbWluLWhlaWdodDogMTAwJSAhaW1wb3J0YW50OyBcbiAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7IFxuICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94ICFpbXBvcnRhbnQ7LypuZXcqL1xuICAgICAgfVxuICAgICAgI3RhYi1ncm91cHtcbiAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICB9XG4gICAgICAjdGFiLWdyb3VwIG1hdC10YWItYm9keSB7XG4gICAgICAgIGZsZXgtZ3JvdzogMSAhaW1wb3J0YW50O1xuICAgICAgfVxuICAgICAgLmNlcnRpZmljYXRle1xuICAgICAgICB3aWR0aDogNDEwcHg7XG4gICAgICAgIGhlaWdodDogNTYzcHg7XG4gICAgICAgIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XG4gICAgICAgIGJvcmRlcjowcHggc29saWQgIzAwMDtcbiAgICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9DZXJ0YmdmaXgucG5nXCIpO1xuICAgICAgICBtYXJnaW4tbGVmdDogNDAlO1xuICAgICAgICBmb250LWZhbWlseTogTXVsaTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDF7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjIwMHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDJ7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjE3MHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDN7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjMzM3B4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDR7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjMyMHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDV7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjExMHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDZ7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjEwMHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmVpbnB1dDd7XG4gICAgICAgICAgYm9yZGVyOjA7XG4gICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgICAgICAgIHdpZHRoOjEwMHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIH1cbiAgICAgIC51bmRlcmxpbmV1bnZlcnNpdHl7XG4gICAgICAgIGJvcmRlcjowO1xuICAgICAgICBvdXRsaW5lOiAwO1xuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGJsYWNrOyBcbiAgICAgICAgd2lkdGg6MjAwcHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyNSU7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB9XG4gICAgLnVuZGVybGluZW5hbWV7XG4gICAgICAgIGJvcmRlcjowO1xuICAgICAgICBvdXRsaW5lOiAwO1xuICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGJsYWNrOyBcbiAgICAgICAgd2lkdGg6MjAwcHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyNSU7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICB9XG4iLCIuY29udGFpbmVyIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LWZhbWlseTogTXVsaTsgfVxuXG4ubWFpbmxpc3QgYSB7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzIyM2U2MDsgfVxuXG4ubmF2LXRhYnMge1xuICBib3JkZXItYm90dG9tOiAwcHggc29saWQgI2RkZDsgfVxuXG4uZmllbGRzZm9ybSBpbnB1dFt0eXBlPVwidGV4dFwiXSB7XG4gIHdpZHRoOiA0MDdweDtcbiAgaGVpZ2h0OiA0MnB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlLWdyZXkpOyB9XG5cbi5maWVsZHNmb3JtMSBpbnB1dFt0eXBlPVwidGV4dFwiXSB7XG4gIHdpZHRoOiA0MzVweDtcbiAgaGVpZ2h0OiA0NXB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlKTsgfVxuXG4uZmllbGRzZm9ybTEgaDUge1xuICAvKiB3aWR0aDogMTMxcHg7XG5cbiAgICBoZWlnaHQ6IDE5cHg7ICovXG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICAvKiB0ZXh0LWFsaWduOiBsZWZ0OyAqL1xuICBjb2xvcjogI2NiY2JjYjsgfVxuXG4uc3VibWl0YnV0dG9uIHtcbiAgd2lkdGg6IDE4NXB4O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmODdkNDI7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE3cHg7XG4gIGZvbnQtd2VpZ2h0OiA5MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjIzO1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZsb2F0OiBub25lO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8qIHRvcDogNTAlOyAqL1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTsgfVxuXG5zZWxlY3QuZm9ybS1jb250cm9sOm5vdChbc2l6ZV0pOm5vdChbbXVsdGlwbGVdKSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDE1OXB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlKTsgfVxuXG4uZm9ybS1jb250cm9sIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGNvbG9yOiAjNDk1MDU3O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2VkNGRhO1xuICBib3JkZXItcmFkaXVzOiAwLjI1cmVtO1xuICB0cmFuc2l0aW9uOiBib3JkZXItY29sb3IgMC4xNXMgZWFzZS1pbi1vdXQsIGJveC1zaGFkb3cgMC4xNXMgZWFzZS1pbi1vdXQ7IH1cblxuLmluaXRpYXRlY2lyY2xlIHtcbiAgd2lkdGg6IDI5LjRweDtcbiAgaGVpZ2h0OiAyOS40cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0ODc5ZmY7IH1cblxuLnJlY3RhbmdsZSB7XG4gIHdpZHRoOiAyMDBweDtcbiAgaGVpZ2h0OiA1NXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZGNjMDA7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTVweDsgfVxuXG4uaXNzdWVkIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDU1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyOiAzcHggc29saWQgZ3JlZW47XG4gIGNvbG9yOiBncmVlbjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDQwcHg7XG4gIHRyYW5zZm9ybTogcm90YXRlKC02ZGVnKTsgfVxuXG4uYmx1ciB7XG4gIGNvbG9yOiB0cmFuc3BhcmVudDtcbiAgdGV4dC1zaGFkb3c6IDAgMCA1cHggcmdiYSgzMSwgMjI2LCAxMDYsIDAuNSk7IH1cblxuLlBUMTAge1xuICBwYWRkaW5nLXRvcDogMTBwcyAhaW1wb3J0YW50OyB9XG5cbi5NVDEwIHtcbiAgbWFyZ2luLXRvcDogMzBweCAhaW1wb3J0YW50OyB9XG5cbi5NQjEwIHtcbiAgbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50OyB9XG5cbiN3cmFwcGVyIHtcbiAgcGFkZGluZzogMTZweDtcbiAgbWluLWhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveCAhaW1wb3J0YW50O1xuICAvKm5ldyovIH1cblxuI3RhYi1ncm91cCB7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50OyB9XG5cbiN0YWItZ3JvdXAgbWF0LXRhYi1ib2R5IHtcbiAgZmxleC1ncm93OiAxICFpbXBvcnRhbnQ7IH1cblxuLmNlcnRpZmljYXRlIHtcbiAgd2lkdGg6IDQxMHB4O1xuICBoZWlnaHQ6IDU2M3B4O1xuICBib3gtc2hhZG93OiAwIDNweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xuICBib3JkZXI6IDBweCBzb2xpZCAjMDAwO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvaW1nL0NlcnRiZ2ZpeC5wbmdcIik7XG4gIG1hcmdpbi1sZWZ0OiA0MCU7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBib3JkZXItcmFkaXVzOiAxMHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDEge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAyMDBweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDIge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAxNzBweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDMge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAzMzNweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDQge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAzMjBweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDUge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAxMTBweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDYge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAxMDBweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVpbnB1dDcge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAxMDBweDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmV1bnZlcnNpdHkge1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7XG4gIHdpZHRoOiAyMDBweDtcbiAgbWFyZ2luLWxlZnQ6IDI1JTtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbi51bmRlcmxpbmVuYW1lIHtcbiAgYm9yZGVyOiAwO1xuICBvdXRsaW5lOiAwO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGJsYWNrO1xuICB3aWR0aDogMjAwcHg7XG4gIG1hcmdpbi1sZWZ0OiAyNSU7XG4gIGZvbnQtc2l6ZTogMThweDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/layout/settings/settings.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/layout/settings/settings.component.ts ***!
  \*******************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_settings_settings_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/settings/settings.service */ "./src/app/services/settings/settings.service.ts");
/* harmony import */ var src_app_services_superadmin_superadmin_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/superadmin/superadmin.service */ "./src/app/services/superadmin/superadmin.service.ts");





var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(settingsService, superadminService) {
        this.settingsService = settingsService;
        this.superadminService = superadminService;
        this.show = true;
    }
    SettingsComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.depForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                            branch: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2)]),
                            levels: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            auth1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            auth2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            auth3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                        });
                        this.fieldForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                            studname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            enrollment: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            degree: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            topic: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            year: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            seal: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            sign: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                        });
                        return [4 /*yield*/, this.superadminService.getAuthorizerAPI()];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        console.log(typeof (res));
                        this.authorizers = res['authorizers'];
                        console.log(this.authorizers);
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsComponent.prototype.showpage = function (b) {
        if (b == "initial") {
            this.show = true;
        }
        else {
            this.show = false;
        }
    };
    SettingsComponent.prototype.openModal = function () {
        console.log("clicked");
    };
    SettingsComponent.prototype.setfields = function () {
        console.log("fieldsdefined");
    };
    SettingsComponent.prototype.assignAuthorizers = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.assignDepartments()];
                    case 1:
                        response = _a.sent();
                        console.log(JSON.stringify(response));
                        if (response["isSuccess"] === true) {
                            console.log(response);
                            alert("Created department and assigned");
                        }
                        else {
                            alert("unsuccess");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsComponent.prototype.assignDepartments = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var d, level1, level2, level3, params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        d = this.depForm.value.branch;
                        console.log(this.depForm.value.auth1);
                        if (this.depForm.value.auth1 != "none")
                            level1 = this.getauthid(this.depForm.value.auth1);
                        else {
                            level1 = "null";
                        }
                        if (this.depForm.value.auth2 != "none") {
                            level2 = this.getauthid(this.depForm.value.auth2);
                        }
                        else {
                            level2 = "null";
                        }
                        if (this.depForm.value.auth3) {
                            level3 = this.getauthid(this.depForm.value.auth3);
                        }
                        else {
                            level3 = "null";
                        }
                        params = {
                            department: d,
                            levels: [level1, level2, level3]
                        };
                        return [4 /*yield*/, this.settingsService.assigndepartments(params)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    SettingsComponent.prototype.getauthid = function (email) {
        for (var i = 0; i < this.authorizers.length; i++) {
            while (email === this.authorizers[i]['email']) {
                console.log(this.authorizers[i]['aid']);
                return this.authorizers[i]['aid'];
            }
        }
    };
    // http://{{blockchain}}/api/dapps/{{dappid}}/department/assignAuthorizers
    // Inputs: 
    // {
    // 	"department": "HR",
    // 	"levels": ["null", "null", "null"]
    // }
    // Outputs: 
    // {
    //     "isSuccess": true,
    //     "message": "Created department and assigned",
    //     "success": true
    // }
    SettingsComponent.prototype.trimauth = function (e) {
        console.log(e.value);
        console.log(e.name);
        //  var name=e.name;
        // var arr = ["level1", "level2", "level3"]
        // this.trimlevels[name]=e.value;
        //   var seld = []
        //  for(var i=0;i<3;i++){
        //    var x = this.depForm.value(arr[i]);
        //        seld.push(x);
        //  }
        //  console.log(x);
        //  console.log(seld);
        // console.log(seld);
        // for (i = 0; i < 3; i++) {
        //     // if (arr[i] != e.id) {
        //     //     x = document.getElementById(arr[i])
        //     //     var option = '<option>none</option>';
        //     //     for (j = 0; j < auth.authorizers.length; j++) {
        //     //         if (!levels.includes(auth.authorizers[j].email))
        //     //                     option += ("<option>" + auth.authorizers[j].email + "</option>");
        //     //             }
        //     //             // if(levels[i]!='none'){
        //     //             //     option+='<option>'+levels[i]+'</option>';
        //     //             // }
        //     //     document.getElementById(arr[i]).innerHTML = option;
        //     // }
        //     var x = document.getElementById(arr[i]);
        //     console.log(x.selectedIndex);
        //     for(var opt = 1 ;opt<x.childElementCount ; opt++){
        //         if(opt != x.selectedIndex){
        //         if(seld.includes(opt) ){
        //             x.children[opt].style.display='none';
        //             console.log(i +" "+ opt);
        //         }
        //         else{
        //             x.children[opt].style.display='block';
        //         }
        //     }
        //     }
        // }
    };
    SettingsComponent.prototype.addfieldinput = function () {
        console.log("hellllllllllll");
    };
    SettingsComponent.prototype.customfieldefine = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.definecustomfields()];
                    case 1:
                        response = _a.sent();
                        console.log(JSON.stringify(response));
                        if (response["isSuccess"] === true) {
                            console.log(response);
                            alert("successful!");
                        }
                        else {
                            alert("unsuccess");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsComponent.prototype.definecustomfields = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var n, a, c, m, ad, fields, identity, params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        n = this.name;
                        a = this.addr;
                        c = this.course;
                        m = this.marks;
                        ad = this.adhar;
                        fields = {
                            Name: n,
                            Address: a,
                            Course: c,
                            Marks: m,
                        };
                        identity = {
                            adharCard: ad
                        };
                        params = {
                            fields: fields,
                            identity: identity
                        };
                        return [4 /*yield*/, this.settingsService.definecustomfields(params)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    SettingsComponent.prototype.getCustomFields = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getcustomfields()];
                    case 1:
                        response = _a.sent();
                        console.log(JSON.stringify(response));
                        if (response["isSuccess"] === true) {
                            console.log(response);
                            alert("successful!");
                        }
                        else {
                            alert("unsuccess");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SettingsComponent.prototype.getcustomfields = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.settingsService.getcustomfields()];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    SettingsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/layout/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.scss */ "./src/app/layout/settings/settings.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_settings_settings_service__WEBPACK_IMPORTED_MODULE_3__["SettingsService"], src_app_services_superadmin_superadmin_service__WEBPACK_IMPORTED_MODULE_4__["SuperadminService"]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/layout/settings/settings.module.ts":
/*!****************************************************!*\
  !*** ./src/app/layout/settings/settings.module.ts ***!
  \****************************************************/
/*! exports provided: SettingsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsModule", function() { return SettingsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _settings_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./settings.component */ "./src/app/layout/settings/settings.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _settings_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./settings-routing.module */ "./src/app/layout/settings/settings-routing.module.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");







var SettingsModule = /** @class */ (function () {
    function SettingsModule() {
    }
    SettingsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_settings_component__WEBPACK_IMPORTED_MODULE_3__["SettingsComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _settings_routing_module__WEBPACK_IMPORTED_MODULE_5__["SettingsRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__["MatTabsModule"]
            ]
        })
    ], SettingsModule);
    return SettingsModule;
}());



/***/ }),

/***/ "./src/app/services/localstorage/localstorage.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/localstorage/localstorage.service.ts ***!
  \***************************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.setEmail = function (email) {
        localStorage.setItem('email', email);
    };
    LocalstorageService.prototype.getEmail = function () {
        return localStorage.getItem('email');
    };
    LocalstorageService.prototype.setLoggedIn = function (login) {
        localStorage.setItem('isLoggedIn', login);
    };
    LocalstorageService.prototype.getLoggedIn = function () {
        return localStorage.getItem('isLoggedIn');
    };
    // sets belrium token
    LocalstorageService.prototype.setBelriumToken = function (token) {
        localStorage.setItem('token', token);
    };
    //gets belrium token
    LocalstorageService.prototype.getBelriumToken = function () {
        return localStorage.getItem('token');
    };
    // sets bkvs-dm token
    LocalstorageService.prototype.setBKVSToken = function (bkvstoken) {
        localStorage.setItem('bkvs-token', bkvstoken);
    };
    //gets bkvs-dm token
    LocalstorageService.prototype.getBKVSToken = function () {
        return localStorage.getItem('bkvs-token');
    };
    // sets role of an user
    LocalstorageService.prototype.setUserRole = function (role) {
        localStorage.setItem('role', role);
    };
    //gets role of an user
    LocalstorageService.prototype.getUserRole = function () {
        return localStorage.getItem('role');
    };
    // sets wallet balance
    LocalstorageService.prototype.setBalance = function (balance) {
        localStorage.setItem('balance', balance);
    };
    //gets wallet balance
    LocalstorageService.prototype.getBalance = function () {
        return localStorage.getItem('balance');
    };
    // sets country of an user
    LocalstorageService.prototype.setCountryName = function (countryName) {
        localStorage.setItem('countryName', countryName);
    };
    //gets country of an user
    LocalstorageService.prototype.getCountryName = function () {
        return localStorage.getItem('countryName');
    };
    //sets dapp id for the user
    LocalstorageService.prototype.setDappId = function (dappid) {
        localStorage.setItem('dappid', dappid);
    };
    //gets dapp id for the suer
    LocalstorageService.prototype.getDappId = function () {
        return localStorage.getItem('dappid');
    };
    LocalstorageService.prototype.setKycStatus = function (kycstatus) {
        localStorage.setItem('kycstatus', kycstatus);
    };
    LocalstorageService.prototype.getKycStatus = function () {
        return localStorage.getItem('kycstatus');
    };
    LocalstorageService.prototype.setWalletAddress = function (address) {
        localStorage.setItem('address', address);
    };
    LocalstorageService.prototype.getWalletAddress = function () {
        return localStorage.getItem('address');
    };
    LocalstorageService.prototype.setOrganization = function (organization) {
        localStorage.setItem('organization', organization);
    };
    LocalstorageService.prototype.getOrganization = function () {
        return localStorage.getItem('organization');
    };
    LocalstorageService.prototype.setUserName = function (name) {
        localStorage.setItem('name', name);
    };
    LocalstorageService.prototype.getUserName = function () {
        return localStorage.getItem('name');
    };
    LocalstorageService.prototype.setIssuerId = function (id) {
        localStorage.setItem('issuerid', id);
    };
    LocalstorageService.prototype.getIssuerId = function () {
        return localStorage.getItem('issuerid');
    };
    LocalstorageService.prototype.setIssuerDept = function (dept) {
        localStorage.setItem('issuerDept', dept);
    };
    LocalstorageService.prototype.getIssuerDept = function () {
        return localStorage.getItem('issuerDept');
    };
    LocalstorageService.prototype.setAuthorizerId = function (id) {
        localStorage.setItem('Authorizerid', id);
    };
    LocalstorageService.prototype.getAuthorizerId = function () {
        return localStorage.getItem('Authorizerid');
    };
    LocalstorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ }),

/***/ "./src/app/services/settings/settings.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/settings/settings.service.ts ***!
  \*******************************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var src_app_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/constants */ "./src/app/constants.ts");






var SettingsService = /** @class */ (function () {
    function SettingsService(http, requestOptions, local) {
        this.http = http;
        this.requestOptions = requestOptions;
        this.local = local;
        this.dappid = local.getDappId();
        this.definefileds = src_app_constants__WEBPACK_IMPORTED_MODULE_5__["Constants"].HOST_URL + this.dappid + '/customFields/define';
        this.getfields = src_app_constants__WEBPACK_IMPORTED_MODULE_5__["Constants"].HOST_URL + this.dappid + '/customFields/get';
        this.assignAuthorizers1Url = src_app_constants__WEBPACK_IMPORTED_MODULE_5__["Constants"].HOST_URL + this.dappid + '/department/assignAuthorizers';
    }
    SettingsService.prototype.definecustomfields = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.definefileds, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SettingsService.prototype.getcustomfields = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getfields, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SettingsService.prototype.assigndepartments = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.assignAuthorizers1Url, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SettingsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_2__["RequestOptionsService"], _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__["LocalstorageService"]])
    ], SettingsService);
    return SettingsService;
}());



/***/ })

}]);
//# sourceMappingURL=settings-settings-module.js.map