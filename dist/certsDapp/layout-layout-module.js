(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["layout-layout-module"],{

/***/ "./src/app/layout/components/footer/footer.component.html":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/footer/footer.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"fixed-bottom\">\n\n   \n    <p class=\"footer\"> Copyright @2018 Belrium. All rights reserved</p>\n</nav>\n"

/***/ }),

/***/ "./src/app/layout/components/footer/footer.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/footer/footer.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\nbutton:disabled {\n  cursor: not-allowed;\n  pointer-events: all !important; }\n:host .vl {\n  margin-top: 9px;\n  border-left: 1px solid #fff;\n  height: 41px;\n  margin-left: -5px; }\n:host .navbar {\n  background-color: #43b02a;\n  height: 45px;\n  padding-right: 0px; }\n:host .navbar .navbar-brand {\n    color: #fff; }\n:host .navbar .nav-item > a {\n    color: #fff; }\n:host .navbar .nav-item > a:hover {\n      color: #fff; }\n.headertext {\n  color: #fff;\n  font-style: bold; }\n.notificationheader {\n  width: 525px;\n  margin-top: -27px; }\n.notificationimage {\n  position: relative;\n  text-align: center;\n  color: #fff; }\n.center-left {\n  position: absolute;\n  top: 5px;\n  left: 16px; }\nli {\n  list-style-type: none; }\na:focus {\n  outline: none; }\n.scroll-container {\n  width: 525px;\n  height: 70vh;\n  top: 5px; }\n.notificationmessage {\n  background-color: #ECF0F1;\n  width: 91%;\n  margin: 5px 0px; }\n/* Set a style for all buttons */\nbutton {\n  background-color: #43b02a;\n  color: #fff;\n  padding: 14px 20px;\n  margin: 8px 0;\n  border: none;\n  cursor: pointer;\n  height: inherit; }\n.notidays {\n  text-align: right;\n  width: 20%;\n  position: relative; }\n.notimessage {\n  text-align: left;\n  height: 100%;\n  line-height: 30px;\n  width: 70%;\n  padding-left: 8px; }\n.bolderfont {\n  font-weight: bolder; }\n.font-100 {\n  font-weight: 100; }\n.close-icon {\n  font-size: 1.3rem;\n  font-weight: 100;\n  cursor: pointer; }\n.img-background {\n  background-color: #43b02a;\n  text-align: center;\n  vertical-align: middle;\n  padding-top: 40px; }\n.text-background {\n  border-right: 3px solid #ECF0F1;\n  text-align: left;\n  vertical-align: middle;\n  padding-top: 40px; }\n.info-header {\n  font-size: 58px;\n  font-weight: bolder;\n  color: #43b02a;\n  text-align: center; }\n.vertical-center {\n  position: absolute;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  top: 50%;\n  left: 55%; }\n.green {\n  color: #43b02a; }\nbutton:hover {\n  opacity: 0.8; }\nbutton:focus {\n  border: none;\n  outline: none; }\nli:focus {\n  border: none;\n  outline: none; }\n.imgcontainer {\n  position: relative; }\n/* The Modal (background) */\n.modal {\n  display: none;\n  /* Hidden by default */\n  position: fixed;\n  /* Stay in place */\n  z-index: 1;\n  /* Sit on top */\n  left: 0;\n  top: 0;\n  width: 100%;\n  /* Full width */\n  height: 100%;\n  /* Full height */\n  overflow: auto;\n  /* Enable scroll if needed */\n  background-color: black;\n  /* Fallback color */\n  background-color: rgba(0, 0, 0, 0.4);\n  /* Black w/ opacity */\n  padding-top: 60px; }\n.terms-condition {\n  font-size: 15px;\n  font-weight: 100; }\n.forgot-pass {\n  color: #333333;\n  text-align: center;\n  margin: 10px 0px;\n  cursor: pointer; }\n.textfieldfont {\n  margin: 6% 0px;\n  padding: 0px;\n  font-size: large; }\n.action-btn {\n  width: 100%;\n  border-radius: 5px;\n  background-color: #ff8200;\n  font-size: large;\n  font-weight: 600; }\n.info-quest {\n  text-align: center;\n  font-weight: 500;\n  font-size: 18px; }\n.policy-header {\n  font-size: 25px;\n  font-weight: bolder;\n  color: #333333; }\n.policy-body::before {\n  content: \"•\";\n  color: #43b02a;\n  display: inline-block;\n  width: 1em;\n  margin-left: -1em;\n  font-weight: bolder;\n  font-size: 25px; }\n.policy-body {\n  font-size: 20px;\n  font-weight: 500;\n  color: #333333;\n  margin: 20px 0px; }\n.unorder-list {\n  list-style: none;\n  padding: 5px; }\n.resend {\n  color: #43b02a;\n  text-align: center;\n  font-size: 12px;\n  cursor: pointer;\n  -webkit-text-decoration: underline #43b02a;\n          text-decoration: underline #43b02a; }\n.timer {\n  color: #43b02a;\n  text-align: center;\n  font-size: 12px; }\n.header-title {\n  background-color: #43b02a;\n  color: #fff;\n  font-size: 28px;\n  font-weight: 600;\n  padding: 8px;\n  text-align: center; }\n/* Modal Content/Box */\n.modal-content {\n  background-color: #fff;\n  margin: 5% auto 15% auto;\n  /* 5% from the top, 15% from the bottom and centered */\n  width: 80%;\n  /* Could be more or less, depending on screen size */ }\n@media screen and (min-width: 992px) {\n  .navbar-expand-lg .navbar-nav .dropdown-menu-right {\n    left: auto !important; }\n  .navbar-expand-lg .navbar-nav .list-notification {\n    left: -474px !important;\n    top: 63px !important; }\n  .card {\n    border: 0px !important; }\n  .mb-3 {\n    margin-bottom: 0px !important; }\n  .card-body {\n    padding: 0px; }\n  .desktop-show {\n    display: block; }\n  .desktop-show1 {\n    padding-left: 0px; } }\n@media screen and (max-width: 992px) {\n  .desktop-show1 {\n    padding-left: 15px;\n    min-width: 100%; }\n  .desktop-show {\n    display: none; }\n  .notificationheader {\n    width: 425px;\n    margin-top: -27px; }\n  .scroll-container {\n    width: 425px;\n    height: 70vh;\n    top: 5px; }\n  .navbar-expand-lg .navbar-nav .dropdown-menu-right {\n    left: auto !important; }\n  .navbar-expand-lg .navbar-nav .list-notification {\n    left: -374px !important;\n    top: 63px !important; }\n  .navbar-expand-lg .navbar-nav .list-notification:hover {\n    left: 0px !important;\n    top: 63px !important;\n    background-color: #fff; }\n  .card {\n    border: 0px !important; }\n  .mb-3 {\n    margin-bottom: 0px !important; }\n  .card-body {\n    padding: 0px; } }\n.footer {\n  color: #333333;\n  font-size: 14px;\n  font-weight: 600;\n  padding: 8px;\n  text-align: center; }\n@media screen and (max-width: 1030px) {\n  i {\n    font-size: 19px !important;\n    color: #fff !important; } }\n@media screen and (max-width: 530px) {\n  i {\n    padding-left: 2px; }\n  button {\n    margin: 0px;\n    padding: 7px 5px; } }\n.bold-font {\n  font-size: 28px;\n  color: #333333;\n  font-weight: 600; }\n.normal-font {\n  font-size: 24px;\n  color: #333333;\n  font-weight: 400; }\n.img-position {\n  height: 90px; }\n::ng-deep .mat-form-field {\n  font-size: 18;\n  margin-top: 10; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyIsIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNVaEI7RUFDSSxtQkFBbUI7RUFDbkIsOEJBQThCLEVBQUE7QUFFbEM7RUFHSSxlQUFjO0VBQ2QsMkJBakJRO0VBa0JSLFlBQVk7RUFDWixpQkFDSixFQUFBO0FBUEE7RUFTUSx5QkF2Qk87RUF3QlAsWUFBWTtFQUNaLGtCQUFrQixFQUFBO0FBWDFCO0lBYVksV0ExQkEsRUFBQTtBQWFaO0lBZ0JZLFdBN0JBLEVBQUE7QUFhWjtNQWtCZ0IsV0EvQkosRUFBQTtBQXFDWjtFQUNJLFdBdENRO0VBdUNSLGdCQUFlLEVBQUE7QUFHbkI7RUFDSSxZQXZDc0I7RUF3Q3RCLGlCQUFnQixFQUFBO0FBRXBCO0VBQ0ksa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixXQWpEUSxFQUFBO0FBbURaO0VBQ0ksa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVLEVBQUE7QUFFZDtFQUNJLHFCQUFxQixFQUFBO0FBR3pCO0VBQ0ksYUFDSixFQUFBO0FBQ0E7RUFDSSxZQTVEc0I7RUE2RHRCLFlBQVk7RUFDWixRQUFPLEVBQUE7QUFFWDtFQUNJLHlCQTlEc0I7RUErRHRCLFVBQVU7RUFDVixlQUFlLEVBQUE7QUFZbkIsZ0NBQUE7QUFDQTtFQUNJLHlCQXRGVztFQXVGWCxXQXRGUTtFQXVGUixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7RUFDWixlQUFlO0VBQ2YsZUFBYyxFQUFBO0FBR2xCO0VBQ0ksaUJBQWdCO0VBQUMsVUFBUztFQUFDLGtCQUFrQixFQUFBO0FBRWpEO0VBQ0ksZ0JBQWU7RUFBQyxZQUFXO0VBQUMsaUJBQWdCO0VBQUMsVUFBUztFQUN0RCxpQkFBaUIsRUFBQTtBQUVyQjtFQUNJLG1CQUFtQixFQUFBO0FBRXZCO0VBQ0ksZ0JBQWdCLEVBQUE7QUFFcEI7RUFDSSxpQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTtBQUVuQjtFQUNJLHlCQWxIVztFQW1IWCxrQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGlCQUFpQixFQUFBO0FBRXJCO0VBQ0ksK0JBaEhzQjtFQWlIdEIsZ0JBQWU7RUFDZixzQkFBc0I7RUFDdEIsaUJBQWlCLEVBQUE7QUFFckI7RUFDSSxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGNBaElXO0VBaUlYLGtCQUFpQixFQUFBO0FBR3JCO0VBQ0ksa0JBQWtCO0VBQ2xCLHdDQUErQjtVQUEvQixnQ0FBK0I7RUFDL0IsUUFBUTtFQUNSLFNBQVMsRUFBQTtBQUViO0VBQ0ksY0EzSVcsRUFBQTtBQXFKZjtFQUNJLFlBQVksRUFBQTtBQUVoQjtFQUNJLFlBQVc7RUFDWCxhQUFZLEVBQUE7QUFFaEI7RUFDSSxZQUFXO0VBQ1gsYUFBWSxFQUFBO0FBVWhCO0VBR0ksa0JBQWtCLEVBQUE7QUFpQnRCLDJCQUFBO0FBQ0E7RUFDSSxhQUFhO0VBQUUsc0JBQUE7RUFDZixlQUFlO0VBQUUsa0JBQUE7RUFDakIsVUFBVTtFQUFFLGVBQUE7RUFDWixPQUFPO0VBQ1AsTUFBTTtFQUNOLFdBQVc7RUFBRSxlQUFBO0VBQ2IsWUFBWTtFQUFFLGdCQUFBO0VBQ2QsY0FBYztFQUFFLDRCQUFBO0VBQ2hCLHVCQUE0QjtFQUFFLG1CQUFBO0VBQzlCLG9DQUFpQztFQUFFLHFCQUFBO0VBQ25DLGlCQUFpQixFQUFBO0FBRXJCO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQixFQUFBO0FBRXBCO0VBQ0ksY0E3TVc7RUE4TVgsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7QUFLbkI7RUFDSSxjQUFjO0VBQ2QsWUFBWTtFQUNaLGdCQUFnQixFQUFBO0FBRXBCO0VBQ0ksV0FBVTtFQUNWLGtCQUFrQjtFQUNsQix5QkF4Tlk7RUF5TlosZ0JBQWdCO0VBQ2hCLGdCQUFnQixFQUFBO0FBRXBCO0VBQ0Esa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7QUFFZjtFQUNJLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsY0F6T1csRUFBQTtBQTRPZjtFQUNJLFlBQVM7RUFDVCxjQWhQVztFQWlQWCxxQkFBcUI7RUFDckIsVUFBVTtFQUNWLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBO0FBRW5CO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQXhQVztFQXlQWCxnQkFBZSxFQUFBO0FBRW5CO0VBQ0ksZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTtBQUdoQjtFQUNJLGNBblFXO0VBb1FYLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZUFBZTtFQUNmLDBDQXZRVztVQXVRWCxrQ0F2UVcsRUFBQTtBQXlRZjtFQUNJLGNBMVFXO0VBMlFYLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7QUFFbkI7RUFDSSx5QkEvUVc7RUFnUlgsV0EvUVE7RUFnUlIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osa0JBQWtCLEVBQUE7QUFFdEIsc0JBQUE7QUFDQTtFQUNJLHNCQXZSUTtFQXdSUix3QkFBd0I7RUFBRSxzREFBQTtFQUUxQixVQUFVO0VBQUUsb0RBQUEsRUFBcUQ7QUF1RHJFO0VBRUE7SUFDSSxxQkFBcUIsRUFBQTtFQUd6QjtJQUNJLHVCQUF1QjtJQUN2QixvQkFBbUIsRUFBQTtFQUd2QjtJQUNJLHNCQUFxQixFQUFBO0VBRXpCO0lBQ0ksNkJBQTRCLEVBQUE7RUFFaEM7SUFDSSxZQUFZLEVBQUE7RUFHaEI7SUFDSSxjQUFjLEVBQUE7RUFFbEI7SUFDSSxpQkFBaUIsRUFBQSxFQUNwQjtBQUlEO0VBRUE7SUFDSSxrQkFBa0I7SUFDbEIsZUFBZSxFQUFBO0VBRW5CO0lBQ0ksYUFBYSxFQUFBO0VBRWpCO0lBQ0ksWUFwWDRCO0lBcVg1QixpQkFBZ0IsRUFBQTtFQUdwQjtJQUNJLFlBelg0QjtJQTBYNUIsWUFBWTtJQUNaLFFBQU8sRUFBQTtFQUdYO0lBQ0kscUJBQXFCLEVBQUE7RUFHekI7SUFDSSx1QkFBdUI7SUFDdkIsb0JBQW1CLEVBQUE7RUFFdkI7SUFDSSxvQkFBb0I7SUFDcEIsb0JBQW1CO0lBQ25CLHNCQTlZUSxFQUFBO0VBaVpaO0lBQ0ksc0JBQXFCLEVBQUE7RUFFekI7SUFDSSw2QkFBNEIsRUFBQTtFQUVoQztJQUNJLFlBQVksRUFBQSxFQUNmO0FBR0Q7RUFDSSxjQTVaVztFQTZaWCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixrQkFBa0IsRUFBQTtBQVF0QjtFQVFJO0lBQ0ksMEJBQXlCO0lBQ3pCLHNCQUF3QixFQUFBLEVBQzNCO0FBSUw7RUFXSTtJQUNJLGlCQUFnQixFQUFBO0VBRXBCO0lBQ0ksV0FBVztJQUNYLGdCQUFlLEVBQUEsRUFFbEI7QUFNTDtFQUNJLGVBQWU7RUFDZixjQWpkVztFQWtkWCxnQkFBZ0IsRUFBQTtBQUlwQjtFQUNJLGVBQWU7RUFDZixjQXhkVztFQXlkWCxnQkFBZ0IsRUFBQTtBQUlwQjtFQUNJLFlBQVcsRUFBQTtBQUlmO0VBQ0ksYUFBYTtFQUNiLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG5idXR0b246ZGlzYWJsZWQge1xuICBjdXJzb3I6IG5vdC1hbGxvd2VkO1xuICBwb2ludGVyLWV2ZW50czogYWxsICFpbXBvcnRhbnQ7IH1cblxuOmhvc3QgLnZsIHtcbiAgbWFyZ2luLXRvcDogOXB4O1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNmZmY7XG4gIGhlaWdodDogNDFweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7IH1cblxuOmhvc3QgLm5hdmJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0M2IwMmE7XG4gIGhlaWdodDogNDVweDtcbiAgcGFkZGluZy1yaWdodDogMHB4OyB9XG4gIDpob3N0IC5uYXZiYXIgLm5hdmJhci1icmFuZCB7XG4gICAgY29sb3I6ICNmZmY7IH1cbiAgOmhvc3QgLm5hdmJhciAubmF2LWl0ZW0gPiBhIHtcbiAgICBjb2xvcjogI2ZmZjsgfVxuICAgIDpob3N0IC5uYXZiYXIgLm5hdi1pdGVtID4gYTpob3ZlciB7XG4gICAgICBjb2xvcjogI2ZmZjsgfVxuXG4uaGVhZGVydGV4dCB7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXN0eWxlOiBib2xkOyB9XG5cbi5ub3RpZmljYXRpb25oZWFkZXIge1xuICB3aWR0aDogNTI1cHg7XG4gIG1hcmdpbi10b3A6IC0yN3B4OyB9XG5cbi5ub3RpZmljYXRpb25pbWFnZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogI2ZmZjsgfVxuXG4uY2VudGVyLWxlZnQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNXB4O1xuICBsZWZ0OiAxNnB4OyB9XG5cbmxpIHtcbiAgbGlzdC1zdHlsZS10eXBlOiBub25lOyB9XG5cbmE6Zm9jdXMge1xuICBvdXRsaW5lOiBub25lOyB9XG5cbi5zY3JvbGwtY29udGFpbmVyIHtcbiAgd2lkdGg6IDUyNXB4O1xuICBoZWlnaHQ6IDcwdmg7XG4gIHRvcDogNXB4OyB9XG5cbi5ub3RpZmljYXRpb25tZXNzYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcbiAgd2lkdGg6IDkxJTtcbiAgbWFyZ2luOiA1cHggMHB4OyB9XG5cbi8qIFNldCBhIHN0eWxlIGZvciBhbGwgYnV0dG9ucyAqL1xuYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQzYjAyYTtcbiAgY29sb3I6ICNmZmY7XG4gIHBhZGRpbmc6IDE0cHggMjBweDtcbiAgbWFyZ2luOiA4cHggMDtcbiAgYm9yZGVyOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGhlaWdodDogaW5oZXJpdDsgfVxuXG4ubm90aWRheXMge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgd2lkdGg6IDIwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XG5cbi5ub3RpbWVzc2FnZSB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGhlaWdodDogMTAwJTtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gIHdpZHRoOiA3MCU7XG4gIHBhZGRpbmctbGVmdDogOHB4OyB9XG5cbi5ib2xkZXJmb250IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjsgfVxuXG4uZm9udC0xMDAge1xuICBmb250LXdlaWdodDogMTAwOyB9XG5cbi5jbG9zZS1pY29uIHtcbiAgZm9udC1zaXplOiAxLjNyZW07XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGN1cnNvcjogcG9pbnRlcjsgfVxuXG4uaW1nLWJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDNiMDJhO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIHBhZGRpbmctdG9wOiA0MHB4OyB9XG5cbi50ZXh0LWJhY2tncm91bmQge1xuICBib3JkZXItcmlnaHQ6IDNweCBzb2xpZCAjRUNGMEYxO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBwYWRkaW5nLXRvcDogNDBweDsgfVxuXG4uaW5mby1oZWFkZXIge1xuICBmb250LXNpemU6IDU4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGNvbG9yOiAjNDNiMDJhO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLnZlcnRpY2FsLWNlbnRlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiA1NSU7IH1cblxuLmdyZWVuIHtcbiAgY29sb3I6ICM0M2IwMmE7IH1cblxuYnV0dG9uOmhvdmVyIHtcbiAgb3BhY2l0eTogMC44OyB9XG5cbmJ1dHRvbjpmb2N1cyB7XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTsgfVxuXG5saTpmb2N1cyB7XG4gIGJvcmRlcjogbm9uZTtcbiAgb3V0bGluZTogbm9uZTsgfVxuXG4uaW1nY29udGFpbmVyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlOyB9XG5cbi8qIFRoZSBNb2RhbCAoYmFja2dyb3VuZCkgKi9cbi5tb2RhbCB7XG4gIGRpc3BsYXk6IG5vbmU7XG4gIC8qIEhpZGRlbiBieSBkZWZhdWx0ICovXG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgLyogU3RheSBpbiBwbGFjZSAqL1xuICB6LWluZGV4OiAxO1xuICAvKiBTaXQgb24gdG9wICovXG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIC8qIEZ1bGwgd2lkdGggKi9cbiAgaGVpZ2h0OiAxMDAlO1xuICAvKiBGdWxsIGhlaWdodCAqL1xuICBvdmVyZmxvdzogYXV0bztcbiAgLyogRW5hYmxlIHNjcm9sbCBpZiBuZWVkZWQgKi9cbiAgYmFja2dyb3VuZC1jb2xvcjogYmxhY2s7XG4gIC8qIEZhbGxiYWNrIGNvbG9yICovXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgLyogQmxhY2sgdy8gb3BhY2l0eSAqL1xuICBwYWRkaW5nLXRvcDogNjBweDsgfVxuXG4udGVybXMtY29uZGl0aW9uIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogMTAwOyB9XG5cbi5mb3Jnb3QtcGFzcyB7XG4gIGNvbG9yOiAjMzMzMzMzO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMTBweCAwcHg7XG4gIGN1cnNvcjogcG9pbnRlcjsgfVxuXG4udGV4dGZpZWxkZm9udCB7XG4gIG1hcmdpbjogNiUgMHB4O1xuICBwYWRkaW5nOiAwcHg7XG4gIGZvbnQtc2l6ZTogbGFyZ2U7IH1cblxuLmFjdGlvbi1idG4ge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY4MjAwO1xuICBmb250LXNpemU6IGxhcmdlO1xuICBmb250LXdlaWdodDogNjAwOyB9XG5cbi5pbmZvLXF1ZXN0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogNTAwO1xuICBmb250LXNpemU6IDE4cHg7IH1cblxuLnBvbGljeS1oZWFkZXIge1xuICBmb250LXNpemU6IDI1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGNvbG9yOiAjMzMzMzMzOyB9XG5cbi5wb2xpY3ktYm9keTo6YmVmb3JlIHtcbiAgY29udGVudDogXCLigKJcIjtcbiAgY29sb3I6ICM0M2IwMmE7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDFlbTtcbiAgbWFyZ2luLWxlZnQ6IC0xZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGZvbnQtc2l6ZTogMjVweDsgfVxuXG4ucG9saWN5LWJvZHkge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiAjMzMzMzMzO1xuICBtYXJnaW46IDIwcHggMHB4OyB9XG5cbi51bm9yZGVyLWxpc3Qge1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBwYWRkaW5nOiA1cHg7IH1cblxuLnJlc2VuZCB7XG4gIGNvbG9yOiAjNDNiMDJhO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZSAjNDNiMDJhOyB9XG5cbi50aW1lciB7XG4gIGNvbG9yOiAjNDNiMDJhO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTJweDsgfVxuXG4uaGVhZGVyLXRpdGxlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQzYjAyYTtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgcGFkZGluZzogOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLyogTW9kYWwgQ29udGVudC9Cb3ggKi9cbi5tb2RhbC1jb250ZW50IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgbWFyZ2luOiA1JSBhdXRvIDE1JSBhdXRvO1xuICAvKiA1JSBmcm9tIHRoZSB0b3AsIDE1JSBmcm9tIHRoZSBib3R0b20gYW5kIGNlbnRlcmVkICovXG4gIHdpZHRoOiA4MCU7XG4gIC8qIENvdWxkIGJlIG1vcmUgb3IgbGVzcywgZGVwZW5kaW5nIG9uIHNjcmVlbiBzaXplICovIH1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgLm5hdmJhci1leHBhbmQtbGcgLm5hdmJhci1uYXYgLmRyb3Bkb3duLW1lbnUtcmlnaHQge1xuICAgIGxlZnQ6IGF1dG8gIWltcG9ydGFudDsgfVxuICAubmF2YmFyLWV4cGFuZC1sZyAubmF2YmFyLW5hdiAubGlzdC1ub3RpZmljYXRpb24ge1xuICAgIGxlZnQ6IC00NzRweCAhaW1wb3J0YW50O1xuICAgIHRvcDogNjNweCAhaW1wb3J0YW50OyB9XG4gIC5jYXJkIHtcbiAgICBib3JkZXI6IDBweCAhaW1wb3J0YW50OyB9XG4gIC5tYi0zIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwcHggIWltcG9ydGFudDsgfVxuICAuY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwcHg7IH1cbiAgLmRlc2t0b3Atc2hvdyB7XG4gICAgZGlzcGxheTogYmxvY2s7IH1cbiAgLmRlc2t0b3Atc2hvdzEge1xuICAgIHBhZGRpbmctbGVmdDogMHB4OyB9IH1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLmRlc2t0b3Atc2hvdzEge1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtaW4td2lkdGg6IDEwMCU7IH1cbiAgLmRlc2t0b3Atc2hvdyB7XG4gICAgZGlzcGxheTogbm9uZTsgfVxuICAubm90aWZpY2F0aW9uaGVhZGVyIHtcbiAgICB3aWR0aDogNDI1cHg7XG4gICAgbWFyZ2luLXRvcDogLTI3cHg7IH1cbiAgLnNjcm9sbC1jb250YWluZXIge1xuICAgIHdpZHRoOiA0MjVweDtcbiAgICBoZWlnaHQ6IDcwdmg7XG4gICAgdG9wOiA1cHg7IH1cbiAgLm5hdmJhci1leHBhbmQtbGcgLm5hdmJhci1uYXYgLmRyb3Bkb3duLW1lbnUtcmlnaHQge1xuICAgIGxlZnQ6IGF1dG8gIWltcG9ydGFudDsgfVxuICAubmF2YmFyLWV4cGFuZC1sZyAubmF2YmFyLW5hdiAubGlzdC1ub3RpZmljYXRpb24ge1xuICAgIGxlZnQ6IC0zNzRweCAhaW1wb3J0YW50O1xuICAgIHRvcDogNjNweCAhaW1wb3J0YW50OyB9XG4gIC5uYXZiYXItZXhwYW5kLWxnIC5uYXZiYXItbmF2IC5saXN0LW5vdGlmaWNhdGlvbjpob3ZlciB7XG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgdG9wOiA2M3B4ICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjsgfVxuICAuY2FyZCB7XG4gICAgYm9yZGVyOiAwcHggIWltcG9ydGFudDsgfVxuICAubWItMyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7IH1cbiAgLmNhcmQtYm9keSB7XG4gICAgcGFkZGluZzogMHB4OyB9IH1cblxuLmZvb3RlciB7XG4gIGNvbG9yOiAjMzMzMzMzO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHBhZGRpbmc6IDhweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEwMzBweCkge1xuICBpIHtcbiAgICBmb250LXNpemU6IDE5cHggIWltcG9ydGFudDtcbiAgICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50OyB9IH1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTMwcHgpIHtcbiAgaSB7XG4gICAgcGFkZGluZy1sZWZ0OiAycHg7IH1cbiAgYnV0dG9uIHtcbiAgICBtYXJnaW46IDBweDtcbiAgICBwYWRkaW5nOiA3cHggNXB4OyB9IH1cblxuLmJvbGQtZm9udCB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgY29sb3I6ICMzMzMzMzM7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7IH1cblxuLm5vcm1hbC1mb250IHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjb2xvcjogIzMzMzMzMztcbiAgZm9udC13ZWlnaHQ6IDQwMDsgfVxuXG4uaW1nLXBvc2l0aW9uIHtcbiAgaGVpZ2h0OiA5MHB4OyB9XG5cbjo6bmctZGVlcCAubWF0LWZvcm0tZmllbGQge1xuICBmb250LXNpemU6IDE4O1xuICBtYXJnaW4tdG9wOiAxMDsgfVxuIiwiJGdyZWVuOiAjNDNiMDJhO1xuJHdoaXRlOiAjZmZmO1xuJGJsYWNrOiAjMzMzMzMzO1xuJHRyYW5zcGFyZW50OiB0cmFuc3BhcmVudDtcbiRmb250LWNvbG9yOiAjNTU1NTU1O1xuJG5vdGlmaWNhdGlvbi13aWR0aDogNTI1cHg7XG4kbm90aWZpY2F0aW9uLXNtYWxsLXdpZHRoOiA0MjVweDtcbiRvcmFuZ2U6ICNmZjgyMDA7XG4kYmFja2dyb3VuZC1jb2xvcjogI0VDRjBGMTtcblxuYnV0dG9uOmRpc2FibGVkIHtcbiAgICBjdXJzb3I6IG5vdC1hbGxvd2VkO1xuICAgIHBvaW50ZXItZXZlbnRzOiBhbGwgIWltcG9ydGFudDtcbiAgfVxuOmhvc3Qge1xuXG4udmwge1xuICAgIG1hcmdpbi10b3A6OXB4O1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgJHdoaXRlO1xuICAgIGhlaWdodDogNDFweDtcbiAgICBtYXJnaW4tbGVmdDogLTVweFxufVxuICAgIC5uYXZiYXIge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZ3JlZW47XG4gICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMHB4O1xuICAgICAgICAubmF2YmFyLWJyYW5kIHtcbiAgICAgICAgICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgICAgIH1cbiAgICAgICAgLm5hdi1pdGVtID4gYSB7XG4gICAgICAgICAgICBjb2xvcjogJHdoaXRlO1xuICAgICAgICAgICAgJjpob3ZlciB7XG4gICAgICAgICAgICAgICAgY29sb3I6ICR3aGl0ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cblxuLmhlYWRlcnRleHQge1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgZm9udC1zdHlsZTpib2xkO1xufVxuXG4ubm90aWZpY2F0aW9uaGVhZGVyIHtcbiAgICB3aWR0aDogJG5vdGlmaWNhdGlvbi13aWR0aDtcbiAgICBtYXJnaW4tdG9wOi0yN3B4O1xufVxuLm5vdGlmaWNhdGlvbmltYWdlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAkd2hpdGU7XG59XG4uY2VudGVyLWxlZnQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDVweDtcbiAgICBsZWZ0OiAxNnB4O1xufVxubGkge1xuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgfVxuXG5hOmZvY3VzIHtcbiAgICBvdXRsaW5lOm5vbmVcbn1cbi5zY3JvbGwtY29udGFpbmVyIHtcbiAgICB3aWR0aDogJG5vdGlmaWNhdGlvbi13aWR0aDtcbiAgICBoZWlnaHQ6IDcwdmg7XG4gICAgdG9wOjVweDtcbn1cbi5ub3RpZmljYXRpb25tZXNzYWdlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmFja2dyb3VuZC1jb2xvcjtcbiAgICB3aWR0aDogOTElO1xuICAgIG1hcmdpbjogNXB4IDBweDtcbn1cbi8vIC8qIEZ1bGwtd2lkdGggaW5wdXQgZmllbGRzICovXG4vLyAvLyBpbnB1dFt0eXBlPXRleHRdLCBpbnB1dFt0eXBlPXBhc3N3b3JkXSwgaW5wdXRbdHlwZT1udW1iZXJdIHtcbi8vIC8vICAgICB3aWR0aDogMTAwJTtcbi8vIC8vICAgICBwYWRkaW5nOiAxMnB4IDIwcHg7XG4vLyAvLyAgICAgbWFyZ2luOiA4cHggMDtcbi8vIC8vICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4vLyAvLyAgICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbi8vIC8vICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuLy8gLy8gfVxuXG4vKiBTZXQgYSBzdHlsZSBmb3IgYWxsIGJ1dHRvbnMgKi9cbmJ1dHRvbiB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJGdyZWVuO1xuICAgIGNvbG9yOiAkd2hpdGU7XG4gICAgcGFkZGluZzogMTRweCAyMHB4O1xuICAgIG1hcmdpbjogOHB4IDA7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBoZWlnaHQ6aW5oZXJpdDtcbiAgICAvLyB3aWR0aDogMTAwJTtcbn1cbi5ub3RpZGF5cyB7XG4gICAgdGV4dC1hbGlnbjpyaWdodDt3aWR0aDoyMCU7cG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm5vdGltZXNzYWdlIHtcbiAgICB0ZXh0LWFsaWduOmxlZnQ7aGVpZ2h0OjEwMCU7bGluZS1oZWlnaHQ6MzBweDt3aWR0aDo3MCU7XG4gICAgcGFkZGluZy1sZWZ0OiA4cHg7XG59XG4uYm9sZGVyZm9udCB7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb250LTEwMCB7XG4gICAgZm9udC13ZWlnaHQ6IDEwMDtcbn1cbi5jbG9zZS1pY29uIHtcbiAgICBmb250LXNpemU6MS4zcmVtO1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuLmltZy1iYWNrZ3JvdW5kIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiRncmVlbjtcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgIHBhZGRpbmctdG9wOiA0MHB4O1xufVxuLnRleHQtYmFja2dyb3VuZCB7XG4gICAgYm9yZGVyLXJpZ2h0OjNweCBzb2xpZCAkYmFja2dyb3VuZC1jb2xvcjtcbiAgICB0ZXh0LWFsaWduOmxlZnQ7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcbn1cbi5pbmZvLWhlYWRlciB7XG4gICAgZm9udC1zaXplOiA1OHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgY29sb3I6JGdyZWVuO1xuICAgIHRleHQtYWxpZ246Y2VudGVyO1xuICAgIC8vIG1hcmdpbi10b3A6NTJweDtcbn1cbi52ZXJ0aWNhbC1jZW50ZXIge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1xuICAgIHRvcDogNTAlO1xuICAgIGxlZnQ6IDU1JTtcbn1cbi5ncmVlbiB7XG4gICAgY29sb3I6JGdyZWVuO1xufVxuLy8gLy8gLmZvcm0tY29udHJvbDpmb2N1cyB7XG4vLyAvLyAgICAgY29sb3I6ICM0OTUwNTc7XG4vLyAvLyAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbi8vIC8vICAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xuLy8gLy8gICAgIG91dGxpbmU6IDA7XG4vLyAvLyAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lO1xuLy8gLy8gICAgIGJveC1zaGFkb3c6IG5vbmU7XG4vLyAvLyB9XG5idXR0b246aG92ZXIge1xuICAgIG9wYWNpdHk6IDAuODtcbn1cbmJ1dHRvbjpmb2N1cyB7XG4gICAgYm9yZGVyOm5vbmU7XG4gICAgb3V0bGluZTpub25lO1xufVxubGk6Zm9jdXMge1xuICAgIGJvcmRlcjpub25lO1xuICAgIG91dGxpbmU6bm9uZTtcbn1cbi8vIC8qIEV4dHJhIHN0eWxlcyBmb3IgdGhlIGNhbmNlbCBidXR0b24gKi9cbi8vIC5jYW5jZWxidG4ge1xuLy8gICAgIHdpZHRoOiBhdXRvO1xuLy8gICAgIHBhZGRpbmc6IDEwcHggMThweDtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjQ0MzM2O1xuLy8gfVxuXG4vLyAvKiBDZW50ZXIgdGhlIGltYWdlIGFuZCBwb3NpdGlvbiB0aGUgY2xvc2UgYnV0dG9uICovXG4uaW1nY29udGFpbmVyIHtcbiAgICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLy8gbWFyZ2luOiAyNHB4IDAgMTJweCAwO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLy8gaW1nLmF2YXRhciB7XG4vLyAgICAgd2lkdGg6IDQwJTtcbi8vICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4vLyB9XG5cbi8vIC5jb250YWluZXIge1xuLy8gICAgIHBhZGRpbmc6IDE2cHg7XG4vLyB9XG5cbi8vIHNwYW4ucHN3IHtcbi8vICAgICBmbG9hdDogcmlnaHQ7XG4vLyAgICAgcGFkZGluZy10b3A6IDE2cHg7XG4vLyB9XG5cbi8qIFRoZSBNb2RhbCAoYmFja2dyb3VuZCkgKi9cbi5tb2RhbCB7XG4gICAgZGlzcGxheTogbm9uZTsgLyogSGlkZGVuIGJ5IGRlZmF1bHQgKi9cbiAgICBwb3NpdGlvbjogZml4ZWQ7IC8qIFN0YXkgaW4gcGxhY2UgKi9cbiAgICB6LWluZGV4OiAxOyAvKiBTaXQgb24gdG9wICovXG4gICAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgd2lkdGg6IDEwMCU7IC8qIEZ1bGwgd2lkdGggKi9cbiAgICBoZWlnaHQ6IDEwMCU7IC8qIEZ1bGwgaGVpZ2h0ICovXG4gICAgb3ZlcmZsb3c6IGF1dG87IC8qIEVuYWJsZSBzY3JvbGwgaWYgbmVlZGVkICovXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsMCwwKTsgLyogRmFsbGJhY2sgY29sb3IgKi9cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsMCwwLDAuNCk7IC8qIEJsYWNrIHcvIG9wYWNpdHkgKi9cbiAgICBwYWRkaW5nLXRvcDogNjBweDtcbn1cbi50ZXJtcy1jb25kaXRpb24ge1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogMTAwO1xufVxuLmZvcmdvdC1wYXNzIHtcbiAgICBjb2xvcjogJGJsYWNrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW46IDEwcHggMHB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cbi8vIGZvcm0ge1xuLy8gICAgIG1hcmdpbjogMHB4IDIwcHg7XG4vLyB9XG4udGV4dGZpZWxkZm9udCB7XG4gICAgbWFyZ2luOiA2JSAwcHg7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG59XG4uYWN0aW9uLWJ0biB7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjokb3JhbmdlO1xuICAgIGZvbnQtc2l6ZTogbGFyZ2U7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5pbmZvLXF1ZXN0IHtcbnRleHQtYWxpZ246IGNlbnRlcjtcbmZvbnQtd2VpZ2h0OiA1MDA7XG5mb250LXNpemU6IDE4cHg7XG59XG4ucG9saWN5LWhlYWRlciB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgY29sb3I6JGJsYWNrO1xufVxuXG4ucG9saWN5LWJvZHk6OmJlZm9yZSB7XG4gICAgY29udGVudDogXCLigKJcIjtcbiAgICBjb2xvcjogJGdyZWVuO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMWVtO1xuICAgIG1hcmdpbi1sZWZ0OiAtMWVtO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1zaXplOiAyNXB4O1xufVxuLnBvbGljeS1ib2R5IHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBjb2xvcjokYmxhY2s7XG4gICAgbWFyZ2luOjIwcHggMHB4O1xufVxuLnVub3JkZXItbGlzdCB7XG4gICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICBwYWRkaW5nOiA1cHg7XG59XG5cbi5yZXNlbmR7XG4gICAgY29sb3I6JGdyZWVuO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lICRncmVlbjtcbn1cbi50aW1lciB7XG4gICAgY29sb3I6JGdyZWVuO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG4uaGVhZGVyLXRpdGxlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkZ3JlZW47XG4gICAgY29sb3I6JHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMjhweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIHBhZGRpbmc6IDhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4vKiBNb2RhbCBDb250ZW50L0JveCAqL1xuLm1vZGFsLWNvbnRlbnQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICR3aGl0ZTtcbiAgICBtYXJnaW46IDUlIGF1dG8gMTUlIGF1dG87IC8qIDUlIGZyb20gdGhlIHRvcCwgMTUlIGZyb20gdGhlIGJvdHRvbSBhbmQgY2VudGVyZWQgKi9cbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xuICAgIHdpZHRoOiA4MCU7IC8qIENvdWxkIGJlIG1vcmUgb3IgbGVzcywgZGVwZW5kaW5nIG9uIHNjcmVlbiBzaXplICovXG59XG4vLyBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxNjAwcHgpIHtcbi8vIC5tb2RhbC1jb250ZW50IHtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmVmZWZlO1xuLy8gICAgIG1hcmdpbjogNSUgYXV0byAxNSUgYXV0bzsgLyogNSUgZnJvbSB0aGUgdG9wLCAxNSUgZnJvbSB0aGUgYm90dG9tIGFuZCBjZW50ZXJlZCAqL1xuICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICM4ODg7XG4gICAgLy8gd2lkdGg6IDcwJTsgLyogQ291bGQgYmUgbW9yZSBvciBsZXNzLCBkZXBlbmRpbmcgb24gc2NyZWVuIHNpemUgKi9cbi8vIH1cbi8vIH1cblxuLy8gLyogVGhlIENsb3NlIEJ1dHRvbiAoeCkgKi9cbi8vIC8vIC5jbG9zZSB7XG4vLyAvLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gLy8gICAgIHJpZ2h0OiAyNXB4O1xuLy8gLy8gICAgIHRvcDogMDtcbi8vIC8vICAgICBjb2xvcjogIzAwMDtcbi8vIC8vICAgICBmb250LXNpemU6IDM1cHg7XG4vLyAvLyAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4vLyAvLyB9XG5cbi8vIC8vIC5jbG9zZTpob3Zlcixcbi8vIC8vIC5jbG9zZTpmb2N1cyB7XG4vLyAvLyAgICAgY29sb3I6IHJlZDtcbi8vIC8vICAgICBjdXJzb3I6IHBvaW50ZXI7XG4vLyAvLyB9XG5cbi8vIC8qIEFkZCBab29tIEFuaW1hdGlvbiAqL1xuLy8gLmFuaW1hdGUge1xuLy8gICAgIC13ZWJraXQtYW5pbWF0aW9uOiBhbmltYXRlem9vbSAwLjZzO1xuLy8gICAgIGFuaW1hdGlvbjogYW5pbWF0ZXpvb20gMC42c1xuLy8gfVxuXG4vLyBALXdlYmtpdC1rZXlmcmFtZXMgYW5pbWF0ZXpvb20ge1xuLy8gICAgIGZyb20gey13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKX0gXG4vLyAgICAgdG8gey13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgxKX1cbi8vIH1cbiAgICBcbi8vIEBrZXlmcmFtZXMgYW5pbWF0ZXpvb20ge1xuLy8gICAgIGZyb20ge3RyYW5zZm9ybTogc2NhbGUoMCl9IFxuLy8gICAgIHRvIHt0cmFuc2Zvcm06IHNjYWxlKDEpfVxuLy8gfVxuXG4vLyAvKiBDaGFuZ2Ugc3R5bGVzIGZvciBzcGFuIGFuZCBjYW5jZWwgYnV0dG9uIG9uIGV4dHJhIHNtYWxsIHNjcmVlbnMgKi9cbi8vIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMwMHB4KSB7XG4vLyAgICAgc3Bhbi5wc3cge1xuLy8gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuLy8gICAgICAgIGZsb2F0OiBub25lO1xuLy8gICAgIH1cbi8vICAgICAuY2FuY2VsYnRuIHtcbi8vICAgICAgICB3aWR0aDogMTAwJTtcbi8vICAgICB9XG4vLyB9XG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcbiAgICBcbi5uYXZiYXItZXhwYW5kLWxnIC5uYXZiYXItbmF2IC5kcm9wZG93bi1tZW51LXJpZ2h0IHtcbiAgICBsZWZ0OiBhdXRvICFpbXBvcnRhbnQ7XG59XG5cbi5uYXZiYXItZXhwYW5kLWxnIC5uYXZiYXItbmF2IC5saXN0LW5vdGlmaWNhdGlvbiB7XG4gICAgbGVmdDogLTQ3NHB4ICFpbXBvcnRhbnQ7XG4gICAgdG9wOjYzcHggIWltcG9ydGFudDtcbn1cblxuLmNhcmQge1xuICAgIGJvcmRlcjowcHggIWltcG9ydGFudDtcbn1cbi5tYi0zIHtcbiAgICBtYXJnaW4tYm90dG9tOjBweCAhaW1wb3J0YW50O1xufVxuLmNhcmQtYm9keSB7XG4gICAgcGFkZGluZzogMHB4O1xufVxuXG4uZGVza3RvcC1zaG93IHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cbi5kZXNrdG9wLXNob3cxIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcbn1cbn1cblxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAgIFxuLmRlc2t0b3Atc2hvdzEge1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBtaW4td2lkdGg6IDEwMCU7XG59XG4uZGVza3RvcC1zaG93IHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuLm5vdGlmaWNhdGlvbmhlYWRlciB7XG4gICAgd2lkdGg6ICRub3RpZmljYXRpb24tc21hbGwtd2lkdGg7XG4gICAgbWFyZ2luLXRvcDotMjdweDtcbn1cblxuLnNjcm9sbC1jb250YWluZXIge1xuICAgIHdpZHRoOiAkbm90aWZpY2F0aW9uLXNtYWxsLXdpZHRoO1xuICAgIGhlaWdodDogNzB2aDtcbiAgICB0b3A6NXB4O1xufVxuXG4ubmF2YmFyLWV4cGFuZC1sZyAubmF2YmFyLW5hdiAuZHJvcGRvd24tbWVudS1yaWdodCB7XG4gICAgbGVmdDogYXV0byAhaW1wb3J0YW50O1xufVxuXG4ubmF2YmFyLWV4cGFuZC1sZyAubmF2YmFyLW5hdiAubGlzdC1ub3RpZmljYXRpb24ge1xuICAgIGxlZnQ6IC0zNzRweCAhaW1wb3J0YW50O1xuICAgIHRvcDo2M3B4ICFpbXBvcnRhbnQ7XG59XG4ubmF2YmFyLWV4cGFuZC1sZyAubmF2YmFyLW5hdiAubGlzdC1ub3RpZmljYXRpb246aG92ZXIge1xuICAgIGxlZnQ6IDBweCAhaW1wb3J0YW50O1xuICAgIHRvcDo2M3B4ICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogJHdoaXRlO1xufVxuXG4uY2FyZCB7XG4gICAgYm9yZGVyOjBweCAhaW1wb3J0YW50O1xufVxuLm1iLTMge1xuICAgIG1hcmdpbi1ib3R0b206MHB4ICFpbXBvcnRhbnQ7XG59XG4uY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwcHg7XG59XG59XG5cbi5mb290ZXJ7XG4gICAgY29sb3I6JGJsYWNrO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIHBhZGRpbmc6IDhweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8vIGkge1xuLy8gICAgIGZvbnQtc2l6ZToxOHB4ICFpbXBvcnRhbnQ7XG4vLyB9XG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMTAzMHB4KSB7XG4gICAgLy8gc3Bhbi5wc3cge1xuICAgIC8vICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIC8vICAgIGZsb2F0OiBub25lO1xuICAgIC8vIH1cbiAgICAvLyAuY2FuY2VsYnRuIHtcbiAgICAvLyAgICB3aWR0aDogMTAwJTtcbiAgICAvLyB9XG4gICAgaSB7XG4gICAgICAgIGZvbnQtc2l6ZToxOXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGNvbG9yOiAkd2hpdGUgIWltcG9ydGFudDtcbiAgICB9XG59XG5cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNTMwcHgpIHtcbiAgICAvLyBzcGFuLnBzdyB7XG4gICAgLy8gICAgZGlzcGxheTogYmxvY2s7XG4gICAgLy8gICAgZmxvYXQ6IG5vbmU7XG4gICAgLy8gfVxuICAgIC8vIC5jYW5jZWxidG4ge1xuICAgIC8vICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIH1cbiAgICAvLyBpIHtcbiAgICAvLyAgICAgZm9udC1zaXplOjE0cHggIWltcG9ydGFudDtcbiAgICAvLyB9XG4gICAgaSB7XG4gICAgICAgIHBhZGRpbmctbGVmdDoycHg7XG4gICAgfVxuICAgIGJ1dHRvbiB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBwYWRkaW5nOjdweCA1cHg7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IC01cHggIWltcG9ydGFudDtcbiAgICB9XG4gICAgLy8gLm5hdi1saW5rIHtcbiAgICAvLyAgICAgbWFyZ2luLXRvcDoxMnB4XG4gICAgLy8gfVxufVxuXG4uYm9sZC1mb250IHtcbiAgICBmb250LXNpemU6IDI4cHg7XG4gICAgY29sb3I6ICRibGFjaztcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIC8vIG1hcmdpbi10b3A6MjRweDtcbn1cblxuLm5vcm1hbC1mb250IHtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgY29sb3I6ICRibGFjaztcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIC8vIG1hcmdpbi1ib3R0b206MjRweDtcbn1cblxuLmltZy1wb3NpdGlvbiB7XG4gICAgaGVpZ2h0OjkwcHg7XG4gICAgLy8gbWFyZ2luLXRvcDo0NHB4O1xufVxuXG46Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkIHtcbiAgICBmb250LXNpemU6IDE4O1xuICAgIG1hcmdpbi10b3A6IDEwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/components/footer/footer.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/components/footer/footer.component.ts ***!
  \**************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/layout/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/layout/components/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/layout/components/header/header.component.html":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/header/header.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <nav class=\"navbar\">\n  <ul class=\"navbar-nav\">\n    <img src=\"../../../../assets/img/belbooks-logo.svg\">\n  </ul>\n</nav> -->\n\n<div class=\"header\"><img src=\"../../../../assets/img/belriumlogo.svg\" style=\"margin-left:1%;\"></div>\n"

/***/ }),

/***/ "./src/app/layout/components/header/header.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/header/header.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar {\n  background: #1b3764;\n  background-color: #1b3764;\n  height: 45px;\n  text-align: left !important;\n  display: flex;\n  width: 100% !important; }\n\n.header {\n  background: #244985;\n  height: 50px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsWUFBWTtFQUNaLDJCQUEyQjtFQUMzQixhQUFhO0VBQ2Isc0JBQ0osRUFBQTs7QUFDQTtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5hdmJhciB7XG4gICAgLy8gYmFja2dyb3VuZDogIzREQTFGRjtcbiAgICBiYWNrZ3JvdW5kOiAjMWIzNzY0O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMxYjM3NjQ7XG4gICAgaGVpZ2h0OiA0NXB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQgIWltcG9ydGFudDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnRcbn1cbi5oZWFkZXJ7XG4gICAgYmFja2dyb3VuZDogIzI0NDk4NTtcbiAgICBoZWlnaHQ6IDUwcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/layout/components/header/header.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/components/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/layout/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/layout/components/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.html":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"sidebar\" [ngClass]=\"{sidebarPushRight: isActive}\">\n    <div class=\"list-group\">\n        <table>\n                <tr>\n                        <td><img src=\"../../../../assets/img/account.jpg\" style=\"border-radius:50px;margin-left:70%\"></td>\n                \n                        <p style=\"color:#ffffff;margin-left:-30%; margin-top: 80%; \">{{email}}</p>\n                        </tr>\n            <tr>\n                <td align=\"center\"> <img src=\"../../../../assets/images/Mask Group 1.png\"></td>\n                <td align=\"center\"> <a (click)=\"navigateRoute()\" class=\"list-group-item\"> \n                        Home\n                    </a></td>\n            </tr>\n\n            <tr>\n                <td align=\"center\" > <img src=\"../../../../assets/images/Mask Group 2.png\"></td>\n\n                <td align=\"center\">\n\n                    <a [routerLink]=\"['/dashboard']\" [routerLinkActive]=\"['router-link-active']\"\n                        class=\"list-group-item\">\n                        Dashboard\n                    </a>\n                </td>\n            </tr>\n\n            <tr>\n                <td align=\"center\"><img src=\"../../../../assets/images/icons8-diploma-100-3.png\">\n                </td>\n                <td align=\"center\"><a [routerLink]=\"['/certificate']\" [routerLinkActive]=\"['router-link-active']\"\n                        class=\"list-group-item\">\n                        Certificate\n                    </a> </td>\n            </tr>\n\n            <tr>\n                <td align=\"center\"><img src=\"../../../../assets/images/icons8-wallet-100-3.png\"></td>\n                <td align=\"center\"> <a [routerLink]=\"['/wallet']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                        My Wallet\n                    </a></td>\n            </tr>\n\n            <tr>\n                <td align=\"center\">\n                    <img src=\"../../../../assets/images/Mask Group 4.png\">\n                </td>\n                <td align=\"center\">\n                    <a [routerLink]=\"['/kyc']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                        KYC\n                    </a>\n                </td>\n            </tr>\n\n            <tr>\n                <td align=\"center\">\n                    <img src=\"../../../../assets/images/Mask Group 5.png\">\n                </td>\n                <td align=\"center\">\n                    <a [routerLink]=\"['/settings']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                        Settings\n                    </a>\n                </td>\n            </tr>\n\n\n            <tr>\n                <td align=\"center\">\n                    <img src=\"../../../../assets/img/logout.svg\">\n                </td>\n                <td align=\"center\" colspan=\"2\">\n                    <a class=\"list-group-item\" (click)=logout()>\n                        Logout\n                    </a>\n                </td>\n            </tr>\n\n        </table>\n    </div>\n\n</nav>"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".sidebar {\n  border-radius: 0;\n  position: fixed;\n  z-index: 1000;\n  top: 50px;\n  left: 235px;\n  font-size: 16px !important;\n  width: 15%;\n  margin-left: -235px;\n  border: 3px solid #1b3764;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: #1b3764;\n  bottom: 0;\n  overflow-x: hidden;\n  padding-bottom: 40px;\n  transition: all 0.2s ease-in-out; }\n  .sidebar .list-group a.list-group-item {\n    background: #1b3764;\n    border: 0;\n    border-radius: 0;\n    padding-bottom: 53px !important;\n    text-decoration: none;\n    height: 20px;\n    font-family: Muli;\n    font-size: 16px;\n    font-weight: normal;\n    font-style: normal;\n    font-stretch: normal;\n    line-height: 1.25;\n    letter-spacing: normal;\n    text-align: left;\n    color: var(--very-light-pink); }\n  .sidebar .list-group a:hover {\n    color: white; }\n  .sidebar .list-group a.router-link-active {\n    border-top-color: #555555;\n    border-bottom-color: #555555;\n    color: white; }\n  .sidebar .list-group .header-fields {\n    padding-top: 10px; }\n  .sidebar .list-group .header-fields > .list-group-item:first-child {\n      border-top: 1px solid rgba(255, 255, 255, 0.2); }\n  .sidebar table {\n    margin-left: 18px; }\n  .sidebar table a {\n    padding: 5px;\n    margin: 5px;\n    margin-top: 35px; }\n  .sidebar tr td:nth-child(1) {\n    width: 30%; }\n  .sidebar tr td:nth-child(2) {\n    width: 70%; }\n  .sidebar .sidebar-dropdown *:focus {\n    border-radius: none;\n    border: none; }\n  .sidebar .sidebar-dropdown .panel-title {\n    font-size: 1rem;\n    height: 50px;\n    margin-bottom: 0; }\n  .sidebar .sidebar-dropdown .panel-title a {\n      color: #999;\n      text-decoration: none;\n      font-weight: 400;\n      background: #1b3764; }\n  .sidebar .sidebar-dropdown .panel-title a span {\n        position: relative;\n        display: block;\n        padding: 0.75rem 1.5rem;\n        padding-top: 1rem; }\n  .sidebar .sidebar-dropdown .panel-title a:hover,\n    .sidebar .sidebar-dropdown .panel-title a:focus {\n      color: #fff;\n      outline: none;\n      outline-offset: -2px; }\n  .sidebar .sidebar-dropdown .panel-title:hover {\n    background: #162c50; }\n  .sidebar .sidebar-dropdown .panel-collapse {\n    border: none; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n      border-radius: 0;\n      background-color: #1b3764;\n      border: 0 solid transparent; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n        color: #999; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n        color: #fff; }\n  .sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n      background: #162c50; }\n  .nested-menu .list-group-item {\n  cursor: pointer; }\n  .nested-menu .nested {\n  list-style-type: none; }\n  .nested-menu ul.submenu {\n  display: none;\n  height: 0; }\n  .nested-menu .expand ul.submenu {\n  display: block;\n  list-style-type: none;\n  height: auto; }\n  .nested-menu .expand ul.submenu li a {\n    color: #fff;\n    padding: 10px;\n    display: block; }\n  @media screen and (max-width: 992px) {\n  .sidebar {\n    top: 35px;\n    left: 0px; } }\n  @media (min-width: 1200px) {\n  .header-fields {\n    display: none; } }\n  ::-webkit-scrollbar {\n  width: 8px; }\n  ::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 0px white;\n  border-radius: 3px; }\n  ::-webkit-scrollbar-thumb {\n  border-radius: 3px;\n  -webkit-box-shadow: inset 0 0 3px white; }\n  .basic-container {\n  padding: 0 0 400px 0; }\n  .basic-container .menu-bar {\n    min-height: auto; }\n  .basic-container .menu-bar .mat-toolbar-row {\n      height: auto; }\n  .side-button {\n  padding: 0.257rem 0.5rem;\n  margin-bottom: -1px;\n  background: #1b3764;\n  border: 0;\n  border-top: 1px solid #1b3764;\n  border-bottom: 1px solid #1b3764;\n  border-radius: 0;\n  color: #555555;\n  text-decoration: none;\n  line-height: 1.5;\n  font-size: 0.875rem;\n  font-weight: 600;\n  width: 100%;\n  text-align: left; }\n  .ic {\n  padding: 0.5rem 0.5rem;\n  margin-bottom: -1px;\n  border: 0;\n  border-radius: 0;\n  text-decoration: none;\n  font-weight: 600; }\n  .side-button:focus, .ic:focus {\n  outline: none; }\n  .side-button:hover {\n  color: green;\n  border-top: 1px solid #555555;\n  border-bottom: 1px solid #555555; }\n  .ic:hover {\n  color: green; }\n  .side-button.router-link-active, .ic.router-link-active {\n  color: green; }\n  .mat-menu-item {\n  font-family: \"Source Sans Pro\"; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixhQUFhO0VBQ2IsU0FBUztFQUNULFdBQVc7RUFDWCwwQkFBMEI7RUFDMUIsVUFBVTtFQUNWLG1CQUFtQjtFQUNuQix5QkFWNkI7RUFXN0IsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQix5QkFiNkI7RUFjN0IsU0FBUztFQUNULGtCQUFrQjtFQUNsQixvQkFBb0I7RUFLcEIsZ0NBQWdDLEVBQUE7RUFwQnBDO0lBd0JZLG1CQXpCcUI7SUEwQnJCLFNBQVE7SUFDUixnQkFBZ0I7SUFDaEIsK0JBQStCO0lBSy9CLHFCQUFxQjtJQUNyQixZQUFZO0lBQ3RCLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsaUJBQWlCO0lBQ2pCLHNCQUFzQjtJQUN0QixnQkFBZ0I7SUFDaEIsNkJBQTZCLEVBQUE7RUExQy9CO0lBbURZLFlBQVksRUFBQTtFQW5EeEI7SUF1RFkseUJBQXlCO0lBQ3pCLDRCQUE0QjtJQUU1QixZQUFZLEVBQUE7RUExRHhCO0lBNkRZLGlCQUFpQixFQUFBO0VBN0Q3QjtNQWdFZ0IsOENBQThDLEVBQUE7RUFoRTlEO0lBcUVRLGlCQUFpQixFQUFBO0VBckV6QjtJQXdFUSxZQUFZO0lBQ1osV0FBVztJQUNYLGdCQUFlLEVBQUE7RUExRXZCO0lBNkVRLFVBQVUsRUFBQTtFQTdFbEI7SUFnRlEsVUFBVSxFQUFBO0VBaEZsQjtJQW9GWSxtQkFBbUI7SUFDbkIsWUFBWSxFQUFBO0VBckZ4QjtJQXdGWSxlQUFlO0lBQ2YsWUFBWTtJQUNaLGdCQUFnQixFQUFBO0VBMUY1QjtNQTRGZ0IsV0FBVztNQUNYLHFCQUFxQjtNQUNyQixnQkFBZ0I7TUFDaEIsbUJBaEdpQixFQUFBO0VBQ2pDO1FBaUdvQixrQkFBa0I7UUFDbEIsY0FBYztRQUNkLHVCQUF1QjtRQUN2QixpQkFBaUIsRUFBQTtFQXBHckM7O01BeUdnQixXQUFXO01BQ1gsYUFBYTtNQUNiLG9CQUFvQixFQUFBO0VBM0dwQztJQStHWSxtQkFBZ0QsRUFBQTtFQS9HNUQ7SUFtSFksWUFBWSxFQUFBO0VBbkh4QjtNQXNIb0IsZ0JBQWdCO01BQ2hCLHlCQXhIYTtNQXlIYiwyQkFBMkIsRUFBQTtFQXhIL0M7UUEwSHdCLFdBQVcsRUFBQTtFQTFIbkM7UUE2SHdCLFdBQVcsRUFBQTtFQTdIbkM7TUFpSW9CLG1CQUFnRCxFQUFBO0VBTXBFO0VBRVEsZUFBZSxFQUFBO0VBRnZCO0VBS1EscUJBQXFCLEVBQUE7RUFMN0I7RUFRUSxhQUFhO0VBQ2IsU0FBUyxFQUFBO0VBVGpCO0VBYVksY0FBYztFQUNkLHFCQUFxQjtFQUNyQixZQUFZLEVBQUE7RUFmeEI7SUFrQm9CLFdBQVc7SUFDWCxhQUFhO0lBQ2IsY0FBYyxFQUFBO0VBTWxDO0VBQ0k7SUFDSSxTQUFTO0lBQ1QsU0FBUyxFQUFBLEVBQ1o7RUFFTDtFQUNJO0lBQ0ksYUFBYSxFQUFBLEVBQ2hCO0VBR0w7RUFDSSxVQUFVLEVBQUE7RUFHZDtFQUNJLHVDQUF3RDtFQUN4RCxrQkFBa0IsRUFBQTtFQUd0QjtFQUNJLGtCQUFrQjtFQUNsQix1Q0FBd0QsRUFBQTtFQUs1RDtFQUNJLG9CQUFvQixFQUFBO0VBRHhCO0lBSU0sZ0JBQWdCLEVBQUE7RUFKdEI7TUFPUSxZQUFZLEVBQUE7RUFNbEI7RUFHRSx3QkFBd0I7RUFDeEIsbUJBQW1CO0VBR25CLG1CQWxONkI7RUFtTjdCLFNBQVM7RUFDRCw2QkFwTnFCO0VBcU5yQixnQ0FyTnFCO0VBc043QixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLHFCQUFxQjtFQUVyQixnQkFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsV0FBVTtFQUNWLGdCQUFnQixFQUFBO0VBR3BCO0VBQ0ksc0JBQXNCO0VBQ3RCLG1CQUFtQjtFQUlyQixTQUFTO0VBQ1QsZ0JBQWdCO0VBRWhCLHFCQUFxQjtFQUdyQixnQkFBZ0IsRUFBQTtFQUlsQjtFQUNJLGFBQVksRUFBQTtFQUdoQjtFQUVJLFlBQVk7RUFDWiw2QkFBNkI7RUFDN0IsZ0NBQWdDLEVBQUE7RUFLcEM7RUFFSSxZQUFZLEVBQUE7RUFNaEI7RUFFSSxZQUFZLEVBQUE7RUFTaEI7RUFDSSw4QkFBOEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9jb21wb25lbnRzL3NpZGViYXIvc2lkZWJhci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjogIzFiMzc2NDtcbi5zaWRlYmFyIHtcbiAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB6LWluZGV4OiAxMDAwO1xuICAgIHRvcDogNTBweDtcbiAgICBsZWZ0OiAyMzVweDtcbiAgICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbiAgICB3aWR0aDogMTUlO1xuICAgIG1hcmdpbi1sZWZ0OiAtMjM1cHg7XG4gICAgYm9yZGVyOiAzcHggc29saWQgJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgb3ZlcmZsb3cteTogYXV0bztcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gICAgYm90dG9tOiAwO1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICBwYWRkaW5nLWJvdHRvbTogNDBweDtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAgIC1tb3otdHJhbnNpdGlvbjogYWxsIDAuMnMgZWFzZS1pbi1vdXQ7XG4gICAgLW1zLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAgIC1vLXRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjJzIGVhc2UtaW4tb3V0O1xuICAgIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwyNTUsMjU1LDAuMyk7XG4gICAgLmxpc3QtZ3JvdXAge1xuICAgICAgICBhLmxpc3QtZ3JvdXAtaXRlbSB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gICAgICAgICAgICBib3JkZXI6MDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNTNweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcbiAgICAgICAgICAgIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gICAgICAgICAgICAvLyBjb2xvcjogIzU1NTU1NTtcbiAgICAgICAgICAgIC8vIGNvbG9yOiBlMGUwZTA7XG4gICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgICAgICBoZWlnaHQ6IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogdmFyKC0tdmVyeS1saWdodC1waW5rKTtcbiAgXG4gICAgICAgICAgICAvLyAuZmEge1xuICAgICAgICAgICAgLy8gICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICAgICAgICAgIC8vIH1cbiAgICAgICAgfVxuICAgICAgICBhOmhvdmVyIHtcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcbiAgICAgICAgICAgIC8vIGNvbG9yOiAjNTU1NTU1O1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICB9XG4gICAgICAgIGEucm91dGVyLWxpbmstYWN0aXZlIHtcbiAgICAgICAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcbiAgICAgICAgICAgIGJvcmRlci10b3AtY29sb3I6ICM1NTU1NTU7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tLWNvbG9yOiAjNTU1NTU1O1xuICAgICAgICAgICAgLy8gY29sb3I6ICM1NTU1NTU7XG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XG4gICAgICAgIH1cbiAgICAgICAgLmhlYWRlci1maWVsZHMge1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDEwcHg7XG4gICAgICAgIFxuICAgICAgICAgICAgPiAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgICAgICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIHRhYmxle1xuICAgICAgICBtYXJnaW4tbGVmdDogMThweDtcbiAgICB9XG4gICAgdGFibGUgYXtcbiAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICBtYXJnaW46IDVweDtcbiAgICAgICAgbWFyZ2luLXRvcDozNXB4O1xuICAgIH1cbiAgICB0ciB0ZDpudGgtY2hpbGQoMSl7XG4gICAgICAgIHdpZHRoOiAzMCU7XG4gICAgfVxuICAgIHRyIHRkOm50aC1jaGlsZCgyKXtcbiAgICAgICAgd2lkdGg6IDcwJTtcbiAgICB9XG4gICAgLnNpZGViYXItZHJvcGRvd24ge1xuICAgICAgICAqOmZvY3VzIHtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IG5vbmU7XG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgIH1cbiAgICAgICAgLnBhbmVsLXRpdGxlIHtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgICAgICAgICBhIHtcbiAgICAgICAgICAgICAgICBjb2xvcjogIzk5OTtcbiAgICAgICAgICAgICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gICAgICAgICAgICAgICAgc3BhbiB7XG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDAuNzVyZW0gMS41cmVtO1xuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMXJlbTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhOmhvdmVyLFxuICAgICAgICAgICAgYTpmb2N1cyB7XG4gICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgICAgICAgICBvdXRsaW5lLW9mZnNldDogLTJweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAucGFuZWwtdGl0bGU6aG92ZXIge1xuICAgICAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xuICAgICAgICB9XG4gICAgICAgIC5wYW5lbC1jb2xsYXBzZSB7XG4gICAgICAgICAgICAvLyBib3JkZXItcmFkaW91czogMDtcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgIC5wYW5lbC1ib2R5IHtcbiAgICAgICAgICAgICAgICAubGlzdC1ncm91cC1pdGVtIHtcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xuICAgICAgICAgICAgICAgICAgICBib3JkZXI6IDAgc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgICAgICAgICAgICAgICAgIGEge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM5OTk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYTpob3ZlciB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAubGlzdC1ncm91cC1pdGVtOmhvdmVyIHtcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cbi5uZXN0ZWQtbWVudSB7XG4gICAgLmxpc3QtZ3JvdXAtaXRlbSB7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB9XG4gICAgLm5lc3RlZCB7XG4gICAgICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICB9XG4gICAgdWwuc3VibWVudSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgIGhlaWdodDogMDtcbiAgICB9XG4gICAgJiAuZXhwYW5kIHtcbiAgICAgICAgdWwuc3VibWVudSB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICAgICAgICAgIGhlaWdodDogYXV0bztcbiAgICAgICAgICAgIGxpIHtcbiAgICAgICAgICAgICAgICBhIHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gICAgLnNpZGViYXIge1xuICAgICAgICB0b3A6IDM1cHg7XG4gICAgICAgIGxlZnQ6IDBweDtcbiAgICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTIwMHB4KSB7XG4gICAgLmhlYWRlci1maWVsZHMge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbn1cblxuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gICAgd2lkdGg6IDhweDtcbn1cblxuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgMHB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xufVxuXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgM3B4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XG59XG5cblxuXG4uYmFzaWMtY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAwIDAgNDAwcHggMDtcbiAgXG4gICAgLm1lbnUtYmFyIHtcbiAgICAgIG1pbi1oZWlnaHQ6IGF1dG87XG4gIFxuICAgICAgLm1hdC10b29sYmFyLXJvdyB7XG4gICAgICAgIGhlaWdodDogYXV0bztcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuXG4gIC5zaWRlLWJ1dHRvbiB7XG4gICAgLy8gICBwYWRkaW5nOjhweDtcblxuICAgIHBhZGRpbmc6IDAuMjU3cmVtIDAuNXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAtMXB4O1xuICAgIC8vICAgcGFkZGluZy1yaWdodDogOHB4O1xuICAgIC8vICAgcGFkZGluZy1sZWZ0OiA4cHg7XG4gICAgYmFja2dyb3VuZDogJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xuICAgIGJvcmRlcjogMDtcbiAgICAgICAgICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yO1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgY29sb3I6ICM1NTU1NTU7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIC8vIGZvbnQtd2VpZ2h0OjgwMDtcbiAgICBsaW5lLWhlaWdodDoxLjU7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIHdpZHRoOjEwMCU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuLmljIHtcbiAgICBwYWRkaW5nOiAwLjVyZW0gMC41cmVtO1xuICAgIG1hcmdpbi1ib3R0b206IC0xcHg7XG4gIC8vICAgcGFkZGluZy1yaWdodDogOHB4O1xuICAvLyAgIHBhZGRpbmctbGVmdDogOHB4O1xuLy8gICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbi8vICAgY29sb3I6ICM1NTU1NTU7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbi8vICAgbGluZS1oZWlnaHQ6MS41O1xuLy8gICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuLy8gICB3aWR0aDoxMDAlO1xuLy8gICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5zaWRlLWJ1dHRvbjpmb2N1cywuaWM6Zm9jdXN7XG4gICAgb3V0bGluZTpub25lO1xufVxuXG4uc2lkZS1idXR0b246aG92ZXIge1xuICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkdG9wbmF2LWJhY2tncm91bmQtY29sb3IsIDUlKTtcbiAgICBjb2xvcjogZ3JlZW47XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICM1NTU1NTU7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM1NTU1NTU7XG4gICAgLy8gYm9yZGVyLWxlZnQ6MXB4IHNvbGlkICNFQ0YwRjE7XG4gICAgLy8gYm9yZGVyLXJpZ2h0OjFweCBzb2xpZCB3aGl0ZTtcbn1cblxuLmljOmhvdmVyIHtcbiAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJHRvcG5hdi1iYWNrZ3JvdW5kLWNvbG9yLCA1JSk7XG4gICAgY29sb3I6IGdyZWVuO1xuICAgIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCAjNTU1NTU1O1xuICAgIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjNTU1NTU1O1xuICAgIC8vIGJvcmRlci1sZWZ0OjFweCBzb2xpZCAjRUNGMEYxO1xuICAgIC8vIGJvcmRlci1yaWdodDoxcHggc29saWQgd2hpdGU7XG59XG4uc2lkZS1idXR0b24ucm91dGVyLWxpbmstYWN0aXZlICwgLmljLnJvdXRlci1saW5rLWFjdGl2ZSB7XG4gICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCR0b3BuYXYtYmFja2dyb3VuZC1jb2xvciwgNSUpO1xuICAgIGNvbG9yOiBncmVlbjtcbiAgICAvLyBib3JkZXItbGVmdDoxcHggc29saWQgI0VDRjBGMTtcbiAgICAvLyBib3JkZXItcmlnaHQ6MXB4IHNvbGlkIHdoaXRlO1xufVxuXG4vLyAuY2RrLW92ZXJsYXktcGFuZSB7XG4vLyAgICAgbWFyZ2luLWxlZnQ6IDE4MHB4O1xuLy8gfVxuXG4ubWF0LW1lbnUtaXRlbSB7XG4gICAgZm9udC1mYW1pbHk6IFwiU291cmNlIFNhbnMgUHJvXCI7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");




var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(route, local) {
        this.route = route;
        this.local = local;
        this.role = this.local.getUserRole();
        this.email = this.local.getEmail();
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent.prototype.navigateRoute = function () {
        if (this.role === "superuser") {
            this.route.navigateByUrl('/superadmin');
        }
        else if (this.role === "issuer") {
            this.route.navigateByUrl('/issuer');
        }
        else if (this.role === "authorizer") {
            this.route.navigateByUrl('/authorizer');
        }
    };
    SidebarComponent.prototype.logout = function () {
        localStorage.clear();
        this.route.navigateByUrl('/home');
    };
    SidebarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/layout/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/layout/components/sidebar/sidebar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_app_services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/layout/layout-routing.module.ts ***!
  \*************************************************/
/*! exports provided: LayoutRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutRoutingModule", function() { return LayoutRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout.component */ "./src/app/layout/layout.component.ts");




var routes = [
    {
        path: '',
        component: _layout_component__WEBPACK_IMPORTED_MODULE_3__["LayoutComponent"],
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'wallet', loadChildren: './wallet/wallet.module#WalletModule' },
            { path: 'superadmin', loadChildren: './superadmin/superadmin.module#SuperadminModule' },
            { path: 'certificate', loadChildren: './certificate/certificate.module#CertificateModule' },
            { path: 'kyc', loadChildren: './kyc/kyc.module#KycModule' },
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule' },
            { path: 'issuer', loadChildren: './issuer/issuer.module#IssuerModule' },
            { path: 'issue-certificate', loadChildren: './issue-certificate/issue-certificate.module#IssueCertificateModule' },
            { path: 'authorizer', loadChildren: './authorizer/authorizer.module#AuthorizerModule' },
            { path: 'issue-certificate/email/:email', loadChildren: './issue-certificate/issue-certificate.module#IssueCertificateModule' },
        ]
    }
];
var LayoutRoutingModule = /** @class */ (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/layout.component.html":
/*!**********************************************!*\
  !*** ./src/app/layout/layout.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header ></app-header>\n<app-sidebar></app-sidebar>\n<section class=\"main-container\">\n    <router-outlet></router-outlet>\n</section>\n<app-footer></app-footer>\n"

/***/ }),

/***/ "./src/app/layout/layout.component.scss":
/*!**********************************************!*\
  !*** ./src/app/layout/layout.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-container {\n  margin-left: 10%;\n  padding: 15px;\n  -ms-overflow-x: hidden;\n  overflow-x: hidden;\n  overflow-y: scroll;\n  position: relative;\n  overflow: hidden;\n  background-color: white; }\n\n@media screen and (max-width: 992px) {\n  .main-container {\n    margin-left: 0px !important;\n    background-color: white; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2xheW91dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUVmLHVCQUF1QixFQUFBOztBQUU1QjtFQUNJO0lBQ0ksMkJBQTJCO0lBQzNCLHVCQUF1QixFQUFBLEVBQzFCIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2xheW91dC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWNvbnRhaW5lciB7XG4gICAgLy8gbWFyZ2luLXRvcDogMCU7XG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICAgIC1tcy1vdmVyZmxvdy14OiBoaWRkZW47XG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBcbiAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5OTJweCkge1xuICAgIC5tYWluLWNvbnRhaW5lciB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/layout/layout.component.ts":
/*!********************************************!*\
  !*** ./src/app/layout/layout.component.ts ***!
  \********************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LayoutComponent = /** @class */ (function () {
    function LayoutComponent() {
    }
    LayoutComponent.prototype.ngOnInit = function () { };
    LayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./layout.component.html */ "./src/app/layout/layout.component.html"),
            styles: [__webpack_require__(/*! ./layout.component.scss */ "./src/app/layout/layout.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LayoutComponent);
    return LayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout.module.ts":
/*!*****************************************!*\
  !*** ./src/app/layout/layout.module.ts ***!
  \*****************************************/
/*! exports provided: LayoutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutModule", function() { return LayoutModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _layout_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layout-routing.module */ "./src/app/layout/layout-routing.module.ts");
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layout.component */ "./src/app/layout/layout.component.ts");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/sidebar/sidebar.component */ "./src/app/layout/components/sidebar/sidebar.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/layout/components/header/header.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/layout/components/footer/footer.component.ts");
/* harmony import */ var _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");










var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _layout_routing_module__WEBPACK_IMPORTED_MODULE_4__["LayoutRoutingModule"]
            ],
            declarations: [_layout_component__WEBPACK_IMPORTED_MODULE_5__["LayoutComponent"], _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_6__["SidebarComponent"], _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"], _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"]],
            providers: [_services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_9__["RequestOptionsService"]]
        })
    ], LayoutModule);
    return LayoutModule;
}());



/***/ }),

/***/ "./src/app/services/localstorage/localstorage.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/localstorage/localstorage.service.ts ***!
  \***************************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.setEmail = function (email) {
        localStorage.setItem('email', email);
    };
    LocalstorageService.prototype.getEmail = function () {
        return localStorage.getItem('email');
    };
    LocalstorageService.prototype.setLoggedIn = function (login) {
        localStorage.setItem('isLoggedIn', login);
    };
    LocalstorageService.prototype.getLoggedIn = function () {
        return localStorage.getItem('isLoggedIn');
    };
    // sets belrium token
    LocalstorageService.prototype.setBelriumToken = function (token) {
        localStorage.setItem('token', token);
    };
    //gets belrium token
    LocalstorageService.prototype.getBelriumToken = function () {
        return localStorage.getItem('token');
    };
    // sets bkvs-dm token
    LocalstorageService.prototype.setBKVSToken = function (bkvstoken) {
        localStorage.setItem('bkvs-token', bkvstoken);
    };
    //gets bkvs-dm token
    LocalstorageService.prototype.getBKVSToken = function () {
        return localStorage.getItem('bkvs-token');
    };
    // sets role of an user
    LocalstorageService.prototype.setUserRole = function (role) {
        localStorage.setItem('role', role);
    };
    //gets role of an user
    LocalstorageService.prototype.getUserRole = function () {
        return localStorage.getItem('role');
    };
    // sets wallet balance
    LocalstorageService.prototype.setBalance = function (balance) {
        localStorage.setItem('balance', balance);
    };
    //gets wallet balance
    LocalstorageService.prototype.getBalance = function () {
        return localStorage.getItem('balance');
    };
    // sets country of an user
    LocalstorageService.prototype.setCountryName = function (countryName) {
        localStorage.setItem('countryName', countryName);
    };
    //gets country of an user
    LocalstorageService.prototype.getCountryName = function () {
        return localStorage.getItem('countryName');
    };
    //sets dapp id for the user
    LocalstorageService.prototype.setDappId = function (dappid) {
        localStorage.setItem('dappid', dappid);
    };
    //gets dapp id for the suer
    LocalstorageService.prototype.getDappId = function () {
        return localStorage.getItem('dappid');
    };
    LocalstorageService.prototype.setKycStatus = function (kycstatus) {
        localStorage.setItem('kycstatus', kycstatus);
    };
    LocalstorageService.prototype.getKycStatus = function () {
        return localStorage.getItem('kycstatus');
    };
    LocalstorageService.prototype.setWalletAddress = function (address) {
        localStorage.setItem('address', address);
    };
    LocalstorageService.prototype.getWalletAddress = function () {
        return localStorage.getItem('address');
    };
    LocalstorageService.prototype.setOrganization = function (organization) {
        localStorage.setItem('organization', organization);
    };
    LocalstorageService.prototype.getOrganization = function () {
        return localStorage.getItem('organization');
    };
    LocalstorageService.prototype.setUserName = function (name) {
        localStorage.setItem('name', name);
    };
    LocalstorageService.prototype.getUserName = function () {
        return localStorage.getItem('name');
    };
    LocalstorageService.prototype.setIssuerId = function (id) {
        localStorage.setItem('issuerid', id);
    };
    LocalstorageService.prototype.getIssuerId = function () {
        return localStorage.getItem('issuerid');
    };
    LocalstorageService.prototype.setIssuerDept = function (dept) {
        localStorage.setItem('issuerDept', dept);
    };
    LocalstorageService.prototype.getIssuerDept = function () {
        return localStorage.getItem('issuerDept');
    };
    LocalstorageService.prototype.setAuthorizerId = function (id) {
        localStorage.setItem('Authorizerid', id);
    };
    LocalstorageService.prototype.getAuthorizerId = function () {
        return localStorage.getItem('Authorizerid');
    };
    LocalstorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ })

}]);
//# sourceMappingURL=layout-layout-module.js.map