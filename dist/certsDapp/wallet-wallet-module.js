(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["wallet-wallet-module"],{

/***/ "./src/app/layout/wallet/wallet-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/wallet/wallet-routing.module.ts ***!
  \********************************************************/
/*! exports provided: WalletRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletRoutingModule", function() { return WalletRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _wallet_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wallet.component */ "./src/app/layout/wallet/wallet.component.ts");




var routes = [
    {
        path: '', component: _wallet_component__WEBPACK_IMPORTED_MODULE_3__["WalletComponent"]
    }
];
var WalletRoutingModule = /** @class */ (function () {
    function WalletRoutingModule() {
    }
    WalletRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], WalletRoutingModule);
    return WalletRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/wallet/wallet.component.html":
/*!*****************************************************!*\
  !*** ./src/app/layout/wallet/wallet.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\" style=\"background-color: #ffffff; background: #ffffff;\">\n    <p style=\"text-align: center;font-size: 16px;margin-top:60px;\">Congratulations! you just got a new wallet on the Belrium network</p>\n  <div class=\"wallet\" >\n      <div class=\"col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12\">\n    <img src=\"../../../assets/images/belbooks-logo.svg\" style=\"margin-left:-60%;\">\n    <br><br><br><br><br>\n    \n    <h4><span style=\"margin-left: 0%;font-size: 16px;\"><b>Wallet Balance</b></span><span style=\"margin-left: 33%;font-size: 16px;\"><b>DAPP Balance</b></span></h4>\n    <h4><span style=\"margin-left: -21%;font-size: 16px;\"><b>0</b></span><span style=\"margin-left: 55%;font-size: 16px;\"><b>0</b></span></h4>\n  \n    <h4 style=\"margin-top: 15%\"><span style=\"margin-left: 0%;font-size: 16px;\"><b>Valid</b></span><span style=\"margin-left: 5%;font-size: 16px;\"><b>MM/YY</b></span><span style=\"margin-left:175px;\"><img src=\"../../../assets/images/chip2.png\" style=\"border-radius: 10px;\"></span></h4>\n    <h4 ><span style=\"margin-left: -50%;font-size: 16px;\"><b></b></span><span style=\"margin-left: 10%;font-size: 16px;\"><b>06/20</b></span></h4>\n  </div>\n \n</div>\n<div class=\"col-12\" style=\"margin-left:33%; padding-top:40% \">\n  <div class=\"form-row\"> \n<button type=\"button\" data-toggle=\"modal\" data-target=\"#recharge\" data-backdrop=\"static\" data-keyboard=\"false\"  class=\"btn btn-recharge\">RECHARGE</button>&nbsp;&nbsp;&nbsp;&nbsp;\n<button type=\"button\" data-toggle=\"modal\" data-target=\"#dappbal\" data-backdrop=\"static\" data-keyboard=\"false\" class=\"btn btn-updatebalance\">UPDATE BALANCE</button>&nbsp;&nbsp;&nbsp;&nbsp;\n<p id=\"p\"><button class=\"regbtn\" type=\"submit\"  (click)=\"openModal()\">DAPP REGISTER</button></p>\n</div>\n</div>\n</div>\n\n\n\n\n\n<div class=\"modal fade\" id=\"dappbal\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">UPDATE DAPP BALANCE</h4>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n        </div>\n         <div class=\"modal-body\">\n          \n            <div class=\"form-group col-md-12\">\n                \n                  <input type=\"text\" class=\"form-control\"  name=\"Wallet\" placeholder=\"Wallet Address\">\n              </div>\n              <div class=\"form-group col-md-12\">\n                  <input type=\"text\" class=\"form-control\"  name=\"amount\" placeholder=\"Amount\" [(ngModel)]=\"amount\">\n                  <h6 class=\"note\">Note:1 bel equals $7</h6>\n                </div>\n\n              <div class=\"form-group col-md-12\">\n                <input type=\"text\" class=\"form-control\"  name=\"passphrase\" placeholder=\"Passphrase\" [(ngModel)]=\"Passphrase\">\n              </div>\n        </div>\n        <div class=\"modal-footer\">\n            <button class=\"submitbtn\" (click) =\"update()\">DONE</button>\n            <button class=\"cancelbtn\"  data-dismiss=\"modal\">CANCEL</button>\n        </div>\n      </div>\n      \n    </div>\n  </div>\n\n  <div class=\"modal fade\" id=\"recharge\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n        <!-- Modal content-->\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n              <h4 class=\"modal-title\">RECHARGE WALLET</h4>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            </div>\n             <div class=\"modal-body\">\n              \n                <div class=\"form-group col-md-12\">\n                  <img style=\"margin-left:25%\" src=\"../../../assets/img/walletrecharge.png\" style=\"border-radius:15px;margin-left:24%;\">\n                  </div>\n                  <div class=\"form-group col-md-12\">\n                      <input type=\"text\" class=\"form-control\"  name=\"amount\" placeholder=\"Amount\">\n                    </div>\n            </div>\n            <div class=\"modal-footer\">\n                <button class=\"submitbtn\" (click)=\"recharge()\">DONE</button>\n                <button class=\"cancelbtn\"  data-dismiss=\"modal\">CANCEL</button>\n            </div>\n          </div>\n          \n        </div>\n      </div>\n      \n      <!-- dapp reg popup -->\n\n<div class=\"backdrop\" [ngStyle]=\"{'display':display}\"></div>\n<div class=\"modal\" tabindex=\"-1\" role=\"dialog\"  [ngStyle]=\"{'display':display}\">\n<div class=\"modal-dialog\" role=\"document\">\n  <div class=\"modal-content\">\n    <div class=\"modal-header\">\n          <!-- <div class=\"topsec\">\n                <img src=\"../../assets\">\n             </div> -->\n             <h4 class=\"modal-title\">DAPP REGISTER</h4>\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"onCloseHandled()\"><span aria-hidden=\"true\">&times;</span></button>\n      \n    </div>\n    <div class=\"modal-body\">\n        <div class=\"col-12\">\n         <input id=\"passphrase\" name=\"passphrase\" placeholder=\"Enter Your Passphrase\"  required=\"required\"  \n       class=\"form-control here\" [(ngModel)]=\"passphrase\">\n        \n    </div>\n    <div class=\"col-12\">\n      <input id=\"dappname\" name=\"dappname\" placeholder=\"DAPP Name\" type=\"text\" required=\"required\" \n      class=\"form-control here\" [(ngModel)]=\"dappname\">\n   </div>\n   <div class=\"col-12\">\n    <textarea class=\"form-control\" id=\"description\" placeholder=\"Description\"  rows=\"3\" [(ngModel)]=\"description\"></textarea>\n </div>\n <div class=\"col-12\">\n  <input  id=\"company1\" name=\"company\" placeholder=\"company\" type=\"text\" required=\"required\" \n   class=\"form-control here\" [(ngModel)]=\"company1\">\n</div>\n    <div class=\"modal-footer\">\n      \n     <button type=\"button\" class=\"submitbtn\"  (click)=\"registerdapp()\" >REGISTER</button>\n     <button type=\"button\" class=\"cancelbtn\" (click)=\"onCloseHandled()\" >CANCEL</button>\n    </div>\n  </div>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/layout/wallet/wallet.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/layout/wallet/wallet.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".wallet {\n  width: 466px;\n  height: 263px;\n  text-align: center;\n  border: 1px #ccc solid;\n  padding: 10px;\n  margin-top: 10%;\n  border-radius: 18px;\n  line-height: 5px;\n  float: left;\n  margin-left: 30%;\n  background-image: url('walletfixbg4.png');\n  opacity: 0.9;\n  background-color: #cccccc;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  position: relative;\n  box-shadow: 1px 1px 1px 1px #ccc;\n  font-family: Muli;\n  font-size: large;\n  color: #000000;\n  font-style: #000000;\n  letter-spacing: normal; }\n\n.btn-recharge {\n  width: 120px;\n  height: 45px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  color: #ffffff;\n  border: 1px solid orange;\n  font-size: 13px; }\n\n.btn-updatebalance {\n  width: 120px;\n  height: 45px;\n  border-radius: 10px;\n  border: solid 1px #d8d8d8;\n  background-color: var(--white);\n  font-size: 13px; }\n\n.regbtn {\n  text-align: center;\n  margin: 0 auto;\n  width: 120px;\n  height: 45px;\n  border-radius: 10px;\n  border: solid 1px #d8d8d8;\n  background-color: var(--white);\n  font-size: 13px; }\n\n#p {\n  text-align: center; }\n\n.modal-dialog {\n  max-width: 25%;\n  margin-top: 20%;\n  margin-left: 45%;\n  font-size: 16px; }\n\n.modal-content {\n  border-radius: 20px;\n  font-size: 16px; }\n\n.form-control {\n  font-size: 16px; }\n\n.form-control ::-webkit-input-placeholder {\n  opacity: 0.31;\n  font-family: Muli;\n  font-size: 10px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a0a0a0; }\n\n.form-control ::-ms-input-placeholder {\n  opacity: 0.31;\n  font-family: Muli;\n  font-size: 10px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a0a0a0; }\n\n.form-control ::placeholder {\n  opacity: 0.31;\n  font-family: Muli;\n  font-size: 10px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a0a0a0; }\n\n.fromcontrol input {\n  height: 35px;\n  border-radius: 4px;\n  border: solid 1px #e6e6e6;\n  background-color: var(--white); }\n\nselect.form-control:not([size]):not([multiple]) {\n  height: 35px; }\n\n.modal-header {\n  border-bottom: 0px;\n  font-size: 16px; }\n\n.modal-body {\n  padding: 6%;\n  font-size: 16px; }\n\n.submitbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white); }\n\n.cancelbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: white;\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: black; }\n\n.modal-content {\n  border: 0px;\n  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);\n  font-size: 16px; }\n\n.modal-footer {\n  justify-content: center;\n  border-top: 0px;\n  font-size: 16px; }\n\n.form-control {\n  margin-top: 5%; }\n\n.modal-title {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #b4b4b4;\n  margin-left: 30%;\n  margin-top: 5%;\n  font-size: 16px; }\n\n.note {\n  font-family: Muli;\n  font-size: 12px;\n  font-weight: bold;\n  font-style: italic;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  color: #a0a0a0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L3dhbGxldC93YWxsZXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsYUFBYTtFQUNiLGVBQWM7RUFDZCxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIseUNBQTZEO0VBQzdELFlBQVk7RUFDWix5QkFBeUI7RUFDekIsMkJBQTJCO0VBQzNCLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLGdDQUFnQztFQUVoQyxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGNBQWE7RUFDYixtQkFBbUI7RUFHbkIsc0JBQXNCLEVBQUE7O0FBa0N4QjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLCtCQUErQjtFQUMvQixjQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLGVBQWUsRUFBQTs7QUFFaEI7RUFDQyxZQUFZO0VBQ1osWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsOEJBQThCO0VBQzlCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxrQkFBa0I7RUFDbEIsY0FBYTtFQUNiLFlBQVk7RUFDWixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHlCQUEwQjtFQUMxQiw4QkFBOEI7RUFDOUIsZUFBZSxFQUFBOztBQUlqQjtFQUNFLGtCQUFrQixFQUFBOztBQU1wQjtFQUNBLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWMsRUFBQTs7QUFFZDtFQUNBLG1CQUFtQjtFQUNuQixlQUFjLEVBQUE7O0FBRWQ7RUFDRSxlQUFjLEVBQUE7O0FBSWhCO0VBQ0EsYUFBYTtFQUViLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsaUJBQWlCO0VBRWpCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsZ0JBQWdCO0VBRWhCLHNCQUFzQjtFQUV0QixnQkFBZ0I7RUFFaEIsY0FBYyxFQUFBOztBQW5CZDtFQUNBLGFBQWE7RUFFYixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGlCQUFpQjtFQUVqQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGdCQUFnQjtFQUVoQixzQkFBc0I7RUFFdEIsZ0JBQWdCO0VBRWhCLGNBQWMsRUFBQTs7QUFuQmQ7RUFDQSxhQUFhO0VBRWIsaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixpQkFBaUI7RUFFakIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixnQkFBZ0I7RUFFaEIsc0JBQXNCO0VBRXRCLGdCQUFnQjtFQUVoQixjQUFjLEVBQUE7O0FBS2Q7RUFDQSxZQUFZO0VBRVosa0JBQWtCO0VBRWxCLHlCQUF5QjtFQUV6Qiw4QkFBOEIsRUFBQTs7QUFHOUI7RUFDQSxZQUFZLEVBQUE7O0FBR1o7RUFDQSxrQkFBa0I7RUFDbEIsZUFBYyxFQUFBOztBQUVkO0VBQ0EsV0FBVTtFQUNWLGVBQWMsRUFBQTs7QUFLZDtFQUNBLFlBQVk7RUFFWixjQUFjO0VBRWQsbUJBQW1CO0VBRW5CLCtCQUErQjtFQUUvQixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGdCQUFnQjtFQUVoQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsa0JBQWtCO0VBRWxCLG1CQUFtQixFQUFBOztBQUtuQjtFQUNBLFlBQVk7RUFFWixjQUFjO0VBRWQsbUJBQW1CO0VBRW5CLHVCQUFzQjtFQUN0QixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGdCQUFnQjtFQUVoQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsa0JBQWtCO0VBRWxCLFlBQVksRUFBQTs7QUFLWjtFQUNBLFdBQVc7RUFDWCx5Q0FBcUM7RUFDckMsZUFBYyxFQUFBOztBQUdkO0VBQ0EsdUJBQXVCO0VBRXZCLGVBQWU7RUFDZixlQUFjLEVBQUE7O0FBSWQ7RUFDQSxjQUFhLEVBQUE7O0FBS2I7RUFDQSxpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGdCQUFnQjtFQUVoQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsZ0JBQWdCO0VBRWhCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWMsRUFBQTs7QUFNZDtFQUVBLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsaUJBQWlCO0VBRWpCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsZ0JBQWdCO0VBRWhCLHNCQUFzQjtFQUl0QixjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvd2FsbGV0L3dhbGxldC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53YWxsZXQge1xuICB3aWR0aDogNDY2cHg7XG4gIGhlaWdodDogMjYzcHg7O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4ICNjY2Mgc29saWQ7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIG1hcmdpbi10b3A6MTAlO1xuICBib3JkZXItcmFkaXVzOiAxOHB4O1xuICBsaW5lLWhlaWdodDogNXB4O1xuICBmbG9hdDogbGVmdDtcbiAgbWFyZ2luLWxlZnQ6IDMwJTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy93YWxsZXRmaXhiZzQucG5nXCIpO1xuICBvcGFjaXR5OiAwLjk7XG4gIGJhY2tncm91bmQtY29sb3I6ICNjY2NjY2M7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3gtc2hhZG93OiAxcHggMXB4IDFweCAxcHggI2NjYztcbiAgLy8gZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IGxhcmdlO1xuICBjb2xvcjojMDAwMDAwO1xuICBmb250LXN0eWxlOiAjMDAwMDAwO1xuICBcbiAgLy8gbGluZS1oZWlnaHQ6IDEuMjU7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIC8vIHRleHQtYWxpZ246IGxlZnQ7XG4gIC8vIGNvbG9yOiB2YXIoLS12ZXJ5LWxpZ2h0LXBpbmspOyAgXG59XG5cbi8vIC5idG4tcmVjaGFyZ2Uge1xuLy8gICB3aWR0aDogMTg1cHg7XG4vLyAgIGhlaWdodDogNDVweDtcbi8vICAgYm9yZGVyLXJhZGl1czogMTBweDtcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tb3JhbmdlKTtcbi8vICAgY29sb3I6I2ZmZmZmZjtcbi8vICAgYm9yZGVyOiAxcHggc29saWQgb3JhbmdlO1xuLy8gICBmb250LXNpemU6IDEzcHg7XG4vLyB9XG4vLyAuYnRuLXVwZGF0ZWJhbGFuY2V7XG4vLyAgIHdpZHRoOiAxODVweDtcbi8vICAgaGVpZ2h0OiA0NXB4O1xuLy8gICBib3JkZXItcmFkaXVzOiAxMHB4O1xuLy8gICBib3JkZXI6IHNvbGlkIDFweCAjZDhkOGQ4O1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7ICBcbi8vICAgZm9udC1zaXplOiAxM3B4OyAgICBcbi8vIH1cblxuLy8gLnJlZ2J0bntcbi8vICAgdGV4dC1hbGlnbjogY2VudGVyO1xuLy8gICBtYXJnaW46MCBhdXRvO1xuLy8gICB3aWR0aDogMTg1cHg7XG4vLyAgIGhlaWdodDogNDVweDtcbi8vICAgYm9yZGVyLXJhZGl1czogMTBweDtcbi8vICAgYm9yZGVyOiBzb2xpZCAxcHggI2Q4ZDhkODtcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUpOyAgXG4vLyAgIGZvbnQtc2l6ZTogMTNweDsgIFxuLy8gfVxuXG4uYnRuLXJlY2hhcmdlIHtcbiAgd2lkdGg6IDEyMHB4O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLW9yYW5nZSk7XG4gIGNvbG9yOiNmZmZmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkIG9yYW5nZTtcbiAgZm9udC1zaXplOiAxM3B4O1xuIH1cbiAuYnRuLXVwZGF0ZWJhbGFuY2V7XG4gIHdpZHRoOiAxMjBweDtcbiAgaGVpZ2h0OiA0NXB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXI6IHNvbGlkIDFweCAjZDhkOGQ4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiB9XG4gXG4gLnJlZ2J0bntcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46MCBhdXRvO1xuICB3aWR0aDogMTIwcHg7XG4gIGhlaWdodDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2Q4ZDhkOCA7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICAvLyBjb2xvcjogIzAwMDAwMDtcbiB9XG5cbiNwe1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuLy9zdHlsZXMgZm9yIHBvcHVwXG5cbi5tb2RhbC1kaWFsb2cge1xubWF4LXdpZHRoOiAyNSU7XG5tYXJnaW4tdG9wOiAyMCU7XG5tYXJnaW4tbGVmdDogNDUlO1xuZm9udC1zaXplOjE2cHg7XG59XG4ubW9kYWwtY29udGVudHtcbmJvcmRlci1yYWRpdXM6IDIwcHg7XG5mb250LXNpemU6MTZweDtcbn1cbi5mb3JtLWNvbnRyb2x7XG4gIGZvbnQtc2l6ZToxNnB4O1xufVxuXG5cbi5mb3JtLWNvbnRyb2wgOjpwbGFjZWhvbGRlcntcbm9wYWNpdHk6IDAuMzE7XG5cbmZvbnQtZmFtaWx5OiBNdWxpO1xuXG5mb250LXNpemU6IDEwcHg7XG5cbmZvbnQtd2VpZ2h0OiBib2xkO1xuXG5mb250LXN0eWxlOiBub3JtYWw7XG5cbmZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG5saW5lLWhlaWdodDogMS4zO1xuXG5sZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG50ZXh0LWFsaWduOiBsZWZ0O1xuXG5jb2xvcjogI2EwYTBhMDtcblxufVxuXG5cbi5mcm9tY29udHJvbCBpbnB1dHtcbmhlaWdodDogMzVweDtcblxuYm9yZGVyLXJhZGl1czogNHB4O1xuXG5ib3JkZXI6IHNvbGlkIDFweCAjZTZlNmU2O1xuXG5iYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG59XG5cbnNlbGVjdC5mb3JtLWNvbnRyb2w6bm90KFtzaXplXSk6bm90KFttdWx0aXBsZV0pIHtcbmhlaWdodDogMzVweDtcbn1cblxuLm1vZGFsLWhlYWRlcntcbmJvcmRlci1ib3R0b206IDBweDsgXG5mb250LXNpemU6MTZweDtcbn1cbi5tb2RhbC1ib2R5e1xucGFkZGluZzo2JTtcbmZvbnQtc2l6ZToxNnB4O1xuLy8gbWFyZ2luLXRvcDo1JTtcbn1cblxuXG4uc3VibWl0YnRue1xud2lkdGg6IDE0NXB4O1xuXG5oZWlnaHQ6IDM2LjhweDtcblxuYm9yZGVyLXJhZGl1czogMTBweDtcblxuYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tb3JhbmdlKTtcblxuZm9udC1mYW1pbHk6IE11bGk7XG5cbmZvbnQtc2l6ZTogMTNweDtcblxuZm9udC13ZWlnaHQ6IDkwMDtcblxuZm9udC1zdHlsZTogbm9ybWFsO1xuXG5mb250LXN0cmV0Y2g6IG5vcm1hbDtcblxubGluZS1oZWlnaHQ6IDEuMjM7XG5cbmxldHRlci1zcGFjaW5nOiBub3JtYWw7XG5cbnRleHQtYWxpZ246IGNlbnRlcjtcblxuY29sb3I6IHZhcigtLXdoaXRlKTtcblxufVxuXG5cbi5jYW5jZWxidG57XG53aWR0aDogMTQ1cHg7XG5cbmhlaWdodDogMzYuOHB4O1xuXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xuXG5iYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xuZm9udC1mYW1pbHk6IE11bGk7XG5cbmZvbnQtc2l6ZTogMTNweDtcblxuZm9udC13ZWlnaHQ6IDkwMDtcblxuZm9udC1zdHlsZTogbm9ybWFsO1xuXG5mb250LXN0cmV0Y2g6IG5vcm1hbDtcblxubGluZS1oZWlnaHQ6IDEuMjM7XG5cbmxldHRlci1zcGFjaW5nOiBub3JtYWw7XG5cbnRleHQtYWxpZ246IGNlbnRlcjtcblxuY29sb3I6IGJsYWNrO1xuXG59XG5cblxuLm1vZGFsLWNvbnRlbnR7XG5ib3JkZXI6IDBweDtcbmJveC1zaGFkb3c6IDAgNXB4IDE1cHggcmdiYSgwLDAsMCwuNSk7XG5mb250LXNpemU6MTZweDsgXG59XG5cbi5tb2RhbC1mb290ZXJ7XG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbi8vIHBhZGRpbmc6IDFyZW07XG5ib3JkZXItdG9wOiAwcHg7XG5mb250LXNpemU6MTZweDtcbn1cblxuXG4uZm9ybS1jb250cm9se1xubWFyZ2luLXRvcDo1JTtcbn1cblxuXG5cbi5tb2RhbC10aXRsZXtcbmZvbnQtZmFtaWx5OiBNdWxpO1xuXG5mb250LXNpemU6IDE1cHg7XG5cbmZvbnQtd2VpZ2h0OiA2MDA7XG5cbmZvbnQtc3R5bGU6IG5vcm1hbDtcblxuZm9udC1zdHJldGNoOiBub3JtYWw7XG5cbmxpbmUtaGVpZ2h0OiAxLjI3O1xuXG5sZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG50ZXh0LWFsaWduOiBsZWZ0O1xuXG5jb2xvcjogI2I0YjRiNDsgXG5tYXJnaW4tbGVmdDogMzAlO1xubWFyZ2luLXRvcDogNSU7XG5mb250LXNpemU6MTZweDtcblxuXG59XG5cblxuLm5vdGV7XG5cbmZvbnQtZmFtaWx5OiBNdWxpO1xuXG5mb250LXNpemU6IDEycHg7XG5cbmZvbnQtd2VpZ2h0OiBib2xkO1xuXG5mb250LXN0eWxlOiBpdGFsaWM7XG5cbmZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG5saW5lLWhlaWdodDogMS4zO1xuXG5sZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG4vLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbmNvbG9yOiAjYTBhMGEwO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/wallet/wallet.component.ts":
/*!***************************************************!*\
  !*** ./src/app/layout/wallet/wallet.component.ts ***!
  \***************************************************/
/*! exports provided: WalletComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletComponent", function() { return WalletComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_WindowRef_window_ref_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/WindowRef/window-ref.service */ "./src/app/services/WindowRef/window-ref.service.ts");
/* harmony import */ var _services_wallet_wallet_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/wallet/wallet.service */ "./src/app/services/wallet/wallet.service.ts");




var WalletComponent = /** @class */ (function () {
    function WalletComponent(winRef, walletService) {
        this.winRef = winRef;
        this.walletService = walletService;
        this.display = 'none';
        this.options = {
            "key": "rzp_test_gUZDy2ThhDous7",
            "amount": "200000",
            "name": "Belfrics",
            "description": "Dapp Register",
            "image": "../../lib/img/belbooks-logo.svg",
            "handler": function (response) {
                // $('#infomsg').text("Payment Successful");
                // $('#InfoModal').modal('show');
                // console.log(response.razorpay_payment_id);
                alert("Payment successful");
            },
            "prefill": {
                "name": "Vinodh Kumar",
                "email": "vinodh.p@belfricsbt.com"
            },
            "notes": {
                "address": "Yo!Chick"
            },
            "theme": {
                "color": "#528ff0"
            }
        };
        console.log('Native window obj', winRef.nativeWindow);
    }
    WalletComponent.prototype.ngOnInit = function () {
        // alert("in wallet");
        // console.log("dgyerfuidcio");
    };
    WalletComponent.prototype.openModal = function () {
        this.display = "block";
    };
    WalletComponent.prototype.onCloseHandled = function () {
        this.display = 'none';
    };
    WalletComponent.prototype.registerdapp = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var regResponse;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.register()];
                    case 1:
                        regResponse = _a.sent();
                        console.log(JSON.stringify(regResponse));
                        if (regResponse["isSuccess"] === true) {
                            console.log(regResponse);
                            alert("registeration successful!");
                        }
                        else {
                            alert("registeration unsuccess");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    WalletComponent.prototype.register = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var pp, dapp, desc, com, params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("hey!");
                        pp = this.passphrase;
                        dapp = this.dappname;
                        desc = this.description;
                        com = this.company1;
                        params = {
                            secret: pp,
                            des: desc,
                            // email:localStorage.getItem("email")
                            email: "pr.superuser@yopmail.com",
                            company: com,
                            // country:localStorage.getItem("country")
                            country: "India",
                            name: dapp,
                            assetType: "certificate"
                        };
                        console.log(params);
                        return [4 /*yield*/, this.walletService.onregister(params)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    WalletComponent.prototype.recharge = function () {
        //  console.log("first");
        var role = localStorage.getItem("roleId");
        console.log(role);
        this.rzp1 = new this.winRef.nativeWindow.Razorpay(this.options);
        this.rzp1.open();
        event.preventDefault();
    };
    WalletComponent.prototype.update = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var amount, passphrase, did, params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        amount = this.amount;
                        passphrase = this.Passphrase;
                        did = localStorage.getItem('dappid');
                        params = {
                            secret: passphrase,
                            dappId: did,
                            amount: Number(amount),
                            countryCode: "IN"
                        };
                        return [4 /*yield*/, this.walletService.onupdate(params)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    WalletComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-wallet',
            template: __webpack_require__(/*! ./wallet.component.html */ "./src/app/layout/wallet/wallet.component.html"),
            styles: [__webpack_require__(/*! ./wallet.component.scss */ "./src/app/layout/wallet/wallet.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_WindowRef_window_ref_service__WEBPACK_IMPORTED_MODULE_2__["WindowRefService"], _services_wallet_wallet_service__WEBPACK_IMPORTED_MODULE_3__["WalletService"]])
    ], WalletComponent);
    return WalletComponent;
}());



/***/ }),

/***/ "./src/app/layout/wallet/wallet.module.ts":
/*!************************************************!*\
  !*** ./src/app/layout/wallet/wallet.module.ts ***!
  \************************************************/
/*! exports provided: WalletModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletModule", function() { return WalletModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _wallet_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wallet.component */ "./src/app/layout/wallet/wallet.component.ts");
/* harmony import */ var _wallet_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./wallet-routing.module */ "./src/app/layout/wallet/wallet-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_WindowRef_window_ref_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/WindowRef/window-ref.service */ "./src/app/services/WindowRef/window-ref.service.ts");







var WalletModule = /** @class */ (function () {
    function WalletModule() {
    }
    WalletModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_wallet_component__WEBPACK_IMPORTED_MODULE_3__["WalletComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _wallet_routing_module__WEBPACK_IMPORTED_MODULE_4__["WalletRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"]
            ],
            providers: [_services_WindowRef_window_ref_service__WEBPACK_IMPORTED_MODULE_6__["WindowRefService"]],
        })
    ], WalletModule);
    return WalletModule;
}());



/***/ }),

/***/ "./src/app/services/wallet/wallet.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/wallet/wallet.service.ts ***!
  \***************************************************/
/*! exports provided: WalletService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletService", function() { return WalletService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var src_app_constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/constants */ "./src/app/constants.ts");
/* harmony import */ var _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");











//getDappBalance API is pending. It has to be added in common
var WalletService = /** @class */ (function () {
    function WalletService(http, request) {
        this.http = http;
        this.request = request;
        this.registerDappUrl = src_app_constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOME_URL + 'makeDapp';
        this.createOrderUrl = src_app_constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SERVERURL + 'razorpay/createOrder';
        this.verifyAndCaptureUrl = src_app_constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SERVERURL + 'rechargeWallet';
    }
    WalletService.prototype.registerDapp = function (data) {
        var params = {
            secret: data.passphrase,
            des: data.description,
            email: data.email,
            company: data.company,
            country: data.country,
            name: data.dappname,
            assetType: data.assetType //"payslip"
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', });
        // const headers = this.request.getDappRequestOptions();
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.registerDappUrl, params, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    WalletService.prototype.createOrder = function (amount) {
        var params = {
            amount: amount
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.createOrderUrl, params, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    WalletService.prototype.verifyAndCapture = function (ord_id, payment_id, signature, walletAddress, countryCode, amount) {
        var params = {
            ord_id: ord_id,
            payment_id: payment_id,
            signature: signature,
            address: walletAddress,
            countryCode: countryCode,
            amount: amount
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.verifyAndCaptureUrl, params, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    WalletService.prototype.onupdate = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'get' });
                        return [4 /*yield*/, this.http.get("http://node1.belrium.io/api/dapps/transaction", options).timeout(300000).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    WalletService.prototype.onregister = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var headers, options, resp;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("third");
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3', });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.registerDappUrl, data).toPromise()];
                    case 1:
                        resp = _a.sent();
                        return [2 /*return*/, resp];
                }
            });
        });
    };
    WalletService.prototype.handleError = function (error) {
        console.error(error);
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().message);
    };
    WalletService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__["RequestOptionsService"]])
    ], WalletService);
    return WalletService;
}());



/***/ })

}]);
//# sourceMappingURL=wallet-wallet-module.js.map