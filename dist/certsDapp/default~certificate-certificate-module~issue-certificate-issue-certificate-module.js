(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~certificate-certificate-module~issue-certificate-issue-certificate-module"],{

/***/ "./src/app/services/certificate/certificate.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/certificate/certificate.service.ts ***!
  \*************************************************************/
/*! exports provided: CertificateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CertificateService", function() { return CertificateService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");











var CertificateService = /** @class */ (function () {
    function CertificateService(http, requestOptions) {
        this.http = http;
        this.requestOptions = requestOptions;
        this.dappid = localStorage.getItem('dappid');
        this.PayslipmonthStatsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/employee/payslip/month/status";
        this.EmployeeDataUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/employeeData";
        this.payslipstatisticUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/payslip/statistic";
        this.PayslipInitialissueUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/payslip/initialIssue";
        this.DepartmentUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/department/get";
        this.EmpDesignationsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/employees/getDesignations";
        this.getbanksUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/getBanks";
        this.registeremployeeUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/registerEmployee";
        this.issueTransactionUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/issueTransactionCall";
        this.customfieldsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/customFields/get";
        this.EmployeeDesignationsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/employees/getDesignations";
        this.assetDataUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + "/department/assets";
        this.getIssuersUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/issuers';
        this.getAuthorizersUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/authorizers';
    }
    CertificateService.prototype.getBlockchaindata = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.PayslipmonthStatsUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.getEmployeeDetails = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.EmployeeDataUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.payslipstatistic = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.payslipstatisticUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.PayslipInitialissue = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.PayslipInitialissueUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CertificateService.prototype.getIssuers = function (limit, offset, filterdep) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            limit: limit,
                            offset: offset,
                            department: filterdep
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getIssuersUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        console.log("issuedata", res.json());
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CertificateService.prototype.getAuthorizers = function (limit, offset, filterdep) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            limit: limit,
                            offset: offset,
                            department: filterdep
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getAuthorizersUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        console.log("authdata", res.json());
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CertificateService.prototype.getCertifcates = function (certSearchData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("searchCertdata" + JSON.stringify(certSearchData));
                        data = {
                            "did": certSearchData['did'],
                            // "limit": certSearchData['limit'],    
                            // "offset": certSearchData['offset'],    
                            "iid": certSearchData['iid'],
                            "status": certSearchData['status'],
                            "year": certSearchData['year'],
                            "month": certSearchData['month']
                        };
                        console.log("asseturl" + this.assetDataUrl);
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.assetDataUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        console.log("certdata", res.json());
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CertificateService.prototype.Department = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            "limit": 5,
                            "offset": 0
                        };
                        console.log("depurl" + this.DepartmentUrl);
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.DepartmentUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        console.log("depdata" + res);
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CertificateService.prototype.EmpDesignations = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.EmpDesignationsUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.getbanks = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.getbanksUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.registeremployee = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.registeremployeeUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CertificateService.prototype.issueTransaction = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.issueTransactionUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.customfields = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.customfieldsUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.EmployeeDesignations = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.EmployeeDesignationsUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    CertificateService.prototype.handleError = function (error) {
        console.error(error);
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().message);
    };
    CertificateService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__["RequestOptionsService"]])
    ], CertificateService);
    return CertificateService;
}());



/***/ })

}]);
//# sourceMappingURL=default~certificate-certificate-module~issue-certificate-issue-certificate-module.js.map