(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["authorizer-authorizer-module"],{

/***/ "./src/app/layout/authorizer/authorizer-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/authorizer/authorizer-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: AuthorizerRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizerRoutingModule", function() { return AuthorizerRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _authorizer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./authorizer.component */ "./src/app/layout/authorizer/authorizer.component.ts");




var routes = [{
        path: '', component: _authorizer_component__WEBPACK_IMPORTED_MODULE_3__["AuthorizerComponent"]
    }
];
var AuthorizerRoutingModule = /** @class */ (function () {
    function AuthorizerRoutingModule() {
    }
    AuthorizerRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AuthorizerRoutingModule);
    return AuthorizerRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/authorizer/authorizer.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/authorizer/authorizer.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"container\" style=\"font-size:1.5rem !important\">\n      <div class=\"container-fluid\" id=\"Rectangle-820\">\n              <div id=\"div1\" [ngClass]=\"activeFlag===1?'active':'inactive'\" (click)=getPendingAuthCerts()>  \n                  <img src=\"../../../assets/images/icons8-rubber-stamp-64-2.png\" style=\"float:left;padding: 20px\" id=\"Ellipse-yellow\">\n                  <h2 style=\"font-size:5rem\">{{pending_authorization_count}}</h2>\n                  <p>PENDING AUTHORIZATION</p>\n              </div>\n          \n              <div id=\"div1\" [ngClass]=\"activeFlag===2?'active':'inactive'\" (click)=getRejectedCerts()>\n                  <img src=\"../../../assets/images/icons8-file-delete-64.png\" style=\"float:left;padding: 20px\" id=\"Ellipse-pink\">   \n                  <h2 style=\"font-size:5rem\">{{rejected_count}}</h2> \n                  <p>CERTIFICATES REJECTED</p>\n              </div>\n          \n              <div id=\"div1\" [ngClass]=\"activeFlag===3?'active':'inactive'\" (click)=getAuthorizedCerts()>\n                  <img src=\"../../../assets/images/Mask Group 6.png\" style=\"float:left;padding: 25px\" id=\"Ellipse-green\">   \n                  <h2 style=\"font-size:5rem\">{{signCount}}</h2> \n                  <p>CERTIFICATES AUTHORIZED</p>\n              </div>   \n\n              <div *ngIf=\"display_zero_pending_auths\">\n                <p class=\"ZeropendingCertsText\">Congrats! you have no pending certificates</p><br>                \n                <div class=\"container\">                    \n                    <img src=\"../../../assets/images/DoneIllus@4x.png\" style=\"padding: 0px 100px 20px 350px;\"/>\n                </div>\n              </div>\n\n              <div *ngIf=\"display_pending_authorizations\">      \n                  <h2>PENDING AUTHORIZATION</h2>                                       \n                  <div class=\"flex-container\" data-toggle=\"modal\" data-target=\"#dappbal\" *ngFor=\"let cert of totalPendingAuths\">                    \n                      <div>Department: {{cert.departmentName}}</div>\n                      <div>id: {{cert.empid}}</div>\n                      <div>issuerid:{{cert.iid}}</div> \n                      <div>Student Name: {{cert.receipientName}}</div>\n                      <div>Time: {{cert.timestampp | date: 'medium'}}</div>\n                      <ol class=\"wizard-progress clearfix\">\n                          <li class=\"active-step\">            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">\n                             initiated\n                            </span>\n                          </li>\n                          <li class=\"active-stepAuth1\">            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">Auth1</span>\n                          </li>\n                          <li class=\"active-stepAuth1\">            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">Auth2</span>\n                          </li>\n                          <li class=\"active-stepAuth1\">            \n                           <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                           <span class=\"step-name\">Auth3</span>\n                         </li>\n                          <li>            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">Issuer</span>\n                          </li>\n                        </ol>                    \n                         <div><input type=\"button\" value=\"Sign\" (click)=\"getCertDetails(cert.pid)\" class=\"viewtable\"></div>\n                    </div>                  \n                </div>\n\n              <div *ngIf=\"display_rejected\">   \n                  <h2>CERTIFICATES REJECTED</h2>                             \n                    <div class=\"flex-container\" *ngFor=\"let cert of totalRejectedAuths\">\n                      <div>Department: {{cert.departmentName}}</div>\n                      <div>id: {{cert.empid}}</div>\n                      <div>issuerid:{{cert.iid}}</div> \n                      <div>Student Name: {{cert.receipientName}}</div>\n                      <div>Time: {{cert.timestampp | date: 'medium'}}</div>\n                      <ol class=\"wizard-progress clearfix\">\n                          <li class=\"active-step\">            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">\n                             initiated\n                            </span>\n                          </li>\n                          <li class=\"active-stepAuth1\">            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">Auth1</span>\n                          </li>\n                          <li class=\"active-stepAuth1\">            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">Auth2</span>\n                          </li>\n                          <li class=\"active-stepAuth1\">            \n                           <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                           <span class=\"step-name\">Auth3</span>\n                         </li>\n                          <li>            \n                            <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                            <span class=\"step-name\">Issuer</span>\n                          </li>\n                        </ol>\n                       \n                        \n                         <div><input type=\"button\" value=\"issue\" class=\"viewtable\"></div>\n                    </div>\n                 \n                  <div class=\"container\">\n                      <input type=\"button\" value=\"RE-ISSUE CERTIFICATE\" style=\"font-size:16px; color:#ffffff\" class=\"issuecertbtn\">\n                  </div>              \n                  </div>                                                                                               \n              \n                <div *ngIf=\"display_authorized\">     \n                    <h2>CERTIFICATES AUTHORIZED</h2>       \n                  <div class=\"col-md-3 nopadding\" style=\"margin-left:70%;\">\n                     \n                    </div>\n                  <div class=\"flex-container\" *ngFor=\"let cert of totalSignedAuths\">\n                    <div>Department: {{cert.departmentName}}</div>\n                    <div>id: {{cert.empid}}</div>\n                    <div>issuerid:{{cert.iid}}</div> \n                    <div>Student Name: {{cert.receipientName}}</div>\n                    <div>Time: {{cert.timestampp | date: 'medium'}}</div>\n                    <ol class=\"wizard-progress clearfix\">\n                        <li class=\"active-step\">            \n                          <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                          <span class=\"step-name\">\n                           initiated\n                          </span>\n                        </li>\n                        <li class=\"active-stepAuth1\">            \n                          <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                          <span class=\"step-name\">Auth1</span>\n                        </li>\n                        <li class=\"active-stepAuth1\">            \n                          <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                          <span class=\"step-name\">Auth2</span>\n                        </li>\n                        <li class=\"active-stepAuth1\">            \n                         <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                         <span class=\"step-name\">Auth3</span>\n                       </li>\n                        <li>            \n                          <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                          <span class=\"step-name\">Issuer</span>\n                        </li>\n                      </ol>                                                                \n                  </div>                                            \n              </div>                  \n</div>\n</div>\n\n    <div class=\"modal fade\" id=\"dappbal\" role=\"dialog\">\n      <div class=\"modal-dialog\">\n        <!-- Modal content-->\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                \n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n            </div>\n             <div class=\"modal-body\">\n                  <h3 style=\"text-align:center\">Do you wish to authorize the certificate?</h3>\n                  <img src=\"../../../assets/images/CertificateDummy.png\" style=\"margin-left:100px;height:300px;width:240px;\">\n                  <div class=\"form-group col-md-12\">                  \n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"comments\" placeholder=\"Comments\">\n                    <br>                    \n                    <div *ngIf=\"rejectFlag\">\n                    <form class=\"form-inline\">\n                        <select class=\"form-control\" style=\"width:100%; height:10%;font-size:20px;\" [(ngModel)]=\"reason\">\n                            <option selected>Select a reason for rejection</option>\n                            <option style=\"color:orange\">Insufficient Credits</option>\n                            <option>Attendance</option>\n                            <option>Feed Due</option>  \n                            <option>other</option>\n                        </select>\n                    </form>\n                  </div>\n                  </div>            \n            </div>\n            <div class=\"modal-footer\">\n                <button class=\"submitbtn\" data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#passphrase\">AUTHORIZE</button>\n                <button class=\"cancelbtn\" (click)=rejectionFlag()>REJECT</button>\n            </div>\n          </div>          \n        </div>\n      </div>\n\n      <!--Passphrase popup-->\n     <div class=\"modal fade\" id=\"passphrase\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n          <!-- Modal content-->\n          <div class=\"modal-content\">\n              <div class=\"modal-header\">\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n              </div>\n               <div class=\"modal-body\">\n                  <div class=\"form-group col-md-12\">\n                      <input type=\"password\" class=\"form-control\" [(ngModel)]=\"passphrase\" placeholder=\"Passphrase\">                     \n                </div>              \n              </div>\n              <div class=\"modal-footer\">\n                  <button class=\"submitbtn\" data-dismiss=\"modal\" (click)=\"authorizeCertificate(passphrase)\">DONE</button>\n                  <button class=\"cancelbtn\"  data-dismiss=\"modal\">CANCEL</button>\n              </div>\n            </div>\n          </div>\n          </div>\n      \n"

/***/ }),

/***/ "./src/app/layout/authorizer/authorizer.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/authorizer/authorizer.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ZeropendingCertsText {\n  width: 500px;\n  height: 20px;\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  margin-left: 400px;\n  margin-top: 300px;\n  color: #969696; }\n\n.active {\n  width: 322px;\n  height: 142px;\n  box-shadow: 3px 5px 10px 0 rgba(0, 0, 0, 0.12);\n  background-color: var(--white); }\n\n.inactive {\n  width: 322px;\n  height: 142px;\n  box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.1);\n  background-color: var(--white); }\n\n#Ellipse-yellow {\n  width: 85px;\n  height: 85px;\n  background-color: #ffc221;\n  border-radius: 50%; }\n\n#Ellipse-green {\n  width: 85px;\n  height: 85px;\n  background-color: #58cb5a;\n  border-radius: 50%; }\n\n#Ellipse-pink {\n  width: 85px;\n  height: 85px;\n  background-color: #ff7791;\n  border-radius: 50%; }\n\n#Rectangle-864 {\n  width: 1068px;\n  height: 20px;\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1);\n  background-color: #ffffff;\n  padding: 0px;\n  margin: 2px; }\n\ntable tr td {\n  text-align: center; }\n\n.flex-container {\n  display: flex;\n  width: 1280px;\n  height: 75px;\n  text-align: center;\n  border: 1px #ccc solid;\n  background: #ffffff;\n  padding: 10px;\n  margin: 25px;\n  border-radius: 10px;\n  line-height: 10px;\n  float: left;\n  margin-right: 40px;\n  font-size: 15px; }\n\n.flex-container > div {\n  margin: 10px;\n  padding: 10px;\n  font-size: 15px; }\n\n.issuecertbtn {\n  width: 200px;\n  height: 45px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  border: 2px solid orange;\n  margin-left: 40%; }\n\n#Rectangle-820 {\n  width: 1144px;\n  height: 768px;\n  border-radius: 5px;\n  background-color: var(--white-grey); }\n\n#div1 {\n  width: 300px;\n  height: 150px;\n  text-align: center;\n  border: 1px #ccc solid;\n  background: #ffffff;\n  padding: 25px;\n  margin: 25px;\n  border-radius: 10px;\n  font-size: 14px;\n  float: left;\n  margin-right: 40px; }\n\n#div1 h2 {\n  font-size: large; }\n\n.div11 td {\n  width: 25%; }\n\n.div11 td span {\n  float: left; }\n\n.modal-dialog {\n  max-width: 25%;\n  margin-top: 20%;\n  margin-left: 45%;\n  font-size: 16px; }\n\n.modal-content {\n  border-radius: 20px;\n  font-size: 16px; }\n\n.form-control {\n  font-size: 16px; }\n\n.submitbtn {\n  width: 185px;\n  height: 45px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  color: #ffffff;\n  border: 1px solid orange;\n  font-size: 13px; }\n\n.cancelbtn {\n  width: 185px;\n  height: 45px;\n  border-radius: 10px;\n  border: solid 1px #d8d8d8;\n  background-color: var(--white);\n  font-size: 13px; }\n\n.viewtable {\n  width: 100px;\n  height: 30px;\n  background: orange;\n  color: #ffffff;\n  border-radius: 5px;\n  border: 1px solid orange; }\n\n.dot-verified-circle {\n  height: 10px;\n  width: 10px;\n  background-color: green;\n  border-radius: 50%;\n  position: absolute;\n  margin-top: 7px; }\n\n.dot-unverified-circle {\n  height: 10px;\n  width: 10px;\n  background-color: red;\n  border-radius: 50%;\n  position: absolute;\n  margin-top: 7px; }\n\n.progressbaractive {\n  position: relative;\n  box-shadow: none;\n  margin: 0px -2px;\n  bottom: -10px;\n  left: 3px;\n  box-shadow: none;\n  background: red;\n  width: 10px; }\n\n.visuallyhidden {\n  display: none; }\n\n.wizard-progress {\n  list-style: none;\n  list-style-image: none;\n  margin: 0;\n  padding: 0;\n  margin-top: 0px;\n  float: left;\n  white-space: nowrap; }\n\n.wizard-progress li {\n  float: left;\n  margin-right: 47px;\n  text-align: center;\n  position: relative;\n  width: 12px; }\n\n.wizard-progress .step-name {\n  display: table-cell;\n  height: 15px;\n  vertical-align: bottom;\n  text-align: center;\n  width: 100px; }\n\n.wizard-progress .step-name-wrapper {\n  display: table-cell;\n  height: 100%;\n  vertical-align: bottom; }\n\n.wizard-progress .step-num {\n  font-size: 14px;\n  font-weight: bold;\n  border: 2px solid  #eaeaea;\n  background-color: #eaeaea !important;\n  border-radius: 50%;\n  width: 20px !important;\n  height: 20px !important;\n  display: inline-block;\n  margin-top: 10px !important; }\n\n.wizard-progress .step-num:after {\n  content: \"\";\n  display: block;\n  background: #ededed !important;\n  height: 2px;\n  width: 40px;\n  position: relative;\n  top: -3px !important;\n  bottom: 5px;\n  left: 18px !important; }\n\n.wizard-progress li:last-of-type .step-num:after {\n  display: none; }\n\n.wizard-progress .active-stepAuth1 .step-num {\n  background-color: yellow !important;\n  border-color: yellow !important; }\n\n.wizard-progress .active-step .step-num {\n  background-color: #4879ff !important;\n  border-color: #b9ccff !important; }\n\n.wizard-progress .active-stepIssued .step-num {\n  background-color: #58cb5a !important;\n  border-color: #b1ffb2 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2F1dGhvcml6ZXIvYXV0aG9yaXplci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUF3TUE7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7O0FBRWhCO0VBQ0EsWUFBWTtFQUNaLGFBQWE7RUFDYiw4Q0FBOEM7RUFDOUMsOEJBQThCLEVBQUE7O0FBRTlCO0VBQ0EsWUFBWTtFQUNaLGFBQWE7RUFDYix3Q0FBd0M7RUFDeEMsOEJBQThCLEVBQUE7O0FBRTlCO0VBQ0EsV0FBVztFQUNYLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsa0JBQWtCLEVBQUE7O0FBRWxCO0VBQ0EsV0FBVztFQUNYLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsa0JBQWtCLEVBQUE7O0FBRWxCO0VBQ0EsV0FBVztFQUNYLFlBQVk7RUFDWix5QkFBMEI7RUFDMUIsa0JBQWtCLEVBQUE7O0FBRWxCO0VBQ0EsYUFBYTtFQUNiLFlBQVk7RUFDWiwwQ0FBMEM7RUFDMUMseUJBQXdCO0VBQ3hCLFlBQVk7RUFDWixXQUFXLEVBQUE7O0FBRVg7RUFDRSxrQkFBa0IsRUFBQTs7QUF3Q3BCO0VBQ0EsYUFBYTtFQUNiLGFBQWE7RUFDYixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUVmO0VBQ0EsWUFBWTtFQUNaLGFBQWE7RUFDYixlQUFlLEVBQUE7O0FBRWY7RUFDSSxZQUFZO0VBQ1osWUFBWTtFQUNaLG1CQUFtQjtFQUNuQiwrQkFBK0I7RUFDL0Isd0JBQXdCO0VBQ3hCLGdCQUFnQixFQUFBOztBQUVwQjtFQUNBLGFBQWE7RUFDYixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLG1DQUFtQyxFQUFBOztBQUVuQztFQUNBLFlBQVk7RUFDWixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLFdBQVc7RUFDWCxrQkFBa0IsRUFBQTs7QUFFbEI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFFaEI7RUFDQSxVQUFVLEVBQUE7O0FBRVY7RUFDQSxXQUFXLEVBQUE7O0FBRVg7RUFDQSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFjLEVBQUE7O0FBRWQ7RUFDQSxtQkFBbUI7RUFDbkIsZUFBYyxFQUFBOztBQUVkO0VBQ0UsZUFBYyxFQUFBOztBQUVoQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLCtCQUErQjtFQUMvQixjQUFhO0VBQ2Isd0JBQXdCO0VBQ3hCLGVBQWUsRUFBQTs7QUFFZjtFQUNBLFlBQVk7RUFDWixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6Qiw4QkFBOEI7RUFDOUIsZUFBZSxFQUFBOztBQUVmO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQix3QkFBd0IsRUFBQTs7QUFFekI7RUFDQyxZQUFZO0VBQ1osV0FBVztFQUNYLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxZQUFZO0VBQ1osV0FBVztFQUNYLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFFakI7RUFDRSxrQkFBa0I7RUFFaEIsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsU0FBUztFQUNULGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsV0FBVyxFQUFBOztBQUtmO0VBQ0UsYUFBYSxFQUFBOztBQUdmO0VBQ0UsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtFQUN0QixTQUFTO0VBQ1QsVUFBVTtFQUNWLGVBQWU7RUFDZixXQUFXO0VBQ1gsbUJBQW1CLEVBQUE7O0FBSXJCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHYjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixZQUFZLEVBQUE7O0FBSWQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsMEJBQTBCO0VBQzFCLG9DQUFvQztFQUdwQyxrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsMkJBQTJCLEVBQUE7O0FBRzdCO0VBQ0UsV0FBVztFQUNYLGNBQWM7RUFDZCw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsb0JBQW1CO0VBQ25CLFdBQVc7RUFDWCxxQkFBcUIsRUFBQTs7QUFHdkI7RUFDRSxhQUFhLEVBQUE7O0FBSWY7RUFDRSxtQ0FBbUM7RUFDbkMsK0JBQStCLEVBQUE7O0FBRWpDO0VBQ0Usb0NBQW9DO0VBQ3BDLGdDQUFnQyxFQUFBOztBQUdsQztFQUNFLG9DQUFvQztFQUNwQyxnQ0FBZ0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9hdXRob3JpemVyL2F1dGhvcml6ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAuWmVyb3BlbmRpbmdDZXJ0c1RleHQge1xuLy8gICAgIHdpZHRoOiA1MDBweDtcbi8vICAgICBoZWlnaHQ6IDIwcHg7XG4vLyAgICAgZm9udC1mYW1pbHk6IE11bGk7XG4vLyAgICAgZm9udC1zaXplOiAxNnB4O1xuLy8gICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4vLyAgICAgZm9udC1zdHlsZTogbm9ybWFsO1xuLy8gICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuLy8gICAgIGxpbmUtaGVpZ2h0OiAxLjI1O1xuLy8gICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4vLyAgICAgdGV4dC1hbGlnbjogbGVmdDtcbi8vICAgICBtYXJnaW4tbGVmdDogNDAwcHg7XG4vLyAgICAgbWFyZ2luLXRvcDogMzAwcHg7XG4vLyAgICAgY29sb3I6ICM5Njk2OTY7XG4vLyB9XG5cbi8vIC5hY3RpdmUge1xuLy8gICB3aWR0aDogMzIycHg7XG4vLyAgIGhlaWdodDogMTQycHg7XG4vLyAgIGJveC1zaGFkb3c6IDNweCA1cHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlKTtcbi8vIH1cblxuLy8gLmluYWN0aXZlIHtcbi8vICAgd2lkdGg6IDMyMnB4O1xuLy8gICBoZWlnaHQ6IDE0MnB4O1xuLy8gICBib3gtc2hhZG93OiAwIDAgM3B4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG4vLyB9XG5cbi8vICNFbGxpcHNlLXllbGxvdyB7XG4vLyAgIHdpZHRoOiA4NXB4O1xuLy8gICBoZWlnaHQ6IDg1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6ICNmZmMyMjE7XG4vLyAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbi8vIH1cblxuLy8gI0VsbGlwc2UtZ3JlZW4ge1xuLy8gICB3aWR0aDogODVweDtcbi8vICAgaGVpZ2h0OiA4NXB4O1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAjNThjYjVhO1xuLy8gICBib3JkZXItcmFkaXVzOiA1MCU7XG4vLyB9XG5cbi8vICNFbGxpcHNlLXBpbmsge1xuLy8gICB3aWR0aDogODVweDtcbi8vICAgaGVpZ2h0OiA4NXB4O1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiAgI2ZmNzc5MTtcbi8vICAgYm9yZGVyLXJhZGl1czogNTAlO1xuLy8gfVxuXG4vLyAjUmVjdGFuZ2xlLTg2NCB7XG4vLyAgIHdpZHRoOiAxMDY4cHg7XG4vLyAgIGhlaWdodDogMjBweDtcbi8vICAgYm94LXNoYWRvdzogMCAxcHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmZmZmY7XG4vLyAgIHBhZGRpbmc6IDBweDtcbi8vICAgbWFyZ2luOiAycHg7XG4vLyB9XG5cbi8vIHRhYmxlIHRyIHRke1xuLy8gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vIH1cbi8vIC5jaXJjbGUxIHtcbi8vICAgd2lkdGg6IDI1cHg7XG4vLyAgIGhlaWdodDogMjVweDtcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4NzlmZjtcbi8vIH1cbi8vIC5jaXJjbGUyIHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbi8vIH1cbi8vIC5jaXJjbGUzIHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbi8vIH1cbi8vIC5jaXJjbGU0IHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbi8vIH1cbi8vIC5jaXJjbGU1IHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuLy8gICBib3JkZXItcmFkaXVzOjUwJTtcbi8vIH1cbi8vIC5jaXJjbGU2IHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6I2VhZWFlYTtcbi8vICAgYm9yZGVyLXJhZGl1czo1MCU7XG4vLyB9XG4vLyAuY2lyY2xlNyB7XG4vLyAgICAgd2lkdGg6IDI1cHg7XG4vLyAgICAgaGVpZ2h0OiAyNXB4O1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4vLyAgIGJvcmRlci1yYWRpdXM6NTAlO1xuLy8gfVxuXG4vLyAuZmxleC1jb250YWluZXIge1xuLy8gICBkaXNwbGF5OiBmbGV4O1xuLy8gICB3aWR0aDogMTA2OHB4O1xuLy8gICBoZWlnaHQ6IDc1cHg7O1xuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyAgIGJvcmRlcjogMXB4ICNjY2Mgc29saWQ7XG4vLyAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4vLyAgIHBhZGRpbmc6IDEwcHg7XG4vLyAgIG1hcmdpbjogMjVweDtcbi8vICAgYm9yZGVyLXJhZGl1czogMTBweDtcbi8vICAgbGluZS1oZWlnaHQ6IDEwcHg7XG4vLyAgIGZsb2F0OiBsZWZ0O1xuLy8gICBtYXJnaW4tcmlnaHQ6IDQwcHg7XG4vLyAgIGZvbnQtc2l6ZTogMTVweDtcbi8vIH1cblxuLy8gLmZsZXgtY29udGFpbmVyID4gZGl2IHtcbi8vICAgbWFyZ2luOiAxMHB4O1xuLy8gICBwYWRkaW5nOiAxMHB4O1xuLy8gICBmb250LXNpemU6IDE1cHg7XG4vLyB9XG4vLyAuaXNzdWVjZXJ0YnRuIHtcbi8vICAgICAgIHdpZHRoOiAyMDBweDtcbi8vICAgICAgIGhlaWdodDogNDVweDtcbi8vICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4vLyAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1vcmFuZ2UpO1xuLy8gICAgICAgYm9yZGVyOiAycHggc29saWQgb3JhbmdlO1xuLy8gICAgICAgbWFyZ2luLWxlZnQ6IDQwJTtcbi8vIH1cblxuLy8gI1JlY3RhbmdsZS04MjAge1xuLy8gICB3aWR0aDogMTE0NHB4O1xuLy8gICBoZWlnaHQ6IDc2OHB4O1xuLy8gICBib3JkZXItcmFkaXVzOiA1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlLWdyZXkpO1xuLy8gfVxuLy8gI2RpdjEge1xuLy8gICB3aWR0aDogMzAwcHg7XG4vLyAgIGhlaWdodDogMTUwcHg7XG4vLyAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgYm9yZGVyOiAxcHggI2NjYyBzb2xpZDtcbi8vICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbi8vICAgcGFkZGluZzogMjVweDtcbi8vICAgbWFyZ2luOiAyNXB4O1xuLy8gICBib3JkZXItcmFkaXVzOiAxMHB4O1xuLy8gICBmb250LXNpemU6IDE0cHg7XG5cbi8vICAgZmxvYXQ6IGxlZnQ7XG4vLyAgIG1hcmdpbi1yaWdodDogNDBweDtcbi8vIH1cbi8vICNkaXYxIGgye1xuLy8gZm9udC1zaXplOiBsYXJnZTtcbi8vIH1cblxuXG4vLyAuZGl2MTEgdGR7XG4vLyB3aWR0aDogMjUlO1xuLy8gfVxuLy8gLmRpdjExIHRkIHNwYW57XG4vLyBmbG9hdDogbGVmdDtcbi8vIH1cblxuLy8gLm1vZGFsLWRpYWxvZyB7XG4vLyAgIG1heC13aWR0aDogMjUlO1xuLy8gICBtYXJnaW4tdG9wOiAyMCU7XG4vLyAgIG1hcmdpbi1sZWZ0OiA0NSU7XG4vLyAgIH1cblxuLy8gICAubW9kYWwtY29udGVudHtcbi8vICAgYm9yZGVyLXJhZGl1czogMjBweDtcbi8vICAgfVxuXG4vLyAgIC5zdWJtaXRidG4ge1xuLy8gICAgIHdpZHRoOiAxODVweDtcbi8vICAgICBoZWlnaHQ6IDQ1cHg7XG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1vcmFuZ2UpO1xuLy8gICAgIGNvbG9yOiNmZmZmZmY7XG4vLyAgICAgYm9yZGVyOiAxcHggc29saWQgb3JhbmdlO1xuLy8gICAgIGZvbnQtc2l6ZTogMTNweDtcbi8vICAgICB9XG4vLyAgICAgLmNhbmNlbGJ0bntcbi8vICAgICB3aWR0aDogMTg1cHg7XG4vLyAgICAgaGVpZ2h0OiA0NXB4O1xuLy8gICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4vLyAgICAgYm9yZGVyOiBzb2xpZCAxcHggI2Q4ZDhkODtcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG4vLyAgICAgZm9udC1zaXplOiAxM3B4O1xuLy8gICAgIH1cbi8vICAgICAudmlld3RhYmxle1xuLy8gICAgICAgd2lkdGg6IDEwMHB4O1xuLy8gICAgICAgaGVpZ2h0OiAzMHB4O1xuLy8gICAgICAgYmFja2dyb3VuZDogb3JhbmdlO1xuLy8gICAgICAgY29sb3I6ICNmZmZmZmY7XG4vLyAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4vLyAgICAgICBib3JkZXI6IDFweCBzb2xpZCBvcmFuZ2U7XG4vLyAgICAgIH1cblxuLlplcm9wZW5kaW5nQ2VydHNUZXh0IHtcbiAgd2lkdGg6IDUwMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBtYXJnaW4tbGVmdDogNDAwcHg7XG4gIG1hcmdpbi10b3A6IDMwMHB4O1xuICBjb2xvcjogIzk2OTY5Njtcbn1cbi5hY3RpdmUge1xud2lkdGg6IDMyMnB4O1xuaGVpZ2h0OiAxNDJweDtcbmJveC1zaGFkb3c6IDNweCA1cHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XG5iYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG59XG4uaW5hY3RpdmUge1xud2lkdGg6IDMyMnB4O1xuaGVpZ2h0OiAxNDJweDtcbmJveC1zaGFkb3c6IDAgMCAzcHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG5iYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG59XG4jRWxsaXBzZS15ZWxsb3cge1xud2lkdGg6IDg1cHg7XG5oZWlnaHQ6IDg1cHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjZmZjMjIxO1xuYm9yZGVyLXJhZGl1czogNTAlO1xufVxuI0VsbGlwc2UtZ3JlZW4ge1xud2lkdGg6IDg1cHg7XG5oZWlnaHQ6IDg1cHg7XG5iYWNrZ3JvdW5kLWNvbG9yOiAjNThjYjVhO1xuYm9yZGVyLXJhZGl1czogNTAlO1xufVxuI0VsbGlwc2UtcGluayB7XG53aWR0aDogODVweDtcbmhlaWdodDogODVweDtcbmJhY2tncm91bmQtY29sb3I6ICAjZmY3NzkxO1xuYm9yZGVyLXJhZGl1czogNTAlO1xufVxuI1JlY3RhbmdsZS04NjQge1xud2lkdGg6IDEwNjhweDtcbmhlaWdodDogMjBweDtcbmJveC1zaGFkb3c6IDAgMXB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbmJhY2tncm91bmQtY29sb3I6I2ZmZmZmZjtcbnBhZGRpbmc6IDBweDtcbm1hcmdpbjogMnB4O1xufVxudGFibGUgdHIgdGR7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi8vIC5jaXJjbGUxIHtcbi8vICAgd2lkdGg6IDI1cHg7XG4vLyAgIGhlaWdodDogMjVweDtcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4NzlmZjtcbi8vIH1cbi8vIC5jaXJjbGUyIHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbi8vIH1cbi8vIC5jaXJjbGUzIHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbi8vIH1cbi8vIC5jaXJjbGU0IHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbi8vIH1cbi8vIC5jaXJjbGU1IHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xuLy8gICBib3JkZXItcmFkaXVzOjUwJTtcbi8vIH1cbi8vIC5jaXJjbGU2IHtcbi8vICAgICB3aWR0aDogMjVweDtcbi8vICAgICBoZWlnaHQ6IDI1cHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6I2VhZWFlYTtcbi8vICAgYm9yZGVyLXJhZGl1czo1MCU7XG4vLyB9XG4vLyAuY2lyY2xlNyB7XG4vLyAgICAgd2lkdGg6IDI1cHg7XG4vLyAgICAgaGVpZ2h0OiAyNXB4O1xuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4vLyAgIGJvcmRlci1yYWRpdXM6NTAlO1xuLy8gfVxuLmZsZXgtY29udGFpbmVyIHtcbmRpc3BsYXk6IGZsZXg7XG53aWR0aDogMTI4MHB4O1xuaGVpZ2h0OiA3NXB4OztcbnRleHQtYWxpZ246IGNlbnRlcjtcbmJvcmRlcjogMXB4ICNjY2Mgc29saWQ7XG5iYWNrZ3JvdW5kOiAjZmZmZmZmO1xucGFkZGluZzogMTBweDtcbm1hcmdpbjogMjVweDtcbmJvcmRlci1yYWRpdXM6IDEwcHg7XG5saW5lLWhlaWdodDogMTBweDtcbmZsb2F0OiBsZWZ0O1xubWFyZ2luLXJpZ2h0OiA0MHB4O1xuZm9udC1zaXplOiAxNXB4O1xufVxuLmZsZXgtY29udGFpbmVyID4gZGl2IHtcbm1hcmdpbjogMTBweDtcbnBhZGRpbmc6IDEwcHg7XG5mb250LXNpemU6IDE1cHg7XG59XG4uaXNzdWVjZXJ0YnRuIHtcbiAgICB3aWR0aDogMjAwcHg7XG4gICAgaGVpZ2h0OiA0NXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tb3JhbmdlKTtcbiAgICBib3JkZXI6IDJweCBzb2xpZCBvcmFuZ2U7XG4gICAgbWFyZ2luLWxlZnQ6IDQwJTtcbn1cbiNSZWN0YW5nbGUtODIwIHtcbndpZHRoOiAxMTQ0cHg7XG5oZWlnaHQ6IDc2OHB4O1xuYm9yZGVyLXJhZGl1czogNXB4O1xuYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUtZ3JleSk7XG59XG4jZGl2MSB7XG53aWR0aDogMzAwcHg7XG5oZWlnaHQ6IDE1MHB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xuYm9yZGVyOiAxcHggI2NjYyBzb2xpZDtcbmJhY2tncm91bmQ6ICNmZmZmZmY7XG5wYWRkaW5nOiAyNXB4O1xubWFyZ2luOiAyNXB4O1xuYm9yZGVyLXJhZGl1czogMTBweDtcbmZvbnQtc2l6ZTogMTRweDtcbmZsb2F0OiBsZWZ0O1xubWFyZ2luLXJpZ2h0OiA0MHB4O1xufVxuI2RpdjEgaDJ7XG5mb250LXNpemU6IGxhcmdlO1xufVxuLmRpdjExIHRke1xud2lkdGg6IDI1JTtcbn1cbi5kaXYxMSB0ZCBzcGFue1xuZmxvYXQ6IGxlZnQ7XG59XG4ubW9kYWwtZGlhbG9nIHtcbm1heC13aWR0aDogMjUlO1xubWFyZ2luLXRvcDogMjAlO1xubWFyZ2luLWxlZnQ6IDQ1JTtcbmZvbnQtc2l6ZToxNnB4O1xufVxuLm1vZGFsLWNvbnRlbnR7XG5ib3JkZXItcmFkaXVzOiAyMHB4O1xuZm9udC1zaXplOjE2cHg7XG59XG4uZm9ybS1jb250cm9se1xuICBmb250LXNpemU6MTZweDtcbn1cbi5zdWJtaXRidG4ge1xuICB3aWR0aDogMTg1cHg7XG4gIGhlaWdodDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tb3JhbmdlKTtcbiAgY29sb3I6I2ZmZmZmZjtcbiAgYm9yZGVyOiAxcHggc29saWQgb3JhbmdlO1xuICBmb250LXNpemU6IDEzcHg7XG4gIH1cbiAgLmNhbmNlbGJ0bntcbiAgd2lkdGg6IDE4NXB4O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogc29saWQgMXB4ICNkOGQ4ZDg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICB9XG4gIC52aWV3dGFibGV7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIGhlaWdodDogMzBweDtcbiAgICBiYWNrZ3JvdW5kOiBvcmFuZ2U7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIG9yYW5nZTtcbiAgIH1cbiAgIC5kb3QtdmVyaWZpZWQtY2lyY2xle1xuICAgIGhlaWdodDogMTBweDtcbiAgICB3aWR0aDogMTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIG1hcmdpbi10b3A6IDdweDtcbiAgfVxuICAuZG90LXVudmVyaWZpZWQtY2lyY2xle1xuICAgIGhlaWdodDogMTBweDtcbiAgICB3aWR0aDogMTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tdG9wOiA3cHg7XG4gIH1cbiAgLnByb2dyZXNzYmFyYWN0aXZle1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIC8vIGhlaWdodDogMnB4O1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIG1hcmdpbjogMHB4IC0ycHg7XG4gICAgICBib3R0b206IC0xMHB4O1xuICAgICAgbGVmdDogM3B4O1xuICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgIGJhY2tncm91bmQ6IHJlZDtcbiAgICAgIHdpZHRoOiAxMHB4O1xuICAgICAgLy8gd2lkdGg6IGNhbGMoMTAwJSAtIDNweCk7XG4gIH1cbiAgXG4gIFxuICAudmlzdWFsbHloaWRkZW4ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3Mge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgbGlzdC1zdHlsZS1pbWFnZTogbm9uZTtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBcbiAgfVxuICBcbiAgLndpemFyZC1wcm9ncmVzcyBsaSB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXJpZ2h0OiA0N3B4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEycHg7XG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgLnN0ZXAtbmFtZSB7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIFxuICB9XG4gIFxuICAud2l6YXJkLXByb2dyZXNzIC5zdGVwLW5hbWUtd3JhcHBlciB7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTsgICAgXG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgLnN0ZXAtbnVtIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgYm9yZGVyOiAycHggc29saWQgICNlYWVhZWE7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VhZWFlYSAhaW1wb3J0YW50O1xuICAgIC8vIGJvcmRlci1jb2xvcjogI2VhZWFlYSAhaW1wb3J0YW50O1xuICAgIC8vIGJvcmRlcjogM3B4IHNvbGlkICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHdpZHRoOiAyMHB4ICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgfVxuICBcbiAgLndpemFyZC1wcm9ncmVzcyAuc3RlcC1udW06YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYmFja2dyb3VuZDogI2VkZWRlZCAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMnB4O1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6LTNweCAhaW1wb3J0YW50O1xuICAgIGJvdHRvbTogNXB4O1xuICAgIGxlZnQ6IDE4cHggIWltcG9ydGFudDtcbiAgfVxuICBcbiAgLndpemFyZC1wcm9ncmVzcyBsaTpsYXN0LW9mLXR5cGUgLnN0ZXAtbnVtOmFmdGVyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIFxuICBcbiAgLndpemFyZC1wcm9ncmVzcyAuYWN0aXZlLXN0ZXBBdXRoMSAuc3RlcC1udW0ge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdyAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1jb2xvcjogeWVsbG93ICFpbXBvcnRhbnQ7XG4gIH1cbiAgLndpemFyZC1wcm9ncmVzcyAuYWN0aXZlLXN0ZXAgLnN0ZXAtbnVtIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDg3OWZmICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLWNvbG9yOiAjYjljY2ZmICFpbXBvcnRhbnQ7XG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgLmFjdGl2ZS1zdGVwSXNzdWVkIC5zdGVwLW51bSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzU4Y2I1YSAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1jb2xvcjogI2IxZmZiMiAhaW1wb3J0YW50O1xuICB9Il19 */"

/***/ }),

/***/ "./src/app/layout/authorizer/authorizer.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/authorizer/authorizer.component.ts ***!
  \***********************************************************/
/*! exports provided: AuthorizerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizerComponent", function() { return AuthorizerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_authorizer_authorizer_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/authorizer/authorizer.service */ "./src/app/services/authorizer/authorizer.service.ts");
/* harmony import */ var _services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/dashboard/dashboard.service */ "./src/app/services/dashboard/dashboard.service.ts");
/* harmony import */ var _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _services_common_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/common/common.service */ "./src/app/services/common/common.service.ts");






var AuthorizerComponent = /** @class */ (function () {
    function AuthorizerComponent(authserv, local, dashboardserv, commonServ) {
        this.authserv = authserv;
        this.local = local;
        this.dashboardserv = dashboardserv;
        this.commonServ = commonServ;
        this.display_zero_pending_auths = true;
        this.display_pending_authorizations = false;
        this.display_rejected = false;
        this.display_authorized = false;
        this.activeFlag = 0;
        this.pending_authorization_count = 0;
        this.rejected_count = 0;
        this.signCount = 0;
        this.rejectFlag = false;
        this.aid = this.local.getAuthorizerId();
    }
    AuthorizerComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.dashboardserv.getAuthData(this.aid)];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        this.pending_authorization_count = response['pendingCount'];
                        this.rejected_count = response['rejectedCount'];
                        this.signCount = response['signCount'];
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthorizerComponent.prototype.getPendingAuthCerts = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var pendingSignsResp;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.activeFlag = 1;
                        this.display_zero_pending_auths = false;
                        this.display_pending_authorizations = true;
                        this.display_rejected = false;
                        this.display_authorized = false;
                        return [4 /*yield*/, this.authserv.pendingSigns(this.aid, 5, 0)];
                    case 1:
                        pendingSignsResp = _a.sent();
                        //   const pendingSignsResp={
                        //     "total": 1,
                        //     "result": [
                        //         {
                        //             "pid": "3",
                        //             "iid": "1",
                        //             "hash": "jXWWc019RenVBbn/AQuyMuKul9Jtq8J8vlk1AIHyMZ8=",
                        //             "sign": "KJ7pHqaG3KgOIsxplxjA2Sfl++oThvXMs+hWiyYPAAoAussbrZiiN4zYJyv+CeTOaG4IO/s6qua+rzJnwKi/AQ==",
                        //             "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //             "timestampp": 1551780962079,
                        //             "status": "pending",
                        //             "authLevel": 2,
                        //             "empid": "JohnBonda",
                        //             "transactionId": "-",
                        //             "did": "1",
                        //             "data": "{\"Name\":\"John Vijaqwdssy Bonda\",\"Address\":\"Visaqwdkhssapatnam\",\"Course\":\"CSsqwdsE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}",
                        //             "receipientEmail": "john.bond629@gmail.com",
                        //             "receipientName": "John Bonda",
                        //             "totalLevels": 3,
                        //             "departmentName": "HR",
                        //             "issuerEmail": "john.qwdqasdsdswsdasdqwd@yopmail.com"
                        //         },
                        //         {
                        //           "pid": "3",
                        //           "iid": "1",
                        //           "hash": "jXWWc019RenVBbn/AQuyMuKul9Jtq8J8vlk1AIHyMZ8=",
                        //           "sign": "KJ7pHqaG3KgOIsxplxjA2Sfl++oThvXMs+hWiyYPAAoAussbrZiiN4zYJyv+CeTOaG4IO/s6qua+rzJnwKi/AQ==",
                        //           "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //           "timestampp": 1551780962079,
                        //           "status": "pending",
                        //           "authLevel": 2,
                        //           "empid": "JohnBonda",
                        //           "transactionId": "-",
                        //           "did": "1",
                        //           "data": "{\"Name\":\"John Vijaqwdssy Bonda\",\"Address\":\"Visaqwdkhssapatnam\",\"Course\":\"CSsqwdsE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}",
                        //           "receipientEmail": "john.bond629@gmail.com",
                        //           "receipientName": "John Bonda",
                        //           "totalLevels": 3,
                        //           "departmentName": "HR",
                        //           "issuerEmail": "john.qwdqasdsdswsdasdqwd@yopmail.com"
                        //       },
                        //       {
                        //         "pid": "3",
                        //         "iid": "1",
                        //         "hash": "jXWWc019RenVBbn/AQuyMuKul9Jtq8J8vlk1AIHyMZ8=",
                        //         "sign": "KJ7pHqaG3KgOIsxplxjA2Sfl++oThvXMs+hWiyYPAAoAussbrZiiN4zYJyv+CeTOaG4IO/s6qua+rzJnwKi/AQ==",
                        //         "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //         "timestampp": 1551780962079,
                        //         "status": "pending",
                        //         "authLevel": 2,
                        //         "empid": "JohnBonda",
                        //         "transactionId": "-",
                        //         "did": "1",
                        //         "data": "{\"Name\":\"John Vijaqwdssy Bonda\",\"Address\":\"Visaqwdkhssapatnam\",\"Course\":\"CSsqwdsE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}",
                        //         "receipientEmail": "john.bond629@gmail.com",
                        //         "receipientName": "John Bonda",
                        //         "totalLevels": 3,
                        //         "departmentName": "HR",
                        //         "issuerEmail": "john.qwdqasdsdswsdasdqwd@yopmail.com"
                        //     }
                        //     ],
                        //     "isSuccess": true,
                        //     "success": true
                        // };
                        this.totalPendingAuths = pendingSignsResp['result'];
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthorizerComponent.prototype.getRejectedCerts = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var rejectedSignResp;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.activeFlag = 2;
                        this.display_zero_pending_auths = false;
                        this.display_pending_authorizations = false;
                        this.display_rejected = true;
                        this.display_authorized = false;
                        return [4 /*yield*/, this.authserv.rejectedPayslips(this.aid)];
                    case 1:
                        rejectedSignResp = _a.sent();
                        //   const rejectedSignResp={
                        //     "rejectedIssues": [
                        //         {
                        //             "pid": "2",
                        //             "aid": "1",
                        //             "iid": "1",
                        //             "reason": "Very bad certificate, thu..",
                        //             "timestampp": 1551781017359,
                        //             "receipientEmail": "john.bond629@gmail.com",
                        //             "receipientName": "John Bonda",
                        //             "authLevel": 2,
                        //             "did": "1",
                        //             "totalLevels": 3,
                        //             "departmentName": "HR",
                        //             "issuerEmail": "john.qwdqasdsdswsdasdqwd@yopmail.com"
                        //         }
                        //     ],
                        //     "total": 1,
                        //     "isSuccess": true,
                        //     "success": true
                        // };
                        this.totalRejectedAuths = rejectedSignResp['rejectedIssues'];
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthorizerComponent.prototype.getAuthorizedCerts = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var signedAuthResp;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.activeFlag = 3;
                        this.display_zero_pending_auths = false;
                        this.display_pending_authorizations = false;
                        this.display_rejected = false;
                        this.display_authorized = true;
                        return [4 /*yield*/, this.authserv.authorizedPayslips(this.aid)];
                    case 1:
                        signedAuthResp = _a.sent();
                        //   const signedAuthResp=
                        //   {
                        //     "signedIssues": [
                        //         {
                        //             "pid": "1",
                        //             "iid": "1",
                        //             "hash": "hZHp6ltETq5N3IiILBrujY8gHDs8YtW6eZl0+IrCFOg=",
                        //             "sign": "w6IgsLo0DqMoneTK7JDKnoXDC5oJruyEVpEk+CzlGcb/FCT5iCzKxfX6Wr3LfZBK0fevNJJOTkYd9Gjyu8fCBw==",
                        //             "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //             "timestampp": 1551781344716,
                        //             "status": "issued",
                        //             "authLevel": 3,
                        //             "empid": "JohnBonda",
                        //             "transactionId": "-",
                        //             "did": "1",
                        //             "data": "{\"Name\":\"John Vijay Bonda\",\"Address\":\"Visakhapatnam\",\"Course\":\"CSE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}",
                        //             "receipientEmail": "john.bond629@gmail.com",
                        //             "receipientName": "John Bonda",
                        //             "totalLevels": 3,
                        //             "departmentName": "HR",
                        //             "issuerEmail": "john.qwdqasdsdswsdasdqwd@yopmail.com"
                        //         },
                        //         {
                        //             "pid": "4",
                        //             "iid": "1",
                        //             "hash": "0KMmQao4WsdHlNKXksIHzvQVofMA8qAPO83rHiQVo+Q=",
                        //             "sign": "CnJSF9xMwvCaGZmNNzSjanvwxmqCCq80my7RJvS06du0AD2SdVDBovRxWMFPPC6xmduclrcwt0PSbIW0zLaFBA==",
                        //             "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //             "timestampp": 1551781037229,
                        //             "status": "authorized",
                        //             "authLevel": 3,
                        //             "empid": "JohnBonda",
                        //             "transactionId": "-",
                        //             "did": "1",
                        //             "data": "{\"Name\":\"John Vijaqwsddssy Bonda\",\"Address\":\"Visaqwdkhsdssapatnam\",\"Course\":\"CSsqwasddsE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}",
                        //             "receipientEmail": "john.bond629@gmail.com",
                        //             "receipientName": "John Bonda",
                        //             "totalLevels": 3,
                        //             "departmentName": "HR",
                        //             "issuerEmail": "john.qwdqasdsdswsdasdqwd@yopmail.com"
                        //         }
                        //     ],
                        //     "total": 2,
                        //     "isSuccess": true,
                        //     "success": true
                        // };
                        this.totalSignedAuths = signedAuthResp['signedIssues'];
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthorizerComponent.prototype.getCertDetails = function (certid) {
        this.certid = certid;
    };
    AuthorizerComponent.prototype.authorizeCertificate = function (passphrase) {
        this.secretPhrase = passphrase;
        this.authorize(this.certid, this.aid, this.secretPhrase);
    };
    AuthorizerComponent.prototype.authorize = function (certid, authid, passphrase) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.authserv.authorize(certid, authid, passphrase)];
                    case 1:
                        response = _a.sent();
                        alert(response['message']);
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthorizerComponent.prototype.reject = function (certid, authid, comments) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.authserv.rejectPayslip(certid, authid, comments)];
                    case 1:
                        response = _a.sent();
                        if (response['success'] === true) {
                            alert("Rejected!");
                        }
                        else {
                            alert("Failed to Reject");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthorizerComponent.prototype.rejectionFlag = function () {
        this.rejectFlag = false;
        this.reject(this.certid, this.aid, this.comments);
    };
    AuthorizerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-authorizer',
            template: __webpack_require__(/*! ./authorizer.component.html */ "./src/app/layout/authorizer/authorizer.component.html"),
            styles: [__webpack_require__(/*! ./authorizer.component.scss */ "./src/app/layout/authorizer/authorizer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_authorizer_authorizer_service__WEBPACK_IMPORTED_MODULE_2__["AuthorizerService"], _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__["LocalstorageService"], _services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_3__["DashboardService"], _services_common_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"]])
    ], AuthorizerComponent);
    return AuthorizerComponent;
}());



/***/ }),

/***/ "./src/app/layout/authorizer/authorizer.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/authorizer/authorizer.module.ts ***!
  \********************************************************/
/*! exports provided: AuthorizerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizerModule", function() { return AuthorizerModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _authorizer_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./authorizer-routing.module */ "./src/app/layout/authorizer/authorizer-routing.module.ts");
/* harmony import */ var _authorizer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./authorizer.component */ "./src/app/layout/authorizer/authorizer.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");






var AuthorizerModule = /** @class */ (function () {
    function AuthorizerModule() {
    }
    AuthorizerModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_authorizer_component__WEBPACK_IMPORTED_MODULE_4__["AuthorizerComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _authorizer_routing_module__WEBPACK_IMPORTED_MODULE_3__["AuthorizerRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"]
            ]
        })
    ], AuthorizerModule);
    return AuthorizerModule;
}());



/***/ }),

/***/ "./src/app/services/authorizer/authorizer.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/authorizer/authorizer.service.ts ***!
  \***********************************************************/
/*! exports provided: AuthorizerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthorizerService", function() { return AuthorizerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");











//displaypayslipdata pop up API is pending. It has to be added in common
var AuthorizerService = /** @class */ (function () {
    function AuthorizerService(http, requestOptions) {
        this.http = http;
        this.requestOptions = requestOptions;
        this.locvals = new _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_9__["LocalstorageService"]();
        this.dappid = this.locvals.getDappId();
        this.penidngSignsUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizers/pendingSigns';
        this.authorizeUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizer/authorize';
        this.rejectPayslipUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizer/reject';
        this.authorizedPayslipsUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizer/statistic/signedIssues';
        this.rejectedPayslipsUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizer/statistic/rejectedIssues';
    }
    AuthorizerService.prototype.pendingSigns = function (aid, limit, offset) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            aid: aid,
                            limit: limit,
                            offset: offset
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.penidngSignsUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    AuthorizerService.prototype.authorize = function (pid, aid, passphrase) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            pid: pid,
                            aid: aid,
                            secret: passphrase
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.authorizeUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    AuthorizerService.prototype.rejectPayslip = function (pid, aid, comments) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            pid: pid,
                            aid: aid,
                            message: comments
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.rejectPayslipUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    AuthorizerService.prototype.authorizedPayslips = function (aid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            aid: aid
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.authorizedPayslipsUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    AuthorizerService.prototype.rejectedPayslips = function (aid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            aid: aid
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.rejectedPayslipsUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    AuthorizerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__["RequestOptionsService"]])
    ], AuthorizerService);
    return AuthorizerService;
}());



/***/ })

}]);
//# sourceMappingURL=authorizer-authorizer-module.js.map