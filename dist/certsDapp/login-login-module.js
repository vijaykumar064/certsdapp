(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/login/login-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/*! exports provided: LoginRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRoutingModule", function() { return LoginRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.component */ "./src/app/login/login.component.ts");




var routes = [
    {
        path: '', component: _login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
    }
];
var LoginRoutingModule = /** @class */ (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\" style=\"height:300% !important;position: fixed !important;width: 100% !important\">\n  <div class=\"row\" style=\"height:100% !important;position: fixed !important;width: 100% !important\">\n    <div class=\"col-4 Ellipse-281\">\n      <img src=\"../../assets/images/belbooks-logo.svg\" class=\"logoimage\">\n    </div>\n    <div class=\"col-8 justify-content-center registerform\" style=\"margin-top:100px;height: 100% !important;\">\n      <div class=\"container-fluid\">\n\n        <br>\n        <br>\n        <br>\n        <div class=\"row\">\n          <div class=\"col-4\">\n\n          </div>\n          <div class=\"col-6\">\n            <h2 style=\"color:#000000;font-size:26px;\">Welcome Back!</h2>\n            <form method=\"post\" action=\"\" id=\"login_form\" novalidate [formGroup]=\"loginForm\" (ngSubmit)=\"login()\">\n\n              <p style=\"color:#000000;font-size:14px;\">Login to Continue</p>\n              <div class=\"input-group\">\n                <input class=\"form-control\" id=\"email\" name=\"email\" type=\"text\" formControlName=\"email\" placeholder=\"Email\" style=\"border-right:1px solid black;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;  \" />\n                <!-- <div class=\"input-group-addon\" style=\"width:50px;\">\n                  <span class=\"glyphicon glyphicon-envelope\"></span>\n                </div> -->\n              </div><br>\n              \n\n\n\n              <div class=\"input-group\">\n                <input class=\"form-control\" id=\"password\" name=\"password\" type=\"password\" formControlName=\"password\" placeholder=\"password\" style=\"border-right:1px solid black;border-left:1px solid black;border-top:1px solid black;border-bottom:1px solid black;  \" />\n                <!-- <div class=\"input-group-addon\" style=\"width:50px;\">\n                  <span class=\"glyphicon glyphicon-lock\"></span>\n                </div> -->\n              </div><br>\n              \n              <!-- <div class=\"form-group\">\n                <div class=\"g-recaptcha\" data-sitekey=\"\"></div>\n                <input class=\"form-control d-none\"  data-recaptcha=\"true\"\n                  data-error=\"Please complete the Captcha\">\n                <div class=\"form-text with-errors\"></div>\n              </div>  -->\n              <br>\n              <br>\n              <br>\n              <button type=\"submit\" class=\"btn submitbutton\"\n                style=\"margin-left:25%;\">LOGIN</button>\n            </form>\n            <br>\n            <br>\n            <div class=\"form-row\" style=\"margin-left:21%;font-size:20px;\">\n              <label><span style=\"font-size:18px;\">Need an Account?</span></label>\n              &nbsp;&nbsp;\n              <a href=\"#\" style=\"color:#1b3764;color:#000000;font-size:16px;\">Register Here</a>\n            </div>\n\n        </div>\n      </div>\n    <!-- </div>\n  </div> -->\n\n          <br>\n          <br>\n\n            <br>\n            <br>\n\n<div class=\"modal\" id=\"hllogin\"  [ngStyle]=\"{'display':display}\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n             \n\n            <button type=\"button\" class=\"close\" (click)=\"onCloseHandled()\" data-dismiss=\"modal\">&times;</button>\n          </div>\n           <div class=\"modal-body\">\n            \n              <div class=\"form-group col-md-12\">\n                  <h4>ENTER YOUR SECRET KEY</h4>\n                <!-- <img style=\"margin-left:25%\" src=\"../../../assets/img/walletrecharge.png\" style=\"border-radius:15px;margin-left:24%;\"> -->\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <input type=\"text\" class=\"form-control\" type=\"password\" name=\"secretkey\" placeholder=\"SecretKey\" [(ngModel)]=\"secretkey\">\n                </div>\n          </div>\n          <div class=\"modal-footer\">\n              <button class=\"submitbtn\" (click)=\"hyperLedgerLogin()\">DONE</button>\n              <button class=\"cancelbtn\" (click)=\"onCloseHandled()\" data-dismiss=\"modal\">CANCEL</button>\n          </div>\n        </div>\n        \n      </div>\n    </div>\n  \n  <!-- <div class=\"backdrop\" [ngStyle]=\"{'display':display}\"></div>\n  <div class=\"modal\" tabindex=\"-1\" role=\"dialog\" [ngStyle]=\"{'display':display}\">\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"onCloseHandled()\"><span\n              aria-hidden=\"true\">&times;</span></button>\n        </div>\n        <div class=\"modal-body\">\n            <h4 class=\"modal-title\">ENTER SECRET KEY</h4>\n          <input id=\"secretkey\" name=\"passphrase\" placeholder=\"Enter your secretkey\" type=\"password\" required=\"required\"\n            class=\"form-control here\" [(ngModel)]=\"secretkey\">\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"onCloseHandled()\">Cancel</button>\n          <button type=\"button\" class=\"btn btn-default\" (click)=\"hyperLedgerLogin()\">Submit</button>\n        </div>\n      </div>\n    </div>\n  </div> -->\n  \n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "body {\n  width: 100%;\n  height: 100%; }\n\n.submitbutton {\n  width: 185px;\n  height: 45px;\n  border-radius: 10px;\n  margin-left: 35%;\n  background-color: #f87d42;\n  font-family: Muli;\n  font-size: 17px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white);\n  float: none; }\n\n.registerform label {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #b4b4b4; }\n\n.registerform h5 {\n  width: 150px;\n  height: 38px;\n  font-family: Muli;\n  font-size: 30px;\n  font-weight: 800;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #4b4b4b; }\n\n.registerform input {\n  width: 320px;\n  height: 40px;\n  font-family: Muli;\n  font-size: 17px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.24;\n  letter-spacing: normal;\n  text-align: left;\n  color: #6f6f6f;\n  border: 0;\n  border-bottom: 1px solid #b4b4b4;\n  border-radius: 0;\n  background: transparent; }\n\n.registerform ::-webkit-input-placeholder {\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 200;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #6f6f6f; }\n\n.registerform ::-ms-input-placeholder {\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 200;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #6f6f6f; }\n\n.registerform ::placeholder {\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 200;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #6f6f6f; }\n\n.registerform select {\n  width: 80%;\n  height: 40px;\n  border-radius: 5px;\n  background-color: var(--white); }\n\n.Ellipse-281 {\n  background-color: #f5f5f5;\n  border-radius: 0 53% 53% 0; }\n\n.logoimage {\n  margin-bottom: -130%; }\n\nmodal-dialog {\n  max-width: 25%;\n  margin-top: 15%;\n  margin-left: 40%;\n  font-size: 16px; }\n\n.modal-content {\n  border-radius: 20px;\n  font-size: 16px; }\n\n.modal-header {\n  border-bottom: 0px;\n  font-size: 16px; }\n\n.modal-body {\n  padding: 6%;\n  font-size: 16px; }\n\n.submitbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white); }\n\n.cancelbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: white;\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: black; }\n\n.modal-content {\n  border: 0px;\n  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);\n  font-size: 16px; }\n\n.modal-footer {\n  justify-content: center;\n  font-size: 16px;\n  border-top: 0px; }\n\n.form-control {\n  margin-top: 5%;\n  font-size: 16px; }\n\n.modal-title {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #b4b4b4;\n  margin-left: 30%;\n  margin-top: 5%; }\n\n.fromcontrol input {\n  height: 35px;\n  border-radius: 4px;\n  border: solid 1px #e6e6e6;\n  background-color: var(--white); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsWUFBWSxFQUFBOztBQUVkO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUdoQjtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsU0FBUztFQUNULGdDQUFnQztFQUNoQyxnQkFBZ0I7RUFDaEIsdUJBQXVCLEVBQUE7O0FBR3pCO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixjQUFjLEVBQUE7O0FBVGhCO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixjQUFjLEVBQUE7O0FBVGhCO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsVUFBVTtFQUNWLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsOEJBQThCLEVBQUE7O0FBR2hDO0VBQ0UseUJBQXlCO0VBQ3pCLDBCQUEwQixFQUFBOztBQU81QjtFQUNFLG9CQUFvQixFQUFBOztBQUd0QjtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWMsRUFBQTs7QUFFaEI7RUFDRSxtQkFBbUI7RUFDbkIsZUFBYyxFQUFBOztBQUloQjtFQUNFLGtCQUFrQjtFQUNsQixlQUFjLEVBQUE7O0FBRWhCO0VBQ0UsV0FBVTtFQUNWLGVBQWMsRUFBQTs7QUFLaEI7RUFDRSxZQUFZO0VBRVosY0FBYztFQUVkLG1CQUFtQjtFQUVuQiwrQkFBK0I7RUFFL0IsaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixnQkFBZ0I7RUFFaEIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixpQkFBaUI7RUFFakIsc0JBQXNCO0VBRXRCLGtCQUFrQjtFQUVsQixtQkFBbUIsRUFBQTs7QUFLckI7RUFDRSxZQUFZO0VBRVosY0FBYztFQUVkLG1CQUFtQjtFQUVuQix1QkFBc0I7RUFDdEIsaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixnQkFBZ0I7RUFFaEIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixpQkFBaUI7RUFFakIsc0JBQXNCO0VBRXRCLGtCQUFrQjtFQUVsQixZQUFZLEVBQUE7O0FBS2Q7RUFDRSxXQUFXO0VBQ1QseUNBQXFDO0VBQ3JDLGVBQWMsRUFBQTs7QUFHbEI7RUFDRSx1QkFBdUI7RUFDdkIsZUFBYztFQUVoQixlQUFlLEVBQUE7O0FBSWY7RUFDRSxjQUFhO0VBQ2IsZUFBYyxFQUFBOztBQUtoQjtFQUNFLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsZ0JBQWdCO0VBRWhCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixnQkFBZ0I7RUFFaEIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixjQUFjLEVBQUE7O0FBT2hCO0VBQ0UsWUFBWTtFQUVaLGtCQUFrQjtFQUVsQix5QkFBeUI7RUFFekIsOEJBQThCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImJvZHl7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG59XG4uc3VibWl0YnV0dG9ue1xuICB3aWR0aDogMTg1cHg7XG4gIGhlaWdodDogNDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbWFyZ2luLWxlZnQ6IDM1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y4N2Q0MjtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjM7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcbiAgZmxvYXQ6IG5vbmU7ICAgIFxuICB9XG5cbi5yZWdpc3RlcmZvcm0gbGFiZWx7ICBcbiAgZm9udC1mYW1pbHk6IE11bGk7ICBcbiAgZm9udC1zaXplOiAxNXB4OyAgXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDsgIFxuICBmb250LXN0cmV0Y2g6IG5vcm1hbDsgIFxuICBsaW5lLWhlaWdodDogMS4yNzsgIFxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsOyAgXG4gIHRleHQtYWxpZ246IGxlZnQ7ICBcbiAgY29sb3I6ICNiNGI0YjQ7XG59XG5cbi5yZWdpc3RlcmZvcm0gaDV7XG4gIHdpZHRoOiAxNTBweDtcbiAgaGVpZ2h0OiAzOHB4O1xuICBmb250LWZhbWlseTogTXVsaTsgIFxuICBmb250LXNpemU6IDMwcHg7ICBcbiAgZm9udC13ZWlnaHQ6IDgwMDsgIFxuICBmb250LXN0eWxlOiBub3JtYWw7ICBcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7ICBcbiAgbGluZS1oZWlnaHQ6IDEuMjc7ICBcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDsgIFxuICB0ZXh0LWFsaWduOiBsZWZ0OyAgXG4gIGNvbG9yOiAjNGI0YjRiO1xufVxuXG4ucmVnaXN0ZXJmb3JtIGlucHV0e1xuICB3aWR0aDogMzIwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDsgIFxuICBmb250LXN0eWxlOiBub3JtYWw7ICBcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7ICBcbiAgbGluZS1oZWlnaHQ6IDEuMjQ7ICBcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDsgIFxuICB0ZXh0LWFsaWduOiBsZWZ0OyAgXG4gIGNvbG9yOiAjNmY2ZjZmO1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYjRiNGI0O1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLnJlZ2lzdGVyZm9ybSA6OnBsYWNlaG9sZGVye1xuICBmb250LWZhbWlseTogTXVsaTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogMjAwO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICBsaW5lLWhlaWdodDogMS4yNTtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICM2ZjZmNmY7XG59XG5cbi5yZWdpc3RlcmZvcm0gc2VsZWN0e1xuICB3aWR0aDogODAlO1xuICBoZWlnaHQ6IDQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUpO1xufVxuXG4uRWxsaXBzZS0yODEge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNWY1O1xuICBib3JkZXItcmFkaXVzOiAwIDUzJSA1MyUgMDtcbn1cblxuLy8gLmlucHV0LWdyb3Vwe1xuLy8gICAgIHdpZHRoOiA4MCU7XG4gIFxuLy8gfVxuLmxvZ29pbWFnZXtcbiAgbWFyZ2luLWJvdHRvbTogLTEzMCU7XG59XG5cbm1vZGFsLWRpYWxvZyB7XG4gIG1heC13aWR0aDogMjUlO1xuICBtYXJnaW4tdG9wOiAxNSU7XG4gIG1hcmdpbi1sZWZ0OiA0MCU7XG4gIGZvbnQtc2l6ZToxNnB4O1xufVxuLm1vZGFsLWNvbnRlbnR7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGZvbnQtc2l6ZToxNnB4O1xufVxuXG5cbi5tb2RhbC1oZWFkZXJ7XG4gIGJvcmRlci1ib3R0b206IDBweDsgXG4gIGZvbnQtc2l6ZToxNnB4O1xufVxuLm1vZGFsLWJvZHl7XG4gIHBhZGRpbmc6NiU7XG4gIGZvbnQtc2l6ZToxNnB4O1xuICAvLyBtYXJnaW4tdG9wOjUlO1xufVxuXG5cbi5zdWJtaXRidG57XG4gIHdpZHRoOiAxNDVweDtcblxuICBoZWlnaHQ6IDM2LjhweDtcblxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuXG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLW9yYW5nZSk7XG5cbiAgZm9udC1mYW1pbHk6IE11bGk7XG5cbiAgZm9udC1zaXplOiAxM3B4O1xuXG4gIGZvbnQtd2VpZ2h0OiA5MDA7XG5cbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuXG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG4gIGxpbmUtaGVpZ2h0OiAxLjIzO1xuXG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG5cbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG5cbn1cblxuXG4uY2FuY2VsYnRue1xuICB3aWR0aDogMTQ1cHg7XG5cbiAgaGVpZ2h0OiAzNi44cHg7XG5cbiAgYm9yZGVyLXJhZGl1czogMTBweDtcblxuICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xuICBmb250LWZhbWlseTogTXVsaTtcblxuICBmb250LXNpemU6IDEzcHg7XG5cbiAgZm9udC13ZWlnaHQ6IDkwMDtcblxuICBmb250LXN0eWxlOiBub3JtYWw7XG5cbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG5cbiAgbGluZS1oZWlnaHQ6IDEuMjM7XG5cbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcblxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgY29sb3I6IGJsYWNrO1xuXG59XG5cblxuLm1vZGFsLWNvbnRlbnR7XG4gIGJvcmRlcjogMHB4O1xuICAgIGJveC1zaGFkb3c6IDAgNXB4IDE1cHggcmdiYSgwLDAsMCwuNSk7IFxuICAgIGZvbnQtc2l6ZToxNnB4O1xufVxuXG4ubW9kYWwtZm9vdGVye1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZm9udC1zaXplOjE2cHg7XG4gIC8vIHBhZGRpbmc6IDFyZW07XG5ib3JkZXItdG9wOiAwcHg7XG59XG5cblxuLmZvcm0tY29udHJvbHtcbiAgbWFyZ2luLXRvcDo1JTtcbiAgZm9udC1zaXplOjE2cHg7XG59XG5cblxuXG4ubW9kYWwtdGl0bGV7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuXG4gIGZvbnQtc2l6ZTogMTVweDtcblxuICBmb250LXdlaWdodDogNjAwO1xuXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcblxuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcblxuICBsaW5lLWhlaWdodDogMS4yNztcblxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG4gIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgY29sb3I6ICNiNGI0YjQ7IFxuICBtYXJnaW4tbGVmdDogMzAlO1xuICBtYXJnaW4tdG9wOiA1JTtcbiBcblxufVxuXG5cblxuLmZyb21jb250cm9sIGlucHV0e1xuICBoZWlnaHQ6IDM1cHg7XG5cbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuXG4gIGJvcmRlcjogc29saWQgMXB4ICNlNmU2ZTY7XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUpO1xuIH0iXX0= */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _services_common_common_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../services/common/common.service */ "./src/app/services/common/common.service.ts");














var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, actRoute, local, userService, commonService) {
        var _this = this;
        this.router = router;
        this.actRoute = actRoute;
        this.local = local;
        this.userService = userService;
        this.commonService = commonService;
        this.logList = [];
        this.display = 'none';
        this.actRoute.queryParams.subscribe(function (params) {
            _this.params = _this.actRoute.snapshot.params.email;
            console.log(_this.params);
        });
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.params != undefined) {
            this.getemail = this.params;
            this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]({ value: this.params, disabled: true }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
            });
        }
        else {
            this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
                email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]({ value: '', disabled: false }, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]),
                password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required])
            });
        }
    };
    LoginComponent.prototype.openModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.display = "block";
                return [2 /*return*/];
            });
        });
    };
    LoginComponent.prototype.onCloseHandled = function () {
        this.display = 'none';
    };
    LoginComponent.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var formdetails, loginResponse, token;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.params === undefined) {
                            this.getemail = this.loginForm.value.email;
                        }
                        formdetails = {
                            email: this.getemail,
                            password: this.loginForm.value.password
                        };
                        console.log(formdetails);
                        return [4 /*yield*/, this.checkLogin(formdetails)];
                    case 1:
                        loginResponse = _a.sent();
                        if (loginResponse["isSuccess"] === true) {
                            this.openModal();
                            token = loginResponse["data"].token;
                            this.local.setBelriumToken(token);
                            console.log("just after login: " + token);
                        }
                        else {
                            console.log("username and password is incorrect");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginComponent.prototype.hyperLedgerLogin = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var token, secret, hllogin, btoken, btoken1, bkvsdm_token, getAddrRes, obj, a, b, c, d, address, getBalRes;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = this.local.getBelriumToken();
                        secret = this.secretkey;
                        return [4 /*yield*/, this.checkHyperLogin(secret, token)];
                    case 1:
                        hllogin = _a.sent();
                        if (!(hllogin["isSuccess"] === true)) return [3 /*break*/, 4];
                        this.local.setEmail(this.getemail);
                        this.local.setLoggedIn(true);
                        btoken = hllogin["data"];
                        btoken1 = JSON.parse(btoken);
                        bkvsdm_token = btoken1.token;
                        this.local.setBKVSToken(bkvsdm_token);
                        console.log(hllogin);
                        console.log("Before kyc status call: " + token);
                        this.getKycStatus(token);
                        return [4 /*yield*/, this.getAddress()];
                    case 2:
                        getAddrRes = _a.sent();
                        obj = getAddrRes["data"];
                        a = obj["countries"];
                        b = a[0];
                        c = b["wallets"];
                        d = c[0];
                        address = d.address;
                        this.local.setWalletAddress(address);
                        return [4 /*yield*/, this.getBalance(address)];
                    case 3:
                        getBalRes = _a.sent();
                        this.local.setBalance(getBalRes);
                        this.getRole();
                        return [3 /*break*/, 5];
                    case 4:
                        console.log("wrong");
                        _a.label = 5;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    LoginComponent.prototype.checkHyperLogin = function (secret, token) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.userService.onCheckHyperLedger(secret, token)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.getKycStatus = function (token) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var kycResponse, a, kycstatus;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.CheckKYCStatus(token)];
                    case 1:
                        kycResponse = _a.sent();
                        console.log(kycResponse);
                        a = kycResponse["data"];
                        kycstatus = a[0].kycResponse;
                        this.local.setKycStatus(kycstatus);
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginComponent.prototype.getAddress = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.ongetAddress()];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.getBalance = function (add) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.ongetBal(add)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.getRole = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, dappid, role, kycStatus, res, a, id, response, b, isdept, res, a, id;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.checkRole()];
                    case 1:
                        data = _a.sent();
                        console.log(data);
                        dappid = data["dappid"];
                        role = data["role"];
                        this.local.setCountryName(data["country"]);
                        this.local.setOrganization(data["company"]);
                        this.local.setUserName(data["name"]);
                        this.local.setUserRole(role);
                        this.local.setDappId(dappid);
                        kycStatus = this.local.getKycStatus();
                        console.log(kycStatus);
                        if (!(kycStatus === "true")) return [3 /*break*/, 2];
                        console.log("kycstatus true");
                        this.router.navigateByUrl('/wallet');
                        return [3 /*break*/, 10];
                    case 2:
                        if (!(role === "new user")) return [3 /*break*/, 3];
                        console.log("wallet page");
                        this.router.navigateByUrl('/wallet');
                        return [3 /*break*/, 10];
                    case 3:
                        if (!(role === "superuser")) return [3 /*break*/, 4];
                        console.log("superadmin page");
                        this.router.navigateByUrl('/superadmin');
                        return [3 /*break*/, 10];
                    case 4:
                        if (!(role === "issuer")) return [3 /*break*/, 7];
                        console.log("Issuer page");
                        return [4 /*yield*/, this.getissuerid()];
                    case 5:
                        res = _a.sent();
                        console.log(res);
                        a = res["result"];
                        id = a.iid;
                        this.local.setIssuerId(id);
                        return [4 /*yield*/, this.issuerdata(id)];
                    case 6:
                        response = _a.sent();
                        console.log(response);
                        b = response["issuer"];
                        console.log(b);
                        isdept = b.departments;
                        this.local.setIssuerDept(isdept);
                        this.router.navigateByUrl('/issuer');
                        return [3 /*break*/, 10];
                    case 7:
                        if (!(role === "authorizer")) return [3 /*break*/, 9];
                        return [4 /*yield*/, this.getauthid()];
                    case 8:
                        res = _a.sent();
                        console.log(res);
                        a = res["result"];
                        id = a.aid;
                        console.log(id);
                        this.local.setAuthorizerId(id);
                        this.router.navigateByUrl('/authorizer');
                        return [3 /*break*/, 10];
                    case 9:
                        console.log("Invalid user role identified...");
                        _a.label = 10;
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    LoginComponent.prototype.getauthid = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            email: this.getemail
                        };
                        return [4 /*yield*/, this.commonService.ongetauth(params)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.getissuerid = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.ongetIssuer()];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.authdata = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.ongetauthdata(id)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.issuerdata = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.oncheckIssuerData(id)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.checkRole = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            email: this.getemail
                        };
                        return [4 /*yield*/, this.commonService.oncheckRole(params)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.CheckKYCStatus = function (token) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.onCheckYCStatus(token)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent.prototype.checkLogin = function (formdetails) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.userService.doLogin(formdetails)];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_11__["LocalstorageService"], _services_user_user_service__WEBPACK_IMPORTED_MODULE_10__["UserService"], _services_common_common_service__WEBPACK_IMPORTED_MODULE_12__["CommonService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/login/login-routing.module.ts");






var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"]],
            imports: [
                _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"]
            ]
        })
    ], LoginModule);
    return LoginModule;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map