(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~settings-settings-module~superadmin-superadmin-module"],{

/***/ "./src/app/services/superadmin/superadmin.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/superadmin/superadmin.service.ts ***!
  \***********************************************************/
/*! exports provided: SuperadminService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperadminService", function() { return SuperadminService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");











var SuperadminService = /** @class */ (function () {
    function SuperadminService(http, requestOptions) {
        this.http = http;
        this.requestOptions = requestOptions;
        this.locvals = new _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_9__["LocalstorageService"]();
        this.dappid = this.locvals.getDappId();
        this.getDepartmentsUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/department/get';
        this.getIssuedPayslipsUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/issuer/data/issuedPayslips';
        this.getSignedPayslipsUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizer/signedPayslips';
        this.getIssuersUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/issuers';
        this.getAuthorizersUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizers';
        this.removeIssuerUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/issuers/remove';
        this.removeAuthorizerUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/authorizers/remove';
        this.getCountriesUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].SWAGGERAPI + 'countries';
        this.addUserUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/registerUser/';
        this.pendingIssuesUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/superuser/statistic/pendingIssues/';
        this.pendingAuthorizationUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/superuser/statistic/pendingAuthorization/';
        this.rejectedIssuesUrl = _constants__WEBPACK_IMPORTED_MODULE_8__["Constants"].HOST_URL + this.dappid + '/superuser/statistic/rejectedIssues';
    }
    SuperadminService.prototype.getDepartments = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getDepartmentsUrl, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json];
                }
            });
        });
    };
    SuperadminService.prototype.getpendingauthresult = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {};
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.pendingAuthorizationUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getpendingissueresult = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {};
                        headers = new Headers({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.pendingIssuesUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getcertificaterejresult = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {};
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.rejectedIssuesUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getIssuedPayslips = function (iid, limit, offset) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            iid: iid,
                            limit: limit,
                            offset: offset
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getIssuedPayslipsUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getSignedPayslips = function (aid, limit, offset) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            aid: aid,
                            limit: limit,
                            offset: offset
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getSignedPayslipsUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getIssuers = function (limit, offset, filterdep) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            limit: limit,
                            offset: offset,
                            department: filterdep
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getIssuersUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getAuthorizers = function (limit, offset, filterdep) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            limit: limit,
                            offset: offset,
                            department: filterdep
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getAuthorizersUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.removeIssuer = function (iid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            iid: iid
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.removeIssuerUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.removeAuthorizer = function (aid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            aid: aid
                        };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.removeAuthorizerUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.addUser = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.addUserUrl, JSON.stringify(data), options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.addAuthorizer = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.addUserUrl, JSON.stringify(data), options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getIssuersAPI = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var param, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        param = {};
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getIssuersUrl, param, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.getAuthorizerAPI = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var param, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        param = {};
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.getAuthorizersUrl, param, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.removeissuersAPI = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var param, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        param = {};
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.removeIssuerUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService.prototype.removeauthorizersAPI = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.removeAuthorizerUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    SuperadminService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__["RequestOptionsService"]])
    ], SuperadminService);
    return SuperadminService;
}());



/***/ })

}]);
//# sourceMappingURL=default~settings-settings-module~superadmin-superadmin-module.js.map