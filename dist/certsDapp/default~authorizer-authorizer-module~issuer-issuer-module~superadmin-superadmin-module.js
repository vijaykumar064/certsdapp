(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~authorizer-authorizer-module~issuer-issuer-module~superadmin-superadmin-module"],{

/***/ "./src/app/services/dashboard/dashboard.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/dashboard/dashboard.service.ts ***!
  \*********************************************************/
/*! exports provided: DashboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function() { return DashboardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");












var DashboardService = /** @class */ (function () {
    function DashboardService(http, local, requestOptions) {
        this.http = http;
        this.local = local;
        this.requestOptions = requestOptions;
        this.dappid = this.local.getDappId();
        this.SuperUserStatsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/superuser/statistic/counts';
        this.IssuerStatsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/issuer/statistic/counts';
        this.AuthorizerStatsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/authorizer/statistic/counts';
        this.RecentIssuedUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/recentIssued';
        this.PayslipInitiatedUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/payslip/initiated';
        this.PayslipIssuedUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/payslip/issued';
        this.IssuerDataUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/issuer/data';
        this.PayslipStatsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/payslip/statistic';
        this.EmployeeDataUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/employeeData';
        this.PayslipDataUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/payslip/getPayslip';
        this.PendingPayslipsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/issuer/pendingIssues';
        this.IssuerEmployeeUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/issuer/employeesRegistered';
        this.DepartmentsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/getDepartments/authorizers';
        this.EmpDesignationsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOST_URL + this.dappid + '/employees/getDesignations';
        this.authid = this.local.getAuthorizerId();
        this.iid = this.local.getIssuerId();
    }
    DashboardService.prototype.getSuperuserData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {};
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.SuperUserStatsUrl, data, options).timeout(1000).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    // async onCheckHyperLedger(secret:string,token:string):Promise<object>
    // {
    //     var data=
    //     {
    //         secret: secret,
    //         token:token
    //     }
    //  const headers = new Headers({ 'Content-Type': 'application/json','magic': '594fe0f3'});
    //  const options = new RequestOptions({ headers: headers, method: 'post' });
    //  const res= await this.http.post(this.hyperledgerLogin,data,options).timeout(300000).toPromise()
    //  return res.json();
    // }
    DashboardService.prototype.getIssuerData = function (iid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = { iid: iid };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.IssuerStatsUrl, data, options).timeout(1000).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    DashboardService.prototype.getAuthData = function (authid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = { aid: authid };
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.AuthorizerStatsUrl, data, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    DashboardService.prototype.recentIssued = function () {
        var data = {
            limit: "5",
            offset: "0"
        };
        var options = this.requestOptions.getDappRequestOptions();
        return this.http.post(this.RecentIssuedUrl, data, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.pendingAuthorizationList = function () {
        var data = {};
        var options = this.requestOptions.getDappRequestOptions();
        return this.http.post(this.PayslipInitiatedUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.totalIssues = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.PayslipIssuedUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getissuerdetails = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.IssuerDataUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getBlockchaindata = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.PayslipStatsUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getEmployeeDetails = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.EmployeeDataUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getPayslipData = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.PayslipDataUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.pendingpayslips = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.PendingPayslipsUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.totalemployee = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.IssuerEmployeeUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getDesignations = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.EmpDesignationsUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.getDepartments = function () {
        var data = {};
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'content-Type': 'application/json', 'magic': '594fe0f3' });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.DepartmentsUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    DashboardService.prototype.handleError = function (error) {
        console.error(error);
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().message);
    };
    DashboardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_10__["LocalstorageService"], _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_11__["RequestOptionsService"]])
    ], DashboardService);
    return DashboardService;
}());



/***/ })

}]);
//# sourceMappingURL=default~authorizer-authorizer-module~issuer-issuer-module~superadmin-superadmin-module.js.map