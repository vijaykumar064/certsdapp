(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["kyc-kyc-module"],{

/***/ "./src/app/dialogs/dialog/dialog.component.css":
/*!*****************************************************!*\
  !*** ./src/app/dialogs/dialog/dialog.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".overlay {\n  position: fixed;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background-color: rgba(0, 0, 0, 0.5);\n  z-index: 9999;\n}\n\n.dialog {\n  z-index: 99999;\n  position: fixed;\n  right: 0;\n  left: 0;\n  top: 20px;\n  margin-right: auto;\n  margin-left: auto;\n  min-height: 200px;\n  width: 90%;\n  max-width: 580px;\n  background-color: #fff;\n  padding: 0px;\n  box-shadow: 0 7px 8px -4px rgba(0, 0, 0, 0.2), 0 13px 19px 2px rgba(0, 0, 0, 0.14), 0 5px 24px 4px rgba(0, 0, 0, 0.12);\n}\n\n@media (min-width: 768px) {\n  .dialog {\n    top: 40px;\n  }\n}\n\n.dialog__close-btn {\n  border: 0;\n  background: none;\n  color: #2d2d2d;\n  position: absolute;\n  top: 8px;\n  right: 8px;\n  font-size: 1.2em;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGlhbG9ncy9kaWFsb2cvZGlhbG9nLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFlO0VBQ2YsTUFBTTtFQUNOLFNBQVM7RUFDVCxPQUFPO0VBQ1AsUUFBUTtFQUNSLG9DQUFvQztFQUNwQyxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLFFBQVE7RUFDUixPQUFPO0VBQ1AsU0FBUztFQUNULGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsc0JBQXNCO0VBQ3RCLFlBQVk7RUFDWixzSEFBc0g7QUFDeEg7O0FBR0E7RUFDRTtJQUNFLFNBQVM7RUFDWDtBQUNGOztBQUVBO0VBQ0UsU0FBUztFQUNULGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVO0VBQ1YsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvZGlhbG9ncy9kaWFsb2cvZGlhbG9nLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIub3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAwO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gIHotaW5kZXg6IDk5OTk7XG59XG5cbi5kaWFsb2cge1xuICB6LWluZGV4OiA5OTk5OTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICByaWdodDogMDtcbiAgbGVmdDogMDtcbiAgdG9wOiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtaW4taGVpZ2h0OiAyMDBweDtcbiAgd2lkdGg6IDkwJTtcbiAgbWF4LXdpZHRoOiA1ODBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgcGFkZGluZzogMHB4O1xuICBib3gtc2hhZG93OiAwIDdweCA4cHggLTRweCByZ2JhKDAsIDAsIDAsIDAuMiksIDAgMTNweCAxOXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMTQpLCAwIDVweCAyNHB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xufVxuXG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuZGlhbG9nIHtcbiAgICB0b3A6IDQwcHg7XG4gIH1cbn1cblxuLmRpYWxvZ19fY2xvc2UtYnRuIHtcbiAgYm9yZGVyOiAwO1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBjb2xvcjogIzJkMmQyZDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDhweDtcbiAgcmlnaHQ6IDhweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/dialogs/dialog/dialog.component.html":
/*!******************************************************!*\
  !*** ./src/app/dialogs/dialog/dialog.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@dialog] *ngIf=\"visible\" class=\"dialog\" >\n  <ng-content></ng-content>\n  <!-- <button *ngIf=\"closable\" (click)=\"close()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button> -->\n</div>\n<!-- <div *ngIf=\"visible\" class=\"overlay\" (click)=\"close()\"></div> -->"

/***/ }),

/***/ "./src/app/dialogs/dialog/dialog.component.ts":
/*!****************************************************!*\
  !*** ./src/app/dialogs/dialog/dialog.component.ts ***!
  \****************************************************/
/*! exports provided: DialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DialogComponent", function() { return DialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");



// @Component({
//   selector: 'app-dialog',
//   templateUrl: './dialog.component.html',
//   styleUrls: ['./dialog.component.css']
// })
var DialogComponent = /** @class */ (function () {
    function DialogComponent() {
        this.closable = true;
        this.visibleChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    DialogComponent.prototype.ngOnInit = function () {
    };
    DialogComponent.prototype.close = function () {
        this.visible = false;
        this.visibleChange.emit(this.visible);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DialogComponent.prototype, "closable", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Boolean)
    ], DialogComponent.prototype, "visible", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], DialogComponent.prototype, "visibleChange", void 0);
    DialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dialog',
            template: __webpack_require__(/*! ./dialog.component.html */ "./src/app/dialogs/dialog/dialog.component.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('dialog', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('void => *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'scale3d(.3, .3, .3)' }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(100)
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => void', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])(100, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ transform: 'scale3d(.0, .0, .0)' }))
                    ])
                ])
            ],
            styles: [__webpack_require__(/*! ./dialog.component.css */ "./src/app/dialogs/dialog/dialog.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DialogComponent);
    return DialogComponent;
}());



/***/ }),

/***/ "./src/app/layout/kyc/kyc-routing.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/kyc/kyc-routing.module.ts ***!
  \**************************************************/
/*! exports provided: KycRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KycRoutingModule", function() { return KycRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _kyc_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./kyc.component */ "./src/app/layout/kyc/kyc.component.ts");




var routes = [{
        path: '', component: _kyc_component__WEBPACK_IMPORTED_MODULE_3__["KycComponent"]
    }];
var KycRoutingModule = /** @class */ (function () {
    function KycRoutingModule() {
    }
    KycRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], KycRoutingModule);
    return KycRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/kyc/kyc.component.html":
/*!***********************************************!*\
  !*** ./src/app/layout/kyc/kyc.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\" style=\"margin-left: 3%;\">\n  <div *ngIf=\"documentUploadModal\" class=\"overlay\" (click)=\"closeDocumentUploadModal()\"></div>    \n  <div class=\"KYC\">\n    <div class=\"Rectangle-820\">\n      <p class=\"KYC-Documents\">KYC Documents</p>\n      <mat-grid-list cols=\"3\" rowHeight=\"1.5:0.5\">\n        <mat-grid-tile *ngFor=\"let doc of kycDocumentTypes;let docloopindex=index\">\n          <div class=\"Rectangle-900\" *ngFor=\"let docMeta of kycDocumentTypes[docloopindex]['kycDocumentMetas']; let docMetaloopindex=index\">\n            <p class=\"Aadhar\">{{docMeta['documentName']}}</p>\n            <div class=\"row\" *ngIf=\"docMeta['kycUserDocument']===null\">\n            <p class=\"Status-Not-uploaded\">Status : Not Uploaded</p>\n            </div>\n            <div class=\"row\" *ngIf=\"docMeta['kycUserDocument']!==null\"> \n                <span class=\"Status-progress\">Status:</span>\n                <ol class=\"wizard-progress clearfix\">                  \n                    <li class=\"active-step\">                        \n                        <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                        <span class=\"step-name\">\n                          Upload\n                        </span>\n                    </li>\n                    <li [ngClass]=\"{'active-step':paymentTransaction!=null}\">\n                        <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                        <span class=\"step-name\">Pay</span>\n                    </li>                   \n                    <li [ngClass]=\"{'active-step':verificationTransaction!=null}\">                        \n                        <span class=\"visuallyhidden\">Step </span><span class=\"step-num\">&nbsp;</span>\n                        <span class=\"step-name\">Verify</span>\n                    </li>\n                </ol>\n            </div>\n            <button class=\"Rectangle-905 Upload\" [ngClass]=\"{'active-btn':docMeta['kycUserDocument']==null}\" style=\"text-align:center\" (click)=\"getDocumentByDocumentMetaIndex(docloopindex, docMetaloopindex,'upload')\">Upload</button>\n            <button class=\"Rectangle-906 Pay\" style=\"text-align:center\" [ngClass]=\"{'active-btn':docMeta['kycUserDocument']!=null && docMeta['kycUserDocument']['paymentTransaction']==null}\" (click)=\"getDocumentByDocumentMetaIndex(docloopindex, docMetaloopindex,'pay')\">Pay</button>\n            <button class=\"Rectangle-907 Verify\" style=\"text-align:center\" [ngClass]=\"{'active-btn':docMeta['kycUserDocument']!=null && docMeta['kycUserDocument']['paymentTransaction']!=null && docMeta['kycUserDocument']['verificationTransaction']==null}\" (click)=\"getDocumentByDocumentMetaIndex(docloopindex, docMetaloopindex,'verify')\">Verify</button>\n          </div>\n        </mat-grid-tile>\n      </mat-grid-list>\n    </div>\n  </div>\n</div>\n\n<!-- Upload Popup -->\n<app-dialog [(visible)]=\"documentUploadModal\">\n  <div class=\"popup_dialouge poup_edit\">\n    <h5>UPLOAD <span class=\"semi-bold\">DOCUMENT</span><button (click)=\"closeDocumentUploadModal()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button></h5>\n    <form role=\"form\" #documentForm=\"ngForm\">\n      <div class=\"form-group-attached\">\n        <div *ngFor='let field of formFields'>          \n          <label [for]='field.label'>{{field.label}}</label>:\n          <input *ngIf=\"field.jspattern!='NA'\" class=\"form-control\" [type]='field.type' [name]='field.name' [(ngModel)]='field.value'/>           \n          <input *ngIf=\"field.jspattern=='NA'\" class=\"form-control\" [type]='field.type' [name]='field.name' [(ngModel)]='field.value'/> \n          <div *ngIf=\"fieldsDefaultErrors[field.name]\" class=\"error\">\n                <div>{{field.label}} is required</div>\n          </div> \n          <div *ngIf=\"fieldsInValidDataErrors[field.name]\" class=\"error\">\n                <div>{{field.validationErrorMessage}}</div>\n          </div>\n          <div *ngIf=\"fieldsMinLengthErrors[field.name]\" class=\"error\">\n                <div>Min Length {{field.minlength}} is required</div>\n          </div>\n          <div *ngIf=\"fieldsMaxLengthErrors[field.name]\" class=\"error\">\n                <div>Length is greater than Max Length {{field.maxlength}}. Press Back Button</div>\n          </div>\n        </div>\n        <div>\n          <label>Document Image</label>:\n          <input \n                type=\"file\"\n                accept=\"image/*\" \n                name=\"myfile\" \n                (change)=\"uploadDocumentImage($event)\"\n                required\n                #documentImage                \n          />\n        </div>\n        <div class=\"form-group text-right popup_padding\">\n            <button type=\"submit\" [disabled]=\"documentForm.invalid || !documentImageUploaded\" class=\"btn btn-default changepassword-btn\" (click)=\"saveAndUpdateUserDocument()\">Upload</button>\n        </div>\n      </div>\n    </form>\n  </div>\n</app-dialog>\n\n<!-- Passphrase popup -->\n<app-dialog [(visible)]=\"documentPassphraseModal\">\n  <div class=\"popup_dialouge poup_edit\">\n    <h5>Enter <span class=\"semi-bold\"></span>Passphrase<button (click)=\"closeDocumentPassphraseModal()\" aria-label=\"Close\" class=\"dialog__close-btn\">X</button></h5>\n   <input type=\"password\" [(ngModel)]=\"passphrase\"/>\n   <button (click)=\"kycPayment()\">Done</button>\n   <button (click)=\"closeDocumentPassphraseModal()\">Cancel</button>\n  </div>\n</app-dialog>\n"

/***/ }),

/***/ "./src/app/layout/kyc/kyc.component.scss":
/*!***********************************************!*\
  !*** ./src/app/layout/kyc/kyc.component.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".Rectangle-820 {\n  width: 1550px;\n  height: 780px;\n  border-radius: 5px;\n  border: solid 1px #707070;\n  background-color: #fdfdfd; }\n\n.KYC {\n  width: 1366px;\n  height: 768px;\n  background-color: #ffffff; }\n\n.KYC-Documents {\n  width: 152px;\n  height: 25px;\n  font-family: Muli;\n  font-size: 20px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  margin: 50px 50px 50px 50px;\n  color: #626262; }\n\n.Rectangle-900 {\n  display: inline-block;\n  width: 430px;\n  height: 163.1px;\n  margin: 0px 20px 20px 20px;\n  border-radius: 15px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n  background-color: white !important; }\n\n.Aadhar {\n  width: 200px;\n  height: 18px;\n  font-family: Muli;\n  font-size: 14px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.29;\n  letter-spacing: normal;\n  color: #626262;\n  text-align: center;\n  padding-top: 40px;\n  padding-left: 50px;\n  padding-bottom: 25px; }\n\n.Status-Not-uploaded {\n  width: 400px;\n  height: 20px;\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 300;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #c6c6c6;\n  padding-top: 1px;\n  padding-left: 100px;\n  padding-bottom: 30px; }\n\n.Status-progress {\n  margin-right: 30px;\n  height: 20px;\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 300;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #c6c6c6;\n  padding-top: 1px;\n  padding-left: 100px;\n  padding-bottom: 30px; }\n\n.active-btn {\n  color: #f87d42 !important;\n  font-weight: 800 !important; }\n\n.inactive-btn {\n  color: #626262;\n  font-weight: normal; }\n\n.Upload {\n  width: 49px;\n  height: 18px;\n  font-family: Muli;\n  font-size: 14px;\n  font-weight: normal;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.29;\n  letter-spacing: normal;\n  text-align: left;\n  color: #626262;\n  border-bottom-left-radius: 15px; }\n\n.Pay {\n  width: 20px;\n  height: 19px;\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: normal;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #626262; }\n\n.Verify {\n  width: 40px;\n  height: 19px;\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: normal;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #626262;\n  border-bottom-right-radius: 15px; }\n\n.Rectangle-905 {\n  width: 140px;\n  height: 47.1px;\n  border: solid 1px #ebebeb;\n  background-color: #ffffff; }\n\n.Rectangle-906 {\n  width: 140px;\n  height: 47.1px;\n  border: solid 1px #ebebeb;\n  background-color: #ffffff; }\n\n.Rectangle-907 {\n  width: 150px;\n  height: 47.1px;\n  border: solid 1px #ebebeb;\n  background-color: #ffffff; }\n\n.modal-dialog {\n  max-width: 25%;\n  margin-top: 20%;\n  margin-left: 45%;\n  font-size: 16px; }\n\n.modal-content {\n  border-radius: 20px;\n  font-size: 16px; }\n\n.visuallyhidden {\n  display: none; }\n\n.wizard-progress {\n  list-style: none;\n  list-style-image: none;\n  margin: 0;\n  padding: 0;\n  margin-top: -10px;\n  margin-bottom: 6px;\n  float: left;\n  white-space: nowrap; }\n\n.wizard-progress li {\n  float: left;\n  margin-right: 47px;\n  text-align: center;\n  position: relative;\n  width: 12px; }\n\n.wizard-progress .step-name {\n  display: table-cell;\n  height: 15px;\n  vertical-align: bottom;\n  text-align: center;\n  width: 100px; }\n\n.wizard-progress .step-name-wrapper {\n  display: table-cell;\n  height: 100%;\n  vertical-align: bottom; }\n\n.wizard-progress .step-num {\n  font-size: 14px;\n  font-weight: bold;\n  border: 2px solid  #eaeaea;\n  background-color: #eaeaea !important;\n  border-radius: 50%;\n  width: 20px !important;\n  height: 20px !important;\n  display: inline-block;\n  margin-top: 10px !important; }\n\n.wizard-progress .step-num:after {\n  content: \"\";\n  display: block;\n  background: #ededed !important;\n  height: 2px;\n  width: 40px;\n  position: relative;\n  top: -13px !important;\n  bottom: 5px;\n  left: 18px !important; }\n\n.wizard-progress li:last-of-type .step-num:after {\n  display: none; }\n\n.wizard-progress .active-stepAuth1 .step-num {\n  background-color: yellow !important;\n  border-color: yellow !important; }\n\n.wizard-progress .active-step .step-num {\n  background-color: #4879ff !important;\n  border-color: #b9ccff !important; }\n\n.wizard-progress .active-stepIssued .step-num {\n  background-color: #58cb5a !important;\n  border-color: #b1ffb2 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2t5Yy9reWMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2IsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQix5QkFBeUI7RUFDekIseUJBQXlCLEVBQUE7O0FBRzNCO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYix5QkFBeUIsRUFBQTs7QUFHM0I7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsMkJBQTJCO0VBQzNCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxxQkFBcUI7RUFDckIsWUFBWTtFQUNaLGVBQWU7RUFDZiwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBR25CLDJDQUEyQztFQUMzQyxrQ0FBa0MsRUFBQTs7QUFHcEM7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsb0JBQW9CLEVBQUE7O0FBR3RCO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLG9CQUFvQixFQUFBOztBQUd0QjtFQUVJLGtCQUFpQjtFQUNqQixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixvQkFBb0IsRUFBQTs7QUFHeEI7RUFFRSx5QkFBeUI7RUFDekIsMkJBQTJCLEVBQUE7O0FBRzdCO0VBRUUsY0FBYztFQUNkLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLFdBQVc7RUFDWCxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsK0JBQStCLEVBQUE7O0FBR2pDO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFHaEI7RUFDRSxXQUFXO0VBQ1gsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGdDQUFnQyxFQUFBOztBQUdsQztFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHlCQUF5QixFQUFBOztBQUczQjtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHlCQUF5QixFQUFBOztBQUczQjtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLHlCQUF5QixFQUFBOztBQUczQjtFQUNFLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWMsRUFBQTs7QUFFZDtFQUNBLG1CQUFtQjtFQUNuQixlQUFjLEVBQUE7O0FBR2Q7RUFDRSxhQUFhLEVBQUE7O0FBR2Y7RUFDRSxnQkFBZ0I7RUFDaEIsc0JBQXNCO0VBQ3RCLFNBQVM7RUFDVCxVQUFVO0VBQ1YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsbUJBQW1CLEVBQUE7O0FBSXJCO0VBQ0UsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTs7QUFHYjtFQUNFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixZQUFZLEVBQUE7O0FBSWQ7RUFDRSxtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLHNCQUFzQixFQUFBOztBQUd4QjtFQUNFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsMEJBQTBCO0VBQzFCLG9DQUFvQztFQUdwQyxrQkFBa0I7RUFDbEIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixxQkFBcUI7RUFDckIsMkJBQTJCLEVBQUE7O0FBRzdCO0VBQ0UsV0FBVztFQUNYLGNBQWM7RUFDZCw4QkFBOEI7RUFDOUIsV0FBVztFQUNYLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIscUJBQW9CO0VBQ3BCLFdBQVc7RUFDWCxxQkFBcUIsRUFBQTs7QUFHdkI7RUFDRSxhQUFhLEVBQUE7O0FBSWY7RUFDRSxtQ0FBbUM7RUFDbkMsK0JBQStCLEVBQUE7O0FBRWpDO0VBQ0Usb0NBQW9DO0VBQ3BDLGdDQUFnQyxFQUFBOztBQUdsQztFQUNFLG9DQUFvQztFQUNwQyxnQ0FBZ0MsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9reWMva3ljLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLlJlY3RhbmdsZS04MjAge1xuICB3aWR0aDogMTU1MHB4O1xuICBoZWlnaHQ6IDc4MHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJvcmRlcjogc29saWQgMXB4ICM3MDcwNzA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZGZkZmQ7XG59XG5cbi5LWUMge1xuICB3aWR0aDogMTM2NnB4O1xuICBoZWlnaHQ6IDc2OHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuXG4uS1lDLURvY3VtZW50cyB7XG4gIHdpZHRoOiAxNTJweDtcbiAgaGVpZ2h0OiAyNXB4O1xuICBmb250LWZhbWlseTogTXVsaTtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1hcmdpbjogNTBweCA1MHB4IDUwcHggNTBweDtcbiAgY29sb3I6ICM2MjYyNjI7XG59XG5cbi5SZWN0YW5nbGUtOTAwIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogNDMwcHg7XG4gIGhlaWdodDogMTYzLjFweDtcbiAgbWFyZ2luOiAwcHggMjBweCAyMHB4IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIC8vYm9yZGVyOiAxcHggc29saWQ7XG4gIC8vYm9yZGVyLWNvbG9yOiBibHVlO1xuICBib3gtc2hhZG93OiAwIDNweCA2cHggMCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG4uQWFkaGFyIHtcbiAgd2lkdGg6IDIwMHB4O1xuICBoZWlnaHQ6IDE4cHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICBsaW5lLWhlaWdodDogMS4yOTtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgY29sb3I6ICM2MjYyNjI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDQwcHg7XG4gIHBhZGRpbmctbGVmdDogNTBweDtcbiAgcGFkZGluZy1ib3R0b206IDI1cHg7XG59XG5cbi5TdGF0dXMtTm90LXVwbG9hZGVkIHtcbiAgd2lkdGg6IDQwMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogI2M2YzZjNjtcbiAgcGFkZGluZy10b3A6IDFweDtcbiAgcGFkZGluZy1sZWZ0OiAxMDBweDtcbiAgcGFkZGluZy1ib3R0b206IDMwcHg7XG59XG5cbi5TdGF0dXMtcHJvZ3Jlc3NcbntcbiAgICBtYXJnaW4tcmlnaHQ6MzBweDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgZm9udC1mYW1pbHk6IE11bGk7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBjb2xvcjogI2M2YzZjNjtcbiAgICBwYWRkaW5nLXRvcDogMXB4O1xuICAgIHBhZGRpbmctbGVmdDogMTAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDMwcHg7XG59XG5cbi5hY3RpdmUtYnRuXG57XG4gIGNvbG9yOiAjZjg3ZDQyICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA4MDAgIWltcG9ydGFudDtcbn1cblxuLmluYWN0aXZlLWJ0blxue1xuICBjb2xvcjogIzYyNjI2MjtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuLlVwbG9hZCB7XG4gIHdpZHRoOiA0OXB4O1xuICBoZWlnaHQ6IDE4cHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI5O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzYyNjI2MjtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTVweDtcbn1cblxuLlBheSB7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDE5cHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzYyNjI2Mjtcbn1cblxuLlZlcmlmeSB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDE5cHg7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzYyNjI2MjtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDE1cHg7XG59XG5cbi5SZWN0YW5nbGUtOTA1IHtcbiAgd2lkdGg6IDE0MHB4O1xuICBoZWlnaHQ6IDQ3LjFweDtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2ViZWJlYjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbn1cblxuLlJlY3RhbmdsZS05MDYge1xuICB3aWR0aDogMTQwcHg7XG4gIGhlaWdodDogNDcuMXB4O1xuICBib3JkZXI6IHNvbGlkIDFweCAjZWJlYmViO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuXG4uUmVjdGFuZ2xlLTkwNyB7XG4gIHdpZHRoOiAxNTBweDtcbiAgaGVpZ2h0OiA0Ny4xcHg7XG4gIGJvcmRlcjogc29saWQgMXB4ICNlYmViZWI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG59XG5cbi5tb2RhbC1kaWFsb2cge1xuICBtYXgtd2lkdGg6IDI1JTtcbiAgbWFyZ2luLXRvcDogMjAlO1xuICBtYXJnaW4tbGVmdDogNDUlO1xuICBmb250LXNpemU6MTZweDtcbiAgfVxuICAubW9kYWwtY29udGVudHtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgZm9udC1zaXplOjE2cHg7XG4gIH1cblxuICAudmlzdWFsbHloaWRkZW4ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3Mge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgbGlzdC1zdHlsZS1pbWFnZTogbm9uZTtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA2cHg7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBcbiAgfVxuICBcbiAgLndpemFyZC1wcm9ncmVzcyBsaSB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXJpZ2h0OiA0N3B4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgd2lkdGg6IDEycHg7XG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgLnN0ZXAtbmFtZSB7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMHB4O1xuICAgIFxuICB9XG4gIFxuICAud2l6YXJkLXByb2dyZXNzIC5zdGVwLW5hbWUtd3JhcHBlciB7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTsgICAgXG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgLnN0ZXAtbnVtIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgYm9yZGVyOiAycHggc29saWQgICNlYWVhZWE7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VhZWFlYSAhaW1wb3J0YW50O1xuICAgIC8vIGJvcmRlci1jb2xvcjogI2VhZWFlYSAhaW1wb3J0YW50O1xuICAgIC8vIGJvcmRlcjogM3B4IHNvbGlkICMwMDA7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIHdpZHRoOiAyMHB4ICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcbiAgfVxuICBcbiAgLndpemFyZC1wcm9ncmVzcyAuc3RlcC1udW06YWZ0ZXIge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgYmFja2dyb3VuZDogI2VkZWRlZCAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMnB4O1xuICAgIHdpZHRoOiA0MHB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6LTEzcHggIWltcG9ydGFudDtcbiAgICBib3R0b206IDVweDtcbiAgICBsZWZ0OiAxOHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgbGk6bGFzdC1vZi10eXBlIC5zdGVwLW51bTphZnRlciB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxuICBcbiAgXG4gIC53aXphcmQtcHJvZ3Jlc3MgLmFjdGl2ZS1zdGVwQXV0aDEgLnN0ZXAtbnVtIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB5ZWxsb3cgIWltcG9ydGFudDtcbiAgICBib3JkZXItY29sb3I6IHllbGxvdyAhaW1wb3J0YW50O1xuICB9XG4gIC53aXphcmQtcHJvZ3Jlc3MgLmFjdGl2ZS1zdGVwIC5zdGVwLW51bSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzQ4NzlmZiAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1jb2xvcjogI2I5Y2NmZiAhaW1wb3J0YW50O1xuICB9XG4gIFxuICAud2l6YXJkLXByb2dyZXNzIC5hY3RpdmUtc3RlcElzc3VlZCAuc3RlcC1udW0ge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM1OGNiNWEgIWltcG9ydGFudDtcbiAgICBib3JkZXItY29sb3I6ICNiMWZmYjIgIWltcG9ydGFudDtcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/layout/kyc/kyc.component.ts":
/*!*********************************************!*\
  !*** ./src/app/layout/kyc/kyc.component.ts ***!
  \*********************************************/
/*! exports provided: KycComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KycComponent", function() { return KycComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var src_app_services_kyc_kyc_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/kyc/kyc.service */ "./src/app/services/kyc/kyc.service.ts");





var KycComponent = /** @class */ (function () {
    function KycComponent(route, kycserv, local) {
        this.route = route;
        this.kycserv = kycserv;
        this.local = local;
        this.documentUpload = false;
        this.display = 'none';
        this.documentUploadModal = false;
        this.documentSearchCriteria = {};
        this.documentPaymentCriteria = {};
        this.documentVerifyCriteria = {};
        this.formFields = [];
        this.documentImageUploaded = false;
        this.fieldsDefaultErrors = [];
        this.fieldMetaDataValidationFlag = false;
        this.fieldsInValidDataErrors = [];
        this.fieldsMinLengthErrors = [];
        this.fieldsMaxLengthErrors = [];
        this.fieldsElementDataInvalidErrors = [];
        this.documentPassphraseModal = false;
        this.token = this.local.getBelriumToken();
        this.bkvsdm_token = this.local.getBKVSToken();
    }
    KycComponent.prototype.getDocumentByDocumentMetaIndex = function (documentTypeLoopIndex, documentMetaLoopIndex, type) {
        if (type == 'upload') {
            console.log("this.kycDocumentTypes" + JSON.stringify(this.kycDocumentTypes));
            console.log("DocumentTypes" + documentTypeLoopIndex + ";" + documentMetaLoopIndex);
            this.documentSearchCriteria['kycDocumentTypeId'] = this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentTypeId'];
            this.documentSearchCriteria['kycDocumentMetaId'] = this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentMetas'][documentMetaLoopIndex]['kycDocumentMetaId'];
            this.getSelectedDocumentSchema(this.documentSearchCriteria);
        }
        else if (type == 'pay') {
            this.documentPaymentCriteria['kycUserDocumentID'] = this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentMetas'][documentMetaLoopIndex]['kycUserDocument']['kycUserDocumentID'];
            this.documentPaymentCriteria['CountryCode'] = this.local.getCountryName();
            console.log("pay userid: " + this.documentPaymentCriteria['kycUserDocumentID']);
            this.openDocumentPassphraseModal();
        }
        else if (type == 'verify') {
            console.log("verify");
        }
    };
    KycComponent.prototype.openDocumentUploadModal = function () {
        this.documentForm.reset();
        this.documentUploadModal = true;
    };
    KycComponent.prototype.closeDocumentUploadModal = function () {
        this.documentUploadModal = false;
        this.documentImageUploaded = false;
    };
    KycComponent.prototype.getSelectedDocumentSchema = function (documentSearchCriteria) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var metaFormFieldRes;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.documentImageUploaded = false;
                        this.documentImage.nativeElement.value = "";
                        return [4 /*yield*/, this.kycserv.getMetaDataFormFields(documentSearchCriteria)];
                    case 1:
                        metaFormFieldRes = _a.sent();
                        console.log("metaFormFieldRes: " + JSON.stringify(metaFormFieldRes));
                        if (metaFormFieldRes) {
                            this.formFields = metaFormFieldRes['data']['formproperties'];
                            console.log(this.formFields);
                            // this.unblockUserInterface();      
                            this.openDocumentUploadModal();
                            this.documentImageUploaded = false;
                            this.documentImage.nativeElement.value = "";
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    KycComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var kycres, kycData, kycDataCountries, countries, countryCode, localKycStatus, BlockchainKycStatus, error_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.kycserv.fetchKycDocument(this.token)];
                    case 1:
                        kycres = _a.sent();
                        if (kycres['isSuccess'] === true) {
                            console.log(JSON.stringify(kycres));
                            console.log(kycres['isSuccess']);
                            kycData = kycres['data'];
                            kycDataCountries = kycData['countries'];
                            countries = kycDataCountries[0];
                            console.log("countries: " + JSON.stringify(countries));
                            countryCode = countries['countryCode'];
                            console.log("country code: " + countryCode);
                            this.documentSearchCriteria['countryCode'] = countryCode;
                            localKycStatus = countries.status;
                            BlockchainKycStatus = countries.blockchainStatus;
                            console.log(countryCode);
                            console.log(localKycStatus + " " + BlockchainKycStatus);
                            //if local and blockchain status both are active or 
                            //local is inactive enable kyc btn must be disabled
                            this.kycDocumentTypes = countries.kycDocumentTypes;
                            console.log(this.kycDocumentTypes);
                            this.local.setCountryName(countryCode);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        alert(error_1.message);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    KycComponent.prototype.uploadKycDoc = function (typeId, metaId) {
        this.documentUploadModal = true;
        this.documentSearchCriteria['kycDocumentMetaId'] = metaId;
        this.documentSearchCriteria['kycDocumentTypeId'] = typeId;
        this.documentSearchCriteria['countryCode'] = this.local.getCountryName();
        this.documentSearchCriteria['metaData'] = {};
    };
    KycComponent.prototype.uploadDocumentImage = function (event) {
        console.log("--event--", event);
        this.documentImageUploaded = true;
        var documentImagePath = { "file": event.target.files[0] };
        var documentImageFile = event.target.files[0];
        this.documentImageFormData = new FormData();
        this.documentImageFormData.append('file', documentImageFile, documentImageFile.name);
        // this.documentImageFormData.append("file", documentImageFile);
        this.documentImageFormData.append("countryCode", this.documentSearchCriteria['countryCode']);
        this.documentImageFormData.append("kycDocumentMetaId", this.documentSearchCriteria['kycDocumentMetaId']);
        this.documentImageFormData.append("kycDocumentTypeId", this.documentSearchCriteria['kycDocumentTypeId']);
    };
    KycComponent.prototype.saveAndUpdateUserDocument = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.readUploadedDocumentData();
                        this.documentUploadModal = false;
                        return [4 /*yield*/, this.saveUserDocument(this.documentImageFormData)];
                    case 1:
                        response = _a.sent();
                        console.log("saved " + response);
                        return [2 /*return*/];
                }
            });
        });
    };
    KycComponent.prototype.saveUserDocument = function (userDocumentFormData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response, error_2;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.kycserv.kycUplaodFile(userDocumentFormData)];
                    case 1:
                        response = _a.sent();
                        console.log("upload response: " + response);
                        return [2 /*return*/, response];
                    case 2:
                        error_2 = _a.sent();
                        alert(error_2.message);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    KycComponent.prototype.readUploadedDocumentData = function () {
        console.log(this.documentSearchCriteria);
        // this.userDocumentData['countryCode']=this.documentSearchCriteria['countryCode'];
        // this.userDocumentData['kycDocumentMetaId']=this.documentSearchCriteria['kycDocumentMetaId'];
        // this.userDocumentData['kycDocumentTypeId']=this.documentSearchCriteria['kycDocumentTypeId'];
        var metaData = {};
        for (var index in this.formFields) {
            metaData[this.formFields[index]['name']] = this.formFields[index]['value'];
        }
        //this.userDocumentData['metaData']=JSON.stringify(metaData);
        this.documentImageFormData.append("metaData", JSON.stringify(metaData));
    };
    KycComponent.prototype.openDocumentPassphraseModal = function () {
        this.documentPassphraseModal = true;
    };
    KycComponent.prototype.kycPayment = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var publickeyRes;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.documentPaymentCriteria['passphrase'] = this.passphrase;
                        return [4 /*yield*/, this.kycserv.getPublicKey(this.passphrase)];
                    case 1:
                        publickeyRes = _a.sent();
                        console.log("public key: " + publickeyRes['account']['publicKey']);
                        return [2 /*return*/];
                }
            });
        });
    };
    KycComponent.prototype.closeDocumentPassphraseModal = function () {
        this.documentPassphraseModal = false;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("documentForm"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], KycComponent.prototype, "documentForm", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('documentImage'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], KycComponent.prototype, "documentImage", void 0);
    KycComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-kyc',
            template: __webpack_require__(/*! ./kyc.component.html */ "./src/app/layout/kyc/kyc.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./kyc.component.scss */ "./src/app/layout/kyc/kyc.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_app_services_kyc_kyc_service__WEBPACK_IMPORTED_MODULE_4__["KycService"], src_app_services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"]])
    ], KycComponent);
    return KycComponent;
}());



/***/ }),

/***/ "./src/app/layout/kyc/kyc.module.ts":
/*!******************************************!*\
  !*** ./src/app/layout/kyc/kyc.module.ts ***!
  \******************************************/
/*! exports provided: KycModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KycModule", function() { return KycModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _kyc_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./kyc.component */ "./src/app/layout/kyc/kyc.component.ts");
/* harmony import */ var _kyc_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./kyc-routing.module */ "./src/app/layout/kyc/kyc-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _dialogs_dialog_dialog_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../dialogs/dialog/dialog.component */ "./src/app/dialogs/dialog/dialog.component.ts");









var KycModule = /** @class */ (function () {
    function KycModule() {
    }
    KycModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_kyc_component__WEBPACK_IMPORTED_MODULE_3__["KycComponent"], _dialogs_dialog_dialog_component__WEBPACK_IMPORTED_MODULE_8__["DialogComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _kyc_routing_module__WEBPACK_IMPORTED_MODULE_4__["KycRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["FlexLayoutModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatGridListModule"]
            ]
        })
    ], KycModule);
    return KycModule;
}());



/***/ }),

/***/ "./src/app/services/kyc/kyc.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/kyc/kyc.service.ts ***!
  \*********************************************/
/*! exports provided: KycService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KycService", function() { return KycService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");












var KycService = /** @class */ (function () {
    function KycService(http, local, requestOptions) {
        this.http = http;
        this.local = local;
        this.requestOptions = requestOptions;
        this.FetchKycDocUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SWAGGERAPI + 'users/document';
        this.MetaDataFormFieldsUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SWAGGERAPI + 'kycdocs/kycdocformfieldmetas';
        this.KycPaymentUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SWAGGERAPI + 'kycuserdocuments/payment';
        this.SendVerificationUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SWAGGERAPI + 'kycuserdocuments/send/verification';
        this.GetPublikeyUrl = 'https://node1.belrium.io/api/accounts/open';
        this.EnableKycUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SWAGGERAPI + 'transactions/enable/account';
        this.kycUploadUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].SWAGGERAPI + 'kycuserdocuments/upload';
    }
    KycService.prototype.fetchKycDocument = function (token) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res, error_1, err;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 4]);
                        options = this.requestOptions.getRequestOptions(token);
                        return [4 /*yield*/, this.http.get(this.FetchKycDocUrl, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        console.log(res.status);
                        console.log(res);
                        return [2 /*return*/, res.json()];
                    case 2:
                        error_1 = _a.sent();
                        err = JSON.parse(error_1._body);
                        alert("Error: " + err['message']);
                        return [4 /*yield*/, this.handleError(error_1)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    KycService.prototype.kycUplaodFile = function (formdata) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var token, bkvsdmToken, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = this.local.getBelriumToken();
                        bkvsdmToken = this.local.getBKVSToken();
                        options = this.requestOptions.getRequestBKVSOptions(token, bkvsdmToken);
                        return [4 /*yield*/, this.http.post(this.kycUploadUrl, formdata, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    KycService.prototype.getMetaDataFormFields = function (metaData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var token, countryCode, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = this.local.getBelriumToken();
                        countryCode = localStorage.getItem('countryCode');
                        options = this.requestOptions.getRequestOptions(token);
                        return [4 /*yield*/, this.http.get(this.MetaDataFormFieldsUrl + '?kycDocumentMetaId=' + metaData['kycDocumentMetaId'] + '&kycDocumentTypeId=' + metaData['kycDocumentTypeId'] + '&countryCode=' + metaData['countryCode'], options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    KycService.prototype.kycPayment = function (kycUserDocumentID, secret) {
        var token = this.local.getBelriumToken();
        var countryCode = localStorage.getItem('countryCode');
        var publicKey = localStorage.getItem('publicKey');
        var data = {
            kycUserDocumentID: kycUserDocumentID,
            publicKey: publicKey,
            secondSecret: "",
            secret: secret,
            senderCountryCode: countryCode
        };
        var options = this.requestOptions.getRequestOptions(token);
        return this.http.put(this.KycPaymentUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    KycService.prototype.sendVerification = function (kycUserDocumentID) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3', 'belrium-token': this.token, 'bkvsdm-token': this.bkvsdm_token });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
        return this.http.post(this.SendVerificationUrl + '?kycUserDocumentID=' + kycUserDocumentID, options).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    KycService.prototype.getPublicKey = function (passPhrase) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var token, countryCode, data, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = this.local.getBelriumToken();
                        countryCode = this.local.getCountryName();
                        data = {
                            secret: passPhrase,
                            countryCode: countryCode
                        };
                        options = this.requestOptions.getRequestOptions(token);
                        return [4 /*yield*/, this.http.post(this.GetPublikeyUrl, data).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    KycService.prototype.doEnableKYC = function (publicKey, secret) {
        var countryCode = localStorage.getItem('countryCode');
        var data = {
            countryCode: countryCode,
            publicKey: publicKey,
            secondSecret: " ",
            secret: secret
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3', 'belrium-token': this.token, });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'put' });
        return this.http.put(this.EnableKycUrl, data).timeout(1000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    KycService.prototype.handleError = function (error) {
        console.error(error);
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().message);
    };
    KycService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_10__["LocalstorageService"], _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_11__["RequestOptionsService"]])
    ], KycService);
    return KycService;
}());



/***/ }),

/***/ "./src/app/services/localstorage/localstorage.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/localstorage/localstorage.service.ts ***!
  \***************************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.setEmail = function (email) {
        localStorage.setItem('email', email);
    };
    LocalstorageService.prototype.getEmail = function () {
        return localStorage.getItem('email');
    };
    LocalstorageService.prototype.setLoggedIn = function (login) {
        localStorage.setItem('isLoggedIn', login);
    };
    LocalstorageService.prototype.getLoggedIn = function () {
        return localStorage.getItem('isLoggedIn');
    };
    // sets belrium token
    LocalstorageService.prototype.setBelriumToken = function (token) {
        localStorage.setItem('token', token);
    };
    //gets belrium token
    LocalstorageService.prototype.getBelriumToken = function () {
        return localStorage.getItem('token');
    };
    // sets bkvs-dm token
    LocalstorageService.prototype.setBKVSToken = function (bkvstoken) {
        localStorage.setItem('bkvs-token', bkvstoken);
    };
    //gets bkvs-dm token
    LocalstorageService.prototype.getBKVSToken = function () {
        return localStorage.getItem('bkvs-token');
    };
    // sets role of an user
    LocalstorageService.prototype.setUserRole = function (role) {
        localStorage.setItem('role', role);
    };
    //gets role of an user
    LocalstorageService.prototype.getUserRole = function () {
        return localStorage.getItem('role');
    };
    // sets wallet balance
    LocalstorageService.prototype.setBalance = function (balance) {
        localStorage.setItem('balance', balance);
    };
    //gets wallet balance
    LocalstorageService.prototype.getBalance = function () {
        return localStorage.getItem('balance');
    };
    // sets country of an user
    LocalstorageService.prototype.setCountryName = function (countryName) {
        localStorage.setItem('countryName', countryName);
    };
    //gets country of an user
    LocalstorageService.prototype.getCountryName = function () {
        return localStorage.getItem('countryName');
    };
    //sets dapp id for the user
    LocalstorageService.prototype.setDappId = function (dappid) {
        localStorage.setItem('dappid', dappid);
    };
    //gets dapp id for the suer
    LocalstorageService.prototype.getDappId = function () {
        return localStorage.getItem('dappid');
    };
    LocalstorageService.prototype.setKycStatus = function (kycstatus) {
        localStorage.setItem('kycstatus', kycstatus);
    };
    LocalstorageService.prototype.getKycStatus = function () {
        return localStorage.getItem('kycstatus');
    };
    LocalstorageService.prototype.setWalletAddress = function (address) {
        localStorage.setItem('address', address);
    };
    LocalstorageService.prototype.getWalletAddress = function () {
        return localStorage.getItem('address');
    };
    LocalstorageService.prototype.setOrganization = function (organization) {
        localStorage.setItem('organization', organization);
    };
    LocalstorageService.prototype.getOrganization = function () {
        return localStorage.getItem('organization');
    };
    LocalstorageService.prototype.setUserName = function (name) {
        localStorage.setItem('name', name);
    };
    LocalstorageService.prototype.getUserName = function () {
        return localStorage.getItem('name');
    };
    LocalstorageService.prototype.setIssuerId = function (id) {
        localStorage.setItem('issuerid', id);
    };
    LocalstorageService.prototype.getIssuerId = function () {
        return localStorage.getItem('issuerid');
    };
    LocalstorageService.prototype.setIssuerDept = function (dept) {
        localStorage.setItem('issuerDept', dept);
    };
    LocalstorageService.prototype.getIssuerDept = function () {
        return localStorage.getItem('issuerDept');
    };
    LocalstorageService.prototype.setAuthorizerId = function (id) {
        localStorage.setItem('Authorizerid', id);
    };
    LocalstorageService.prototype.getAuthorizerId = function () {
        return localStorage.getItem('Authorizerid');
    };
    LocalstorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ })

}]);
//# sourceMappingURL=kyc-kyc-module.js.map