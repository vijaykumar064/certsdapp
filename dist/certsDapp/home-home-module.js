(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");




var routes = [
    {
        path: '', component: _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
    }
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"home\">\n  <header>\n    <nav class=\"navbar navbar-default\">\n      <div class=\"container-fluid\">\n        <div class=\"navbar-header\">\n          <!-- <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n          <span class=\"icon-bar\"></span>\n          <span class=\"icon-bar\"></span>\n          <span class=\"icon-bar\"></span>                        \n        </button> -->\n\n          <img src=\"../../assets/img/belbooks-logo.svg\">\n        </div>\n        <!-- <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n        <ul class=\"nav navbar-nav\">\n      \n        </ul> -->\n        <!-- <ul class=\"nav navbar-nav navbar-right\" style=\"margin-top: 0%;\"> -->\n        <a [routerLink]=\"['/login']\" [routerLinkActive]=\"['router-link-active']\"\n          style=\"margin-left:85%;margin-top:-10%;\">Login</a>\n        <a [routerLink]=\"['/register']\" [routerLinkActive]=\"['router-link-active']\"\n          style=\"color:#f87d42;font-size:16px;text-decoration: none;margin-left:0%;\">Get Started</a>\n        <!-- </ul> -->\n        <!-- </div> -->\n      </div>\n    </nav>\n  </header>\n  <section class=\"bel_dashboard_sec1\">\n    <div class=\"container-fluid\">\n      <div class=\"row\">\n        <div class=\"col-xl-12 col-lg-12 col-md-12 col-12 nopadding \">\n          <div class=\"col-xl-12 col-lg-12 col-md-12 col-12 nopadding landing-page\">\n            \n            <br><br><br>\n            <!-- <div class=\"col-md-6\" > -->\n            <div class=\"col-md-6 nopadding landing-banner-2\" style=\"float:left\">\n            <img src=\"../../assets/img/CertificateBannerIllus.jpg\" style=\"float:left; margin:30px auto;\">\n          </div>\n          </div>\n          <!-- <div class=\"col-md-6 nopadding landing-banner-1\" style=\"float:right\"> -->\n            \n          <div class=\"landing-banner-text\" style=\"position: absolute;width: 40%;left: 750px;top: 150px;\">\n            <h1 id='titleroll' style=\"display:inline-block;font-size:50px;\">&nbsp;</h1>\n            <p style=\"font-size: 18px;\">Weather you are an expert or a beginner. Belbooks is going to become the first platform which will faster\n              all your needs in a simple, easy and intuitive way</p>\n              <!-- <div class=\"form-group row\"> -->\n                \n                <!-- <div class=\"col-xs-10\"> -->\n                  <!-- <input class=\"form-control\" id=\"ex2\" type=\"text\" placeholder=\"Email Address\" > -->\n                  <input type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"email\" size=\"10\"\n                  placeholder=\"Email Address\" aria-label=\"Recipient's username\" aria-describedby=\"basic-addon2\">\n                <!-- </div>   -->\n                <button class=\"btn btn-primary\" type=\"button\" (click)=\"checkExists()\"\n              style=\"background: #102654  ;color:#ffffff;\">PROCEED</button>              \n              <!-- </div> -->\n              \n<!-- \n            <input type=\"text\" class=\"form-control\" id=\"email\" name=\"email\" [(ngModel)]=\"email\" size=\"10\"\n              placeholder=\"Email Address\" aria-label=\"Recipient's username\" aria-describedby=\"basic-addon2\">\n            <button class=\"btn btn-primary\" type=\"button\" (click)=\"checkExists()\"\n              style=\"background: #102654  ;color:#ffffff;\">PROCEED</button> -->\n              <br><br>\n              <h6 style=\"padding-bottom:0px;padding-top: 0px; text-align:left; color:red\" id=\"p1\"></h6>\n              <h5 style=\"font-size: 16px;\">Already using BelCerts? <a href=\"#\" style=\"color:#f87d42;text-decoration: none;\"> Sign in </a></h5>\n\n          </div>\n         \n        </div>\n      </div>\n    </div>\n\n<!-- </div>\n</div> -->\n</section>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/home/home.component.scss":
/*!******************************************!*\
  !*** ./src/app/home/home.component.scss ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".home {\n  font-size: 18px; }\n\n.btn-outline-secondary {\n  width: 168.7px;\n  height: 47.4px;\n  background-color: var(--dark-slate-blue); }\n\n.btn-primary {\n  width: 168.7px;\n  height: 47.4px;\n  background-color: var(--dark-slate-blue);\n  font-size: 16px;\n  border-radius: 4px;\n  margin-top: -78px;\n  margin-left: 44%; }\n\n.form-control {\n  width: 300px;\n  height: 47.4px;\n  border-radius: 5px;\n  border: solid 1px #dbdbdb;\n  background-color: var(--white);\n  font-size: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBYyxFQUFBOztBQUVoQjtFQUNFLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsd0NBQXdDLEVBQUE7O0FBRTFDO0VBQ0UsY0FBYztFQUNkLGNBQWM7RUFDZCx3Q0FBd0M7RUFDeEMsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLDhCQUE4QjtFQUM5QixlQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaG9tZXtcbiAgZm9udC1zaXplOjE4cHg7XG59XG4uYnRuLW91dGxpbmUtc2Vjb25kYXJ5e1xuICB3aWR0aDogMTY4LjdweDtcbiAgaGVpZ2h0OiA0Ny40cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWRhcmstc2xhdGUtYmx1ZSk7XG59XG4uYnRuLXByaW1hcnl7XG4gIHdpZHRoOiAxNjguN3B4O1xuICBoZWlnaHQ6IDQ3LjRweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tZGFyay1zbGF0ZS1ibHVlKTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIG1hcmdpbi10b3A6IC03OHB4O1xuICBtYXJnaW4tbGVmdDogNDQlOyBcblxufVxuLmZvcm0tY29udHJvbHtcbiAgd2lkdGg6IDMwMHB4O1xuICBoZWlnaHQ6IDQ3LjRweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3JkZXI6IHNvbGlkIDFweCAjZGJkYmRiO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIGZvbnQtc2l6ZToxNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_user_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user/user.service */ "./src/app/services/user/user.service.ts");
/* harmony import */ var _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var HomeComponent = /** @class */ (function () {
    function HomeComponent(userService, local, router) {
        this.userService = userService;
        this.local = local;
        this.router = router;
    }
    // Check if user already logged in
    HomeComponent.prototype.ngOnInit = function () {
        var text1 = ['Decentralized', 'Digital Certification'];
        var i = 0;
        var doit = true;
        var interval;
        var interval2;
        var scrollElement = document.getElementById('titleroll');
        function printit(printedText) {
            var size = 0;
            function close() {
                clearInterval(interval);
            }
            interval = setInterval(function () {
                if (doit) {
                    if (size == printedText.length) {
                        doit = false;
                        interval2 = setInterval(function () {
                            if (scrollElement.innerText.length != 1) {
                                scrollElement.innerText = scrollElement.innerText.slice(0, -1);
                            }
                            else {
                                doit = true;
                                caller();
                                close();
                                close1();
                                return;
                            }
                            function close1() {
                                clearInterval(interval2);
                            }
                        }, 200);
                    }
                    else {
                        scrollElement.innerText += printedText[size];
                    }
                    size += 1;
                }
            }, 200);
        }
        function caller() {
            setTimeout(function () {
                if (i == text1.length) {
                    i = 0;
                }
                printit(text1[i]);
                i += 1;
            }, 200);
        }
        caller();
    };
    // Initicate login
    HomeComponent.prototype.checkExists = function () {
        var _this = this;
        console.log("HI");
        this.userService.userExist(this.email).subscribe(function (users) {
            if ((users === '0') || (users === '00')) {
                _this.router.navigate(['/login/email/' + _this.email]);
            }
            if (users === '-1') {
                _this.router.navigate(['/register']);
            }
        }, function (error) {
            _this.errorMessage = error;
            if (_this.errorMessage === 'Invalid Username') {
                console.log(_this.errorMessage);
            }
            else {
                console.log(_this.errorMessage);
            }
        });
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            providers: [_services_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"]],
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_user_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"], _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_3__["LocalstorageService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");






var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_3__["HomeRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"]
            ]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/services/localstorage/localstorage.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/localstorage/localstorage.service.ts ***!
  \***************************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.setEmail = function (email) {
        localStorage.setItem('email', email);
    };
    LocalstorageService.prototype.getEmail = function () {
        return localStorage.getItem('email');
    };
    LocalstorageService.prototype.setLoggedIn = function (login) {
        localStorage.setItem('isLoggedIn', login);
    };
    LocalstorageService.prototype.getLoggedIn = function () {
        return localStorage.getItem('isLoggedIn');
    };
    // sets belrium token
    LocalstorageService.prototype.setBelriumToken = function (token) {
        localStorage.setItem('token', token);
    };
    //gets belrium token
    LocalstorageService.prototype.getBelriumToken = function () {
        return localStorage.getItem('token');
    };
    // sets bkvs-dm token
    LocalstorageService.prototype.setBKVSToken = function (bkvstoken) {
        localStorage.setItem('bkvs-token', bkvstoken);
    };
    //gets bkvs-dm token
    LocalstorageService.prototype.getBKVSToken = function () {
        return localStorage.getItem('bkvs-token');
    };
    // sets role of an user
    LocalstorageService.prototype.setUserRole = function (role) {
        localStorage.setItem('role', role);
    };
    //gets role of an user
    LocalstorageService.prototype.getUserRole = function () {
        return localStorage.getItem('role');
    };
    // sets wallet balance
    LocalstorageService.prototype.setBalance = function (balance) {
        localStorage.setItem('balance', balance);
    };
    //gets wallet balance
    LocalstorageService.prototype.getBalance = function () {
        return localStorage.getItem('balance');
    };
    // sets country of an user
    LocalstorageService.prototype.setCountryName = function (countryName) {
        localStorage.setItem('countryName', countryName);
    };
    //gets country of an user
    LocalstorageService.prototype.getCountryName = function () {
        return localStorage.getItem('countryName');
    };
    //sets dapp id for the user
    LocalstorageService.prototype.setDappId = function (dappid) {
        localStorage.setItem('dappid', dappid);
    };
    //gets dapp id for the suer
    LocalstorageService.prototype.getDappId = function () {
        return localStorage.getItem('dappid');
    };
    LocalstorageService.prototype.setKycStatus = function (kycstatus) {
        localStorage.setItem('kycstatus', kycstatus);
    };
    LocalstorageService.prototype.getKycStatus = function () {
        return localStorage.getItem('kycstatus');
    };
    LocalstorageService.prototype.setWalletAddress = function (address) {
        localStorage.setItem('address', address);
    };
    LocalstorageService.prototype.getWalletAddress = function () {
        return localStorage.getItem('address');
    };
    LocalstorageService.prototype.setOrganization = function (organization) {
        localStorage.setItem('organization', organization);
    };
    LocalstorageService.prototype.getOrganization = function () {
        return localStorage.getItem('organization');
    };
    LocalstorageService.prototype.setUserName = function (name) {
        localStorage.setItem('name', name);
    };
    LocalstorageService.prototype.getUserName = function () {
        return localStorage.getItem('name');
    };
    LocalstorageService.prototype.setIssuerId = function (id) {
        localStorage.setItem('issuerid', id);
    };
    LocalstorageService.prototype.getIssuerId = function () {
        return localStorage.getItem('issuerid');
    };
    LocalstorageService.prototype.setIssuerDept = function (dept) {
        localStorage.setItem('issuerDept', dept);
    };
    LocalstorageService.prototype.getIssuerDept = function () {
        return localStorage.getItem('issuerDept');
    };
    LocalstorageService.prototype.setAuthorizerId = function (id) {
        localStorage.setItem('Authorizerid', id);
    };
    LocalstorageService.prototype.getAuthorizerId = function () {
        return localStorage.getItem('Authorizerid');
    };
    LocalstorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map