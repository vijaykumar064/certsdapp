(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/services/user/user.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/user/user.service.ts ***!
  \***********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");











var UserService = /** @class */ (function () {
    function UserService(http, requestOptions) {
        this.http = http;
        this.requestOptions = requestOptions;
        this.existUrl = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOME_URL + 'user/exists';
        this.url = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOME_URL + 'user/login/';
        this.hyperledgerLogin = _constants__WEBPACK_IMPORTED_MODULE_9__["Constants"].HOME_URL + 'user/hllogin/';
    }
    UserService.prototype.userExist = function (email) {
        var body = {
            email: email
        };
        // const headers = new Headers({ 'Content-Type': 'application/json',});
        // const header=requestOptions.getDappRequestOptions();
        // const options = new RequestOptions({ headers: headers, method: 'post' });
        var options = this.requestOptions.getDappRequestOptions();
        return this.http.post(this.existUrl, body, options).timeout(300000)
            .map(function (res) {
            console.log("data", res.json());
            return res.json();
        })
            .catch(this.handleError);
    };
    UserService.prototype.doLogin = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3', });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.url, data, options).timeout(300000).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    UserService.prototype.onCheckHyperLedger = function (secret, token) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var data, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = {
                            secret: secret,
                            token: token
                        };
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.hyperledgerLogin, data, options).timeout(300000).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    UserService.prototype.handleError = function (error) {
        console.error(error);
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().message);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _services_requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_10__["RequestOptionsService"]])
    ], UserService);
    return UserService;
}());



/***/ })

}]);
//# sourceMappingURL=common.js.map