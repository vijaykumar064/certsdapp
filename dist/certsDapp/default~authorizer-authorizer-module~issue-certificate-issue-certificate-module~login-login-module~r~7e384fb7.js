(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~authorizer-authorizer-module~issue-certificate-issue-certificate-module~login-login-module~r~7e384fb7"],{

/***/ "./src/app/services/common/common.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/common/common.service.ts ***!
  \***************************************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_timeoutWith__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/timeoutWith */ "./node_modules/rxjs-compat/_esm5/add/operator/timeoutWith.js");
/* harmony import */ var rxjs_add_operator_timeout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/timeout */ "./node_modules/rxjs-compat/_esm5/add/operator/timeout.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/toPromise */ "./node_modules/rxjs-compat/_esm5/add/operator/toPromise.js");
/* harmony import */ var rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_toPromise__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../constants */ "./src/app/constants.ts");
/* harmony import */ var _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../requestoptions/request-options.service */ "./src/app/services/requestoptions/request-options.service.ts");










var CommonService = /** @class */ (function () {
    function CommonService(http, local, requestOptions) {
        this.http = http;
        this.local = local;
        this.requestOptions = requestOptions;
        this.dappid = this.local.getDappId();
        this.KYCLoginUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SWAGGERAPI + 'user/countries/kyc';
        this.getAddssUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOME_URL + 'user/wallet';
        this.getBalUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOME_URL + 'user/balance';
        this.getRoleUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOME_URL + 'user/dappid';
        this.getCountriesUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].SWAGGERAPI + 'countries';
        this.getIssuerUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOST_URL + this.dappid + '/issuers/getId';
        this.getAuthorizerUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOST_URL + this.dappid + '/authorizers/getId';
        this.getIssuerDataUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOST_URL + this.dappid + '/issuer/data';
        this.getAuthorizerDataUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOST_URL + this.dappid + '/authorizer/data';
        this.getDepartments = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOST_URL + this.dappid + '/department/get';
        this.findUserUrl = _constants__WEBPACK_IMPORTED_MODULE_7__["Constants"].HOST_URL + this.dappid + '/receipient/email/exists';
    }
    CommonService.prototype.onCheckYCStatus = function (token) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("in service: " + token);
                        options = this.requestOptions.getRequestOptions(token);
                        return [4 /*yield*/, this.http.get(this.KYCLoginUrl, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.getCountries = function () {
        return this.http.get(this.getCountriesUrl).map(function (res) {
            var body = res['_body'];
            console.log(typeof (body));
            console.log(JSON.parse(body));
            var country1 = JSON.parse(body);
            var countries = country1['data'];
            console.log(countries);
            return countries;
        });
    };
    CommonService.prototype.getDepartmentsApi = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.http.post(this.getDepartments, {}).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.ongetAddress = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var token, params, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = this.local.getBelriumToken();
                        params = {
                            token: token
                        };
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getAddssUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.ongetBal = function (address) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var token, param, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        token = localStorage.getItem('BEL token');
                        param = {
                            address: address,
                            token: token
                        };
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getBalUrl, param, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.oncheckRole = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getRoleUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.ongetIssuer = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var email, params, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        email = this.local.getEmail();
                        params = {
                            email: email
                        };
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getIssuerUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.oncheckIssuerData = function (iid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var param, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        param = {
                            iid: iid
                        };
                        console.log(iid);
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getIssuerDataUrl, JSON.stringify(param), options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.ongetauth = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getAuthorizerUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.ongetauthdata = function (aid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, headers, options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            aid: aid
                        };
                        headers = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["Headers"]({ 'Content-Type': 'application/json', 'magic': '594fe0f3' });
                        options = new _angular_http__WEBPACK_IMPORTED_MODULE_3__["RequestOptions"]({ headers: headers, method: 'post' });
                        return [4 /*yield*/, this.http.post(this.getAuthorizerDataUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService.prototype.handleError = function (error) {
        console.error(error);
        return rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(error.json().message);
    };
    CommonService.prototype.userregisterd = function (params) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var options, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        options = this.requestOptions.getDappRequestOptions();
                        return [4 /*yield*/, this.http.post(this.findUserUrl, params, options).toPromise()];
                    case 1:
                        res = _a.sent();
                        return [2 /*return*/, res.json()];
                }
            });
        });
    };
    CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_3__["Http"], _localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_8__["LocalstorageService"], _requestoptions_request_options_service__WEBPACK_IMPORTED_MODULE_9__["RequestOptionsService"]])
    ], CommonService);
    return CommonService;
}());

// http://{{blockchain}}/api/dapps/{{dappid}}/receipient/email/exists
// Inputs: 
// {
//     "email": "john.bond629@gmail.com"
// }
// Outputs:
// {
//     "exists": true,
//     "data": {
//         "email": "john.bond629@gmail.com",
//         "empid": "JohnBonda",
//         "name": "John Bonda",
//         "identity": "{\"Adhar Card\":\"35298df293852\"}",
//         "iid": "1",
//         "extra": "{\"designation\":\"SDE\",\"bank\":\"State Bank of India\",\"accountNumber\":\"fowienwfo23392892g\",\"salary\":\"25000\"}",
//         "walletAddress": "A4NqmunbtbZEfsvtNEcE5nRkGcXGavS3yVIN",
//         "department": "HR",
//         "deleted": "0"
//     },
//     "success": true
// }
// {
//     "exists": false,
//     "success": true
// }


/***/ }),

/***/ "./src/app/services/localstorage/localstorage.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/localstorage/localstorage.service.ts ***!
  \***************************************************************/
/*! exports provided: LocalstorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalstorageService", function() { return LocalstorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LocalstorageService = /** @class */ (function () {
    function LocalstorageService() {
    }
    LocalstorageService.prototype.setEmail = function (email) {
        localStorage.setItem('email', email);
    };
    LocalstorageService.prototype.getEmail = function () {
        return localStorage.getItem('email');
    };
    LocalstorageService.prototype.setLoggedIn = function (login) {
        localStorage.setItem('isLoggedIn', login);
    };
    LocalstorageService.prototype.getLoggedIn = function () {
        return localStorage.getItem('isLoggedIn');
    };
    // sets belrium token
    LocalstorageService.prototype.setBelriumToken = function (token) {
        localStorage.setItem('token', token);
    };
    //gets belrium token
    LocalstorageService.prototype.getBelriumToken = function () {
        return localStorage.getItem('token');
    };
    // sets bkvs-dm token
    LocalstorageService.prototype.setBKVSToken = function (bkvstoken) {
        localStorage.setItem('bkvs-token', bkvstoken);
    };
    //gets bkvs-dm token
    LocalstorageService.prototype.getBKVSToken = function () {
        return localStorage.getItem('bkvs-token');
    };
    // sets role of an user
    LocalstorageService.prototype.setUserRole = function (role) {
        localStorage.setItem('role', role);
    };
    //gets role of an user
    LocalstorageService.prototype.getUserRole = function () {
        return localStorage.getItem('role');
    };
    // sets wallet balance
    LocalstorageService.prototype.setBalance = function (balance) {
        localStorage.setItem('balance', balance);
    };
    //gets wallet balance
    LocalstorageService.prototype.getBalance = function () {
        return localStorage.getItem('balance');
    };
    // sets country of an user
    LocalstorageService.prototype.setCountryName = function (countryName) {
        localStorage.setItem('countryName', countryName);
    };
    //gets country of an user
    LocalstorageService.prototype.getCountryName = function () {
        return localStorage.getItem('countryName');
    };
    //sets dapp id for the user
    LocalstorageService.prototype.setDappId = function (dappid) {
        localStorage.setItem('dappid', dappid);
    };
    //gets dapp id for the suer
    LocalstorageService.prototype.getDappId = function () {
        return localStorage.getItem('dappid');
    };
    LocalstorageService.prototype.setKycStatus = function (kycstatus) {
        localStorage.setItem('kycstatus', kycstatus);
    };
    LocalstorageService.prototype.getKycStatus = function () {
        return localStorage.getItem('kycstatus');
    };
    LocalstorageService.prototype.setWalletAddress = function (address) {
        localStorage.setItem('address', address);
    };
    LocalstorageService.prototype.getWalletAddress = function () {
        return localStorage.getItem('address');
    };
    LocalstorageService.prototype.setOrganization = function (organization) {
        localStorage.setItem('organization', organization);
    };
    LocalstorageService.prototype.getOrganization = function () {
        return localStorage.getItem('organization');
    };
    LocalstorageService.prototype.setUserName = function (name) {
        localStorage.setItem('name', name);
    };
    LocalstorageService.prototype.getUserName = function () {
        return localStorage.getItem('name');
    };
    LocalstorageService.prototype.setIssuerId = function (id) {
        localStorage.setItem('issuerid', id);
    };
    LocalstorageService.prototype.getIssuerId = function () {
        return localStorage.getItem('issuerid');
    };
    LocalstorageService.prototype.setIssuerDept = function (dept) {
        localStorage.setItem('issuerDept', dept);
    };
    LocalstorageService.prototype.getIssuerDept = function () {
        return localStorage.getItem('issuerDept');
    };
    LocalstorageService.prototype.setAuthorizerId = function (id) {
        localStorage.setItem('Authorizerid', id);
    };
    LocalstorageService.prototype.getAuthorizerId = function () {
        return localStorage.getItem('Authorizerid');
    };
    LocalstorageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LocalstorageService);
    return LocalstorageService;
}());



/***/ })

}]);
//# sourceMappingURL=default~authorizer-authorizer-module~issue-certificate-issue-certificate-module~login-login-module~r~7e384fb7.js.map