(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["issue-certificate-issue-certificate-module"],{

/***/ "./src/app/layout/issue-certificate/issue-certificate-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/layout/issue-certificate/issue-certificate-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: IssueCertificateRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IssueCertificateRoutingModule", function() { return IssueCertificateRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _issue_certificate_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./issue-certificate.component */ "./src/app/layout/issue-certificate/issue-certificate.component.ts");




var routes = [
    {
        path: '', component: _issue_certificate_component__WEBPACK_IMPORTED_MODULE_3__["IssueCertificateComponent"]
    }
];
var IssueCertificateRoutingModule = /** @class */ (function () {
    function IssueCertificateRoutingModule() {
    }
    IssueCertificateRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], IssueCertificateRoutingModule);
    return IssueCertificateRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/issue-certificate/issue-certificate.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/layout/issue-certificate/issue-certificate.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\">\n  <div class=\"col-md-6\" style=\"margin-top:10%\">\n      <form [formGroup]=\"certform\">\n          Email\n          <div class=\"input-group\">\n                  <!-- <label for=\"exampleInputEmail1\">Name</label> -->\n                  <input class=\"form-control\" name=\"email\" type=\"text\" [(ngModel)]=\"setemail\" formControlName=\"email1\"/>\n                  <div class=\"input-group-addon\" style=\"width:50px;\" (click)=\"finduser()\">\n                    <span  class=\"glyphicon glyphicon-search\"></span>\n                  </div>\n            </div><br>\n                <!-- <div class=\"form-row\"> -->\n                    <label for=\"inputEmail4\">Name</label><br>\n             <div class=\"input-group\">\n                    \n\n                     <input type=\"text\" class=\"form-control\" [(ngModel)]=\"getusername\" formControlName=\"username\" id=\"NAme\">\n            </div>\n                <!-- </div>   -->\n\n          <div class=\"form-row\">\n              <div class=\"form-group col-md-4\">\n                  <label for=\"inputEmail4\">ID</label>\n                  <input type=\"text\" class=\"form-control\"  formControlName=\"userid\" [(ngModel)]=\"getuserid\"  id=\"userid\">\n              </div>\n              <div class=\"form-group col-md-8\">\n                  <label for=\"yearofgraduation\">Year of graduation </label>\n                  <input type=\"text\" class=\"form-control\" formControlName=\"gradyear\" [(ngModel)]=\"getyear\" id=\"gradyear\">\n              </div>\n          </div>\n          <div class=\"form-group\">\n              <label for=\"degree\">Degree</label>\n              <input type=\"text\" class=\"form-control\" formControlName=\"degree\"[(ngModel)]=\"getdegree\"  id=\"degree\">\n          </div>\n          <div class=\"form-group\">\n              <label for=\"topic\">department</label>\n                      <!-- <select class=\"form-control\" style=\"width:100%; height:10%;font-size:20px;\">\n                           <option selected>Select Department</option> \n                         \n                      </select> -->\n                      <input type=\"text\" class=\"form-control\" [(ngModel)]=\"getuserdep\" formControlName=\"userdep\" id=\"userdep\">\n          </div>\n\n          <button type=\"button\"  data-toggle=\"modal\" data-target=\"#issuepopup\" class=\"issuecertificatebtn\">ISSUE CERTIFICATE</button>\n      </form>\n\n  </div>\n  <div class=\"col-md-6\" style=\"margin-top:10%\">\n      <div class=\"certificate\">\n          <br><br><br><br><br>\n          <h3 style=\"text-align:center\"><b>{{university}}</b></h3>\n        \n          <br><br>\n          <p style=\"margin-left: 8%;\">This is to Certify that&nbsp;&nbsp;&nbsp;<span id=\"\"><input type=\"text\"\n                      class=\"underlineinput1\" [(ngModel)]=\"getusername\" disabled> </span> </p>\n          <p style=\"margin-left: 8%;\">is awarded with degree of&nbsp;&nbsp;&nbsp; <span id=\"\"><input type=\"text\"\n                      class=\"underlineinput2\" value=\"\" [(ngModel)]=\"getdegree\" disabled> </span></p>\n          <p style=\"margin-left: 8%;\"><span id=\"\"><input type=\"text\" class=\"underlineinput3\" value=\"\"  disabled> </span></p>\n          <p style=\"margin-left: 8%;\">in&nbsp;&nbsp;&nbsp;<span id=\"\"> <input type=\"text\" [(ngModel)]=\"getuserdep\" class=\"underlineinput4\" value=\"\" disabled> </span> </p>\n          <p style=\"margin-left: 8%;\">on having passed the examinations in&nbsp;&nbsp;<span id=\"\"> <input type=\"text\"\n                      class=\"underlineinput5\" value=\"\" [(ngModel)]=\"getyear\" disabled> </span> </p>\n          <br>\n          <p style=\"margin-left: 8%;\">Enrollment No:<span id=\"\"> <input type=\"text\" class=\"underlineinput6\" [(ngModel)]=\"getuserid\" value=\"\" disabled>\n              </span></p>\n          <p style=\"margin-left: 8%;\">Date:<span id=\"\"> <input type=\"text\" class=\"underlineinput7\" value=\"\" disabled>\n              </span></p>\n\n      </div>\n  </div>\n</div>\n\n\n\n<div class=\"modal\"  [ngStyle]=\"{'display':reguser}\" id=\"adduser\" role=\"dialog\">\n        <div class=\"modal-dialog\">\n          <div class=\"modal-content\">\n            <div class=\"modal-header\">\n              <h4 class=\"modal-title\">ADD NEW USER</h4>\n              <button type=\"button\" class=\"close\" (click)=\"onCloseHandled()\" data-dismiss=\"modal\">&times;</button>\n            </div>\n            <div class=\"modal-body\">\n              <form class=\"dapp-balance\" action=\"\" method=\"post\" [formGroup]=\"userregform\">\n                <div class=\"form-group row\">\n                  <div class=\"col-6\">\n                    <div class=\"input-group\">\n                      <select name=\"countrycode\" formControlName=\"countrycode\" id=\"country\" class=\"form-control\">\n                        <option>Country Code</option>\n                \n                            <option *ngFor=\"let country of countries\" [value]=\"country.countryName\">{{country.countryName}} </option>\n                      </select>\n                    </div>\n                  </div>\n                  <div class=\"col-6\">\n                    <div class=\"input-group\">\n                      <input formControlName=\"email\" name=\"email\" placeholder=\"Email\" type=\"email\" [(ngModel)]=\"findemail\" class=\"form-control\" required=\"\">\n                    </div>\n                  </div>\n                </div>\n                <div class=\"form-group row\">\n                  <div class=\"col-6\">\n                    <div class=\"input-group\">\n                      <input formControlName=\"fname\" name=\"fname\" placeholder=\"First Name\" type=\"text\" class=\"form-control\">\n                    </div>\n                  </div>\n                  <div class=\"col-6\">\n                    <div class=\"input-group\">\n                      <input formControlName=\"lname\" name=\"lname\" placeholder=\"Last Name\" type=\"text\" class=\"form-control\">\n                    </div>\n                  </div>\n                </div>\n                <div class=\"form-group row\">\n                  <div class=\"col-6\">\n                    <div class=\"input-group\">\n                      <input formControlName=\"empid1\" name=\"employeeid\" placeholder=\"Employeeid\" type=\"text\" [(ngModel)]=\"userid1\" class=\"form-control\">\n                    </div>\n                  </div>\n                  <div class=\"col-6\">\n                    <div class=\"input-group\">\n                      <input formControlName=\"adhaar\" name=\"adhaar\" placeholder=\"adhaar\" type=\"text\" class=\"form-control\">\n                    </div>\n                  </div>\n                </div>\n                <div class=\"form-group row\">\n                  \n                 <div class=\"col-6\">\n                        <select name=\"department\" formControlName=\"department\" id=\"dep\" class=\"form-control\">\n                          <option>Department</option>\n                          <option>{{issuerdep}}</option>\n                        </select>\n                      </div>\n                </div>\n              </form>\n            </div>\n            <div class=\"modal-footer\">\n              <button class=\"submitbtn\" (click)=\"registeruser(); onCloseHandled();\">REGISTER</button>\n              <button class=\"cancelbtn\" (click)=\"onCloseHandled()\">CANCEL</button>\n            </div>\n          </div>\n      </div>\n</div>\n\n\n\n<!-- [(ngModel)]=\"issuername\" -->\n\n\n\n<div class=\"modal fade\" id=\"issuepopup\" role=\"dialog\">\n    <div class=\"modal-dialog\">\n      <!-- Modal content-->\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n             \n\n            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n          </div>\n           <div class=\"modal-body\">\n            \n              <div class=\"form-group col-md-12\">\n                  <h4>ENTER YOUR PASSPHRASE</h4>\n                <!-- <img style=\"margin-left:25%\" src=\"../../../assets/img/walletrecharge.png\" style=\"border-radius:15px;margin-left:24%;\"> -->\n                </div>\n                <div class=\"form-group col-md-12\">\n                    <input type=\"text\" class=\"form-control\" type=\"password\" name=\"secretkey\" placeholder=\"SecretKey\" [(ngModel)]=\"passphrase\">\n                </div>\n          </div>\n          <div class=\"modal-footer\">\n              <button class=\"submitbtn\"  data-dismiss=\"modal\" (click)=\"initiatecert()\">INITIAL ISSUE</button>\n              <button class=\"cancelbtn\"  data-dismiss=\"modal\">CANCEL</button>\n          </div>\n        </div>\n        \n      </div>\n    </div>"

/***/ }),

/***/ "./src/app/layout/issue-certificate/issue-certificate.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/layout/issue-certificate/issue-certificate.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  font-family: Muli;\n  font-size: 16px; }\n\n.issuecertificatebtn {\n  width: 185px;\n  height: 45px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  color: #ffffff;\n  margin: 33%; }\n\n.certificate {\n  width: 410px;\n  height: 563px;\n  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);\n  border: 0px solid #000;\n  background-image: url('Certbgfix.png');\n  margin-left: 40%;\n  font-family: Muli; }\n\n.underlineinput1 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 180px;\n  font-size: 18px; }\n\n.underlineinput2 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 146px;\n  font-size: 18px; }\n\n.underlineinput3 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 333px;\n  font-size: 18px; }\n\n.underlineinput4 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 320px;\n  font-size: 18px; }\n\n.underlineinput5 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 80px;\n  font-size: 18px; }\n\n.underlineinput6 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 100px;\n  font-size: 18px; }\n\n.underlineinput7 {\n  border: 0;\n  outline: 0;\n  background: transparent;\n  border-bottom: 1px solid black;\n  width: 100px;\n  font-size: 18px; }\n\n.form-group {\n  font-size: 16px; }\n\n.modal-dialog {\n  max-width: 30%;\n  margin-top: 15%;\n  margin-left: 40%; }\n\n.modal-content {\n  border-radius: 20px; }\n\n.modal-header {\n  border-bottom: 0px; }\n\n.modal-body {\n  padding: 6%; }\n\n.modal-footer {\n  justify-content: center;\n  border-top: 0px; }\n\n.modal-content {\n  border: 0px;\n  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5); }\n\n.submitbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white); }\n\n.cancelbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: white;\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: black; }\n\n.modal-title {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #b4b4b4;\n  margin-left: 25%;\n  margin-top: 5%; }\n\n.form-control {\n  font-size: 16px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L2lzc3VlLWNlcnRpZmljYXRlL2lzc3VlLWNlcnRpZmljYXRlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQWlCO0VBQ2pCLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxZQUFZO0VBQ1osWUFBWTtFQUNaLG1CQUFtQjtFQUNuQiwrQkFBK0I7RUFDL0IsY0FBYztFQUNkLFdBQVcsRUFBQTs7QUFFZjtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsMkNBQTJDO0VBQzNDLHNCQUFxQjtFQUNyQixzQ0FBMEQ7RUFDMUQsZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixXQUFVO0VBQ1YsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVuQjtFQUNJLFNBQVE7RUFDUixVQUFVO0VBQ1YsdUJBQXVCO0VBQ3ZCLDhCQUE4QjtFQUM5QixZQUFXO0VBQ1gsZUFBZSxFQUFBOztBQUVuQjtFQUNJLGVBQWMsRUFBQTs7QUFPbEI7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLG1CQUFtQixFQUFBOztBQUlyQjtFQUNFLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLFdBQVUsRUFBQTs7QUFLWjtFQUNFLHVCQUF1QjtFQUV6QixlQUFlLEVBQUE7O0FBSWY7RUFDRSxXQUFXO0VBQ1QseUNBQXFDLEVBQUE7O0FBSXpDO0VBQ0UsWUFBWTtFQUVaLGNBQWM7RUFFZCxtQkFBbUI7RUFFbkIsK0JBQStCO0VBRS9CLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsZ0JBQWdCO0VBRWhCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixrQkFBa0I7RUFFbEIsbUJBQW1CLEVBQUE7O0FBS3JCO0VBQ0UsWUFBWTtFQUVaLGNBQWM7RUFFZCxtQkFBbUI7RUFFbkIsdUJBQXNCO0VBQ3RCLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsZ0JBQWdCO0VBRWhCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixrQkFBa0I7RUFFbEIsWUFBWSxFQUFBOztBQUlkO0VBQ0UsaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixnQkFBZ0I7RUFFaEIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixpQkFBaUI7RUFFakIsc0JBQXNCO0VBRXRCLGdCQUFnQjtFQUVoQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGNBQWMsRUFBQTs7QUFJaEI7RUFFRSxlQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvaXNzdWUtY2VydGlmaWNhdGUvaXNzdWUtY2VydGlmaWNhdGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5pc3N1ZWNlcnRpZmljYXRlYnRue1xuICAgIHdpZHRoOiAxODVweDtcbiAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1vcmFuZ2UpO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIG1hcmdpbjogMzMlO1xufVxuLmNlcnRpZmljYXRle1xuICB3aWR0aDogNDEwcHg7XG4gIGhlaWdodDogNTYzcHg7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDZweCAwIHJnYmEoMCwgMCwgMCwgMC4xNik7XG4gIGJvcmRlcjowcHggc29saWQgIzAwMDtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9DZXJ0YmdmaXgucG5nXCIpO1xuICBtYXJnaW4tbGVmdDogNDAlO1xuICBmb250LWZhbWlseTogTXVsaTtcbn1cbi51bmRlcmxpbmVpbnB1dDF7XG4gICAgYm9yZGVyOjA7XG4gICAgb3V0bGluZTogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgIHdpZHRoOjE4MHB4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cbi51bmRlcmxpbmVpbnB1dDJ7XG4gICAgYm9yZGVyOjA7XG4gICAgb3V0bGluZTogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgIHdpZHRoOjE0NnB4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cbi51bmRlcmxpbmVpbnB1dDN7XG4gICAgYm9yZGVyOjA7XG4gICAgb3V0bGluZTogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgIHdpZHRoOjMzM3B4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cbi51bmRlcmxpbmVpbnB1dDR7XG4gICAgYm9yZGVyOjA7XG4gICAgb3V0bGluZTogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgIHdpZHRoOjMyMHB4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cbi51bmRlcmxpbmVpbnB1dDV7XG4gICAgYm9yZGVyOjA7XG4gICAgb3V0bGluZTogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgYmxhY2s7IFxuICAgIHdpZHRoOjgwcHg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuLnVuZGVybGluZWlucHV0NntcbiAgICBib3JkZXI6MDtcbiAgICBvdXRsaW5lOiAwO1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBibGFjazsgXG4gICAgd2lkdGg6MTAwcHg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuLnVuZGVybGluZWlucHV0N3tcbiAgICBib3JkZXI6MDtcbiAgICBvdXRsaW5lOiAwO1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBibGFjazsgXG4gICAgd2lkdGg6MTAwcHg7XG4gICAgZm9udC1zaXplOiAxOHB4O1xufVxuLmZvcm0tZ3JvdXB7XG4gICAgZm9udC1zaXplOjE2cHg7XG59XG5cblxuXG5cblxuLm1vZGFsLWRpYWxvZyB7XG4gICAgbWF4LXdpZHRoOiAzMCU7XG4gICAgbWFyZ2luLXRvcDogMTUlO1xuICAgIG1hcmdpbi1sZWZ0OiA0MCU7XG4gIH1cbiAgLm1vZGFsLWNvbnRlbnR7XG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgfVxuICBcbiAgXG4gIC5tb2RhbC1oZWFkZXJ7XG4gICAgYm9yZGVyLWJvdHRvbTogMHB4OyBcbiAgfVxuICAubW9kYWwtYm9keXtcbiAgICBwYWRkaW5nOjYlO1xuICAgIC8vIG1hcmdpbi10b3A6NSU7XG4gIH1cbiAgXG4gIFxuICAubW9kYWwtZm9vdGVye1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIC8vIHBhZGRpbmc6IDFyZW07XG4gIGJvcmRlci10b3A6IDBweDtcbiAgfVxuICBcbiAgXG4gIC5tb2RhbC1jb250ZW50e1xuICAgIGJvcmRlcjogMHB4O1xuICAgICAgYm94LXNoYWRvdzogMCA1cHggMTVweCByZ2JhKDAsMCwwLC41KTsgXG4gIH1cbiAgXG4gIFxuICAuc3VibWl0YnRue1xuICAgIHdpZHRoOiAxNDVweDtcbiAgXG4gICAgaGVpZ2h0OiAzNi44cHg7XG4gIFxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIFxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLW9yYW5nZSk7XG4gIFxuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBcbiAgICBmb250LXNpemU6IDEzcHg7XG4gIFxuICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gIFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIFxuICAgIGxpbmUtaGVpZ2h0OiAxLjIzO1xuICBcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIFxuICAgIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gIFxuICB9XG4gIFxuICBcbiAgLmNhbmNlbGJ0bntcbiAgICB3aWR0aDogMTQ1cHg7XG4gIFxuICAgIGhlaWdodDogMzYuOHB4O1xuICBcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlO1xuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBcbiAgICBmb250LXNpemU6IDEzcHg7XG4gIFxuICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gIFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIFxuICAgIGxpbmUtaGVpZ2h0OiAxLjIzO1xuICBcbiAgICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICBcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIFxuICAgIGNvbG9yOiBibGFjaztcbiAgXG4gIH1cblxuICAubW9kYWwtdGl0bGV7XG4gICAgZm9udC1mYW1pbHk6IE11bGk7XG4gIFxuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBcbiAgICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgXG4gICAgbGluZS1oZWlnaHQ6IDEuMjc7XG4gIFxuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIFxuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gIFxuICAgIGNvbG9yOiAjYjRiNGI0OyBcbiAgICBtYXJnaW4tbGVmdDogMjUlO1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgXG4gIH1cblxuICAuZm9ybS1jb250cm9sXG4gIHtcbiAgICBmb250LXNpemU6MTZweDtcbiAgfVxuICBcbiAgIl19 */"

/***/ }),

/***/ "./src/app/layout/issue-certificate/issue-certificate.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/layout/issue-certificate/issue-certificate.component.ts ***!
  \*************************************************************************/
/*! exports provided: IssueCertificateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IssueCertificateComponent", function() { return IssueCertificateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_common_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/common/common.service */ "./src/app/services/common/common.service.ts");
/* harmony import */ var _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var _services_certificate_certificate_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/certificate/certificate.service */ "./src/app/services/certificate/certificate.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var IssueCertificateComponent = /** @class */ (function () {
    function IssueCertificateComponent(common, actRoute, local, certificateservice) {
        var _this = this;
        this.common = common;
        this.actRoute = actRoute;
        this.local = local;
        this.certificateservice = certificateservice;
        this.reguser = "none";
        this.getuserid = "";
        this.actRoute.queryParams.subscribe(function (params) {
            _this.params = _this.actRoute.snapshot.params.email;
            console.log(_this.params);
        });
    }
    IssueCertificateComponent.prototype.ngOnInit = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.userregform = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                            countrycode: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            fname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            lname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            empid1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            adhaar: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            department: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                        });
                        this.certform = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                            email1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            userid: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            gradyear: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            degree: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                            userdep: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
                        });
                        if (!(this.params != undefined)) return [3 /*break*/, 2];
                        this.searchemail = this.params;
                        return [4 /*yield*/, this.autofill()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        this.iid = this.local.getIssuerId();
                        this.university = this.local.getOrganization();
                        this.issuerdep = this.local.getIssuerDept();
                        this.common.getCountries().subscribe(function (response) {
                            _this.countries = response;
                            console.log(_this.countries);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    IssueCertificateComponent.prototype.onCloseHandled = function () {
        this.reguser = 'none';
    };
    IssueCertificateComponent.prototype.finduser = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, result;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.findemail = this.certform.value.email1;
                        params = {
                            email: this.certform.value.email1
                        };
                        return [4 /*yield*/, this.common.userregisterd(params)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        if (result['success'] === true) {
                            if (result['exists'] === false) {
                                this.reguser = "block";
                            }
                            else if (result['exists'] === true) {
                                console.log(result['data']);
                                this.employeedata = result['data'];
                                this.getuserid = this.employeedata['empid'];
                                this.getuserdep = this.employeedata['department'];
                                this.getusername = this.employeedata['name'];
                                // this.certform.setValue(this.employeedata['userid']:{ this.employeedata['empid']})
                                // this.certform.value.userid=this.employeedata['empid'];
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    IssueCertificateComponent.prototype.autofill = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, result;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            email: this.searchemail
                        };
                        return [4 /*yield*/, this.common.userregisterd(params)];
                    case 1:
                        result = _a.sent();
                        console.log(result);
                        if (result['success'] === true) {
                            if (result['exists'] === false) {
                                this.reguser = "block";
                            }
                            else if (result['exists'] === true) {
                                console.log(result['data']);
                                this.employeedata = result['data'];
                                this.setemail = this.params;
                                this.getuserid = this.employeedata['empid'];
                                this.getuserdep = this.employeedata['department'];
                                this.getusername = this.employeedata['name'];
                                // this.certform.setValue(this.employeedata['userid']:{ this.employeedata['empid']})
                                // this.certform.value.userid=this.employeedata['empid'];
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    IssueCertificateComponent.prototype.registeruser = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var e, sel, dep, department, params, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        e = (document.getElementById("country"));
                        sel = e.selectedIndex - 1;
                        this.countrycode = this.countries[sel]['countryCode'];
                        this.countrycode = this.countrycode ? this.countrycode : "undefined";
                        this.countryid = this.countries[sel]['countryID'];
                        console.log(this.countryid);
                        console.log(this.iid);
                        console.log(this.userregform.value.department);
                        dep = (document.getElementById("dep"));
                        department = dep.options[dep.selectedIndex].value;
                        console.log(department);
                        params = {
                            countryCode: this.countrycode,
                            email: this.userregform.value.email,
                            empid: this.userregform.value.empid1,
                            lastName: this.userregform.value.lname,
                            name: this.userregform.value.fname,
                            identity: {
                                Aadhaar: this.userregform.value.adhaar
                            },
                            extra: {},
                            groupName: "Dapps",
                            iid: this.iid,
                            department: department
                        };
                        console.log(params);
                        return [4 /*yield*/, this.certificateservice.registeremployee(params)];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        if (!(res['isSuccess'] === true)) return [3 /*break*/, 5];
                        if (!(res['message'] === "Awaiting wallet address")) return [3 /*break*/, 2];
                        alert("User Registered Successfully,'" + res['message'] + "'");
                        return [3 /*break*/, 4];
                    case 2:
                        alert("User Registered Successfully");
                        this.empregresponse = res['employee'];
                        return [4 /*yield*/, this.fillempdetails()];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        alert(res['message']);
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    // {
    //   "message": "Registered",
    //   "isSuccess": true,
    //   "employee": {
    //       "email": "john.bond629999@yopmail.com",
    //       "empid": "JohnBonda4",
    //       "name": "John Bonda",
    //       "identity": "{\"Adhar Card\":\"35298df293852\"}",
    //       "iid": "1",
    //       "walletAddress": "AFmpZHZZ4e2HsJatoYW2Ebh2pkmJ78AZYdIN",
    //       "department": "HR",
    //       "deleted": "0",
    //       "extra": "{\"designation\":\"SDE\",\"bank\":\"State Bank of India\",\"accountNumber\":\"fowienwfo23392892g\",\"salary\":\"25000\"}"
    //   },
    //   "success": true
    // }
    IssueCertificateComponent.prototype.fillempdetails = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                console.log(this.empregresponse);
                this.getuserid = this.empregresponse['empid'];
                this.getuserdep = this.empregresponse['department'];
                this.getusername = this.empregresponse['name'];
                return [2 /*return*/];
            });
        });
    };
    IssueCertificateComponent.prototype.initiatecert = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            empid: this.certform.value.userid,
                            issuerid: this.iid,
                            secret: this.passphrase,
                            data: {
                                name: this.certform.value.username,
                                degree: this.certform.value.degree,
                                department: this.certform.value.userdep,
                                gradyear: this.certform.value.gradyear
                            }
                        };
                        console.log(params);
                        return [4 /*yield*/, this.certificateservice.PayslipInitialissue(params)];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        if (res['isSuccess'] === true) {
                            alert(res['message']);
                        }
                        else {
                            alert(res['message']);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    IssueCertificateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-issue-certificate',
            template: __webpack_require__(/*! ./issue-certificate.component.html */ "./src/app/layout/issue-certificate/issue-certificate.component.html"),
            styles: [__webpack_require__(/*! ./issue-certificate.component.scss */ "./src/app/layout/issue-certificate/issue-certificate.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__["LocalstorageService"], _services_certificate_certificate_service__WEBPACK_IMPORTED_MODULE_5__["CertificateService"]])
    ], IssueCertificateComponent);
    return IssueCertificateComponent;
}());

// Employee Registration
// POST Call
// http://{{blockchain}}/api/dapps/{{dappid}}/registerEmployee
// Inputs:
// {
//    "countryCode": "IN",
//    "email": "john.bond629@gmail.com",
//    "empid": "JohnBonda",
//    "lastName": "Bonda",
//    "name": "John",
//    "identity": {
//        "Adhar Card": "35298df293852"
//    },
//    "extra": {
//        "designation": "SDE",
//        "bank": "State Bank of India",
//        "accountNumber": "fowienwfo23392892g",
//        "salary": "25000"
//    },
//    "groupName": "Dapps",
//    "iid": "1",
//    "department": "HR"
// }
// Outputs:
// {
//    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImpvaG4uYm9uZDYyOUBnbWFpbC5jb20iLCJ0aW1lc3RhbXAiOjE1NTE3NzA5Mjc5MDcsImlhdCI6MTU1MTc3MDkyNywiZXhwIjoxNTUxODU3MzI3fQ.DzU7MYyWIWhgY78MpRejnv94UN5pGQH50UFtxhP6SB4",
//    "message": "Awaiting wallet address",
//    "isSuccess": true,
//    "success": true
// }
// Initial Issue
// POST Call
// http://{{blockchain}}/api/dapps/{{dappid}}/payslip/initialIssue
// Inputs: 
// {
//     "empid": "JohnBonda",
//     "issuerid": "1",
//     "secret": "cactus peasant return inside filter morning wasp floor museum nature iron can",
//     "data": {
//         "Name": "JohnnyYo",
//         "Address": "Visakhapaasdadtnam",
//         "Course": "CSqwqE",
//         "Marks": 100
//     }
// }
// Outputs:
// {
//     "message": "Payslip initiated",
//     "isSuccess": true,
//     "success": true
// }


/***/ }),

/***/ "./src/app/layout/issue-certificate/issue-certificate.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/layout/issue-certificate/issue-certificate.module.ts ***!
  \**********************************************************************/
/*! exports provided: IssueCertificateModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IssueCertificateModule", function() { return IssueCertificateModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _issue_certificate_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./issue-certificate-routing.module */ "./src/app/layout/issue-certificate/issue-certificate-routing.module.ts");
/* harmony import */ var _issue_certificate_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./issue-certificate.component */ "./src/app/layout/issue-certificate/issue-certificate.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");






var IssueCertificateModule = /** @class */ (function () {
    function IssueCertificateModule() {
    }
    IssueCertificateModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_issue_certificate_component__WEBPACK_IMPORTED_MODULE_4__["IssueCertificateComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _issue_certificate_routing_module__WEBPACK_IMPORTED_MODULE_3__["IssueCertificateRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]
            ]
        })
    ], IssueCertificateModule);
    return IssueCertificateModule;
}());



/***/ })

}]);
//# sourceMappingURL=issue-certificate-issue-certificate-module.js.map