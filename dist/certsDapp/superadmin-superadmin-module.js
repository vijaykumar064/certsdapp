(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["superadmin-superadmin-module"],{

/***/ "./src/app/layout/superadmin/superadmin-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/superadmin/superadmin-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: SuperadminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperadminRoutingModule", function() { return SuperadminRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _superadmin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./superadmin.component */ "./src/app/layout/superadmin/superadmin.component.ts");




var routes = [{
        path: '', component: _superadmin_component__WEBPACK_IMPORTED_MODULE_3__["SuperadminComponent"]
    }];
var SuperadminRoutingModule = /** @class */ (function () {
    function SuperadminRoutingModule() {
    }
    SuperadminRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], SuperadminRoutingModule);
    return SuperadminRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/superadmin/superadmin.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/superadmin/superadmin.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div [@.disabled]=\"true\">\n    <mat-tab-group disableUnderline={true} dynamicHeight=\"true\">\n      <mat-tab label=\"OVERVIEW\">\n          <ng-template \n          *ngIf=\"tableshow;then showBlock; else notShowBlock\">\n        </ng-template>\n        \n        <div class=\"container-fluid PT10 \" id=\"Rectangle-820\">\n          <div id=\"div1\" (click)=\"getpendingauth()\" [ngClass]=\"activeFlag===1?'activecard':'inactivecard'\">\n            <img src=\"../../../assets/images/icons8-rubber-stamp-64-2@2x.png\" style=\"float:left;border-radius:50px;padding:20px;\" id=\"Ellipse-261\"\n              height=\"20\" width=\"20\">\n              <h2 style=\"font-size:5rem\">{{pendingCountAuth}}</h2>\n            <p>Pending Autherization</p>\n              </div>\n\n          <div id=\"div1\" (click)=\"getpendingissues()\" [ngClass]=\"activeFlag===2?'activecard':'inactivecard'\">\n            <img src=\"../../../assets/images/icons8-certificate-64.png\" style=\"float:left;border-radius:50px;padding:20px;\" id=\"Rectangle-862\">\n            <h2 style=\"font-size:5rem\">{{pendingCountIssue}}</h2>\n            <p>Pending Issue</p>\n             </div>\n\n          <div id=\"div1\" (click)=\"getcertificatesRej()\" [ngClass]=\"activeFlag===3?'activecard':'inactivecard'\">\n            <img src=\"../../../assets/images/icons8-file-delete-64.png\" style=\"float:left;border-radius:50px;padding:20px;\" id=\"Ellipse-262\">\n            <h2 style=\"font-size:5rem\">{{pendingCountRejected}}</h2>\n            <p>Certificates Rejected</p>\n          </div>\n          <br>\n          <!-- <h4 class=\"sa-title\">CERTIFICATE DETAILS</h4> -->\n          \n          <!-- <div *ngIf=\"display_pending_issues\">\n            <div class=\"container\">\n              <h2>PENDING ISSUES</h2>\n              <div class=\"progress\">\n                <div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\"\n                  aria-valuemax=\"100\" style=\"width:20%;border-radius:12px;background-color: orange;\">\n      \n                </div>\n              </div>\n            </div>\n            <div class=\"flex-container\" *ngFor=\"let pending of totalpending\">\n              <div>Deartment: Electrical and Electronics</div>\n              <div>id:{{pending.empid}}</div>\n              <div>issuerid:{{pending.empname}}</div>\n              <div>Student Name: {{pending.empname}}</div> -->\n      \n\n\n          <div *ngIf=\"display_pending_authorization\">\n              <h2>Pending Autherization</h2>\n              <div class=\"flex-container\" *ngFor=\"let pending1 of res1\" >\n                  \n                  <div>Certificate ID:{{pending1.pid}}</div>\n                  <div>Issuer ID:{{pending1.iid}}</div>\n                  <div>Time:{{pending1.timestampp | date:'medium'}}</div>\n                  <div>Student ID:{{pending1.empid}}</div>\n                  <div>Department ID:{{pending1.did}}</div>\n\n              </div>\n                </div>   \n                <div *ngIf=\"display_pending_issued\">\n                    <h2>Pending Issued</h2>\n                    <div class=\"flex-container\" *ngFor=\"let pending2 of res2\">\n                        \n                        <div>Certificate ID:{{pending2.pid}}</div>\n                        <div>Department:{{pending2.departmentName}}</div>\n                        <div>Issuer ID:{{pending2.iid}}</div>\n                        <div>Time:{{pending2.timestampp| date:'medium'}}</div>\n                        <div>Student ID:{{pending2.empid}}</div>\n                    </div>\n                      </div>   \n                      <div *ngIf=\"display_certificate_rejected\">\n                          <h2>Certificate Rejected</h2>\n                          <div class=\"flex-container\" *ngFor=\"let pending3 of res3\">\n                              \n                              <div>Rejected ID:{{pending3.pid}}</div>\n                              <div>Auth ID:{{pending3.aid}}</div>\n                              <div>Issuer ID:{{pending3.iid}}</div>\n                              <div>Time:{{pending3.timestampp| date:'medium'}}</div>\n                              <div>Reason:{{pending3.reason}}</div>\n                          </div>\n                            </div>   \n        </div> \n\n      </mat-tab>\n    \n\n\n      <mat-tab label=\"ORGANISATION\">\n        <div class=\"tabbable\">\n          <ul class=\"nav nav-tabs\">\n            <li class=\"active\" ><a data-toggle=\"tab\" href=\"#tab1\">ISSUER</a></li>\n            <li class=\"\" (click)=\"getAuthorizers();\"><a data-toggle=\"tab\" href=\"#tab2\">AUTHORIZER</a></li>\n            \n          </ul>\n          <div class=\"tab-content\">\n            <div id=\"tab1\" class=\"tab-pane active\">\n              <table class=\"table table-hover superadmintables\">\n                <thead>\n                  <tr>\n                    <th scope=\"col\">Empid</th>\n                    <th scope=\"col\">Email</th>\n                    <!-- <th scope=\"col\">Designation</th> -->\n                    <th scope=\"col\">Department</th>\n                    <th scope=\"col\">Timestamp</th>\n                    <th scope=\"col\">Count</th>\n                    <th></th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <!-- <tr>\n                    <th scope=\"row\">1</th>\n                    <td>Mark</td>\n                    <td>Otto</td>\n                    <td>@mdo</td>\n                    <td>cdvfv</td>\n                    <td>dcftysdf</td>\n                    <td><button type=\"button\" class=\"btn btn-link\" style=\"color:#f87d42;font-size: 16px;\">remove</button>\n                    </td>\n                  </tr> -->\n                   <tr *ngFor=\"let issuer of issuerresponse\">\n                    <!-- <tr ng-repeat=\"issuer in data\"> -->\n                    <td>{{issuer['iid']}}</td>\n                    <td>{{issuer['email']}}</td>\n                     <td>{{issuer['departments']}}</td>\n                    <td>{{issuer['timestampp']}}</td>\n                    <td>{{issuer['issuesCount']}}</td>\n                    <td><Button class=\"btn btn-danger\" (click)=\"issuerremove(issuer['iid'])\">Remove</Button></td>\n                  \n                  </tr>\n\n                 \n                </tbody>\n              </table>\n             \n              <br>\n              <br><br>\n  \n              <button class=\"adduserbtn\" data-toggle=\"modal\" (click)=\"getdepoptions()\" data-target=\"#addissuer\">ADD NEW ISSUER</button>\n              <br>\n              <br>\n  \n            </div>\n            <div id=\"tab2\" class=\"tab-pane\">\n              <table id=\"example1\" class=\"table table-hover superadmintables\">\n                <thead>\n                  <tr>\n                    <th scope=\"col\">Empid</th>\n                    <th scope=\"col\">Email</th>\n                    <!-- <th scope=\"col\">Department</th> -->\n                    <th scope=\"col\">Timestamp</th>\n                    <th scope=\"col\">Rejected Count</th>\n                    <th scope=\"col\">Signed Count</th>\n                    <th></th>\n                </thead>\n                <tbody>\n                  \n                    <tr *ngFor=\"let authorizer of authorizerresponse\" >\n                        <!-- <tr ng-repeat=\"issuer in data\"> -->\n                        <td>{{authorizer['aid']}}</td>\n                        <td>{{authorizer['email']}}</td>\n                        <!-- <td > <span>{{authorizer['departments'][i]['name']}}</span></td> -->\n                        <td>{{authorizer['timestampp']}}</td>\n                        <td>{{authorizer['rejectedCount']}}</td>\n                        <td>{{authorizer['signedCount']}}</td>\n                        <td><Button class=\"btn btn-danger\" (click)=\"authremove1(authorizer['aid'])\">Remove</Button></td>\n                      </tr>\n                </tbody>\n              </table><br>\n              <br>\n              <br>\n              <button class=\"adduserbtn\" data-toggle=\"modal\" data-target=\"#addauth\">ADD NEW AUTHORIZER</button>\n              <br>\n              <br>\n              </div>\n            </div>\n        </div>\n      </mat-tab>\n    </mat-tab-group>\n  </div>\n  <br>\n  <br>\n\n\n</div>\n\n<div class=\"modal fade\" id=\"addissuer\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" >ADD NEW ISSUER</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"form-group col-md-12\">\n          <select id=\"dept\" class=\"form-control dropdown\" [(ngModel)]=\"selectedoption\" class=\"form-control\" [multiple]=\"true\">\n            <!-- <option value=\"volvo\">Volvo</option>\n            <option value=\"saab\">Saab</option>\n            <option value=\"mercedes\">Mercedes</option>\n            <option value=\"audi\">Audi</option> -->\n            <option value=\"\">Department</option>\n            <option *ngFor=\"let dept of deptnames\" [value]=\"dept.name\">{{dept.name}}</option>\n          </select>\n        </div>\n        <div class=\"form-group col-md-12\">\n          <!-- <div class=\"input-group\">\n                    <input type=\"text\" class=\"form-control\" name=\"enrollment no.\" placeholder=\"Enrollment no.\" required>\n                  </div> -->\n          <!-- <input type=\"text\" class=\"form-control\" name=\"country\" placeholder=\"Country ID\" [(ngModel)]=\"country\"> -->\n         \n          <select id=\"ide\"  [(ngModel)]=\"selectedName\" class=\"form-control\">\n            <option value=\"\">Select country</option>\n            <option *ngFor=\"let country of countries\" [value]=\"country.countryName\">{{country.countryName}}</option>\n          </select>\n\n        </div>\n\n        <div class=\"form-group col-md-12\">\n          <input type=\"text\" class=\"form-control\" name=\"issuername\" placeholder=\"Name\" [(ngModel)]=\"issuername\">\n        </div>\n        <div class=\"form-group col-md-12\">\n          <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Email ID\" [(ngModel)]=\"email\">\n        </div>\n        <div class=\"form-group col-md-12\">\n          <input type=\"text\" class=\"form-control\" name=\"role\" placeholder=\"ISSUER\" [(ngModel)]=\"role\" disabled>\n        </div>\n        <!-- <select id=\"dep\" aria-placeholder=\"Departments\">\n            <option></option>\n            <option></option>\n            </select>\n            <input type=\"text\" placeholder=\"Country Code\">\n            <input type=\"text\" placeholder=\"Email ID\"> -->\n      </div>\n      <div class=\"modal-footer\">\n\n        <button class=\"submitbtn\" data-dismiss=\"modal\" (click)=\"onsubmitIssuer()\">DONE</button>\n         <button class=\"cancelbtn\" data-dismiss=\"modal\"  >CANCEL</button>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n\n<!-- \n<div class=\"backdrop\" [ngStyle]=\"{'display':display}\"></div>\n<div class=\"modal\" tabindex=\"-1\" role=\"dialog\"  [ngStyle]=\"{'display':display}\">\n <div class=\"modal-dialog\" role=\"document\">\n   <div class=\"modal-content\">\n     <div class=\"modal-header\">\n           \n       <button type=\"button\" class=\"close\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n       <h4 class=\"modal-title\">ADD NEW ISSUER</h4>\n     </div>\n     <div class=\"modal-body\">\n       \n        <select id=\"dep\" aria-placeholder=\"Departments\">\n          <option></option>\n          <option></option>\n          </select>\n          <input type=\"text\" placeholder=\"Country Code\">\n          <input type=\"text\" placeholder=\"Email ID\">\n     </div>\n     <div class=\"modal-footer\">\n       <button type=\"button\" class=\"btn btn-default\" (click)=\"closemodel()\">Cancel</button>\n      <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Submit</button>\n     </div>\n   </div>\n </div>\n</div> -->\n\n\n<div class=\"modal fade\" id=\"addauth\" role=\"dialog\">\n  <div class=\"modal-dialog\">\n\n    <!-- Modal content-->\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\">ADD NEW AUTHORIZER</h4>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n      </div>\n      <div class=\"modal-body\">\n        <!-- <div class=\"form-group col-md-12\">\n                <select class=\"form-control dropdown\">\n                  <option>Departments</option>\n                </select>\n              </div>  -->\n\n              <!-- (change)=onChangeCountry($event.target.value) -->\n        <div class=\"form-group col-md-12\">\n          <!-- <div class=\"input-group\">\n                    <input type=\"text\" class=\"form-control\" name=\"enrollment no.\" placeholder=\"Enrollment no.\" required>\n                  </div> -->\n                  <select id=\"ide1\"  [(ngModel)]=\"selectedName\" class=\"form-control\">\n                    <option value=\"\">Select country</option>\n                    <option *ngFor=\"let country of countries\" [value]=\"country.countryName\">{{country.countryName}}</option>\n                  </select>\n        </div>\n        <div class=\"form-group col-md-12\">\n          <input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Name\" [(ngModel)]=\"authName\" >\n        </div>\n        <div class=\"form-group col-md-12\">\n          <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Email ID\" [(ngModel)]=\"authid\">\n        </div>\n        <div class=\"form-group col-md-12\">\n          <input type=\"text\" class=\"form-control\" name=\"role\" [(ngModel)]=\"authrole\" placeholder=\"Authorizer\" disabled>\n        </div>\n        <!-- <select id=\"dep\" aria-placeholder=\"Departments\">\n            <option></option>\n            <option></option>\n            </select>\n            <input type=\"text\" placeholder=\"Country Code\">\n            <input type=\"text\" placeholder=\"Email ID\"> -->\n      </div>\n      <div class=\"modal-footer\">\n        <button class=\"submitbtn\" data-dismiss=\"modal\"  (click)=\"onsubmitAuthorizer()\">DONE</button>\n        <button class=\"cancelbtn\" data-dismiss=\"modal\">CANCEL</button>\n      </div>\n    </div>\n\n  </div>\n</div>\n\n\n<script>\n  function change() {\n    document.getElementById('change1').style.cssText = 'text-decoration:underline';\n  }\n</script>\n\n\n\n"

/***/ }),

/***/ "./src/app/layout/superadmin/superadmin.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/superadmin/superadmin.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  font-size: 15px; }\n\nul li a:hover {\n  text-decoration: underline;\n  width: 100%; }\n\n.authorizer {\n  width: 150px;\n  height: 50px;\n  font-family: Muli;\n  font-size: 14px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #b9b9b9; }\n\n.activecard {\n  width: 322px;\n  height: 142px;\n  box-shadow: 3px 5px 10px 0 rgba(0, 0, 0, 0.12);\n  background-color: var(--white); }\n\n.inactivecard {\n  width: 322px;\n  height: 142px;\n  box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.1);\n  background-color: var(--white); }\n\n.issuer {\n  width: 155px;\n  height: 50px;\n  font-family: Muli;\n  font-size: 14px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #1b3764; }\n\n.superadmintables td {\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: normal;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: center;\n  color: #484848; }\n\n.superadmintables th {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 800;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.4;\n  letter-spacing: normal;\n  text-align: center;\n  color: #797979; }\n\n.superadmintables {\n  margin-left: 2%;\n  width: 98%;\n  height: 350px;\n  border-radius: 5px;\n  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.08); }\n\n.adduserbtn {\n  width: 220px;\n  height: 50px;\n  border-radius: 10px;\n  background-color: #f87d42;\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white);\n  float: none;\n  position: absolute;\n  /* top: 50%; */\n  left: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%); }\n\n/* .adduserfont{\nfont-family: Muli;\n\nfont-size: 16px;\n\nfont-weight: 900;\n\nfont-style: normal;\n\nfont-stretch: normal;\n\nline-height: 1.25;\n\nletter-spacing: normal;\n\ntext-align: center;\n\ncolor: var(--white);\n} */\n\n.mainlist a {\n  width: 125px;\n  height: 20px;\n  font-family: Muli;\n  font-size: 20px;\n  font-weight: 800;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  color: #223e60; }\n\n.nav-tabs {\n  border-bottom: 0px solid #ddd; }\n\n#Rectangle-820 {\n  width: 1144px;\n  height: 768px;\n  border-radius: 5px;\n  background-color: var(--white-grey); }\n\n#div1 {\n  width: 300px;\n  height: 150px;\n  text-align: center;\n  border: 1px #ccc solid;\n  background: #ffffff;\n  padding: 25px;\n  margin: 25px;\n  border-radius: 10px;\n  float: left;\n  margin-right: 40px; }\n\n.div11 {\n  width: 1068px;\n  height: 75px;\n  text-align: center;\n  border: 1px #ccc solid;\n  background: #ffffff;\n  padding: 25px;\n  margin: 25px;\n  border-radius: 10px;\n  line-height: 5px;\n  float: left;\n  margin-right: 40px; }\n\n/* table td{\n    padding: 15px;\n    margin: 5px;\n\n    \n  }  */\n\n.div11 td {\n  width: 33.333%; }\n\n.div11 td span {\n  float: left; }\n\np {\n  color: #000000; }\n\n#Ellipse-261 {\n  width: 85px;\n  height: 85px;\n  background-color: #ffc221; }\n\n#Rectangle-862 {\n  width: 85px;\n  height: 85px;\n  box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.1);\n  background-color: #00b15f; }\n\n#Ellipse-262 {\n  width: 85px;\n  height: 85px;\n  background-color: #ff7791; }\n\n#Rectangle-864 {\n  width: 1068px;\n  height: 20px;\n  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1);\n  background-color: #ffffff;\n  padding: 0px;\n  margin: 2px; }\n\nimg {\n  border-radius: 50%; }\n\ntable tr td {\n  text-align: center; }\n\na:not([href]):not([tabindex]) {\n  color: #223e60;\n  text-decoration: none; }\n\n.modal-dialog {\n  max-width: 25%;\n  margin-top: 15%;\n  margin-left: 40%; }\n\n.modal-content {\n  border-radius: 20px; }\n\n.form-control ::-webkit-input-placeholder {\n  opacity: 0.31;\n  font-family: Muli;\n  font-size: 10px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a0a0a0; }\n\n.form-control ::-ms-input-placeholder {\n  opacity: 0.31;\n  font-family: Muli;\n  font-size: 10px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a0a0a0; }\n\n.form-control ::placeholder {\n  opacity: 0.31;\n  font-family: Muli;\n  font-size: 10px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.3;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a0a0a0; }\n\n.fromcontrol input {\n  height: 35px;\n  border-radius: 4px;\n  border: solid 1px #e6e6e6;\n  background-color: var(--white); }\n\nselect.form-control:not([size]):not([multiple]) {\n  height: 35px; }\n\n.modal-header {\n  border-bottom: 0px; }\n\n.modal-body {\n  padding: 6%; }\n\n.submitbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: var(--orange);\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: var(--white); }\n\n.cancelbtn {\n  width: 145px;\n  height: 36.8px;\n  border-radius: 10px;\n  background-color: white;\n  font-family: Muli;\n  font-size: 13px;\n  font-weight: 900;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.23;\n  letter-spacing: normal;\n  text-align: center;\n  color: black; }\n\n.modal-content {\n  border: 0px;\n  box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5); }\n\n.modal-footer {\n  justify-content: center;\n  border-top: 0px; }\n\n.form-control {\n  margin-top: 5%; }\n\n.modal-title {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: 600;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #b4b4b4;\n  margin-left: 30%;\n  margin-top: 5%; }\n\n.sa-title {\n  font-family: Muli;\n  font-size: 15px;\n  font-weight: bold;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.27;\n  letter-spacing: normal;\n  text-align: left;\n  color: #a8a8a8;\n  margin-left: 2%; }\n\n.PT20 {\n  padding-top: 20px !important; }\n\n/* Styles for tab labels */\n\n.mat-tab-label {\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 800;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left;\n  min-width: 90px !important;\n  padding: 5px;\n  background-color: transparent;\n  color: var(--dark-slate-blue);\n  font-weight: 800; }\n\n/* Styles for the active tab label */\n\n.mat-tab-label.mat-tab-label-active {\n  min-width: 90px !important;\n  padding: 5px;\n  background-color: transparent;\n  color: #223e60;\n  font-family: Muli;\n  font-size: 16px;\n  font-weight: 800;\n  font-style: normal;\n  font-stretch: normal;\n  line-height: 1.25;\n  letter-spacing: normal;\n  text-align: left; }\n\n/* Styles for the ink bar */\n\n.mat-ink-bar {\n  background-color: #223e60 !important; }\n\n/* custom tab css */\n\n.tabbable {\n  margin: 15px;\n  height: 600 px !important;\n  overflow: hidden;\n  border-bottom: 1px solid #b9b9b9; }\n\n.nav-tabs {\n  margin: 0; }\n\n.tab-content {\n  padding: 15px;\n  height: 90% !important;\n  border-left: 1px solid #b9b9b9  !important;\n  border-right: 1px solid #b9b9b9 !important; }\n\n.nav-tabs > .active > a, .nav-tabs > .active > a:hover, .nav-tabs > .active > a:focus {\n  border-color: #b9b9b9 !important;\n  border-bottom-color: transparent !important;\n  color: #223e60 !important; }\n\n.nav-tabs {\n  border-bottom: 1px solid #b9b9b9 !important; }\n\n.nav-tabs > li > a {\n  color: #b9b9b9 !important;\n  font-size: 14px !important;\n  font-family: Muli !important;\n  font-weight: 800 !important; }\n\n.flex-container {\n  display: flex;\n  width: 1068px;\n  height: 75px;\n  text-align: center;\n  border: 1px #ccc solid;\n  background: #ffffff;\n  padding: 10px;\n  margin: 25px;\n  border-radius: 10px;\n  line-height: 10px;\n  float: left;\n  margin-right: 40px;\n  font-size: 15px; }\n\n.flex-container > div {\n  margin: 10px;\n  padding: 10px;\n  font-size: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2p5b3RzbmEvY2VydHNkYXBwL3NyYy9hcHAvbGF5b3V0L3N1cGVyYWRtaW4vc3VwZXJhZG1pbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvbGF5b3V0L3N1cGVyYWRtaW4vc3VwZXJhZG1pbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWMsRUFBQTs7QUFFaEI7RUFDRSwwQkFBMEI7RUFDMUIsV0FBVyxFQUFBOztBQUViO0VBQ0ksWUFBWTtFQUVaLFlBQVk7RUFFWixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGlCQUFpQjtFQUVqQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsZ0JBQWdCO0VBRWhCLGNBQWMsRUFBQTs7QUFHbEI7RUFDRSxZQUFZO0VBQ1osYUFBYTtFQUNiLDhDQUE4QztFQUM5Qyw4QkFBOEIsRUFBQTs7QUFHL0I7RUFDQyxZQUFZO0VBQ1osYUFBYTtFQUNiLHdDQUF3QztFQUN4Qyw4QkFBOEIsRUFBQTs7QUFHaEM7RUFDSSxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUdsQjtFQUNJLGlCQUFpQjtFQUVyQixlQUFlO0VBQ2YsbUJBQW1CO0VBRW5CLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixrQkFBa0I7RUFFbEIsY0FBYyxFQUFBOztBQUlkO0VBQ0EsaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixnQkFBZ0I7RUFFaEIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixnQkFBZ0I7RUFFaEIsc0JBQXNCO0VBRXRCLGtCQUFrQjtFQUVsQixjQUFjLEVBQUE7O0FBR2Q7RUFDSSxlQUFlO0VBRWYsVUFBVTtFQUVWLGFBQWE7RUFFYixrQkFBa0I7RUFFbEIseUNBQXlDLEVBQUE7O0FBRTdDO0VBRUEsWUFBWTtFQUVaLFlBQVk7RUFFWixtQkFBbUI7RUFFbkIseUJBQXlCO0VBRXpCLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsZ0JBQWdCO0VBRWhCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixrQkFBa0I7RUFFbEIsbUJBQW1CO0VBRW5CLFdBQVc7RUFFWCxrQkFBa0I7RUFDbEIsY0FBQTtFQUNBLFNBQVM7RUFFVCx3Q0FBZ0M7VUFBaEMsZ0NBQWdDLEVBQUE7O0FBSWhDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7R0NwQ0c7O0FEeURIO0VBRVEsWUFBWTtFQUVaLFlBQVk7RUFFWixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGdCQUFnQjtFQUVoQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsZ0JBQWdCO0VBRWhCLGNBQWMsRUFBQTs7QUFNdEI7RUFFQSw2QkFBNkIsRUFBQTs7QUFLN0I7RUFDSSxhQUFhO0VBQ2IsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixtQ0FBbUMsRUFBQTs7QUFFdkM7RUFDSSxZQUFZO0VBQ1osYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixZQUFZO0VBQ1osbUJBQW1CO0VBRW5CLFdBQVc7RUFDWCxrQkFBa0IsRUFBQTs7QUFFdEI7RUFDSSxhQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBRXRCOzs7OztNQ3ZFTTs7QUQ4RU47RUFDRSxjQUFjLEVBQUE7O0FBRWhCO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0ksY0FBYyxFQUFBOztBQUVsQjtFQUNJLFdBQVc7RUFDWCxZQUFZO0VBQ1oseUJBQXlCLEVBQUE7O0FBRTNCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWix3Q0FBd0M7RUFDeEMseUJBQXlCLEVBQUE7O0FBRTNCO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWix5QkFBeUIsRUFBQTs7QUFFM0I7RUFDRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLDBDQUEwQztFQUMxQyx5QkFBd0I7RUFDeEIsWUFBWTtFQUNaLFdBQVcsRUFBQTs7QUFJYjtFQUNFLGtCQUFrQixFQUFBOztBQUVwQjtFQUNJLGtCQUFrQixFQUFBOztBQUd0QjtFQUNHLGNBQWM7RUFDZCxxQkFBcUIsRUFBQTs7QUFJMUI7RUFDRSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLG1CQUFtQixFQUFBOztBQUlyQjtFQUNFLGFBQWE7RUFFYixpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGlCQUFpQjtFQUVqQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGdCQUFnQjtFQUVoQixzQkFBc0I7RUFFdEIsZ0JBQWdCO0VBRWhCLGNBQWMsRUFBQTs7QUFuQmhCO0VBQ0UsYUFBYTtFQUViLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsaUJBQWlCO0VBRWpCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsZ0JBQWdCO0VBRWhCLHNCQUFzQjtFQUV0QixnQkFBZ0I7RUFFaEIsY0FBYyxFQUFBOztBQW5CaEI7RUFDRSxhQUFhO0VBRWIsaUJBQWlCO0VBRWpCLGVBQWU7RUFFZixpQkFBaUI7RUFFakIsa0JBQWtCO0VBRWxCLG9CQUFvQjtFQUVwQixnQkFBZ0I7RUFFaEIsc0JBQXNCO0VBRXRCLGdCQUFnQjtFQUVoQixjQUFjLEVBQUE7O0FBS2hCO0VBQ0UsWUFBWTtFQUVaLGtCQUFrQjtFQUVsQix5QkFBeUI7RUFFekIsOEJBQThCLEVBQUE7O0FBRy9CO0VBQ0MsWUFBWSxFQUFBOztBQUdkO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsV0FBVSxFQUFBOztBQUtaO0VBQ0UsWUFBWTtFQUVaLGNBQWM7RUFFZCxtQkFBbUI7RUFFbkIsK0JBQStCO0VBRS9CLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsZ0JBQWdCO0VBRWhCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixrQkFBa0I7RUFFbEIsbUJBQW1CLEVBQUE7O0FBS3JCO0VBQ0UsWUFBWTtFQUVaLGNBQWM7RUFFZCxtQkFBbUI7RUFFbkIsdUJBQXNCO0VBQ3RCLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsZ0JBQWdCO0VBRWhCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixrQkFBa0I7RUFFbEIsWUFBWSxFQUFBOztBQUtkO0VBQ0UsV0FBVztFQUNULHlDQUFxQyxFQUFBOztBQUd6QztFQUNFLHVCQUF1QjtFQUV6QixlQUFlLEVBQUE7O0FBSWY7RUFDRSxjQUFhLEVBQUE7O0FBS2Y7RUFDRSxpQkFBaUI7RUFFakIsZUFBZTtFQUVmLGdCQUFnQjtFQUVoQixrQkFBa0I7RUFFbEIsb0JBQW9CO0VBRXBCLGlCQUFpQjtFQUVqQixzQkFBc0I7RUFFdEIsZ0JBQWdCO0VBRWhCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQU1oQjtFQUNFLGlCQUFpQjtFQUVqQixlQUFlO0VBRWYsaUJBQWlCO0VBRWpCLGtCQUFrQjtFQUVsQixvQkFBb0I7RUFFcEIsaUJBQWlCO0VBRWpCLHNCQUFzQjtFQUV0QixnQkFBZ0I7RUFFaEIsY0FBYztFQUVkLGVBQWMsRUFBQTs7QUFvQmhCO0VBQ0UsNEJBQTRCLEVBQUE7O0FBRTlCLDBCQUFBOztBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLGdCQUFnQjtFQUNoQiwwQkFBMEI7RUFDMUIsWUFBWTtFQUNaLDZCQUE2QjtFQUM3Qiw2QkFBNkI7RUFDN0IsZ0JBQWdCLEVBQUE7O0FBR2xCLG9DQUFBOztBQUNBO0VBQ0UsMEJBQTBCO0VBQzFCLFlBQVk7RUFDWiw2QkFBNkI7RUFDN0IsY0FBYztFQUVkLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixvQkFBb0I7RUFDcEIsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixnQkFBZ0IsRUFBQTs7QUFHbEIsMkJBQUE7O0FBQ0E7RUFDRSxvQ0FBb0MsRUFBQTs7QUFHdEMsbUJBQUE7O0FBT0E7RUFDRSxZQUFZO0VBQ1oseUJBQXdCO0VBQ3hCLGdCQUFnQjtFQUNoQixnQ0FBZ0MsRUFBQTs7QUFHbEM7RUFDRSxTQUFTLEVBQUE7O0FBR1g7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLDBDQUEwQztFQUMxQywwQ0FBMEMsRUFBQTs7QUFHNUM7RUFDRSxnQ0FBZ0M7RUFDaEMsMkNBQTJDO0VBQzdDLHlCQUF1QixFQUFBOztBQUV2QjtFQUNFLDJDQUEyQyxFQUFBOztBQUc3QztFQUNFLHlCQUF3QjtFQUN4QiwwQkFBMEI7RUFDMUIsNEJBQTRCO0VBQzVCLDJCQUEyQixFQUFBOztBQUc3QjtFQUNFLGFBQWE7RUFDYixhQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFHaEI7RUFDQyxZQUFZO0VBQ1osYUFBYTtFQUNiLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xheW91dC9zdXBlcmFkbWluL3N1cGVyYWRtaW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xuICBmb250LXNpemU6MTVweDtcbn1cbnVsIGxpIGE6aG92ZXJ7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICB3aWR0aDogMTAwJTtcbn1cbi5hdXRob3JpemVyIHtcbiAgICB3aWR0aDogMTUwcHg7XG5cbiAgICBoZWlnaHQ6IDUwcHg7XG5cbiAgICBmb250LWZhbWlseTogTXVsaTtcblxuICAgIGZvbnQtc2l6ZTogMTRweDtcblxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuXG4gICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG5cbiAgICBsaW5lLWhlaWdodDogMS4yNztcblxuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG5cbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuXG4gICAgY29sb3I6ICNiOWI5Yjk7XG59XG5cbi5hY3RpdmVjYXJke1xuICB3aWR0aDogMzIycHg7XG4gIGhlaWdodDogMTQycHg7XG4gIGJveC1zaGFkb3c6IDNweCA1cHggMTBweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlKTtcbiB9XG4gXG4gLmluYWN0aXZlY2FyZCB7XG4gIHdpZHRoOiAzMjJweDtcbiAgaGVpZ2h0OiAxNDJweDtcbiAgYm94LXNoYWRvdzogMCAwIDNweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUpO1xuIH1cblxuLmlzc3VlciB7XG4gICAgd2lkdGg6IDE1NXB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgICBmb250LWZhbWlseTogTXVsaTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICAgIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBjb2xvcjogIzFiMzc2NDtcbn1cblxuLnN1cGVyYWRtaW50YWJsZXMgdGQge1xuICAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuXG5mb250LXNpemU6IDE2cHg7XG5mb250LXdlaWdodDogbm9ybWFsO1xuXG5mb250LXN0eWxlOiBub3JtYWw7XG5cbmZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG5saW5lLWhlaWdodDogMS4yNztcblxubGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcblxudGV4dC1hbGlnbjogY2VudGVyO1xuXG5jb2xvcjogIzQ4NDg0ODtcbn1cblxuXG4uc3VwZXJhZG1pbnRhYmxlcyB0aHtcbmZvbnQtZmFtaWx5OiBNdWxpO1xuXG5mb250LXNpemU6IDE1cHg7XG5cbmZvbnQtd2VpZ2h0OiA4MDA7XG5cbmZvbnQtc3R5bGU6IG5vcm1hbDtcblxuZm9udC1zdHJldGNoOiBub3JtYWw7XG5cbmxpbmUtaGVpZ2h0OiAxLjQ7XG5cbmxldHRlci1zcGFjaW5nOiBub3JtYWw7XG5cbnRleHQtYWxpZ246IGNlbnRlcjtcblxuY29sb3I6ICM3OTc5Nzk7XG59XG5cbi5zdXBlcmFkbWludGFibGVzIHtcbiAgICBtYXJnaW4tbGVmdDogMiU7XG5cbiAgICB3aWR0aDogOTglO1xuXG4gICAgaGVpZ2h0OiAzNTBweDtcblxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcblxuICAgIGJveC1zaGFkb3c6IDAgMCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMDgpO1xufVxuLmFkZHVzZXJidG57XG5cbndpZHRoOiAyMjBweDtcblxuaGVpZ2h0OiA1MHB4O1xuXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xuXG5iYWNrZ3JvdW5kLWNvbG9yOiAjZjg3ZDQyO1xuXG5mb250LWZhbWlseTogTXVsaTtcblxuZm9udC1zaXplOiAxNXB4O1xuXG5mb250LXdlaWdodDogOTAwO1xuXG5mb250LXN0eWxlOiBub3JtYWw7XG5cbmZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG5saW5lLWhlaWdodDogMS4yNTtcblxubGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcblxudGV4dC1hbGlnbjogY2VudGVyO1xuXG5jb2xvcjogdmFyKC0td2hpdGUpO1xuXG5mbG9hdDogbm9uZTtcblxucG9zaXRpb246IGFic29sdXRlO1xuLyogdG9wOiA1MCU7ICovXG5sZWZ0OiA1MCU7XG5cbnRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuXG59XG5cbi8qIC5hZGR1c2VyZm9udHtcbmZvbnQtZmFtaWx5OiBNdWxpO1xuXG5mb250LXNpemU6IDE2cHg7XG5cbmZvbnQtd2VpZ2h0OiA5MDA7XG5cbmZvbnQtc3R5bGU6IG5vcm1hbDtcblxuZm9udC1zdHJldGNoOiBub3JtYWw7XG5cbmxpbmUtaGVpZ2h0OiAxLjI1O1xuXG5sZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG50ZXh0LWFsaWduOiBjZW50ZXI7XG5cbmNvbG9yOiB2YXIoLS13aGl0ZSk7XG59ICovXG5cblxuLm1haW5saXN0IGF7XG5cbiAgICAgICAgd2lkdGg6IDEyNXB4O1xuICAgICAgXG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgIFxuICAgICAgICBmb250LWZhbWlseTogTXVsaTtcbiAgICAgIFxuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICBcbiAgICAgICAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgICAgIFxuICAgICAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgICBcbiAgICAgICAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gICAgICBcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuMjU7XG4gICAgICBcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgICAgIFxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgICAgXG4gICAgICAgIGNvbG9yOiAjMjIzZTYwO1xufVxuXG5cblxuXG4ubmF2LXRhYnMge1xuXG5ib3JkZXItYm90dG9tOiAwcHggc29saWQgI2RkZDtcblxufVxuXG5cbiNSZWN0YW5nbGUtODIwIHtcbiAgICB3aWR0aDogMTE0NHB4O1xuICAgIGhlaWdodDogNzY4cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXdoaXRlLWdyZXkpO1xuICB9XG4jZGl2MSB7XG4gICAgd2lkdGg6IDMwMHB4O1xuICAgIGhlaWdodDogMTUwcHg7O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3JkZXI6IDFweCAjY2NjIHNvbGlkO1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgcGFkZGluZzogMjVweDtcbiAgICBtYXJnaW46IDI1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcblxuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbi1yaWdodDogNDBweDtcbn1cbi5kaXYxMSB7XG4gICAgd2lkdGg6IDEwNjhweDtcbiAgICBoZWlnaHQ6IDc1cHg7O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBib3JkZXI6IDFweCAjY2NjIHNvbGlkO1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgcGFkZGluZzogMjVweDtcbiAgICBtYXJnaW46IDI1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBsaW5lLWhlaWdodDogNXB4O1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG1hcmdpbi1yaWdodDogNDBweDtcbn1cbi8qIHRhYmxlIHRke1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgbWFyZ2luOiA1cHg7XG5cbiAgICBcbiAgfSAgKi9cbiAgXG4uZGl2MTEgdGR7XG4gIHdpZHRoOiAzMy4zMzMlO1xufVxuLmRpdjExIHRkIHNwYW57XG4gIGZsb2F0OiBsZWZ0O1xufVxuXG5we1xuICAgIGNvbG9yOiAjMDAwMDAwO1xufVxuI0VsbGlwc2UtMjYxIHtcbiAgICB3aWR0aDogODVweDtcbiAgICBoZWlnaHQ6IDg1cHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYzIyMTtcbiAgfVxuICAjUmVjdGFuZ2xlLTg2MiB7XG4gICAgd2lkdGg6IDg1cHg7XG4gICAgaGVpZ2h0OiA4NXB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCAzcHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwYjE1ZjtcbiAgfVxuICAjRWxsaXBzZS0yNjIge1xuICAgIHdpZHRoOiA4NXB4O1xuICAgIGhlaWdodDogODVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY3NzkxO1xuICB9XG4gICNSZWN0YW5nbGUtODY0IHtcbiAgICB3aWR0aDogMTA2OHB4O1xuICAgIGhlaWdodDogMjBweDtcbiAgICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjojZmZmZmZmO1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICBtYXJnaW46IDJweDtcbiAgfVxuXG5cbiAgaW1nIHtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIH1cbiAgdGFibGUgdHIgdGR7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cblxuICBhOm5vdChbaHJlZl0pOm5vdChbdGFiaW5kZXhdKSB7XG4gICAgIGNvbG9yOiAjMjIzZTYwOyBcbiAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lOyBcbn1cblxuXG4ubW9kYWwtZGlhbG9nIHtcbiAgbWF4LXdpZHRoOiAyNSU7XG4gIG1hcmdpbi10b3A6IDE1JTtcbiAgbWFyZ2luLWxlZnQ6IDQwJTtcbn1cbi5tb2RhbC1jb250ZW50e1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xufVxuXG5cbi5mb3JtLWNvbnRyb2wgOjpwbGFjZWhvbGRlcntcbiAgb3BhY2l0eTogMC4zMTtcblxuICBmb250LWZhbWlseTogTXVsaTtcblxuICBmb250LXNpemU6IDEwcHg7XG5cbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG5cbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuXG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG5cbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcblxuICB0ZXh0LWFsaWduOiBsZWZ0O1xuXG4gIGNvbG9yOiAjYTBhMGEwO1xuXG59XG5cblxuLmZyb21jb250cm9sIGlucHV0e1xuICBoZWlnaHQ6IDM1cHg7XG5cbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuXG4gIGJvcmRlcjogc29saWQgMXB4ICNlNmU2ZTY7XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUpO1xuIH1cblxuIHNlbGVjdC5mb3JtLWNvbnRyb2w6bm90KFtzaXplXSk6bm90KFttdWx0aXBsZV0pIHtcbiAgaGVpZ2h0OiAzNXB4O1xufVxuXG4ubW9kYWwtaGVhZGVye1xuICBib3JkZXItYm90dG9tOiAwcHg7IFxufVxuLm1vZGFsLWJvZHl7XG4gIHBhZGRpbmc6NiU7XG4gIC8vIG1hcmdpbi10b3A6NSU7XG59XG5cblxuLnN1Ym1pdGJ0bntcbiAgd2lkdGg6IDE0NXB4O1xuXG4gIGhlaWdodDogMzYuOHB4O1xuXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tb3JhbmdlKTtcblxuICBmb250LWZhbWlseTogTXVsaTtcblxuICBmb250LXNpemU6IDEzcHg7XG5cbiAgZm9udC13ZWlnaHQ6IDkwMDtcblxuICBmb250LXN0eWxlOiBub3JtYWw7XG5cbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG5cbiAgbGluZS1oZWlnaHQ6IDEuMjM7XG5cbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcblxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgY29sb3I6IHZhcigtLXdoaXRlKTtcblxufVxuXG5cbi5jYW5jZWxidG57XG4gIHdpZHRoOiAxNDVweDtcblxuICBoZWlnaHQ6IDM2LjhweDtcblxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuXG4gIGJhY2tncm91bmQtY29sb3I6d2hpdGU7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuXG4gIGZvbnQtc2l6ZTogMTNweDtcblxuICBmb250LXdlaWdodDogOTAwO1xuXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcblxuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcblxuICBsaW5lLWhlaWdodDogMS4yMztcblxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICBjb2xvcjogYmxhY2s7XG5cbn1cblxuXG4ubW9kYWwtY29udGVudHtcbiAgYm9yZGVyOiAwcHg7XG4gICAgYm94LXNoYWRvdzogMCA1cHggMTVweCByZ2JhKDAsMCwwLC41KTsgXG59XG5cbi5tb2RhbC1mb290ZXJ7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAvLyBwYWRkaW5nOiAxcmVtO1xuYm9yZGVyLXRvcDogMHB4O1xufVxuXG5cbi5mb3JtLWNvbnRyb2x7XG4gIG1hcmdpbi10b3A6NSU7XG59XG5cblxuXG4ubW9kYWwtdGl0bGV7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuXG4gIGZvbnQtc2l6ZTogMTVweDtcblxuICBmb250LXdlaWdodDogNjAwO1xuXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcblxuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcblxuICBsaW5lLWhlaWdodDogMS4yNztcblxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG4gIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgY29sb3I6ICNiNGI0YjQ7IFxuICBtYXJnaW4tbGVmdDogMzAlO1xuICBtYXJnaW4tdG9wOiA1JTtcbiBcblxufVxuXG5cbi5zYS10aXRsZXtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG5cbiAgZm9udC1zaXplOiAxNXB4O1xuXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcblxuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcblxuICBsaW5lLWhlaWdodDogMS4yNztcblxuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuXG4gIHRleHQtYWxpZ246IGxlZnQ7XG5cbiAgY29sb3I6ICNhOGE4YTg7XG4gICBcbiAgbWFyZ2luLWxlZnQ6MiU7XG59XG4vLyAubmF2LXRhYnM+bGk+YSB7XG4vLyAgIGNvbG9yOiNiOWI5Yjk7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuLy8gICBmb250LXNpemU6IDE0cHg7XG4vLyAgIGZvbnQtZmFtaWx5OiBNdWxpO1xuLy8gICBmb250LXdlaWdodDogODAwO1xuLy8gICB9XG4gIFxuLy8gICAubmF2LXRhYnMgPiBsaSA+IGE6aG92ZXIsIC5uYXYtdGFicyA+IC5hY3RpdmUgPiBhLCAubmF2LXRhYnMgPiAuYWN0aXZlID4gYTpob3ZlciB7XG4vLyAgIGNvbG9yOiB3aGl0ZTtcbi8vICAgYmFja2dyb3VuZDogYmx1ZTtcbi8vICAgfVxuXG4vLyAgIC5uYXYtdGFicz4uYWN0aXZlPmEsIC5uYXYtdGFicz4uYWN0aXZlPmE6aG92ZXIsIC5uYXYtdGFicz4uYWN0aXZlPmE6Zm9jdXMge1xuLy8gICAgIGJvcmRlci1jb2xvcjogI2Q0NTUwMDtcbi8vICAgICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcbi8vIH1cblxuLlBUMjB7XG4gIHBhZGRpbmctdG9wOiAyMHB4ICFpbXBvcnRhbnQ7XG59XG4vKiBTdHlsZXMgZm9yIHRhYiBsYWJlbHMgKi9cbi5tYXQtdGFiLWxhYmVsIHtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIG1pbi13aWR0aDogOTBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogdmFyKC0tZGFyay1zbGF0ZS1ibHVlKTs7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG5cbi8qIFN0eWxlcyBmb3IgdGhlIGFjdGl2ZSB0YWIgbGFiZWwgKi9cbi5tYXQtdGFiLWxhYmVsLm1hdC10YWItbGFiZWwtYWN0aXZlIHtcbiAgbWluLXdpZHRoOiA5MHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDVweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiMyMjNlNjAgOyAgXG4gIC8vIGhlaWdodDogMjBweDtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi8qIFN0eWxlcyBmb3IgdGhlIGluayBiYXIgKi9cbi5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMjNlNjAgIWltcG9ydGFudDtcbn1cblxuLyogY3VzdG9tIHRhYiBjc3MgKi9cbi8vIFtyb2xlPVwidGFiXCJdW2FyaWEtc2VsZWN0ZWQ9XCJ0cnVlXCJdIHtcbi8vICAgYm9yZGVyLXJhZGl1czogMDtcbi8vICAgY29sb3I6ICMxYjM3NjQ7XG4vLyAgIC8vIG91dGxpbmU6IDA7XG4vLyB9XG5cbi50YWJiYWJsZSB7XG4gIG1hcmdpbjogMTVweDtcbiAgaGVpZ2h0OiA2MDAgcHghaW1wb3J0YW50O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2I5YjliOTtcbn1cblxuLm5hdi10YWJzIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4udGFiLWNvbnRlbnQge1xuICBwYWRkaW5nOiAxNXB4O1xuICBoZWlnaHQ6IDkwJSAhaW1wb3J0YW50OyBcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjYjliOWI5ICAhaW1wb3J0YW50OyBcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2I5YjliOSAhaW1wb3J0YW50O1xufVxuXG4ubmF2LXRhYnM+LmFjdGl2ZT5hLCAubmF2LXRhYnM+LmFjdGl2ZT5hOmhvdmVyLCAubmF2LXRhYnM+LmFjdGl2ZT5hOmZvY3VzIHtcbiAgYm9yZGVyLWNvbG9yOiAjYjliOWI5ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG5jb2xvcjojMjIzZTYwIWltcG9ydGFudDtcbn1cbi5uYXYtdGFicyB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYjliOWI5ICFpbXBvcnRhbnQ7XG5cbn1cbi5uYXYtdGFicz5saT4gYXtcbiAgY29sb3I6I2I5YjliOSAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgZm9udC1mYW1pbHk6IE11bGkgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDgwMCAhaW1wb3J0YW50O1xufVxuXG4uZmxleC1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTA2OHB4O1xuICBoZWlnaHQ6IDc1cHg7O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4ICNjY2Mgc29saWQ7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIG1hcmdpbjogMjVweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbGluZS1oZWlnaHQ6IDEwcHg7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tcmlnaHQ6IDQwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiB9XG4gXG4gLmZsZXgtY29udGFpbmVyID4gZGl2IHtcbiAgbWFyZ2luOiAxMHB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICBmb250LXNpemU6IDE1cHg7XG4gfSIsIi5jb250YWluZXIge1xuICBmb250LXNpemU6IDE1cHg7IH1cblxudWwgbGkgYTpob3ZlciB7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICB3aWR0aDogMTAwJTsgfVxuXG4uYXV0aG9yaXplciB7XG4gIHdpZHRoOiAxNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmb250LWZhbWlseTogTXVsaTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjc7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiAjYjliOWI5OyB9XG5cbi5hY3RpdmVjYXJkIHtcbiAgd2lkdGg6IDMyMnB4O1xuICBoZWlnaHQ6IDE0MnB4O1xuICBib3gtc2hhZG93OiAzcHggNXB4IDEwcHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7IH1cblxuLmluYWN0aXZlY2FyZCB7XG4gIHdpZHRoOiAzMjJweDtcbiAgaGVpZ2h0OiAxNDJweDtcbiAgYm94LXNoYWRvdzogMCAwIDNweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0td2hpdGUpOyB9XG5cbi5pc3N1ZXIge1xuICB3aWR0aDogMTU1cHg7XG4gIGhlaWdodDogNTBweDtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzFiMzc2NDsgfVxuXG4uc3VwZXJhZG1pbnRhYmxlcyB0ZCB7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI3O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjNDg0ODQ4OyB9XG5cbi5zdXBlcmFkbWludGFibGVzIHRoIHtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuNDtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzc5Nzk3OTsgfVxuXG4uc3VwZXJhZG1pbnRhYmxlcyB7XG4gIG1hcmdpbi1sZWZ0OiAyJTtcbiAgd2lkdGg6IDk4JTtcbiAgaGVpZ2h0OiAzNTBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBib3gtc2hhZG93OiAwIDAgNXB4IDAgcmdiYSgwLCAwLCAwLCAwLjA4KTsgfVxuXG4uYWRkdXNlcmJ0biB7XG4gIHdpZHRoOiAyMjBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjg3ZDQyO1xuICBmb250LWZhbWlseTogTXVsaTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogOTAwO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICBsaW5lLWhlaWdodDogMS4yNTtcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogdmFyKC0td2hpdGUpO1xuICBmbG9hdDogbm9uZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICAvKiB0b3A6IDUwJTsgKi9cbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTsgfVxuXG4vKiAuYWRkdXNlcmZvbnR7XG5mb250LWZhbWlseTogTXVsaTtcblxuZm9udC1zaXplOiAxNnB4O1xuXG5mb250LXdlaWdodDogOTAwO1xuXG5mb250LXN0eWxlOiBub3JtYWw7XG5cbmZvbnQtc3RyZXRjaDogbm9ybWFsO1xuXG5saW5lLWhlaWdodDogMS4yNTtcblxubGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcblxudGV4dC1hbGlnbjogY2VudGVyO1xuXG5jb2xvcjogdmFyKC0td2hpdGUpO1xufSAqL1xuLm1haW5saXN0IGEge1xuICB3aWR0aDogMTI1cHg7XG4gIGhlaWdodDogMjBweDtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjU7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiAjMjIzZTYwOyB9XG5cbi5uYXYtdGFicyB7XG4gIGJvcmRlci1ib3R0b206IDBweCBzb2xpZCAjZGRkOyB9XG5cbiNSZWN0YW5nbGUtODIwIHtcbiAgd2lkdGg6IDExNDRweDtcbiAgaGVpZ2h0OiA3NjhweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZS1ncmV5KTsgfVxuXG4jZGl2MSB7XG4gIHdpZHRoOiAzMDBweDtcbiAgaGVpZ2h0OiAxNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXI6IDFweCAjY2NjIHNvbGlkO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBwYWRkaW5nOiAyNXB4O1xuICBtYXJnaW46IDI1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGZsb2F0OiBsZWZ0O1xuICBtYXJnaW4tcmlnaHQ6IDQwcHg7IH1cblxuLmRpdjExIHtcbiAgd2lkdGg6IDEwNjhweDtcbiAgaGVpZ2h0OiA3NXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlcjogMXB4ICNjY2Mgc29saWQ7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDI1cHg7XG4gIG1hcmdpbjogMjVweDtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbGluZS1oZWlnaHQ6IDVweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1yaWdodDogNDBweDsgfVxuXG4vKiB0YWJsZSB0ZHtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICAgIG1hcmdpbjogNXB4O1xuXG4gICAgXG4gIH0gICovXG4uZGl2MTEgdGQge1xuICB3aWR0aDogMzMuMzMzJTsgfVxuXG4uZGl2MTEgdGQgc3BhbiB7XG4gIGZsb2F0OiBsZWZ0OyB9XG5cbnAge1xuICBjb2xvcjogIzAwMDAwMDsgfVxuXG4jRWxsaXBzZS0yNjEge1xuICB3aWR0aDogODVweDtcbiAgaGVpZ2h0OiA4NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZjMjIxOyB9XG5cbiNSZWN0YW5nbGUtODYyIHtcbiAgd2lkdGg6IDg1cHg7XG4gIGhlaWdodDogODVweDtcbiAgYm94LXNoYWRvdzogMCAwIDNweCAwIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwYjE1ZjsgfVxuXG4jRWxsaXBzZS0yNjIge1xuICB3aWR0aDogODVweDtcbiAgaGVpZ2h0OiA4NXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY3NzkxOyB9XG5cbiNSZWN0YW5nbGUtODY0IHtcbiAgd2lkdGg6IDEwNjhweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBib3gtc2hhZG93OiAwIDFweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMSk7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IDBweDtcbiAgbWFyZ2luOiAycHg7IH1cblxuaW1nIHtcbiAgYm9yZGVyLXJhZGl1czogNTAlOyB9XG5cbnRhYmxlIHRyIHRkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbmE6bm90KFtocmVmXSk6bm90KFt0YWJpbmRleF0pIHtcbiAgY29sb3I6ICMyMjNlNjA7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgfVxuXG4ubW9kYWwtZGlhbG9nIHtcbiAgbWF4LXdpZHRoOiAyNSU7XG4gIG1hcmdpbi10b3A6IDE1JTtcbiAgbWFyZ2luLWxlZnQ6IDQwJTsgfVxuXG4ubW9kYWwtY29udGVudCB7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7IH1cblxuLmZvcm0tY29udHJvbCA6OnBsYWNlaG9sZGVyIHtcbiAgb3BhY2l0eTogMC4zMTtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiAjYTBhMGEwOyB9XG5cbi5mcm9tY29udHJvbCBpbnB1dCB7XG4gIGhlaWdodDogMzVweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBib3JkZXI6IHNvbGlkIDFweCAjZTZlNmU2O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS13aGl0ZSk7IH1cblxuc2VsZWN0LmZvcm0tY29udHJvbDpub3QoW3NpemVdKTpub3QoW211bHRpcGxlXSkge1xuICBoZWlnaHQ6IDM1cHg7IH1cblxuLm1vZGFsLWhlYWRlciB7XG4gIGJvcmRlci1ib3R0b206IDBweDsgfVxuXG4ubW9kYWwtYm9keSB7XG4gIHBhZGRpbmc6IDYlOyB9XG5cbi5zdWJtaXRidG4ge1xuICB3aWR0aDogMTQ1cHg7XG4gIGhlaWdodDogMzYuOHB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1vcmFuZ2UpO1xuICBmb250LWZhbWlseTogTXVsaTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogOTAwO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICBsaW5lLWhlaWdodDogMS4yMztcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogdmFyKC0td2hpdGUpOyB9XG5cbi5jYW5jZWxidG4ge1xuICB3aWR0aDogMTQ1cHg7XG4gIGhlaWdodDogMzYuOHB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgZm9udC1mYW1pbHk6IE11bGk7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXN0cmV0Y2g6IG5vcm1hbDtcbiAgbGluZS1oZWlnaHQ6IDEuMjM7XG4gIGxldHRlci1zcGFjaW5nOiBub3JtYWw7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IGJsYWNrOyB9XG5cbi5tb2RhbC1jb250ZW50IHtcbiAgYm9yZGVyOiAwcHg7XG4gIGJveC1zaGFkb3c6IDAgNXB4IDE1cHggcmdiYSgwLCAwLCAwLCAwLjUpOyB9XG5cbi5tb2RhbC1mb290ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYm9yZGVyLXRvcDogMHB4OyB9XG5cbi5mb3JtLWNvbnRyb2wge1xuICBtYXJnaW4tdG9wOiA1JTsgfVxuXG4ubW9kYWwtdGl0bGUge1xuICBmb250LWZhbWlseTogTXVsaTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICBsaW5lLWhlaWdodDogMS4yNztcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICNiNGI0YjQ7XG4gIG1hcmdpbi1sZWZ0OiAzMCU7XG4gIG1hcmdpbi10b3A6IDUlOyB9XG5cbi5zYS10aXRsZSB7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtc3RyZXRjaDogbm9ybWFsO1xuICBsaW5lLWhlaWdodDogMS4yNztcbiAgbGV0dGVyLXNwYWNpbmc6IG5vcm1hbDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICNhOGE4YTg7XG4gIG1hcmdpbi1sZWZ0OiAyJTsgfVxuXG4uUFQyMCB7XG4gIHBhZGRpbmctdG9wOiAyMHB4ICFpbXBvcnRhbnQ7IH1cblxuLyogU3R5bGVzIGZvciB0YWIgbGFiZWxzICovXG4ubWF0LXRhYi1sYWJlbCB7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBtaW4td2lkdGg6IDkwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZzogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6IHZhcigtLWRhcmstc2xhdGUtYmx1ZSk7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7IH1cblxuLyogU3R5bGVzIGZvciB0aGUgYWN0aXZlIHRhYiBsYWJlbCAqL1xuLm1hdC10YWItbGFiZWwubWF0LXRhYi1sYWJlbC1hY3RpdmUge1xuICBtaW4td2lkdGg6IDkwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZzogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICMyMjNlNjA7XG4gIGZvbnQtZmFtaWx5OiBNdWxpO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC1zdHJldGNoOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjI1O1xuICBsZXR0ZXItc3BhY2luZzogbm9ybWFsO1xuICB0ZXh0LWFsaWduOiBsZWZ0OyB9XG5cbi8qIFN0eWxlcyBmb3IgdGhlIGluayBiYXIgKi9cbi5tYXQtaW5rLWJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMjNlNjAgIWltcG9ydGFudDsgfVxuXG4vKiBjdXN0b20gdGFiIGNzcyAqL1xuLnRhYmJhYmxlIHtcbiAgbWFyZ2luOiAxNXB4O1xuICBoZWlnaHQ6IDYwMCBweCAhaW1wb3J0YW50O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2I5YjliOTsgfVxuXG4ubmF2LXRhYnMge1xuICBtYXJnaW46IDA7IH1cblxuLnRhYi1jb250ZW50IHtcbiAgcGFkZGluZzogMTVweDtcbiAgaGVpZ2h0OiA5MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjYjliOWI5ICAhaW1wb3J0YW50O1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjYjliOWI5ICFpbXBvcnRhbnQ7IH1cblxuLm5hdi10YWJzID4gLmFjdGl2ZSA+IGEsIC5uYXYtdGFicyA+IC5hY3RpdmUgPiBhOmhvdmVyLCAubmF2LXRhYnMgPiAuYWN0aXZlID4gYTpmb2N1cyB7XG4gIGJvcmRlci1jb2xvcjogI2I5YjliOSAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzIyM2U2MCAhaW1wb3J0YW50OyB9XG5cbi5uYXYtdGFicyB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYjliOWI5ICFpbXBvcnRhbnQ7IH1cblxuLm5hdi10YWJzID4gbGkgPiBhIHtcbiAgY29sb3I6ICNiOWI5YjkgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtZmFtaWx5OiBNdWxpICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiA4MDAgIWltcG9ydGFudDsgfVxuXG4uZmxleC1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogMTA2OHB4O1xuICBoZWlnaHQ6IDc1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggI2NjYyBzb2xpZDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgcGFkZGluZzogMTBweDtcbiAgbWFyZ2luOiAyNXB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBsaW5lLWhlaWdodDogMTBweDtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1yaWdodDogNDBweDtcbiAgZm9udC1zaXplOiAxNXB4OyB9XG5cbi5mbGV4LWNvbnRhaW5lciA+IGRpdiB7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZzogMTBweDtcbiAgZm9udC1zaXplOiAxNXB4OyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/layout/superadmin/superadmin.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/superadmin/superadmin.component.ts ***!
  \***********************************************************/
/*! exports provided: SuperadminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperadminComponent", function() { return SuperadminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_common_common_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/common/common.service */ "./src/app/services/common/common.service.ts");
/* harmony import */ var src_app_services_superadmin_superadmin_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/superadmin/superadmin.service */ "./src/app/services/superadmin/superadmin.service.ts");
/* harmony import */ var src_app_services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/localstorage/localstorage.service */ "./src/app/services/localstorage/localstorage.service.ts");
/* harmony import */ var src_app_services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/dashboard/dashboard.service */ "./src/app/services/dashboard/dashboard.service.ts");






var SuperadminComponent = /** @class */ (function () {
    //  public onChangeDepartment:Function;
    // private getIssuers: Function;
    function SuperadminComponent(commonService, superadminService, dashboardService, local) {
        this.commonService = commonService;
        this.superadminService = superadminService;
        this.dashboardService = dashboardService;
        this.local = local;
        this.show = true;
        this.tableshow = true;
        this.deps = [];
        this.show1 = true;
        this.display = 'none';
        this.count = 0;
        this.activeFlag = 1;
        this.display_pending_issued = false;
        this.display_pending_authorization = true;
        this.display_certificate_rejected = false;
    }
    SuperadminComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.commonService.getCountries().subscribe(function (response) {
            _this.countries = response;
        });
        this.getStatisticCount();
        //this.getAuthorizers();
        this.getIssuers();
        this.dappid = this.local.getDappId();
    };
    SuperadminComponent.prototype.getStatisticCount = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var res;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("Inside");
                        return [4 /*yield*/, this.dashboardService.getSuperuserData()];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        this.pendingCountAuth = res['pendingAuthorizationCount'];
                        this.pendingCountIssue = res['pendingIssuesCount'];
                        this.pendingCountRejected = res['rejectedIssuesCount'];
                        return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.getpendingauth = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.activeFlag = 1;
                        console.log("In getpendingauth");
                        this.display_pending_authorization = true;
                        this.display_certificate_rejected = false;
                        this.display_pending_issued = false;
                        return [4 /*yield*/, this.superadminService.getpendingauthresult()];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        // const response = {
                        //   "isSuccess": true,
                        //   "total": 1,
                        //   "pendingAuthorization": [
                        //     {
                        //       "pid": "3",
                        //       "iid": "1",
                        //       "hash": "jXWWc019RenVBbn/AQuyMuKul9Jtq8J8vlk1AIHyMZ8=",
                        //       "sign": "KJ7pHqaG3KgOIsxplxjA2Sfl++oThvXMs+hWiyYPAAoAussbrZiiN4zYJyv+CeTOaG4IO/s6qua+rzJnwKi/AQ==",
                        //       "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //       "timestampp": 1551780962079,
                        //       "status": "pending",
                        //       "authLevel": 2,
                        //       "empid": "JohnBonda",
                        //       "transactionId": "-",
                        //       "did": "1",
                        //       "data": "{\"Name\":\"John Vijaqwdssy Bonda\",\"Address\":\"Visaqwdkhssapatnam\",\"Course\":\"CSsqwdsE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}"
                        //     }
                        //   ],
                        //   "success": true
                        // }
                        this.res1 = response['pendingAuthorization'];
                        return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.getpendingissues = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("In getpendingissues");
                        this.activeFlag = 2;
                        this.display_certificate_rejected = false;
                        this.display_pending_issued = true;
                        this.display_pending_authorization = false;
                        return [4 /*yield*/, this.superadminService.getpendingissueresult()];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        // const response = {
                        //   "isSuccess": true,
                        //   "total": 1,
                        //   "pendingIssues": [
                        //     {
                        //       "pid": "4",
                        //       "iid": "1",
                        //       "hash": "0KMmQao4WsdHlNKXksIHzvQVofMA8qAPO83rHiQVo+Q=",
                        //       "sign": "CnJSF9xMwvCaGZmNNzSjanvwxmqCCq80my7RJvS06du0AD2SdVDBovRxWMFPPC6xmduclrcwt0PSbIW0zLaFBA==",
                        //       "publickey": "a54ae3542d908af53725e447d1fbb942e9ccf4bafa7f3c612c47e71781c245c6",
                        //       "timestampp": 1551781037229,
                        //       "status": "authorized",
                        //       "authLevel": 3,
                        //       "empid": "JohnBonda",
                        //       "transactionId": "-",
                        //       "did": "1",
                        //       "departmentName": "HR",
                        //       "data": "{\"Name\":\"John Vijaqwsddssy Bonda\",\"Address\":\"Visaqwdkhsdssapatnam\",\"Course\":\"CSsqwasddsE\",\"Marks\":100,\"identity\":{\"Adhar Card\":\"35298df293852\"}}"
                        //     }
                        //   ],
                        //   "success": true
                        // }
                        this.res2 = response["pendingIssues"];
                        return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.getcertificatesRej = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("In getcertificates");
                        this.activeFlag = 3;
                        this.display_certificate_rejected = true;
                        this.display_pending_issued = false;
                        this.display_pending_authorization = false;
                        return [4 /*yield*/, this.superadminService.getcertificaterejresult()];
                    case 1:
                        response = _a.sent();
                        console.log(response);
                        // const response = {
                        //   "isSuccess": true,
                        //   "total": 1,
                        //   "pendingAuthorization": [
                        //     {
                        //       "pid": "2",
                        //       "aid": "1",
                        //       "iid": "1",
                        //       "reason": "Very bad certificate",
                        //       "timestampp": 1551781017359
                        //     }
                        //   ],
                        //   "success": true
                        // }
                        this.res3 = response['rejectedIssues'];
                        console.log(this.res3);
                        return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.getdepoptions = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var res, result;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.commonService.getDepartmentsApi()];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        result = res['departments'];
                        this.deptnames = result;
                        console.log(result);
                        return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.setflag = function (a) {
        console.log(a);
        console.log("clicked");
        if (a == 'issuer') {
            console.log("issuer");
            this.show = true;
        }
        else {
            console.log("auth");
            this.show = false;
        }
        console.log(this.show);
    };
    SuperadminComponent.prototype.showpage = function (b) {
        if (b == 'initial') {
            this.show1 = true;
        }
        else {
            this.show1 = false;
        }
    };
    SuperadminComponent.prototype.onsubmitIssuer = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var sel, finaldep, i, params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        //   var finaldep=[];
                        //  var dep={};
                        //  dep['name']=this.selectedoption;
                        //  finaldep[0]=dep;
                        //  dep[0]=this.selectedoption
                        this.e = (document.getElementById("ide"));
                        sel = this.e.selectedIndex - 1;
                        this.countrycode = this.countries[sel]['countryCode'];
                        this.countrycode = this.countrycode ? this.countrycode : "undefined";
                        this.countryid = this.countries[sel]['countryID'];
                        console.log(this.countryid + this.countrycode);
                        finaldep = [];
                        //  var dep={};
                        console.log(this.selectedoption.length + this.selectedoption);
                        console.log(typeof (this.selectedoption));
                        //  dep['name']=this.selectedoption;
                        //  finaldep[0]=dep;
                        // "departments": [
                        //   {
                        //       "name": "HR"
                        //   }]
                        for (i = 0; i < 3; i++) {
                            console.log(this.selectedoption[i]);
                        }
                        for (i = 0; i < this.selectedoption.length; i++) {
                            // dep['name']=this.selectedoption[i];
                            finaldep[i] = this.createobj(this.selectedoption[i]);
                            console.log(finaldep[i]);
                            console.log(finaldep);
                        }
                        console.log(finaldep);
                        params = {
                            email: this.email,
                            countryId: this.countryid,
                            countrycode: this.countrycode,
                            name: this.issuername,
                            departments: finaldep,
                            type: "merchant",
                            dappid: this.dappid,
                            role: "issuer"
                        };
                        console.log(params);
                        return [4 /*yield*/, this.superadminService.addUser(params)];
                    case 1:
                        promise = _a.sent();
                        console.log(promise);
                        if (!(promise['isSuccess'] === true)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.getIssuers()];
                    case 2:
                        _a.sent();
                        alert("Issuer Registered Successfully");
                        return [3 /*break*/, 4];
                    case 3:
                        alert(promise['message']);
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.createobj = function (dep) {
        var department = {};
        department['name'] = dep;
        return department;
    };
    SuperadminComponent.prototype.onsubmitAuthorizer = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var sel, params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log("hey");
                        this.e = (document.getElementById("ide1"));
                        sel = this.e.selectedIndex - 1;
                        this.countrycode = this.countries[sel]['countryCode'];
                        this.countrycode = this.countrycode ? this.countrycode : "undefined";
                        this.countryid = this.countries[sel]['countryID'];
                        console.log(this.countryid + this.countrycode);
                        console.log(this.countrycode);
                        params = {
                            countryId: String(this.countryid),
                            countryCode: this.countrycode,
                            name: this.authName,
                            email: this.authid,
                            type: "user",
                            dappid: this.dappid,
                            role: "authorizer"
                        };
                        console.log(params);
                        return [4 /*yield*/, this.superadminService.addAuthorizer(params)];
                    case 1:
                        promise = _a.sent();
                        if (!(promise['isSuccess'] === true)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.getAuthorizers()];
                    case 2:
                        _a.sent();
                        alert("Authorizer Registered Successfully");
                        return [3 /*break*/, 4];
                    case 3:
                        alert(promise['message']);
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.getIssuers = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var count_issuers, index, res, total, total_issuers, issuer_data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        count_issuers = 0;
                        index = 1;
                        return [4 /*yield*/, this.getIssuersfun()];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        console.log(res["total"]);
                        total = res["total"];
                        total_issuers = total;
                        count_issuers = total_issuers;
                        if (total_issuers === 0) {
                            console.log("addIssuer");
                        }
                        issuer_data = '';
                        this.issuerresponse = res["issuers"];
                        console.log(this.issuerresponse);
                        return [2 /*return*/];
                }
            });
        });
    };
    // else{
    //   console.log("invalid");
    // }  
    // }
    SuperadminComponent.prototype.getAuthorizers = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var count_issuers, index, res, total, total_issuers;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        count_issuers = 0;
                        index = 1;
                        return [4 /*yield*/, this.getAuthorizerfun()];
                    case 1:
                        res = _a.sent();
                        console.log(res);
                        console.log(res["total"]);
                        total = res["total"];
                        total_issuers = total;
                        count_issuers = total_issuers;
                        if (total_issuers === 0) {
                            console.log("addAuthorizer");
                        }
                        this.authorizerresponse = res["authorizers"];
                        console.log(this.authorizerresponse);
                        return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.getAuthorizerfun = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.superadminService.getAuthorizerAPI()];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    SuperadminComponent.prototype.getIssuersfun = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.superadminService.getIssuersAPI()];
                    case 1:
                        promise = _a.sent();
                        return [2 /*return*/, promise];
                }
            });
        });
    };
    SuperadminComponent.prototype.issuerremove = function (iid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            iid: iid
                        };
                        return [4 /*yield*/, this.superadminService.removeissuersAPI(params)];
                    case 1:
                        promise = _a.sent();
                        if (!(promise['isSuccess'] === true)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.getIssuers()];
                    case 2:
                        _a.sent();
                        alert("Issuer removed successfully");
                        return [3 /*break*/, 4];
                    case 3:
                        alert("failed");
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent.prototype.authremove1 = function (aid) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var params, promise;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        params = {
                            aid: aid
                        };
                        return [4 /*yield*/, this.superadminService.removeauthorizersAPI(params)];
                    case 1:
                        promise = _a.sent();
                        if (!(promise['isSuccess'] === true)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.getAuthorizers()];
                    case 2:
                        _a.sent();
                        alert("authorizer removed successfully");
                        return [3 /*break*/, 4];
                    case 3:
                        alert("failed");
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SuperadminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-superadmin',
            template: __webpack_require__(/*! ./superadmin.component.html */ "./src/app/layout/superadmin/superadmin.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./superadmin.component.scss */ "./src/app/layout/superadmin/superadmin.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_common_common_service__WEBPACK_IMPORTED_MODULE_2__["CommonService"],
            src_app_services_superadmin_superadmin_service__WEBPACK_IMPORTED_MODULE_3__["SuperadminService"],
            src_app_services_dashboard_dashboard_service__WEBPACK_IMPORTED_MODULE_5__["DashboardService"],
            src_app_services_localstorage_localstorage_service__WEBPACK_IMPORTED_MODULE_4__["LocalstorageService"]])
    ], SuperadminComponent);
    return SuperadminComponent;
}());



/***/ }),

/***/ "./src/app/layout/superadmin/superadmin.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/superadmin/superadmin.module.ts ***!
  \********************************************************/
/*! exports provided: SuperadminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuperadminModule", function() { return SuperadminModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _superadmin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./superadmin.component */ "./src/app/layout/superadmin/superadmin.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _superadmin_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./superadmin-routing.module */ "./src/app/layout/superadmin/superadmin-routing.module.ts");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");







var SuperadminModule = /** @class */ (function () {
    function SuperadminModule() {
    }
    SuperadminModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_superadmin_component__WEBPACK_IMPORTED_MODULE_3__["SuperadminComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _superadmin_routing_module__WEBPACK_IMPORTED_MODULE_5__["SuperadminRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_6__["MatTabsModule"]
            ]
        })
    ], SuperadminModule);
    return SuperadminModule;
}());



/***/ })

}]);
//# sourceMappingURL=superadmin-superadmin-module.js.map