export interface NavItem {
    displayName: string;
    displayId: number;
    disabled?: boolean;
    iconName: string;
    route?: string;
    children?: NavItem[];
  }
  