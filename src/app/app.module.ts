import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {CommonModule} from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthguardService } from './services/guard/authguard.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { NavService } from '../nav.service';
import { MatListModule,MatIconModule,  MatMenuModule, MatInputModule,MatFormFieldModule } from '@angular/material';
import { ShareDataService } from './layout/certificate/shareData.service';
import { BrowserModule} from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WindowRefService } from './services/WindowRef/window-ref.service';
import { RequestOptionsService} from './services/requestoptions/request-options.service';
import { ShareModule } from './share.module';
import { BlockUIModule } from 'ng-block-ui';
// import { MdRadioModule } from '@angular2-material/radio';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatInputModule,
    MatFormFieldModule,    
    ShareModule,
    BlockUIModule.forRoot()
  ],
  providers: [AuthguardService, NavService, ShareDataService,WindowRefService,RequestOptionsService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
