import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthguardService } from './services/guard/authguard.service';

const routes: Routes = [
  { path: '', loadChildren: './layout/layout.module#LayoutModule', canActivate: [AuthguardService]},
  { path: 'home', loadChildren: './home/home.module#HomeModule' },
  { path: 'login/email/:email', loadChildren: './login/login.module#LoginModule' },
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'register',loadChildren:'./register/register.module#RegisterModule' },
  { path: 'register/email/:email', loadChildren: './register/register.module#RegisterModule' },
  { path : 'asset-select', loadChildren:'./assets-select/asset-select.module#AssetSelectModule'}
];
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
