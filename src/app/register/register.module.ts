import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegisterComponent } from './register.component';
import { RegisterRoutingModule } from './register-routing.module';
import {FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ShareModule } from '../share.module'; 
import { NgxCaptchaModule } from 'ngx-captcha';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,
    NgxCaptchaModule
  ]
})
export class RegisterModule { }
