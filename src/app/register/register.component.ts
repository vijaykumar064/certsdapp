import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'; 
import { RegisterService } from '../services/register/register.service';
import { CommonService } from '../services/common/common.service';
import { Reg } from '../reg';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ValidationService } from 'src/app/services/Validations/validation.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    @BlockUI() blockUI: NgBlockUI;
  AlertModal:boolean=false;
  heading:string;
  alertmsg:string;
  countrycode: string;
  countryid: string;
  type: string = "merchant";
  regList :Reg[]=[];
  email:string;
  password:string;
  name:string;
  lastName:string;
  ErrorModal: boolean=false;
  errorMessage: string;
  alertHead: string;
  regForm: FormGroup;
  countries:{}; 
  companyname1:FormControl;
  params:string;
  disableemail:boolean=false;
  public captchaIsLoaded = false;
  public captchaSuccess = false;
  public captchaIsExpired = false;
  public captchaResponse?: string;

  public theme: 'light' | 'dark' = 'light';
  public size: 'compact' | 'normal' = 'normal';
  public lang = 'en';
  constructor(private formBuilder: FormBuilder,private router: Router, private actRoute: ActivatedRoute,private servService: RegisterService,private commonService:CommonService) { 
    this.actRoute.queryParams.subscribe(params => {
      this.params = this.actRoute.snapshot.params.email;
      
    });
  }
    

    async ngOnInit() {
        try{
        this.registerForm = this.formBuilder.group({
            universityName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]],
            userName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z ]{2,20}$/)]],
            AdminEmail: ['', [Validators.required, Validators.email]],
            country: ['', Validators.required],
            password: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,14}$/)]],
          //  confirmpassword: ['', [Validators.required,(control => ValidationService.confirmPassword(control, this.registerForm, 'newPassword'))]]
          confirmpassword: ['', [Validators.required]]
        },
        {
          validator:[
            //ValidationService.match('password','confirmPassword'),
            ValidationService.MatchPassword
          ]
         });

      const response = await this.commonService.getCountries();
      this.countries = response;  
      if(this.params != undefined){
        // this.regForm.value.AdminEmail=this.params;
        this.registerForm.controls['AdminEmail'].setValue(this.params);
        this.disableemail=true;
        
            }
      
    }
    catch(error)
  {
    this.openAlertModal('error message',error['message']);    
  }
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }


onChangeCountry(){
    var e = (document.getElementById("ide")) as HTMLSelectElement;
              var sel = e.selectedIndex - 1;
              this.countrycode = this.countries[sel]['countryCode'];
              this.countrycode = this.countrycode ? this.countrycode: "undefined";
              this.countryid = this.countries[sel]['countryID'];          
              console.log(this.countryid)
  }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        alert('SUCCESS!! :-)')
    }

    async registerUser() {  
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
          
            return;
        }

      
        const regResponse = await this.checkSignup(); 
      //  console.log(regResponse)   
        if(regResponse['isSuccess'])
        {    
          this.openAlertModal("Success!","registration successful! check verification mail sent your email.");
    
        }
        else{    
          this.openErrorModal(regResponse['message'],"Error!");
          // this.openAlertModal("Error!",regResponse['message']);
        }
      }
      
      async checkSignup() 
          {
            this.email= this.f.AdminEmail.value;      
            this.lastName=this.f.universityName.value;
            this.name= this.f.userName.value;
            this.password= this.f.password.value;      
            var regData = {
              countrycode: this.countrycode,
              countryId:String(this.countryid),
              email: this.email,
              lastName: this.lastName,
              name: this.name,
              password: this.password,
              type: "merchant"
            };
           // alert("reddata"+ regData);
          try
          {
            this.blockUserInterface();
            let promise = await this.servService.onReg(regData);
            this.unblockUserInterface();
            return promise;
          }
          catch(error)
          {
            this.unblockUserInterface();
            this.openErrorModal(error['message'],"Error Message");
            throw error;
          }
        }
          
        openErrorModal(message,heading)
        {
          this.alertHead=heading;
          this.errorMessage=message;
          this.ErrorModal=true;
        }
        //close error pop up
        closeErrorModal()
        {
          this.ErrorModal=false;
        }
      
        openAlertModal(heading:string,msg:string){
          this.AlertModal=true;
          this.heading=heading;
          this.alertmsg=msg;
        }
        
        closeAlertModal(){
          this.AlertModal=false;
          setTimeout(() => {
            // alert(this.registerForm.value.AdminEmail);
            this.router.navigate(['/login/email/' + this.registerForm.value.AdminEmail]);

            // this.router.navigateByUrl('/login/email/' + this.regForm.value.AdminEmail);
           }, 1000);
        }
          /* It blocks UI. */
          public blockUserInterface() {
            this.blockUI.start("Wait...");
          }
        
          /* It unblocks UI.*/
          public unblockUserInterface() {
            this.blockUI.stop();
          }
          handleReset(): void {
            this.captchaSuccess = false;
            this.captchaResponse = undefined;
            this.captchaIsExpired = false;
          }
        
          handleSuccess(captchaResponse: string): void {
            this.captchaSuccess = true;
            this.captchaResponse = captchaResponse;
            this.captchaIsExpired = false;
          }
        
          handleLoad(): void {
            this.captchaIsLoaded = true;
            this.captchaIsExpired = false;
          }
        
          handleExpire(): void {
            this.captchaSuccess = false;
            this.captchaIsExpired = true;
        }
}