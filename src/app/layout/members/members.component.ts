import { Component, OnInit } from '@angular/core';
import { MembersService } from 'src/app/services/members/members.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Router } from '@angular/router';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { CertificateService } from 'src/app/services/certificate/certificate.service';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  members=[];
  smembers=[];
  name:string;
  alertHead: any;
  errorMessage: any;
  ErrorModal: boolean;
  limit:number=20;
  offset:number=0;
  searchScroll: boolean=false;
  soffset: number=0;
  slimit: number=20;
  scrollDisable:boolean=false;
  selector:any;
  // slimit: number=5;
  noMembers: boolean;
  canIssue: boolean=false;
  rowheight:string="0.8:0.5";
  userregform:FormGroup;
  countrycode: any;
  countries: any;
  countryid: any;
  issuerdepartments: string[];
  university: string;
  iid: any;
  registerStudentBtn: boolean=false;
  
  constructor(private router:Router,private local:LocalstorageService, 
    private memberServ:MembersService, private certificateservice:CertificateService,
    private common:CommonService) {
   }

  async ngOnInit() 
  {
    var role=this.local.getUserRole();
    if(role=="issuer")
    {
      this.registerStudentBtn=true;
    }
    else
    {
      this.registerStudentBtn=false;
    }
    this.userregform = new FormGroup({
      countrycode: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required,Validators.email]),
      fname: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
      lname: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
      empid1: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z0-9]{1,10}$/)]),
      adhaar: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z0-9]{1,20}$/)]),
      department: new FormControl('', [Validators.required]),
    })
    this.getMembers(); 
    this.iid=this.local.getIssuerId();
    this.university=this.local.getOrganization();
    var departments=this.local.getIssuerDept();
    this.issuerdepartments=departments.split(',');    
  }

  async getMembers()
  {
    var userRole=this.local.getUserRole();
    if(userRole=="issuer")
    {
      this.rowheight="0.8:0.65";
      this.canIssue=true;
    }    
    this.searchScroll=false;
    try
    {
      this.blockUserInterface();
      const countriesRes=await this.common.getCountries();
      this.countries = countriesRes;
      console.log(countriesRes);
      const response = await this.memberServ.getAllMembers(this.limit,this.offset);
      console.log(response['employees']);      
      if(response['employees']=='')
      {
        this.noMembers=true;
      }
      else
      {
        this.noMembers=false;
        for(var i=0;i<response['employees'].length;i++)
        {
          this.members.push(response['employees'][i]);        
        }
      // this.members = response['employees'];          
  //   this.members=[
  //     {
  //         "empid": "1",
  //         "email": "cd_emp1@yopmail.com",
  //         "name": "john bonda",
  //         "department": "cse",
  //         "assetCount": 5
  //     },
  //     {
  //         "empid": "2",
  //         "email": "cd_emp3@yopmail.com",
  //         "name": "vinodh polamarasetty",
  //         "department": "cse",
  //         "assetCount": 0
  //     },
  //     {
  //         "empid": "4",
  //         "email": "cd_emp4@yopmail.com",
  //         "name": "jyotsna akkinapalli",
  //         "department": "cse",
  //         "assetCount": 0
  //     },
  //     {
  //         "empid": "5",
  //         "email": "cd_emp6@yopmail.com",
  //         "name": "chandini vysyaraju",
  //         "department": "cse",
  //         "assetCount": 0
  //     },
  //     {
  //       "empid": "6",
  //       "email": "cd_emp6@yopmail.com",
  //       "name": "chandini vysyaraju",
  //       "department": "cse",
  //       "assetCount": 0
  //   },
  //   {
  //     "empid": "7",
  //     "email": "cd_emp6@yopmail.com",
  //     "name": "chandini vysyaraju",
  //     "department": "cse",
  //     "assetCount": 0
  // }
  // ];        
      }
    }
    catch(error)    
    {
        this.openErrorModal(error['message'],"error message");
    }
    this.unblockUserInterface();
  }
 
  async searchMember(name)
  {
    if(name=='')
    {      
      this.searchScroll=false;
      this.getMembers();
      this.members=[];
    }
    else
    {
      this.searchScroll=true;
      this.blockUserInterface();
      const searchRes = await this.memberServ.searchMembers(name,'name',this.slimit,this.soffset);
      console.log(searchRes);
      this.unblockUserInterface();  
      if(searchRes['result'].length==0)
      {
        this.noMembers=true;
      }
      else
      {
        this.noMembers=false;
        for(var i=0;i<searchRes['result'].length;i++)
        {
          this.smembers.push(searchRes['result'][i]);        
        }
        this.members=this.smembers;
      }
    }
  }

  issueCertificate(member)
  {
    this.router.navigate(['/issue-certificate/email/' + member.email]);
  }

  onScrollDown(name){  
    if(this.searchScroll)
    {
      this.soffset = this.soffset + this.slimit;      
      this.searchMember(name);
    }  
    else
    {
      this.offset = this.offset + this.limit;      
      this.getMembers();
    }
    }
  
    onUp(){  
    }

    async registeruser() {
      var e = (document.getElementById("country")) as HTMLSelectElement;
      var sel = e.selectedIndex - 1;
      this.countrycode = this.countries[sel]['countryCode'];
      this.countrycode = this.countrycode ? this.countrycode : "undefined";
      this.countryid = this.countries[sel]['countryID'];
      console.log(this.countryid);
      console.log(this.iid);
  
      console.log(this.userregform.value.department);
  
  
      var dep = (document.getElementById("dep")) as HTMLSelectElement;
      var department = dep.options[dep.selectedIndex].value;
      console.log(department);
      var params = {
        countryCode: this.countrycode,
        email: this.userregform.value.email,
        empid: this.userregform.value.empid1,
        lastName: this.userregform.value.lname,
        name: this.userregform.value.fname,
        identity: {
          Aadhaar: this.userregform.value.adhaar
        },
        extra: {
        },
        groupName: "Dapps",
        iid: this.iid,
        department: department
      }
      console.log(params);
      try {
        this.blockUserInterface();
        const res = await this.certificateservice.registeremployee(params);
        this.unblockUserInterface();
        console.log(res);
        if (res['isSuccess'] === true) {
          if (res['message'] === "Awaiting wallet address")
          {
            var msg = "User Registered Successfully," + res['message'];
            this.openErrorModal(msg,"Success Message")
          }      
          else {
            this.openErrorModal("User Registered Successfully","Success Message");                    
          }
        }
        else {
          this.openErrorModal(res['message'],"Error!");        
        }
      }
      catch (error) {      
        this.openErrorModal(error['message'], "Error Message");
      }
    }
 

   //open error pop up
   openErrorModal(message,heading)
   {
     this.alertHead=heading;
     this.errorMessage=message;
     this.ErrorModal=true;
   }
   //close error pop up
   closeErrorModal()
   {
     this.ErrorModal=false;
   }
   
  /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }

}
