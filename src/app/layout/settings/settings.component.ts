import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { SettingsService } from '../../services/settings/settings.service';
import { SuperadminService } from 'src/app/services/superadmin/superadmin.service';
import { ValidationService } from '../../services/Validations/validation.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
// import { Pipe, PipeTransform } from '@angular/core';

// @Pipe({
//   name: 'lengthOfArray',
//   pure:true
// })

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;

  searchForm: FormGroup;
  departmentname:string;
  levelscount:any;
show:boolean=true;
depname:string;
showModal:boolean=false;
b:string;
depForm:FormGroup;
fieldForm:FormGroup;
branch:string;
// levels:string;
auth1:string;
auth2:string;
auth3:string;
studname:string;
enrollment:string;
degree:string;
topic:string;
year:string;
seal:string;
sign:string;
name:string;
addr:string;
course:string;
marks:string;
adhar:string;
dept:string;
authlevels:string[];
authorizers:[];
level:string[];
departmentcount:[];
authdepartments:[];
tableshow:boolean=false;
value1:number;
arrobj2:Array<number>;
AlertModal:boolean=false;
heading:string;
alertmsg:string;
levelArr:string[] = new Array();

storelevel:string[] = new Array();
  passphrase: any;
  servicefee: any;
  serviceinput: boolean=true;
  constructor(private settingsService:SettingsService,private superadminService:SuperadminService,private fb:FormBuilder,private local:LocalstorageService) { }
 async ngOnInit() {
   this.depForm=new FormGroup({
     branch:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
    levels:new FormControl('',[Validators.required])
   })
   this.fieldForm=new FormGroup({
    studname:new FormControl('',[Validators.required]),
    enrollment:new FormControl('',[Validators.required]),
    degree:new FormControl('',[Validators.required]),
    topic:new FormControl('',[Validators.required]),
    year:new FormControl('',[Validators.required]),
    seal:new FormControl('',[Validators.required]),
    sign:new FormControl('',[Validators.required]),
   })

   try{
     this.blockUserInterface();
    var res=await this.superadminService.getAuthorizers(5,0,'');
    console.log(res);
    this.authorizers=res['authorizers'];
    console.log(this.authorizers);
    this.searchForm = this.fb.group({
    properties: this.fb.array([])
  });  
  await this.getdepartments();
  await this.getServiceFee();
  this.depForm.value.levels='1';
  this. getlevelscount();
  this.unblockUserInterface();
  }
   catch(error)
   {
      this.openAlertModal("error message",error['message']);
      this.unblockUserInterface()
   }
  }

  showpage(b) {
    if (b == "initial") {
      this.show = true;
    }
    else {
      this.show = false;
    }
  }

  openModal() {
    console.log("clicked");
  }

  setfields() {
    console.log("fieldsdefined");
  }

  async assignAuthorizers() {    
    try{
    const response = await this.assignDepartments();
    console.log(JSON.stringify(response))
    if (response["isSuccess"] === true) {
      console.log(response);
      this.openAlertModal("Success!", response['message']);
      this.getdepartments();
    }
    else {
      this.openAlertModal("Error!", response['message']);
    }
  }
  catch(error)
  {
    this.openAlertModal('error message',error['message']);
  }
  }

  async assignDepartments() {

    var d = this.depForm.value.branch;
    var lcount = this.depForm.value.levels;
    console.log(lcount);
    var arr = [];
    var j=0;
    for (var i = 1; i <= lcount; i++) {
      var p = "level" + i;
      console.log(p);
      var val = document.getElementById(p) as HTMLSelectElement;
      var a = val.options[val.selectedIndex].value;
      console.log(val);
      if (a != "none"){
        arr[j] = this.getauthid(a);
        j++;
      } 
      else {
        // arr[i - 1] = "null";
      }
    }
    console.log(arr);
    var params: any = {
      department: d,
      levels: arr
    }
    this.blockUserInterface();
    let promise = await this.settingsService.assigndepartments(params);
    this.unblockUserInterface();
    return promise;
  }


  getauthid(email) {
    for (var i = 0; i < this.authorizers.length; i++) {
      while (email === this.authorizers[i]['email']) {
        console.log(this.authorizers[i]['aid']);
        return this.authorizers[i]['aid']
      }
    }

  }


  trimauth(e, i) {
    var a = i;
    // console.log(e);
    // console.log(e.id);
    // console.log(e.name);
    // console.log(e.value);
    var strauth = e.value;
    this.levelArr[i] = e.value;
    this.storelevel[e.name] = strauth;
    // console.log(this.storelevel);

    // this.storelevel[i]=strauth;
    // console.log(this.depForm.value.levels);
    var seld: number[] = new Array();
    var j = 1;
    for (i = 0; i < this.depForm.value.levels; i++) {
      var p = "level" + j;
      var x = document.getElementById(p) as HTMLSelectElement;
      // console.log(x);
      // console.log(x.selectedIndex);
      seld.push(x.selectedIndex);
      j++;
    }
    console.log(seld);
    var k = 1;
    for (i = 0; i < this.depForm.value.levels; i++) {
      var p = "level" + k;
      var x = document.getElementById(p) as HTMLSelectElement;
      //  console.log(x);
      for (var opt = 1; opt < x.childElementCount; opt++) {
        console.log(opt + "option");
        console.log(x.selectedIndex + "selind");
        if (opt != x.selectedIndex) {
          if (seld.includes(opt)) {
            console.log(x.children[opt]);
            x.children[opt].setAttribute("style", "display:none;");
            //  x.children[opt].setAttribute('display',none);
          }
          else {
            x.children[opt].setAttribute("style", "display:block;");

            console.log(x.children[opt]);

          }
        }
      }
      k++;
    }

  }

  async getdepartments() {
    try{
      let depresponse = await this.settingsService.getdepwithcount();
    console.log(depresponse);
    this.departmentcount = depresponse['departments'];
    }    
    catch(error)
    {
      this.openAlertModal('error message',error['message']);
      throw error;
    }
  }

  async view(name) {        
    console.log(name);
    this.depname = name;

    this.departmentname = name;
    console.log(this.departmentname);
    var data = {
      department: name
    }
    this.blockUserInterface();
    let authdepresponse = await this.settingsService.authdep(data);
    this.unblockUserInterface();
    console.log(authdepresponse);
    if (authdepresponse['isSuccess'] === true) {
      this.authdepartments = authdepresponse['levels'];
    }
    // this.showModal = true;
    this.viewdata(this.authdepartments);
  }

  async edit(name){
    // console.log(flag);
    console.log(name);
    this.depname = name;

    this.departmentname = name;
    console.log(this.departmentname);
    var data = {
      department: name
    }
    this.blockUserInterface();
    let authdepresponse = await this.settingsService.authdep(data);
    this.unblockUserInterface();
    console.log(authdepresponse);
    if (authdepresponse['isSuccess'] === true) {
      this.authdepartments = authdepresponse['levels'];
    }
    // this.showModal = true;
    this.editdata(this.authdepartments);

  }


  hide() {
    console.log(this.showModal)
    this.showModal = false;

  }
  editdata(authdep) {
    var authlength = this.levelArr.length;
    for (let k = 0; k < authlength; k++) {
      this.levelArr.pop();
    }

    var len = (<FormArray>this.searchForm.get('properties')).length;
    if (len >= 1) {
      for (i = 0; i < len; i++) {
        (<FormArray>this.searchForm.get('properties')).removeAt(0);
      }
    }
    this.tableshow = true;
    console.log(authdep.length);
    for (var i = 0; i < authdep.length; i++) {
      console.log(i);
      (<FormArray>this.searchForm.get('properties')).push(new FormControl());
      this.levelArr.push(authdep[i]['email']);
    }
    console.log(authdep);
    setTimeout(function () {
      this.depname = this.departmentname;
      var b = document.getElementById('levelval') as HTMLSelectElement;
      b.value = authdep.length;
      b.disabled=false;
      for (var i = 0; i < authdep.length; i++) {
        var j = i + 1
        var p = "level" + j;
        console.log(p);
        var a = document.getElementById(p) as HTMLSelectElement;
        console.log(a);
        a.value = "helloo";
      
        if (authdep[i]['email'] != "null") {
          a.value = authdep[i]['email'];
          console.log(a.value);
        }
      
      }
    }, 1000);
  }  

  viewdata(authdep){
    var authlength = this.levelArr.length;
    for (let k = 0; k < authlength; k++) {
      this.levelArr.pop();
    }

    var len = (<FormArray>this.searchForm.get('properties')).length;
    if (len >= 1) {
      for (i = 0; i < len; i++) {
        (<FormArray>this.searchForm.get('properties')).removeAt(0);

      }

    }
    this.tableshow = true;
    console.log(authdep.length);
    for (var i = 0; i < authdep.length; i++) {
      console.log(i);
      (<FormArray>this.searchForm.get('properties')).push(new FormControl());
      this.levelArr.push(authdep[i]['email']);
    }
    console.log(authdep);
    setTimeout(function () {
      this.depname = this.departmentname;
      var b = document.getElementById('levelval') as HTMLSelectElement;
      b.value = authdep.length;
      b.disabled=true;
      for (var i = 0; i < authdep.length; i++) {
        var j = i + 1
        var p = "level" + j;
        console.log(p);
        var a = document.getElementById(p) as HTMLSelectElement;
        console.log(a);
        a.disabled=true;
        if (authdep[i]['email'] != "null") {
          a.value = authdep[i]['email'];
        
          console.log(a.value);
        }
      
      }
    }, 1000);
  }

  getlevelscount() {
    this.arrobj2 = new Array(this.value1);
    console.log(this.arrobj2.length);
    this.tableshow = true;
    var len = (<FormArray>this.searchForm.get('properties')).length;
    console.log(len);
    if (len >= 1) {
      for (i = 0; i < len; i++) {
        (<FormArray>this.searchForm.get('properties')).removeAt(0);
        this.levelArr.pop();
      }
    }
    console.log(this.depForm.value.levels);
    for (var i = 1; i <= this.depForm.value.levels; i++) {
      this.levelArr.push('');
      console.log(i);
      (<FormArray>this.searchForm.get('properties')).push(new FormControl());
    }
  }

  openAlertModal(heading, msg) {
    this.AlertModal = true;
    this.heading = heading;
    this.alertmsg = msg;
  }

  closeAlertModal() {
    this.AlertModal = false;

  }
  /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }

  async setServiceFee(){
    var servicefee=this.servicefee;
    var dappid=this.local.getDappId();
    var secret=this.passphrase
    var params={
      fee:servicefee,
      dappid:dappid,
      secret:secret
  }
  try{

   const res= await this.settingsService.setServiceFee(params);
   if(res['isSuccess']){
this.openAlertModal('Success!!',"Service Fee set successfully");
   }
   else{
     this.openAlertModal('Error!!',res['message'])
   }
  }
  catch(error)
  {
    this.openAlertModal('error!!',error['message']);
    throw error;
  }
}

async getServiceFee(){

  var dappid=this.local.getDappId();
  var params={
    dappid:dappid
}
try{

 const res= await this.settingsService.getServiceFee(params);
 console.log(res)
 if(res['isSuccess']){
   if(res['verificationFee']!=0){
    this.servicefee=res['verificationFee'];
    this.serviceinput=true;
   }
  
 }
 else{
   this.openAlertModal('Error!!',res['message'])
 }
}
catch(error)
{
  this.openAlertModal('error!!',error['message']);
  throw error;
}
}


enableservicefee(){
  this.serviceinput=false;
}
}





