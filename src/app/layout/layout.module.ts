import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RequestOptionsService } from '../services/requestoptions/request-options.service';
import { ShareModule } from '../share.module';


@NgModule({
    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        LayoutRoutingModule,
        ShareModule      
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent, FooterComponent],
    providers: [RequestOptionsService]
})
export class LayoutModule { }
