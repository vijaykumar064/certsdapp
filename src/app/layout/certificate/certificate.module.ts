import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CertificateComponent} from './certificate.component'
import { CertificateRoutingModule } from './certificate-routing.module';
import {FormsModule } from '@angular/forms'

import {PlatformModule} from '@angular/cdk/platform';
import {MenuListItemComponent} from '../../menu-list-item/menu-list-item.component';
import { MatCardModule,MatListModule, MatIconModule, MatInputModule, MatGridListModule} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BlockUIModule } from 'ng-block-ui';
import { ShareModule } from '../../share.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [CertificateComponent, MenuListItemComponent],
  imports: [
    CommonModule,
    CertificateRoutingModule,
    FormsModule,   
    PlatformModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatGridListModule,
    FlexLayoutModule,
    BlockUIModule,
    NgbModule.forRoot(),
    ShareModule,
    InfiniteScrollModule
  ]
  
})
export class CertificateModule { }
