import { Injectable } from "@angular/core";  
import { HttpClient } from "@angular/common/http";  

import { Observable, BehaviorSubject } from "rxjs";  
import { Constants } from 'src/app/constants';
  
  
@Injectable()  
export class ShareDataService {  
  
    
    private selDepartmentSubject = new BehaviorSubject(null);  
    changeEmitted$ = this.selDepartmentSubject.asObservable();
    dappid: string;
    
    constructor(private http: HttpClient) {  
      
    }  
    async getassetlist():Promise<Object>    
  {
  
    console.log("getdappId for services",localStorage.getItem('dappid'))
    this.dappid =localStorage.getItem('dappid');
    const res = await this.http.get(Constants.HOST_URL + this.dappid + '/requester/list/assets').toPromise();
    return res;
  }  
  async getAssetByAssetId(assetId,address):Promise<Object>    
  {
    this.dappid ="ada9bf509a8f3d2e36219bf113c0e77ed3736e1911c2e1c1a6c81d5fd2d2261a"
    var body =
      {
        "assetId":assetId,
        "address": address
      }
    const res = await this.http.post(Constants.HOST_URL + this.dappid + '/requester/asset/get',body).toPromise();
   
    return res;
  }

  async makeViewRequest(assetId,sceret):Promise<Object>
  {
    var data={ 
      secret:sceret,
      assetId:assetId
    }
    try
    {
      const res=await this.http.post(Constants.HOST_URL  + this.dappid + '/requester/viewRequest',data).toPromise();
      return res;
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      throw err;
    }
  }

  async getrequestbyowner(secret,assetId,requesterWalletAddress):Promise<Object>    
  {
    this.dappid ="ada9bf509a8f3d2e36219bf113c0e77ed3736e1911c2e1c1a6c81d5fd2d2261a"
     var body=
      {
        "secret": secret,
        "assetId": assetId,
        "requesterWalletAddress": requesterWalletAddress,
      }
    const res = await this.http.post(Constants.HOST_URL + this.dappid + '/owner/verifyViewRequest',body).toPromise();
    return res;
  }  

  async getrequestByIssuer(assetId,requesterWalletAddress):Promise<Object>    
  {
    this.dappid ="ada9bf509a8f3d2e36219bf113c0e77ed3736e1911c2e1c1a6c81d5fd2d2261a"
     var body=
      {
        "secret": "clinic umbrella badge future creek plug order unaware awesome mistake recipe vibrant",
        "assetId": "assetId",
        "requesterWalletAddress": "requesterWalletAddress",
        "email": "akshay@yopmail.com"
      }
    const res = await this.http.post(Constants.HOST_URL + this.dappid + '/issuer/verifyViewRequest',body).toPromise();
    console.log("line no 49",res)
    return res;
  }

    sendDepartment(department) {  
        // let data = this.http.get<Employee>(this.baseURL + 'api/GetEmployeeDetails/'+id);  
        this.selDepartmentSubject.next(department);  
    }  
  
    getDepartment(){  
        return this.selDepartmentSubject.asObservable();  
    }  
}  