import { Component, OnInit, ChangeDetectorRef,ViewEncapsulation  } from '@angular/core';
import {NavItem} from '../../../nav-item';
import {NavService} from '../../../nav.service';
import { ShareDataService } from './shareData.service';
import { CertificateService } from '../../services/certificate/certificate.service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridList } from '@angular/material';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import * as moment from 'moment';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
 
@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CertificateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  ErrorModal: boolean=false;
  errorMessage: string;
  alertHead: string;  
  depSearch: string;
  certCountInit: number;
  certCountLength: number ;
  display_zercerts:boolean =false;
  nodepart_selection:boolean=true;
  depList;
  offset:number=0;
  limit: number=7;
  departmentName:string;
  SelectedAssetYear: string="";
  selectedIssuer: string="";
  selectedAuthorizer:string="";
  SelectedAssetMonth:string="";
  department:string;
  private certificationSearchCriteria={};
  _timeout: any = null;
  breakpoint: number;
  status=["Initiated","Authorized","Issued", "Rejected"];
  years=["2018","2019"];
  months=[];
  issuerList=[];
  authorizerList=[];
  statusSel="";
  viewdetailsStatus:string="";
  private assetData={};
  private arrayOfdataKeys=[];
  private totslcertKeys=[];
  allcat:any[] =[];
  certsResponse=[];
  certDetails={};
  certs=[]; arraysharedcertificate=[];arrayrequestsendcertificate=[];totalsharedkeys=[];totalrequestedkeys=[];
  certSearchFlag: boolean=false;
  navItemsShow: NavItem[] ;
  navItems: NavItem[]=[];
  tabs:boolean=false;
  showmessage:boolean=false;box:boolean=true;certbutton:boolean=true;
  Response: any;tables:boolean=false;sharedcertificatevalues=[];SharedCertificatespage:boolean=false;SharedCertificatedata:boolean=false;  
  certDetail={};showpopup:boolean=false;showMsg:any;requestsharingPopup:boolean=false;userpassphraseId:any;userAssetId:any;
  requestReceivePage:boolean=false;arrayreceivedcertificate=[];totalreceivedKeys=[];walletPopUp:boolean=false;userWallet:any;assetarray:any;
  assetId:any;requestButton:boolean=false;

  constructor(private certService:CertificateService,private navService: NavService, private cd : ChangeDetectorRef, private shareDataService: ShareDataService) { }
  
  sharedcertificateKeys = ["assetId","message"];
  // To get list of Issuers to show in Issuer Search dropdown
  async getIssuers(){
    try{
    let response=await this.certService.getIssuers(5,0,'');
    // console.log("Issuerresponse" + JSON.stringify(response));
    this.issuerList = response["issuers"]; 
    }
    catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      } 

   }

   // To get list of Authorizers to show in Authorizer Search dropdown
   async getAuthorizers(){
    try{
     let response=await this.certService.getAuthorizers(5,0,'');
     // console.log("Autheriozerresponse" + JSON.stringify(response));
      this.authorizerList = response["authorizers"];  
    }
    catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }
    
    }
    buttonclick(){
      this.tabs=true;
      this.showmessage=true;
      console.log("if condition *****",localStorage.getItem('role'));
      this.box=false;
      this.certbutton=false;
    }
  async getDepartment(){
    try{
      let response=await this.certService.Department();
      console.log("depresponse" + JSON.stringify(response));
      this.depList = response["departments"];
      console.log("depresponse1" + this.depList.length);
      for(let i=0; i< this.depList.length; i++){       
        if(this.depList[i].name.trim()!=""){
          console.log("depresponsenew" + this.depList[i].name);
        this.navItems.push({ displayName: this.depList[i].name,displayId: this.depList[i].did, iconName: 'recent_actors',
        route: 'certificate',
        children:[] }) 
        }    
      }
    }
    catch(error)
    {      
      if(Object.prototype.hasOwnProperty.call(error,'message')){
       
        let errMsg: string = error['message']
        if(errMsg.indexOf("404")){
          this.openErrorModal("Not able to connect to server, Please try again","Error Message");
        }else{
        this.openErrorModal(error['message'],"Error Message");
        }
      }
    } 
  }
  
  /* The function to search department */
  onSearch(){
    
    this._timeout  = null;
    if(this._timeout){ //if there is already a timeout in process cancel it
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
   
      var se1= '' + this.depSearch;
       this._timeout = null;
      //  this.lc.run(() => this.name1 = this.name);
      var res = this.navItems.filter(function f(o) {
        if (o.displayName.includes(se1)) return true
      
        if (o.children) {
          return (o.children = o.children.filter(f)).length
        }
      })
      this.navItemsShow = res;
      console.log(JSON.stringify(res, null, 2))
  
    },1000);
    
  }

  onResize(event) {

    if(event.target.innerWidth < 900){
      //alert("point");
      this.breakpoint=1;
    }
    else if(event.target.innerWidth >= 900 && event.target.innerWidth <=1550){
      this.breakpoint=2;
    }
    else if(event.target.innerWidth > 1550){
      this.breakpoint=3;
    }
    
  }

   // Pupulate month dropdown after year selection
   selectYearChange(selYear){
    var date = new Date();
    var currentYear = date.getFullYear();
    var currentMonth = date.getMonth();

    if(currentYear.toString() == this.SelectedAssetYear.toString()){

      let monthnames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      this.months = monthnames.slice(0,currentMonth + 1);
    }else{
      this.months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    } 
  }


  getColor(status){
   
    switch (status) {
      case 'issued':
          return "#58cb5a";
          
      case 'pending':
          return "#f16821";

      case 'rejected':
          return "#ff3131";    
      
      case 'authorized':
          return "#fffa67";
          case 'Initiated':
          return "#4879ff"    
      default:
      return "#fff";    
  }
}

  async ngOnInit() {
    this.navItemsShow = this.navItems;
    // this.breakpoint = (window.innerWidth <= 400) ? 1 : 3;
    // if(window.innerWidth <= 1200){
    //   this.breakpoint=1
    // }else if(window.innerWidth >= 1200 && window.innerWidth <=1550 ){
    //   this.breakpoint=2;
    // }
    // else if(window.innerWidth > 1550){
    //   this.breakpoint=3;
    // }

    if(window.innerWidth < 900){
      this.breakpoint=1
    }
    else if(window.innerWidth >= 900 && window.innerWidth <=1550){
      this.breakpoint=2
    }
    else if(window.innerWidth > 1550){
      this.breakpoint=3;
    }

   let dep= await this.getDepartment();
   let dep1= await this.getIssuers();
   let dep2= await this.getAuthorizers();
    this.shareDataService.changeEmitted$.subscribe(data => {
      console.log("data", data);
      this.departmentName =data;
      // ...
      })

   // this.departmentName=localStorage.getItem('selCertDepartment');
  }

  async getViewDetails(certId){
   this.certDetails=[];
   const result = this.certsResponse.filter(s => s.pid.includes(certId));
   
   this.viewdetailsStatus=result[0]["status"];
   this.certDetails["sign"] = result[0]["sign"];
     this.certDetails["publickey"] = result[0]["publickey"];
     this.certDetails["transactionId"] = result[0]["transactionId"];
     this.certDetails["hash"] = result[0]["hash"];
     this.certDetails["timestampp"] = result[0]["timestampp"];
     this.certDetails["issuedto"] = result[0]["empid"];
   let response= await this.certService.payslipstatistic(certId);
   let verifiedby="";
   
   console.log("certdetailresponse" + JSON.stringify(response));
   if (result[0]["status"] == "issued"){
    for(let i=0; i< response["signedAuthorizersCount"]; i++){
      if(i > 1){
      verifiedby = verifiedby +",";
      }
      verifiedby= response["signatures"][i]["email"];  
    } 
    this.certDetails["issuedby"]=response["issuer"]["email"];
    this.certDetails["verifiedby"]=verifiedby;
  }
    else if(result[0]["status"] == "rejected"){
    let  rejectedby = response["rejectedBy"]["email"];  
     let comment = response["reason"];
      this.certDetails["rejectedby"]=rejectedby;
    this.certDetails["comment"]=comment;
    }  
    
  }

  onScrollDown(depname){
  
    this.offset = this.offset + this.limit;
    this.reloadCertificates(depname, true, this.limit, this.offset,this.certSearchFlag);
   
  }

  onUp(){
   
  }

  async certSearch(){
    this.offset=0;
    this.certSearchFlag=true;
    let month:string;    
    if(this.SelectedAssetMonth){
      let monthnumber=this.months.indexOf(this.SelectedAssetMonth)+1
       month= String(monthnumber);
       month = month.padStart(2, "0");
     }else{
       month="";     
     }
 
    this.certificationSearchCriteria['status']=this.statusSel.toLocaleLowerCase();
    this.certificationSearchCriteria['year']=this.SelectedAssetYear;
    this.certificationSearchCriteria['month']=month;
    this.certificationSearchCriteria['iid']=this.selectedIssuer;
    this.certificationSearchCriteria['authorizer']=this.selectedAuthorizer;
    console.log("certsearch"+JSON.stringify(this.certificationSearchCriteria));
    this.reloadCertificates(this.department,false,this.limit,0, true);
  
  }

  /* function to clear search */
  clearSearchParameter(){
    this.certificationSearchCriteria['status']="";
    this.certificationSearchCriteria['year']="";
    this.certificationSearchCriteria['month']="";
    this.certificationSearchCriteria['iid']="";
    this.certificationSearchCriteria['authorizer']="";
    this.statusSel="";
    this.SelectedAssetMonth="";
    this.SelectedAssetYear="";
    this.certificationSearchCriteria['iid']=this.selectedIssuer;
    this.selectedIssuer="";
  }

  /* THe finction to load certificates */
  async reloadCertificates(department, isReload, limit, offset, searchflag){   
   
    this.blockUserInterface();
  
   this.department= department
   this.nodepart_selection= false;
    this.departmentName = department['displayName'];
    this.certificationSearchCriteria['did']=department['displayId'];
    this.certificationSearchCriteria['limit']=limit;
    this.certificationSearchCriteria['offset']=offset;
    if(!searchflag){      
    this.clearSearchParameter();
    this.certificationSearchCriteria['status']=this.statusSel.toLocaleLowerCase();
    this.certificationSearchCriteria['year']=undefined;
    this.certificationSearchCriteria['month']=undefined;
    this.certificationSearchCriteria['iid']=undefined;
    this.certificationSearchCriteria['authorizer']=undefined;
    
    }
    let response= await this.certService.getCertifcates(this.certificationSearchCriteria);
    console.log("certresponse" + JSON.stringify(response));
    this.unblockUserInterface();
    if(response["result"]){
  //   console.log("certresponse1" + JSON.stringify(response));
      this.certsResponse=response["result"];
      this.display_zercerts = false;
      
      if(!isReload){
          this.offset=0;
        this.certCountInit=0
        this.certCountLength=this.certsResponse.length;       
        this.certs=[];
      }
      else{
        alert("responselength"+ JSON.stringify(this.certsResponse.length ))
        // if(this.certsResponse.length < this.limit){
        //   this.certCountInit=(this.certCountInit + this.certsResponse.length +1);
        // }else{
        //   this.certCountInit=(this.certCountInit + this.offset);
        // }

        this.certCountInit=(this.certCountInit + this.offset);
        alert("responselength1"+ JSON.stringify(this.certCountInit))
        this.certCountLength=this.certCountLength + this.certsResponse.length
      } 
      console.log("certresponse2" + this.certsResponse.length);
     
      for (let j=0; j< this.certsResponse.length; j++)
          {
            let jsonData=JSON.parse(this.certsResponse[j]["data"]);
      
            this.assetData = this.certsResponse[j]["data"];
            this.arrayOfdataKeys = Object.keys(jsonData);

            // this.arrayOfdataKeys = Object.keys(this.assetData);
            // let certDataJson ={};
            let certDataJson ='{"id":' + this.certsResponse[j]["pid"] + '}'

            var obj=JSON.parse(certDataJson);
            let IssueDate = new Date(this.certsResponse[j]["timestampp"]);
            let certDateShow=moment(IssueDate , "YYYY-MM-DD HH:mm:ss").format("DD-MMM-YYYY");
           
            // console.log("certDatakey"+  j + ":" + this.arrayOfdataKeys);
            // console.log("certDatakeylen"+  j + ":" + this.arrayOfdataKeys.length);
            for(let i=0; i< this.arrayOfdataKeys.length; i++){
              var newField = this.arrayOfdataKeys[i];
              if(newField !="identity"){
              obj[newField] = jsonData[newField];
              }
            }           
            obj['date']= certDateShow;
            obj['status']=this.certsResponse[j]["status"];
            
            console.log("appendJson"+ JSON.stringify(obj));
            this.certs.push(obj);
            
            this.totslcertKeys[ this.certCountInit + j] = Object.keys(obj);
           // this.certs.push({id:this.certsResponse[j]["pid"], degree:jsonData["degree"], title:jsonData["department"], date:new Date(this.certsResponse[j]["timestampp"]), status:this.certsResponse[j]["status"]})
           console.log("totalkeys"+ JSON.stringify(this.totslcertKeys));
          }
          console.log("totalcerts"+ JSON.stringify( this.certs));
          
      
      // }else{
        if(response["total"]==0){
          this.display_zercerts = true;
        }
        else{
          this.display_zercerts = false;
        }
      }
    // }
  
    this.cd.detectChanges();
  }

   /* It blocks UI. */
   public blockUserInterface(){
    this.blockUI.start("Wait...");
  }
  
    /* It unblocks UI.*/
    public unblockUserInterface(){
    this.blockUI.stop();
  }

   //open error pop up
   openErrorModal(message,heading)
   {
     this.alertHead=heading;
     this.errorMessage=message;
     this.ErrorModal=true;
   }
   //close error pop up
   closeErrorModal()
   {
     this.ErrorModal=false;
   }
   //new certificate menu contet

   async getmyCertificate(){
     this.blockUserInterface();
    let res= await this.shareDataService.getassetlist();
    this.unblockUserInterface();
this.Response=res['data'];
for (let j=0; j< this.Response.length; j++)
{
console.log("line no 485",this.Response[j])
let jsonData=(this.Response[j]);
this.assetData = this.Response[j];
this.arrayOfdataKeys = Object.keys(jsonData);
var object={
  assetId:"",requesterWalletAddress:"",ownerWalletAddress:"",issuerWalletAddress:"",ownerStatus:"",issuerStatus:"",trsId:""
}
if(this.Response[j]["assetId"]!='undefined'){
  object.assetId=this.Response[j]["assetId"] 
}
if(this.Response[j]["requesterWalletAddress"]!='undefined'){
          object.requesterWalletAddress=this.Response[j]["requesterWalletAddress"] }
          if(this.Response[j]["ownerWalletAddress"]!='undefined'){
            object.ownerWalletAddress=this.Response[j]["ownerWalletAddress"]
          }
          if(this.Response[j]["issuerWalletAddress"]!='undefined'){
            object.issuerWalletAddress=this.Response[j]["issuerWalletAddress"]
          }
          if(this.Response[j]["ownerStatus"]!='undefined'){
            object.ownerStatus=this.Response[j]["ownerStatus"]
          }
          if(this.Response[j]["issuerStatus"]!='undefined'){
            object.issuerStatus=this.Response[j]["issuerStatus"]
          }if(this.Response[j]["trsId"]!='undefined'){
            object.trsId=this.Response[j]["trsId"]
          }else{
          }
this.certs.push(object);
this.totslcertKeys = [];

}
for (let j=0; j< this.Response.length; j++){
this.totslcertKeys.push(["assetId","requesterWalletAddress","ownerWalletAddress","issuerWalletAddress","ownerStatus","issuerStatus","trsId"])
}
   }
       async closeshowmsg(){
     this.showpopup=false;this.requestsharingPopup=false;
     this.walletPopUp=false;
     console.log("close message")
     if(this.userpassphraseId && this.userAssetId){
      //  this.assetarray.push({"userpassphraseId":this.userpassphraseId,"userAssetId":this.userAssetId})
      var sceret=this.userpassphraseId;
      var assetId=this.userAssetId;
      this.blockUserInterface();
     let res= await this.shareDataService.makeViewRequest(assetId,sceret);
      this.unblockUserInterface();
     if(res["message"]=="Request Pending From issuer End." || res["message"]=="Same process in a block"){
     }
     else{
console.log("anithaaaaaaaaa")
      if(res["transactionId"]){
        // this.blockUserInterface();
        var arr=[]; arr=this.certs;var arrlist=[];
        let obj={};
        arr.forEach(async e=>{
          if(e.assetId && e.requesterWalletAddress ){
            if(!e.ownerWalletAddress){
              let res= await this.shareDataService.getrequestbyowner(sceret,assetId,e.requesterWalletAddress);
            }
          }
            });        
      }
     }
     }
     if(this.userWallet){
      }
   }

   requestsharing(){
       this.requestsharingPopup=true;
      
   }
   Cancel(){
     this.requestsharingPopup=false;
   }
   
   clickApprove(assetId){
     this.assetId=assetId;
     this.walletPopUp=true;
   }
   async getsharedview(certId){
    this.certDetail=[];
    const result = this.certsResponse.filter(s => s.pid.includes(certId));
    this.viewdetailsStatus=result[0]["status"];
    this.certDetail["sign"] = result[0]["sign"];
      this.certDetail["publickey"] = result[0]["publickey"];
      this.certDetail["transactionId"] = result[0]["transactionId"];
      this.certDetail["hash"] = result[0]["hash"];
      this.certDetail["timestampp"] = result[0]["timestampp"];
      this.certDetail["issuedto"] = result[0]["empid"];
      this.certDetail["status"] = result[0]["status"];
      this.certDetail["iid"] = result[0]["iid"];
      this.certDetail["authLevel"] = result[0]["authLevel"];
   }
   async selectTab(tab){
     console.log("tab .........",tab)
    if(tab=="MyCertificate"){
      this.requestButton=false;this.SharedCertificatedata=false;this.SharedCertificatespage=false;
      this.certDetail=[];this.totslcertKeys=[];
      this.getmyCertificate();
      var elements=[];
elements.push(this.certs);
this.tables=true;
   }
     if(tab =='SharedCertificates'){ 
      this.tables=false; this.SharedCertificatespage=false;this.totalsharedkeys=[];this.arraysharedcertificate=[];this.requestButton=false;
      this.certDetail=[];this.totslcertKeys=[];
       LocalstorageService.prototype.getWalletAddress = function () {
      return localStorage.getItem('address');
   }
   console.log("Address",localStorage.getItem('address'));
   this.getmyCertificate();
   var arr=[]; arr=this.certs;var arrlist=[];
   arr.forEach(async e=>{
    let obj={};
     if(e.assetId && e.requesterWalletAddress){
      arrlist.push({'assetId':e.assetId ,'requesterWalletAddress':e.requesterWalletAddress})
      console.log("arraylist.....",arrlist)
      if(arrlist){
        for(var i=0; i<arrlist.length; i++){
          let assetId=arrlist[i].assetId; let requesterWalletAddress=arrlist[i].requesterWalletAddress;
          this.sharedcertificatevalues=[];
          this.sharedcertificatevalues["assetId"]=arrlist[i].assetId;
          this.blockUserInterface();
         let res= await this.shareDataService.getAssetByAssetId(assetId,requesterWalletAddress);   
         this.unblockUserInterface();
         if(res["message"]=="Asset details"){
          var response=[];var data=[]
          if(res["data"])var data=res["data"];
          this.certsResponse=data;
          this.display_zercerts = false;     
          this.display_zercerts = false;          
          for (let j=0; j< this.certsResponse.length; j++)
          {
            let jsonData=JSON.parse(this.certsResponse[j]["data"]);
            this.assetData = this.certsResponse[j]["data"];
            this.arrayOfdataKeys = Object.keys(jsonData);
            let certDataJson ='{"id":' + this.certsResponse[j]["pid"] + '}'
        
             obj=JSON.parse(certDataJson);
            let IssueDate = new Date(this.certsResponse[j]["timestampp"]);
            let certDateShow=moment(IssueDate , "YYYY-MM-DD HH:mm:ss").format("DD-MMM-YYYY");
        
            for(let i=0; i< this.arrayOfdataKeys.length; i++){
              var newField = this.arrayOfdataKeys[i];
              if(newField !="identity"){
              obj[newField] = jsonData[newField];
              }
            }           
            obj['date']= certDateShow;
            obj['status']=this.certsResponse[j]["status"];
            this.arraysharedcertificate.push(obj);   
         //  this.certs.push({id:this.certsResponse[j]["pid"], degree:jsonData["degree"], title:jsonData["department"], date:new Date(this.certsResponse[j]["timestampp"]), status:this.certsResponse[j]["status"]})
          }
          for (let j=0; j<=this.certsResponse.length; j++)
          {
            this.totalsharedkeys.push(["id","name","degree","department","gradyear","date","status"])
            }
          this.SharedCertificatedata=true;
         }
          else{
        this.showpopup=true; 
        this.showMsg="The Authorized List is Empty";
        this.unblockUserInterface();
        this.SharedCertificatespage=false;
     }
        }
      }
     }
   })
  }
        if(tab=="RequestSent"){
          this.tables=false; this.SharedCertificatedata=false;this.arrayrequestsendcertificate=[];this.totalrequestedkeys=[];
          this.requestReceivePage=false;this.certDetail=[];this.totslcertKeys=[];
          this.getmyCertificate();
      LocalstorageService.prototype.getWalletAddress = function () {
        return localStorage.getItem('address');
      }
      var arr=[]; arr=this.certs;var arrlist=[];
      let obj={};
      this.blockUserInterface();
      arr.forEach(async e=>{
        if(e.assetId && e.requesterWalletAddress){
        arrlist.push({'assetId':e.assetId ,'requesterWalletAddress':e.requesterWalletAddress})}
          });
        if(arrlist){
          for(var i=0; i<arrlist.length; i++){
            let assetId=arrlist[i].assetId; let requesterWalletAddress=arrlist[i].requesterWalletAddress;
            this.sharedcertificatevalues=[];
            this.sharedcertificatevalues["assetId"]=arrlist[i].assetId;
            this.blockUserInterface();
            let res= await this.shareDataService.getAssetByAssetId(assetId,requesterWalletAddress);
            this.unblockUserInterface();
            if(res["message"]!="Asset details"){
              obj={"assetId":assetId,"Message":res["message"]}
              this.totalrequestedkeys.push(["assetId" ,"Message"]);
              this.arrayrequestsendcertificate.push(obj);  
            }
          };      this.unblockUserInterface();

              this.requestButton=true; this.SharedCertificatespage=true;
        }

      }if(tab=="RequestReceive"){
        this.SharedCertificatespage=false;this.tables=false; this.SharedCertificatedata=false;this.requestButton=false;
this.certDetail=[];this.totslcertKeys=[];
        LocalstorageService.prototype.getWalletAddress = function () {
          return localStorage.getItem('address');
      }
      this.getmyCertificate();
      console.log("Address",localStorage.getItem('address'))
      var arr=[]; arr=this.certs;var arrlist=[];
      arr.forEach(async e=>{
        let obj={};
        if(e.assetId && e.requesterWalletAddress){
          arrlist.push({'assetId':e.assetId ,'requesterWalletAddress':e.requesterWalletAddress,'ownerWalletAddress':e.ownerWalletAddress,'issuerWalletAddress':e.issuerWalletAddress
        ,'issuerStatus':e.issuerStatus,'ownerStatus':e.ownerStatus})
                  obj={'assetId':e.assetId ,'requesterWalletAddress':e.requesterWalletAddress,'ownerWalletAddress':e.ownerWalletAddress,'issuerWalletAddress':e.issuerWalletAddress
                  ,'issuerStatus':e.issuerStatus,'ownerStatus':e.ownerStatus}
              this.arrayreceivedcertificate.push(obj);
              this.totalreceivedKeys.push(["assetId" ,"requesterWalletAddress",'ownerWalletAddress',"issuerWalletAddress","issuerStatus","ownerStatus"]);
              console.log("this.totalrequestedkeys",this.totalreceivedKeys);
              console.log("this.cearrayrequestsendcertificaterts",this.arrayreceivedcertificate);
              this.requestReceivePage=true;
      }
      });
      }
   }
  }
