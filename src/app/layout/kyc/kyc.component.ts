import { Component, OnInit, ChangeDetectorRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
import { KycService } from 'src/app/services/kyc/kyc.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';


@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class KycComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild("documentForm") documentForm:any;
  @ViewChild('documentImage') documentImage:any;
 public documentImageUploaded:boolean=false;
 public documentPassphraseModal:boolean=false;
 public documentUploadModal:boolean=false;
 public bkvsdm_token:string;
 public kycDocumentTypes:string[];
 public formFields=[];
 public documentSearchCriteria={};
 public documentPaymentCriteria={};
 public documentEnableCriteria={};
 public countries:{};
 public documentImageFormData:FormData;
 public token:string;
 public passphrase:string;
 public enablekycpassphrase:string;
 public documentChannelData: any;
 public userCountryDocuments={};
 public kycIndex: any;
 public documentKYCPaymentModal: boolean;
 public documentEnableKycPassphraseModal: boolean;
  ErrorModal: boolean=false;
  errorMessage: string;
  alertHead: string;
  showEnableKycBtn=false;
  private fieldsDefaultErrors:any=[];
 private fieldMetaDataValidationFlag:boolean=false;
public fieldsInValidDataErrors:any=[];
public fieldsMinLengthErrors:any=[];
public fieldsMaxLengthErrors:any=[];
public fieldsElementDataInvalidErrors:any=[];
  
  constructor(public route: Router,public kycserv: KycService,public local: LocalstorageService, public cd : ChangeDetectorRef) { 
    this.token = this.local.getBelriumToken();
    this.bkvsdm_token = this.local.getBKVSToken();
  }

  async ngOnInit() {
    try
    {
      var data = JSON.parse(await this.getUserCountriesDocument());
      let localKYCStatus = data.data.countries[0].status;
        let blockchainKYCStatus = data.data.countries[0].blockchainStatus;        
        if (((localKYCStatus == 'ACTIVE' && blockchainKYCStatus == 'ACTIVE') || localKYCStatus == 'INACTIVE')) {
          this.showEnableKycBtn=false;
        }
        else {
          this.showEnableKycBtn=true;            
        }       
      console.log("kyc document status: "+data);
    }
    catch(error)
    {
      this.unblockUserInterface();          
      this.openErrorModal(error['message'],"Error Message");
    }
  }

  public async getDocumentByDocumentMetaIndex(documentTypeLoopIndex, documentMetaLoopIndex,type)
  {    
    this.kycDocumentTypes=this.userCountryDocuments['kycDocumentTypes'];
    if(type=='upload')
    {
      this.documentSearchCriteria['countryCode']=this.local.getCountryName();
      this.documentSearchCriteria['kycDocumentTypeId']=this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentTypeId'];
      this.documentSearchCriteria['kycDocumentMetaId']=this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentMetas'][documentMetaLoopIndex]['kycDocumentMetaId'];
      this.getSelectedDocumentSchema(this.documentSearchCriteria);
    }
    else if(type=='pay')
    {        
      this.documentPaymentCriteria['kycUserDocumentID']=this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentMetas'][documentMetaLoopIndex]['kycUserDocument']['kycUserDocumentID'];
      this.documentPaymentCriteria['CountryCode']=this.local.getCountryName();
      console.log("pay userid: "+this.documentPaymentCriteria['kycUserDocumentID']);
      this.openDocumentPassphraseModal();
    }
    else if(type=='verify')
    {   
      try
      {   
        this.blockUserInterface();
        const verifyRes = await this.kycserv.sendVerification(this.kycDocumentTypes[documentTypeLoopIndex]['kycDocumentMetas'][documentMetaLoopIndex]['kycUserDocument']['kycUserDocumentID']);
        // console.log(verifyRes);  
        this.unblockUserInterface();    
      }
      catch(error)
      {
        this.unblockUserInterface();    
        this.openErrorModal(error['message'],"Error Message");
      }
    }
  }

public openDocumentUploadModal()
{
  this.documentForm.reset();
  this.documentUploadModal=true;
}

public closeDocumentUploadModal(){
  this.documentUploadModal=false;
  this.documentImageUploaded=false;
}

public async getSelectedDocumentSchema(documentSearchCriteria)
{        
      this.documentImageUploaded=false;
      this.documentImage.nativeElement.value="";     
      try
      {  
        this.blockUserInterface(); 
        const metaFormFieldRes = await this.kycserv.getMetaDataFormFields(documentSearchCriteria);
        console.log("metaFormFieldRes: "+JSON.stringify(metaFormFieldRes));
        if(metaFormFieldRes)
        {      
          this.formFields=metaFormFieldRes['data']['formproperties'];
          console.log(this.formFields);             
          this.openDocumentUploadModal();
          this.documentImageUploaded=false;
          this.documentImage.nativeElement.value="";       
          this.unblockUserInterface();
        }
      }
      catch(error)
      {
        this.unblockUserInterface();    
        this.openErrorModal(error['message'],"Error Message");
      }
}


//    /* It gets country document.*/
   public async getUserCountriesDocument()
   {    
    try
    {
      this.blockUserInterface();
      const success = await this.kycserv.fetchKycDocument(this.token);
      this.unblockUserInterface();
      this.countries=success['data']['countries'];
      for(let countriesIndex in this.countries){
        if(this.countries[countriesIndex] && this.countries[countriesIndex]['kycDocumentTypes']){
          for(let documentTypeIndex in this.countries[countriesIndex]['kycDocumentTypes']){
            if(this.countries[countriesIndex]['kycDocumentTypes'][documentTypeIndex] && this.countries[countriesIndex]['kycDocumentTypes'][documentTypeIndex]['kycDocumentMetas']){
              for(let documentMetaIndex in this.countries[countriesIndex]['kycDocumentTypes'][documentTypeIndex]['kycDocumentMetas']){
                this.countries[countriesIndex]['kycDocumentTypes'][documentTypeIndex]['isMandatory']= this.countries[countriesIndex]['kycDocumentTypes'][documentTypeIndex]['kycDocumentMetas'][documentMetaIndex]['isMandatory'];
                if(this.countries[countriesIndex]['kycDocumentTypes'][documentTypeIndex]['isMandatory']){
                  break;
                }
              }
            }
          }
        }
      }
      console.log("My country code: "+this.countries[0]['countryCode']);
      this.local.setCountryName(this.countries[0]['countryCode']);
      for(let countryIndex in this.countries){
        if(this.countries[countryIndex] && this.countries[countryIndex]['kycDocumentTypes']){
          for(let kycDocumentTypeIndex in this.countries[countryIndex]['kycDocumentTypes']){
            if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex] && this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas']){
              for(let kycDocumentMetaIndex in this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas']){
                this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['uploadStatus']=undefined;
                this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['documentStatus']=undefined;                                
                this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['paymentStatus']=undefined;
                this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['verificationStatus']=undefined;
                if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex] && this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['channelData']){
                  this.documentChannelData=JSON.parse(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['channelData']);
                }                  
                if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex] && this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']){
                  if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['uploadedAt']){
                    let uploadedDate=this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['uploadedAt'];
                    this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['uploadedAt']=new Date(uploadedDate).toLocaleString();
                  }
                  if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['documentStatus']){
                    this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['documentStatus']=this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['documentStatus']; 
                    this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['uploadStatus']=true;                  
                  }
                  if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['paymentTransaction'] && this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['paymentTransaction']['transactionStatus']){
                    this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['paymentStatus']=this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['paymentTransaction']['transactionStatus'];
                  }
                  if(this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['verificationTransaction'] && this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['verificationTransaction']['transactionStatus']){
                    this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['verificationStatus']=this.countries[countryIndex]['kycDocumentTypes'][kycDocumentTypeIndex]['kycDocumentMetas'][kycDocumentMetaIndex]['kycUserDocument']['verificationTransaction']['transactionStatus'];
                  }
                }
              }
            }
          }
        }
      }
      this.userCountryDocuments=success['data']['countries'];
      this.userCountryDocuments=this.userCountryDocuments[0];
      console.log("KYC documents fetched: "+JSON.stringify(success));
      return JSON.stringify(success);      
    }
    catch(error)
    {
      this.unblockUserInterface();   
      throw error; 
    }
  }
  
  uploadKycDoc(typeId:number,metaId:number)
  {
    this.documentUploadModal=true;
    this.documentSearchCriteria['kycDocumentMetaId']=metaId;
    this.documentSearchCriteria['kycDocumentTypeId']=typeId;
    this.documentSearchCriteria['countryCode']=this.local.getCountryName();
    this.documentSearchCriteria['metaData']={};
  }

  public uploadDocumentImage(event){
    console.log("--event--", event);
    this.documentImageUploaded=true;
    let documentImagePath={"file":event.target.files[0]};
    let documentImageFile: File = event.target.files[0];
    this.documentImageFormData = new FormData();
    this.documentImageFormData.append('file', documentImageFile, documentImageFile.name);
    this.documentImageFormData.append("countryCode", this.documentSearchCriteria['countryCode']);
    this.documentImageFormData.append("kycDocumentMetaId", this.documentSearchCriteria['kycDocumentMetaId']);
    this.documentImageFormData.append("kycDocumentTypeId", this.documentSearchCriteria['kycDocumentTypeId']);
  }

  public async saveAndUpdateUserDocument(){
    this.readUploadedDocumentData();
    this.documentUploadModal=false;
    this.blockUserInterface();
    const response = await this.saveUserDocument(this.documentImageFormData);    
    console.log("saved "+response);        
    setTimeout(() => {      
      this.getUserCountriesDocument();
      this.route.navigated = false;
      this.route.navigate([this.route.url]);
      }, 5000);   
      this.unblockUserInterface();
  }
 
  public async saveUserDocument(userDocumentFormData){    
    try
    {
      this.blockUserInterface();
      const response = await this.kycserv.kycUplaodFile(userDocumentFormData);
      this.unblockUserInterface();
      console.log("upload response: "+response);
      return response;
    }
    catch(error)
    {     
      this.unblockUserInterface();    
      this.openErrorModal(error.message,"Error Message");
    }
  }

  public readUploadedDocumentData()
  {
    console.log(this.documentSearchCriteria);   
    let metaData={};
 
    for(let index in this.formFields){
      metaData[this.formFields[index]['name']]=this.formFields[index]['value'];
    }
    this.documentImageFormData.append("metaData", JSON.stringify(metaData));    
  }

  openDocumentPassphraseModal()
  {
    this.documentPassphraseModal=true;    
  }

  async kycPayment(passphrase)
  {
    this.documentPaymentCriteria['passphrase']=this.passphrase;
    console.log(this.passphrase);
    try
    {
      this.blockUserInterface();
      const publickeyRes= await this.kycserv.getPublicKey(this.passphrase);
      if(publickeyRes['success']==true)
      {
        this.closeDocumentPassphraseModal();
        var publicKey=publickeyRes['account']['publicKey'];
        console.log("public key: "+publicKey);
        this.documentPaymentCriteria['publickey']=publicKey;
        console.log(this.documentPaymentCriteria);
        const paymentRes = await this.kycserv.kycPayment(this.documentPaymentCriteria);
        console.log(JSON.stringify(paymentRes));
        setTimeout(() => {      
          this.getUserCountriesDocument();
          this.route.navigated = false;
          this.route.navigate([this.route.url]);
          }, 5000);   
          this.unblockUserInterface();
      }
    }
    catch(error)
    {
      this.unblockUserInterface();    
      this.openErrorModal(error['message'],"Error Message");
    }
  }

  public fieldMetaDataValidation(currentField){
    let minLength=5;
    let maxLength=10;
    this.fieldMetaDataValidationFlag=false;
    let value=currentField.value;
    if(currentField.invalid){
        this.fieldsDefaultErrors[currentField.name]=false;
        this.fieldsMinLengthErrors[currentField.name]=false;
        this.fieldsInValidDataErrors[currentField.name]=true;
        this.fieldsMaxLengthErrors[currentField.name]=false;
        this.fieldMetaDataValidationFlag=true;
    }
    if(currentField.valid){
        this.fieldsDefaultErrors[currentField.name]=false;
        this.fieldsMinLengthErrors[currentField.name]=false;
        this.fieldsInValidDataErrors[currentField.name]=false;
        this.fieldsMaxLengthErrors[currentField.name]=false;
        this.fieldMetaDataValidationFlag=true;
    }
    if(currentField.value.length==0){
        this.fieldsDefaultErrors[currentField.name]=true;
        this.fieldsMinLengthErrors[currentField.name]=false;
        this.fieldsInValidDataErrors[currentField.name]=false;
        this.fieldsMaxLengthErrors[currentField.name]=false;
        this.fieldMetaDataValidationFlag=true;
    }
    if(value.length<minLength && !this.fieldsInValidDataErrors[currentField.name] && !this.fieldsDefaultErrors[currentField.name]){
        this.fieldsDefaultErrors[currentField.name]=false;
        this.fieldsMinLengthErrors[currentField.name]=true;
        this.fieldsInValidDataErrors[currentField.name]=false;
        this.fieldsMaxLengthErrors[currentField.name]=false;
        this.fieldMetaDataValidationFlag=true;
    }
    if(value.length>maxLength && !this.fieldsInValidDataErrors[currentField.name]){
        this.fieldsDefaultErrors[currentField.name]=false;
        this.fieldsInValidDataErrors[currentField.name]=false;
        this.fieldsMinLengthErrors[currentField.name]=false;
        this.fieldsMaxLengthErrors[currentField.name]=true;
        this.fieldMetaDataValidationFlag=true;
    }
    if(!this.fieldsDefaultErrors[currentField.name] && !this.fieldsInValidDataErrors[currentField.name] && !this.fieldsMinLengthErrors[currentField.name] && !this.fieldsMaxLengthErrors[currentField.name]){
      this.fieldsElementDataInvalidErrors[currentField.name]=true;
      this.fieldMetaDataValidationFlag=false;
    }
    if(this.fieldsDefaultErrors[currentField.name] || this.fieldsInValidDataErrors[currentField.name] || this.fieldsMinLengthErrors[currentField.name] || this.fieldsMaxLengthErrors[currentField.name]){
      this.fieldMetaDataValidationFlag=true;
    }
   }

  closeDocumentPassphraseModal()
  {
    this.documentPassphraseModal=false;
  }

  openDocumentEnableKycPassphraseModal()
  {
    this.documentEnableKycPassphraseModal=true;
  }

  async enableKycDocument()
  {
    try
    {
      this.blockUserInterface();
      var publicKeyRes = await this.kycserv.getPublicKey(this.enablekycpassphrase);
      if(publicKeyRes['success']==true)
      {
        this.closeDocumentEnableKycPassphraseModal();
        var publicKey=publicKeyRes['account']['publicKey'];
        console.log("public key: "+publicKey);
        this.documentEnableCriteria['passphrase']=this.enablekycpassphrase;
        this.documentEnableCriteria['publickey']=publicKey;
        this.documentEnableCriteria['secondSecret']=" ";
        this.documentEnableCriteria['countryCode']=this.local.getCountryName();
        const enableRes = await this.kycserv.doEnableKYC(this.documentEnableCriteria);
        console.log(enableRes);
        setTimeout(() => {      
          this.getUserCountriesDocument();
          this.route.navigated = false;
          this.route.navigate([this.route.url]);
          }, 5000);   
          this.unblockUserInterface();
      }
    }
    catch(error)
    {
      this.unblockUserInterface();    
      this.openErrorModal(error['message'],"Error Message");
    }
  }

  closeDocumentEnableKycPassphraseModal()
  {
    this.documentEnableKycPassphraseModal=false;
  }

   //open error pop up
  openErrorModal(message,heading)
  {
    this.alertHead=heading;
    this.errorMessage=message;
    this.ErrorModal=true;
  }
  //close error pop up
  closeErrorModal()
  {
    this.ErrorModal=false;
  }
    /* It blocks UI. */
    public blockUserInterface() {
      this.blockUI.start("Wait...");
    }
  
    /* It unblocks UI.*/
    public unblockUserInterface() {
      this.blockUI.stop();
    }
}
