import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {KycComponent } from './kyc.component';
import { KycRoutingModule } from './kyc-routing.module';
import { FormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatGridListModule} from '@angular/material';
import { ShareModule } from '../../share.module';

@NgModule({
  declarations: [KycComponent],
  imports: [
    CommonModule,
    KycRoutingModule,
    FormsModule,
    FlexLayoutModule,
    MatGridListModule,
    ShareModule
  ]
})
export class KycModule { }
