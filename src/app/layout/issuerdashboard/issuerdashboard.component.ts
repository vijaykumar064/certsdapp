import { Component, OnInit, ElementRef, ViewChild  } from '@angular/core';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import {SuperadminService} from 'src/app/services/superadmin/superadmin.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { MultiDataSet, Label } from 'ng2-charts';


@Component({
  selector: 'app-issuerdashboard',
  templateUrl: './issuerdashboard.component.html',
  styleUrls: ['./issuerdashboard.component.scss']
})
export class IssuerdashboardComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild('chart') canvas: ElementRef;

    iid:string;
    certsissued:string;
    certsrejeted:string;
    studentscount:string;
    issuers:[];
  chartColors1;
  chartColors;
  year=new Date().getFullYear();
  month=new Date().getMonth()+1;
  filteryear=new Date().getFullYear();
    issueremail:string;
    initialissueremail:string;
  chartDataArr1:Array<any>=[];
piechartData:Array<any>=[];
  barChartLabels1: Array<any>=[];
  barChartData1:any = [
    { 
        data: [],
        label:'Month'
    }
];
public barChartType1 = 'bar';
public barChartLegend1 = true;

public barChartOptions1 = {
  scaleShowVerticalLines: true,
  responsive: true,
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      },
        barThickness : 15,
    }]
}
}

chartDataArr:Array<any>=[];

barChartLabels: Array<any>=[];
barChartData:any = [
  { 
      data: [],
      label:'Department'
  }
];
public barChartType = 'horizontalBar';
public barChartLegend = true;

public barChartOptions= {
scaleShowVerticalLines: true,
responsive: true,
scales: {
  yAxes: [{
    ticks: {
      beginAtZero: true
    },
      barThickness : 15,
  }]
}
}
  alertHead: any;
  errorMessage: any;
  ErrorModal: boolean;

   // Doughnut
 public doughnutChartLabels: Array<any>=[];
 public doughnutChartData: any = [
  { 
    data: [],
    label:'Reason'
}
  ];
 public doughnutChartType= 'doughnut';

 public pieChartOptions={
 
  plugins: {
    datalabels: {
      backgroundColor: function(context) {
        return context.dataset.backgroundColor;
      },
      borderColor: 'white',
      borderRadius: 25,
      borderWidth: 2,
      color: 'white',
      display: function(context) {
        var dataset = context.dataset;
        var count = dataset.data.length;
        var value = dataset.data[context.dataIndex];
        return value > count * 1.5;
      },
      font: {
        weight: 'bold'
      },
      formatter: Math.round
    }
  }

}

  constructor(private dashboardService:DashboardService,private local:LocalstorageService,private superadminservice:SuperadminService) { }
  async ngOnInit() {
    try{
    this.issueremail=this.local.getEmail();
this.iid=this.local.getIssuerId()
const issuerdata=await this.dashboardService.getIssuerData(this.iid);
console.log(issuerdata);
if(issuerdata['isSuccess']==true){
  this.certsissued=issuerdata['issuedIssuesCount'];
  this.certsrejeted=issuerdata['rejectedIssuesCount'];
}

const stdcount=await this.dashboardService.getSuperuserDashboardCounts();
console.log(stdcount)
if(stdcount['isSuccess']){
  this.studentscount =stdcount['recipientsCount'];
}
await this.getmonthlycount(this.issueremail);

await this.gettopdep();
const gradient = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
gradient.addColorStop(0, 'blue');
 gradient.addColorStop(1, 'white');

this.chartColors1 = [
  {
      backgroundColor: gradient
  }
];

const gradient1 = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
gradient1.addColorStop(0, 'green');
 gradient1.addColorStop(1, 'white');

this.chartColors1 = [
  {
      backgroundColor: gradient1
  }
];

await this.rejectReasonStats();
    }
    catch(error)
    {
      this.openErrorModal(error['message'],'error message');
      throw error;
    }
  }


async getmonthlycount(email){
  try
  {
  const response=await this.dashboardService.getRoleBasedStatistics(this.year,'issuer',email);
  console.log(response);
  this.chartDataArr1=[];
  this.barChartLabels1=[];
  this.barChartData1=[];
  var months=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
  for(var i=0;i<12;i++){
    this.chartDataArr1.push(Number(response['monthsCount'][i]));
    this.barChartLabels1.push(months[i])
     
      }
      console.log("deplabelarr" + JSON.stringify(this.barChartLabels1));
      console.log("depdataarr" + JSON.stringify(this.chartDataArr1));
      this.barChartData1.push({data:this.chartDataArr1, label:'Month'}); 
    }
  catch(error)    
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}


async gettopdep(){
  try{
  const response=await this.dashboardService.getIssuerDepRanks(this.iid,5,this.month,this.filteryear);
  console.log(response);

  if(response['isSuccess']==true){
    const data=response['result'];
    this.chartDataArr=[];
    this.barChartLabels=[];
    this.barChartData=[];
     for(var i=0;i<data.length;i++){    
     this.chartDataArr.push(Number(data[i]['issuedCount'])); 
     this.barChartLabels.push(data[i]['name']);
     }
     this.barChartData.push({data:this.chartDataArr, label:'Department'}); 
   }
  }
  catch(error)    
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}

async rejectReasonStats(){
  try
  {
  const res=await this.dashboardService.rejectReasonStatistics();
  
  if(res['isSuccess']){
    console.log(res['reasons']);
    const data=res['reasons']
    this.doughnutChartData=[];
    this.doughnutChartLabels=[];
    this.piechartData=[];
    for(var i=0;i<data.length;i++){
      this.piechartData.push(data[i]['count']);
      this.doughnutChartLabels.push(data[i]['reason']);

    }
    this.doughnutChartData.push({data:this.piechartData,label:'Reason'});
    console.log(this.doughnutChartData);
  }
}
catch(error)    
{
  this.openErrorModal(error['message'],'error message');
  throw error;
}
 
}


//open error pop up
openErrorModal(message,heading)
{
  this.alertHead=heading;
  this.errorMessage=message;
  this.ErrorModal=true;
}
//close error pop up
closeErrorModal()
{
  this.ErrorModal=false;
}

/* It blocks UI. */
public blockUserInterface() {
  this.blockUI.start("Wait...");
}

/* It unblocks UI.*/
public unblockUserInterface() {
  this.blockUI.stop();
}
}
