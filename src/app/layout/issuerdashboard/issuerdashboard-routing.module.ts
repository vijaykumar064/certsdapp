import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssuerdashboardComponent } from './issuerdashboard.component';

const routes: Routes = [{
  path:'',component:IssuerdashboardComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IssuerdashboardRoutingModule { }
