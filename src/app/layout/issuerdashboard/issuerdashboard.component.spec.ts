import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssuerdashboardComponent } from './issuerdashboard.component';

describe('IssuerdashboardComponent', () => {
  let component: IssuerdashboardComponent;
  let fixture: ComponentFixture<IssuerdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssuerdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssuerdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
