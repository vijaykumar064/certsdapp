import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IssuerdashboardComponent} from './issuerdashboard.component';
import { IssuerdashboardRoutingModule } from './issuerdashboard-routing.module';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../share.module'; 

// import { GoogleComboChartService } from '../Services/google-combo-chart.service';
// import { GooglePieChartService } from '../Services/google-pie-chart.service';
// import { ComboChartComponent } from '../Charts/combochart.component'
// import { PieChartComponent } from '../Charts/piechart.component'
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [IssuerdashboardComponent],
  imports: [
    CommonModule,
    IssuerdashboardRoutingModule,
    Ng2GoogleChartsModule,
    ChartsModule,
    FormsModule,
    ShareModule
  ]
  // providers: [GoogleComboChartService,GooglePieChartService],
})
export class IssuerdashboardModule { }
