import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssuerComponent } from './issuer.component';

const routes: Routes = [{
  path :'', component: IssuerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IssuerRoutingModule { }
