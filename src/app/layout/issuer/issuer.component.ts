import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard/dashboard.service'
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { IssuerService } from 'src/app/services/issuer/issuer.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';


@Component({
  selector: 'app-issuer',
  templateUrl: './issuer.component.html',
  styleUrls: ['./issuer.component.scss']
})
export class IssuerComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  display_issued:boolean=false;
  display_pending_issues:boolean=false;
  display_rejected:boolean=false;
  iid:string;
  activeFlag:number=0;
  pendingCount:number;
  issuedCount:number;
  rejectedCount:number;
  totalissues:[];
  totalpending:{};
  totalRejected:{};
  pendinglimit:number=6;
  pendingoffset:number=0;
  // pendingdiff:number=this.pendinglimit-this.pendingoffset;
  rejectionlimit:number=6;
  rejectionoffset:number=0;
  // rejectiondiff:number=this.rejectionlimit-this.rejectionoffset;
  issuedlimit:number=6;
  issuedoffset:number=0;
  // issueddiff:number=this.issuedlimit-this.issuedoffset;
  issuepid:string;
  passphrase:string;
  message1:string;
  message2:string;
  message3:string;
  showimage:boolean=false;
  ErrorModal: boolean=false;
  errorMessage: string;
  alertHead: string;
  AlertModal:boolean=false;
  heading:string;
  alertmsg:string;
  issuerdepartments:{};
  selecteddep:string='';
  pendingissuesfilter:string='';
  certsrejectedfilter:string='';
  certsissuedfilter:string='';
  selectedAll:any;
  selectedRejectedAll:any;
  selectedIssuedAll:any;
  selected:boolean=false;
  dappid: string;
  authStatus=[];
  authRejection=[];
  pendingList=[];
  AuthIssued=[];
  imgMessage: string;
  bulkpid=[];
  finalpassphrase:string;
  BulkModal:boolean=false;
  successmsg:string='';
  failuremsg:string='';
  displaysuccess:boolean=false;
  displayfailure:boolean=false;
  constructor(private dashboardService:DashboardService,private local:LocalstorageService,private issuerService:IssuerService,private route:Router ) 
  { 
    this.dappid=this.local.getDappId();
    this.iid=this.local.getIssuerId();
  }
  async ngOnInit() { 
    try{
      this.blockUserInterface();
      await this.getissuerdata(); 
      await this.getPendingCerts();  
      this.unblockUserInterface();
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"Error Message");
    }
    this.issuerdepartments=this.local.getIssuerDept().split(',');    
  }
  
  async getissuerdata(){    
    try
    {
      this.blockUserInterface();
      let promise= await this.dashboardService.getIssuerData(this.iid);
      this.pendingCount=promise['pendingIssuesCount'];    
      if(this.pendingCount > 0){
        this.activeFlag=1;
      }     
      this.issuedCount=promise['issuedIssuesCount'];
      this.rejectedCount=promise['rejectedIssuesCount'];
      this.unblockUserInterface();
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"Error Message");
      throw error;
    }
  }


  
  
  async applyPendingfilter(){
    if((this.pendingissuesfilter=='none')||(this.pendingissuesfilter=='Departments')){
      this.pendingissuesfilter='';
    }
    this.pendingList=[];
      this.pendinglimit=6;
      this.pendingoffset=0;
      await this.getPendingCerts();
  }
 async onPendingIssuesScrollDown(){
// this.pendinglimit=this.pendinglimit+this.pendingdiff;
this.pendingoffset=this.pendingoffset+this.pendinglimit;
await this.getPendingCerts();
  }



  async getPendingCerts()
  {
    this.activeFlag=1;
    this.showimage=false;
    this.display_issued=false;
    this.display_rejected=false;
    this.display_pending_issues=false;
    if(this.pendingCount===0){
      this.showimage=true;
      this.imgMessage="No certificates issued yet! Ready to start?";
    }
    else
    {
      this.display_pending_issues=true;
    try{
      if(this.pendingList.length<=this.pendingoffset){
      this.blockUserInterface();
    let response=await this.issuerService.pendingissues(this.iid,this.pendinglimit,this.pendingoffset,this.pendingissuesfilter);
  this.totalpending=response['result'];
 
  for(var key in this.totalpending)
  {
    console.log(this.totalpending[key]['totalLevels']); 
    this.authStatus=[];   
    for(var j=0;j<this.totalpending[key]['totalLevels'];j++)
    {
      if(this.totalpending[key]['authLevel']>=j+1)
      {
        this.authStatus.push({level:'Auth '+j,status: 'signed'});
      }
      else
      {
        this.authStatus.push({level:'Auth '+j,status: 'unsigned'});
      }
    }
    this.pendingList.push({departmentName:this.totalpending[key]['departmentName'],pid:this.totalpending[key]['pid'], iid:this.totalpending[key]['iid'], receipientName:this.totalpending[key]['receipientName'],selected:false })
  }
  this.unblockUserInterface();
}
}
    catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
        throw error;
      }
    }
}

async applyRejectedfilter(){
  if((this.certsrejectedfilter=='none')||(this.certsrejectedfilter=='Departments')){
    this.certsrejectedfilter='';
  }
  this.authRejection=[];
    this.rejectionlimit=6;
    this.rejectionoffset=0;
    await this.getRejectedCerts();
}
async onRejectedIssuesScrollDown(){
// this.rejectionlimit=this.rejectionlimit+this.rejectiondiff;
this.rejectionoffset=this.rejectionoffset+this.rejectionlimit;
await this.getRejectedCerts();
}
  async getRejectedCerts()
  {
    this.activeFlag=2;
    this.showimage=false;
    this.display_issued=false;    
    this.display_pending_issues=false;
    if(this.rejectedCount===0){
      this.showimage=true;
      this.imgMessage="You haven't rejected any certificates yet!";
    }
    else
    {
      this.display_rejected=true;
 
    try{
      if(this.authRejection.length<=this.rejectionoffset){
      this.blockUserInterface();
    const response = await this.issuerService.rejectedCerts(this.iid,this.rejectionlimit,this.rejectionoffset,this.certsrejectedfilter);
  this.totalRejected=response['result'];
  for(var key in this.totalRejected)
  {   
    this.authStatus=[];   
    for(var j=0;j<this.totalRejected[key]['totalLevels'];j++)
    {      
      if(j+1<this.totalRejected[key]['authLevel'])
      {
        this.authStatus.push({level:'Auth '+j,status: 'signed'});
      }
      else if(this.totalRejected[key]['authLevel']==j+1)
      {
        this.authStatus.push({level:'Auth '+j,status: 'rejected'});
      }
      else
      {
        this.authStatus.push({level:'Auth '+j,status: 'unsigned'});
      }
    }
    this.authRejection.push({departmentName: this.totalRejected[key]['departmentName'],pid: this.totalRejected[key]['pid'],iid: this.totalRejected[key]['iid'],receipientName:this.totalRejected[key]['receipientName'],totalLevels: this.totalRejected[key]['totalLevels'],authStatus:this.authStatus, selected:false});
  }
  this.unblockUserInterface();
}
    }
    catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }      
    }
  }
  onUp(){

  }

  async applyIssuedfilter(){
    if((this.certsissuedfilter=='none')||(this.certsissuedfilter=='Departments')){
      this.certsissuedfilter='';
    }
    this.AuthIssued=[];
      this.issuedlimit=6;
      this.issuedoffset=0;
      await this.getIssuedCerts();
  }
  async onIssuedScrollDown(){
  // this.issuedlimit=this.issuedlimit+this.issueddiff;
  this.issuedoffset=this.issuedoffset+this.issuedlimit;
  await this.getIssuedCerts();
  }

  async getIssuedCerts()
  {
    this.activeFlag=3;
    this.showimage=false;
    this.display_pending_issues=false;
    this.display_rejected=false;
    if(this.issuedCount===0){
      this.showimage=true;
      this.imgMessage="You haven't issued any certificates yet!";
    }
    else
    {
      this.display_issued=true;   
    try{
      if(this.AuthIssued.length<=this.issuedoffset){
      this.blockUserInterface();
    const response=await this.issuerService.totalIssuerpayslips(this.iid,this.issuedlimit,this.issuedoffset,this.certsissuedfilter);
  this.totalissues=response['result'];  
  for(var key in this.totalissues)
  {    
    this.authStatus=[];   
    for(var j=0;j<this.totalissues[key]['totalLevels'];j++)
    {      
      if(j+1<this.totalissues[key]['authLevel'])
      {
        this.authStatus.push({level:'Auth '+j,status: 'signed'});
      }
      else if(this.totalissues[key]['authLevel']==j+1)
      {
        this.authStatus.push({level:'Auth '+j,status: 'rejected'});
      }
      else
      {
        this.authStatus.push({level:'Auth '+j,status: 'unsigned'});
      }
    }  
    this.AuthIssued.push({departmentName: this.totalissues[key]['departmentName'],pid: this.totalissues[key]['pid'],iid: this.totalissues[key]['iid'],receipientName:this.totalissues[key]['receipientName'],totalLevels: this.totalissues[key]['totalLevels'],authStatus:this.authStatus, selected:false});

  }
  this.unblockUserInterface();

}
    }
    catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }
    }
 }


 async finalissuecert(){
    var params={
     secret:this.passphrase,
     pid: this.issuepid,
     fee: "0",
     iid: this.iid,
     senderPublicKey: " "
   }
try{
  this.blockUserInterface();
const response=await this.issuerService.finalIssue(params);
this.unblockUserInterface();
console.log(response);
if(response['isSuccess']===true){
  this.openAlertModal("Success!","certificate issued successfully");
  await this.getissuerdata();
 this.pendinglimit=6;
 this.pendingoffset=0;
 this.pendingList=[];
  await this.getPendingCerts();

}
else{
  this.openAlertModal("Error!",response['message']);
}
}
catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }
 }

 getpid(a){
   console.log(a);
   this.issuepid=a;
 }

 navigatetocert(){
   this.route.navigateByUrl('/issue-certificate');
 }

 certreissue(email){
   this.route.navigateByUrl('/issue-certificate/email/'+email);
 }


  //open error pop up
  openErrorModal(message,heading)
  {
    this.alertHead=heading;
    this.errorMessage=message;
    this.ErrorModal=true;
  }
  //close error pop up
  closeErrorModal()
  {
    this.ErrorModal=false;
  }

  openAlertModal(heading,msg){
    this.AlertModal=true;
    this.heading=heading;
    this.alertmsg=msg;
  }
  
  closeAlertModal(){
    this.AlertModal=false;
  
  }

  checkAll(o) {
    console.log(o.checked)  
    // console.log();
    var boxes = document.getElementsByClassName("c1");
    for (var x = 0; x < boxes.length; x++) {
      var obj = boxes[x];
      console.log(obj);
      obj.removeAttribute('checked');
      // if (obj.getAttribute('type') == "checkbox") {
        //  if (obj.getAttribute(name) != "check")

        if (obj.getAttribute('type') == "checkbox") {
          if (obj.getAttribute('name') != "check")

          obj.setAttribute('checked',o.checked)
        }
        //  if(o.checked===true){

        //  }
        //   else
        //   obj.setAttribute('checked','false')

          // else
          // obj.setAttribute('checked',o.checked)

      //  }
    }
  }



//   selectAll() {
//     var boxes = document.getElementsByClassName("c1");

//     for (var i = 0; i <boxes.length; i++) {
//       this.selected = this.selectedAll;
//     }
//   }
//   checkIfAllSelected() {
//     var boxes = document.getElementsByClassName("c1");
    
//      if(this.selected == true){
//       this.selectedAll=true;
//      }
//   }
// }

selectAll(tabtype) { 
 if(tabtype==="pending"){
    for (var i = 0; i < this.pendingList.length; i++) {
      this.pendingList[i].selected = this.selectedAll;
    // this.pendingList[i].selected = true;
    }
 }
 else if(tabtype==="rejected"){
  for (var i = 0; i < this.authRejection.length; i++) {
    this.authRejection[i].selected = this.selectedRejectedAll;
  // this.pendingList[i].selected = true;
  }
 }
//else if(tabtype==="issued"){
//   for (var i = 0; i < this.issuedList.length; i++) {
//     this.issuedList[i].selected = this.selectedIssuedAll;
//   // this.pendingList[i].selected = true;
//   }
//  }
}
checkIfAllSelected(tabtype) {
  if(tabtype==="pending"){
  this.selectedAll = this.pendingList.every(function(item:any) {
      return item.selected == true;
    })
  }
  else if(tabtype==="rejected"){
    this.selectedRejectedAll = this.authRejection.every(function(item:any) {
      return item.selected == true;
    })
  }
  // else if(tabtype==="issued"){
  //   this.selectedIssuedAll = this.issuedList.every(function(item:any) {
  //     return item.selected == true;
  //   })
  // } 
}






bulkissue(){
  console.log(this.pendingList);
  console.log(this.pendingList.length);
  console.log(this.pendingList[0].selected)
this.bulkpid=[];
  for(var i=0;i<this.pendingList.length;i++){
    if(this.pendingList[i].selected==true){
      this.bulkpid.push(this.pendingList[i].pid);
    }
  }
  console.log(this.bulkpid);
}


async bulkfinalissue(){
  console.log(this.bulkpid);
 console.log( this.finalpassphrase)

 var params={
  pids: this.bulkpid,
   fee: "0",
   iid:this.iid,
   secret: this.finalpassphrase,
   senderPublicKey: " "
 }
try{
  this.blockUserInterface();
const response=await this.issuerService.bulkfinalIssue(params);
this.unblockUserInterface();
console.log(response);
var successarr=[];
var failarr=[];
if(response['isSuccess']===true){
for(var i=0;i<response['results'].length;i++){
  if(response['results'][i]['result']['isSuccess']!=false){
   var successobj={
    pid:response['results'][i]['pid'],
    }
    successarr.push(response['results'][i]['pid']);
  }
  else{
      var failedobj={
        pid:response['results'][i]['pid'],
        message:response['results'][i]['result']['message']
      }
      failarr.push(failedobj);
  }
}
console.log(successarr);
console.log(failarr);
this.BulkModal=true;
if(successarr.length>0){
  this.displaysuccess=true;
  this.successmsg=successarr+"Issued Successfully."
}
if(failarr.length>0)
this.displayfailure=true;
this.failuremsg=JSON.stringify(failarr);
// this.openAlertModal("Alert!",JSON.stringify(response['results']))
await this.getissuerdata();
this.pendinglimit=6;
this.pendingoffset=0;
this.pendingList=[]; 
await this.getPendingCerts();
}
else{
this.openAlertModal("Error!",response['message'])
}
}
catch(error)
   {
     this.openErrorModal(error['message'],"Error Message");
   }

}



closeBulkModal(){
  this.BulkModal=false;
}
  /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }
}
