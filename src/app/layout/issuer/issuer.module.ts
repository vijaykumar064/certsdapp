import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IssuerComponent } from './issuer.component';
import { IssuerRoutingModule } from './issuer-routing.module';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ShareModule } from '../../share.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [IssuerComponent],
  imports: [
    CommonModule,
    IssuerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,
    InfiniteScrollModule
  ]
})
export class IssuerModule { }
