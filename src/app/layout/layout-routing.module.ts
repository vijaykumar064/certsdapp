import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'wallet' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'wallet', loadChildren: './wallet/wallet.module#WalletModule' },
            { path: 'superadmin',loadChildren:'./superadmin/superadmin.module#SuperadminModule'},
            { path: 'certificate',loadChildren:'./certificate/certificate.module#CertificateModule'},
            { path: 'kyc',loadChildren:'./kyc/kyc.module#KycModule'},
            { path: 'settings',loadChildren:'./settings/settings.module#SettingsModule'},
            { path: 'issuer', loadChildren:'./issuer/issuer.module#IssuerModule'},
            { path: 'issue-certificate', loadChildren:'./issue-certificate/issue-certificate.module#IssueCertificateModule'},
            { path: 'issue-certificate/email/:email', loadChildren:'./issue-certificate/issue-certificate.module#IssueCertificateModule'},            
            {path: 'authorizer',loadChildren:'./authorizer/authorizer.module#AuthorizerModule'},
            {path: 'issuerdashboard',loadChildren:'./issuerdashboard/issuerdashboard.module#IssuerdashboardModule'},          
            { path: 'issue-certificate/email/:email', loadChildren: './issue-certificate/issue-certificate.module#IssueCertificateModule' },
            { path: 'members', loadChildren: './members/members.module#MembersModule'},
            { path: 'authorizer-dashboard', loadChildren: './authorizer-dashboard/authorizer-dashboard.module#AuthorizerDashboardModule'},
            { path: 'packages', loadChildren: './packages/packages.module#PackagesModule'},
            { path: 'wallet/result/:result/id/:id', loadChildren: './wallet/wallet.module#WalletModule' },
            { path: 'error', loadChildren: './error/error.module#ErrorModule' },



        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
