import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperadminComponent } from './superadmin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuperadminRoutingModule } from './superadmin-routing.module';
import { MatTabsModule } from '@angular/material/tabs'; 
import { ShareModule } from 'src/app/share.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

//import { ShareModule } from 'src/app/share.module';


@NgModule({
  declarations: [SuperadminComponent],
  imports: [
    CommonModule,
    SuperadminRoutingModule,
    FormsModule,
    MatTabsModule,
    ShareModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule
  ]
})
export class SuperadminModule { }
