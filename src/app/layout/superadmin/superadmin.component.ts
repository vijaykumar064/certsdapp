import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonService } from '../../services/common/common.service';
import { SuperadminService } from 'src/app/services/superadmin/superadmin.service';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-superadmin',
  templateUrl: './superadmin.component.html',
  styleUrls: ['./superadmin.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SuperadminComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  show: boolean = true;
  showBlock: boolean =false;
  notShowBlock: boolean =false;
  authrole: string;
  tableshow: boolean = true;
  a: string;
  b: string;
  issuerresponse: [];
  authorizerresponse: [];
  deps = [];
  testemail: string;
  countrycode: string;
  countryid: string;
  show1: boolean = true;
  display = 'none';
  countries: {};
  deptnames: [];
  response: string;
  resp: string;
  selectedName: string;
  selectedName1:string;
  issuername: string;
  email: string;
  role: string;
  selectedoption: string;
  dept: string;
  ccode: string;
  name: string;
  seloption: string;
  dappid: string;
  authName: string;
  authid: string;
  count = 0;
  selecteddep:string='';
  totalpending: {};
  activeFlag: number = 1;
  pendingCountAuth: number;
  pendingCountIssue: number;
  pendingCountRejected: number;
  display_pending_issued: boolean = false;
  display_pending_authorization: boolean =false;
  display_certificate_rejected: boolean = false;
  authRejection=[];
  message:string;
  message1:string;
  message2:string;
  AlertModal:boolean=false;
  heading:string;
  alertmsg:string;
  IssuerModal:boolean=false;
  AuthModal:boolean=false;
  issuerid:string;
  authorizerid:string;
  showtext:boolean=false;
  showimg:boolean=false;
  pedningissuesfilter:string='';
  // initial_display:boolean=true;
  pid1: string;
  AuthSuccessModal:boolean=false;
  res1: [];
  res2: [];
  res3= [];
  offset:number=0;
  limit: number=7;
  rejectedlimit:number=6;
  rejectedoffset:number=0;
  issueslimit:number=6;
  issuesoffset:number=0;
  pendingauthfilter:string='';
  e: any;
  certsrejectedfilter:string='';
  ErrorModal: boolean=false;
  errorMessage: string;
  alertHead: string;
  imgMessage: string;
   selectedTab:string='OVERVIEW';
   branch:string='';
  departments: any;
  addDeptForm:FormGroup;
  //  selectedTab:string='ORGANIZATION';
  

  
  constructor(private commonService: CommonService,
    private superadminService: SuperadminService,
    private dashboardService: DashboardService,
    private local: LocalstorageService,
    private settingsService:SettingsService) { }

  async ngOnInit()
  {
    this.addDeptForm=new FormGroup({
      branch:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)])    
    });    
    try
    {
      this.blockUserInterface();
      this.selectedName="";
      this.selectedName1="";
      const res=await this.commonService.getCountries();
      this.countries = res;
      console.log(res);      
      await this.getStatisticCount();
      await  this.getdepoptions();   
      const issuers=await this.getIssuers();
      console.log(issuers.length);
      if(true){
        this.selectedTab='ORGANIZATION';       
      }
      this.dappid=this.local.getDappId();
      await this.getpendingauth();                
      await this.getdepartments();
      this.unblockUserInterface();
    }
    catch(error)
    {
      this.unblockUserInterface();
      this.openErrorModal(error['message'],"Error Message");
    }          
  }

  async createDepartment()
  {    
    var params=
    {
      department:this.branch,
      levels:[]
    };
    try
    {
      this.blockUserInterface();    
      const deptRes = await this.settingsService.assigndepartments(params);
      if(deptRes['isSuccess'])
      {        
        this.openErrorModal(deptRes['message'],"Success Message");   
        this.getdepartments();
      }    
      this.unblockUserInterface();
    }
    catch(error)
    {
      this.unblockUserInterface();
      this.openErrorModal(error['message'],"Error Message");
    }    
  }

  async getdepartments() {
    try
    {
      let depresponse = await this.settingsService.getdepwithcount();
      console.log(depresponse);
      this.departments = depresponse['departments'];
    }
    catch(error)
    {
      this.openErrorModal(error['message'],'error message');
      throw error;
    }  
  }

  closeIssuerModal(){
    this.IssuerModal=false;
  }
  closeAuthModal(){
    this.AuthModal=false;
  }
  closeAlertModal(){
this.AlertModal=false;
  }
  closeAuthSuccessModal()
{
  this.AuthSuccessModal=false;
}
  openissuermodal(iid){
    console.log(iid);
    this.IssuerModal=true;
    this.issuerid=iid;
  }

  openauthmodal(aid){
    console.log(aid);
    this.AuthModal=true;
    this.authorizerid=aid;
  }
openAlertodal(heading,msg){
  this.AlertModal=true;
this.heading=heading;
this.alertmsg=msg;
}

private async getStatisticCount()
{
  try
  {
    this.blockUserInterface();
    const res = await this.dashboardService.getSuperuserData(); 
    this.pendingCountAuth = res['pendingAuthorizationCount']; 
    this.pendingCountIssue = res['pendingIssuesCount'];   
    this.pendingCountRejected = res['rejectedIssuesCount'];
    this.unblockUserInterface();
  }
  catch(error)
  {
    this.openErrorModal(error['message'],"Error Message");
    throw error;
  }
}


async applyPendingfilter(){
  if((this.pendingauthfilter=='none')||(this.pendingauthfilter=='Departments')){
      this.pendingauthfilter='';
  }
  this.res1=[];
    this.limit=6;
    this.offset=0;
    await this.getpendingauth();
}
async onPendingScrollDown(){  
  this.offset = this.offset + this.limit;
  await this.getpendingauth();

}
async getpendingauth() 
{
  this.activeFlag = 1;  
  this.showimg=false;
  this.display_pending_authorization = false;
  this.display_certificate_rejected = false;
  // this.display_pending_issued = false;
  console.log(this.pendingCountAuth==0)
  if(this.pendingCountAuth===0)
  {
    this.showimg=true;
    this.imgMessage="OOPS! No pending signatures at Authorizers!";
  }
  else
    {
    this.display_pending_authorization = true;
    try{
      this.blockUserInterface();
    let response=await this.superadminService.getpendingauthresult(this.limit,this.offset,this.pendingauthfilter);
     console.log(response);
    this.res1 = response['result'];
    this.unblockUserInterface();
    }  
    catch(error)
      {
        this.unblockUserInterface();
        this.openErrorModal(error['message'],"Error Message");
        throw error;
      }
    }
  }

  async applyPendingIssuesfilter(){
    if((this.pedningissuesfilter=='none')||(this.pedningissuesfilter=='Departments')){
      this.pedningissuesfilter='';
    }
    this.res2=[];
      this.issueslimit=6;
      this.issuesoffset=0;
      await this.getpendingissues();
  }

async onPendingIssuesScrollDown(){
  this.issuesoffset = this.issuesoffset + this.issueslimit;
  await this.getpendingissues();
} 
async getpendingissues() 
{
  this.activeFlag = 2;
  this.showimg=false;
  this.display_pending_issued = false;

  this.display_certificate_rejected = false;
  this.display_pending_authorization = false;

  if(this.pendingCountIssue==0)
  {
    this.showimg=true;
    this.imgMessage="OOPS! No pending certificates at issuers";
  }
  else
  {
    this.display_pending_issued = true;
    try{
      if(this.res2.length<=this.issuesoffset){
      this.blockUserInterface();
    let response=await this.superadminService.getpendingissueresult(this.issueslimit,this.issuesoffset,this.pedningissuesfilter);
    console.log(response);
    this.res2 = response["result"];
    this.unblockUserInterface();
      }
    }
   
    catch(error)
    {
      this.unblockUserInterface();
      this.openErrorModal(error['message'],"Error Message");
    }
  }
  }

  onUp(){

  }
async rejectedfilter(){
     if((this.certsrejectedfilter=='none')||(this.certsrejectedfilter=='Departments')){
    this.certsrejectedfilter='';
  }
  this.authRejection=[];
    this.rejectedlimit=6;
    this.rejectedoffset=0;
    await this.getcertificatesRej();
}


 async onRejetedScrollDown(){
this.rejectedoffset=this.rejectedoffset+this.rejectedlimit;
await this.getcertificatesRej();
  }
async getcertificatesRej() 
{
  this.activeFlag = 3;
  this.showimg=false;
  this.display_pending_issued = false;
  this.display_pending_authorization = false;
    if(this.pendingCountRejected===0)
    {
      this.showimg=true;  
      this.imgMessage="OOPS! No Rejected certificates at authorizers";
    }
    else
    {
    //  alert(this.certsrejectedfilter);

      this.display_certificate_rejected = true;
try{

  //  alert(this.res3.length)
  if(this.authRejection.length<=this.rejectedoffset){
  this.blockUserInterface();
     let response=await this.superadminService.getcertificaterejresult(this.rejectedlimit,this.rejectedoffset,this.certsrejectedfilter);
     this.res3=response['result']
  for(var key in this.res3)
    this.authRejection.push({authorizerEmail: this.res3[key]['authorizerEmail'],department: this.res3[key]['department'],count: this.res3[key]['count']});

    this.unblockUserInterface();
} 
} 
catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }
    }
  }
 
async getdepoptions() 
{
    try
    {
      const res = await this.commonService.getDepartmentsApi();    
      let result = res['departments'];
      this.deptnames = result;    
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"Error Message");
      throw error;
    }
}

setflag(a) 
{
  if (a == 'issuer') 
  {  
    this.show = true
  }
  else 
  {
    this.show = false;
  }
}

showpage(b) 
{
  if (b == 'initial') 
  {
    this.show1 = true;
  }
  else {
    this.show1 = false;
  }
}

async onsubmitIssuer() 
{
  this.e = (document.getElementById("ide")) as HTMLSelectElement;
  var sel = this.e.selectedIndex - 1;
  this.countrycode = this.countries[sel]['countryCode'];
  this.countrycode = this.countrycode ? this.countrycode : "undefined";
  this.countryid = this.countries[sel]['countryID'];
  var finaldep = [];
  for (var i = 0; i < this.selectedoption.length; i++) 
  {      
    finaldep[i] = this.createobj(this.selectedoption[i]);  
  }
  var params: any =
  {
    email: this.email,
    countryId: this.countryid,
    countrycode: this.countrycode,
    name: this.issuername,
    departments: finaldep,
    type: "merchant",
    dappid:this.dappid ,
    role: "issuer"
  };
  try{
    this.blockUserInterface();
    let promise = await this.superadminService.addUser(params);    
    if (promise['isSuccess'] === true) 
    {
      this.openAlertodal("Success","Issuer Registered Successfully");
      await this.getIssuers();
    }
    else 
    {
      this.openAlertodal("Error",promise['message']);
    }
    this.unblockUserInterface();
  }
  catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }
  }

  createobj(dep) {
    var department = {};
    department['name'] = dep;
    return department;
  }

  async onsubmitAuthorizer() 
  {    
    this.e = (document.getElementById("ide1")) as HTMLSelectElement;
    var sel = this.e.selectedIndex - 1;
    this.countrycode = this.countries[sel]['countryCode'];
    this.countrycode = this.countrycode ? this.countrycode : "undefined";
    this.countryid = this.countries[sel]['countryID'];
    var params: any =
    {
      countryId: String(this.countryid),
      countryCode: this.countrycode,
      name: this.authName,
      email: this.authid,
      type: "user",
      dappid:this.dappid,
      role: "authorizer"
    };    
    try
    {
      this.blockUserInterface();
      let promise = await this.superadminService.addAuthorizer(params);
      if (promise['isSuccess'] === true) 
      {
        this.AuthSuccessModal=true;
        await this.getAuthorizers();    
      }
      else {
        this.openAlertodal("Error!!",promise['message']);
      }
      this.unblockUserInterface();
    } 
    catch(error)
    {
      this.openErrorModal(error['message'],"Error Message");
    }
  }

  async getIssuers()
   {
    var count_issuers = 0;
    var index = 1;
    try{
      this.blockUserInterface();
    const res = await this.superadminService.getIssuers(5,0,'');
    console.log(res);
    console.log(res["total"]);
    let total = res["total"];
    var total_issuers = total;
    count_issuers = total_issuers;
    var issuer_data = '';
    this.issuerresponse = res["issuers"];
    this.unblockUserInterface();
    return this.issuerresponse;
    }
    catch(error)
  {
    this.unblockUserInterface();
    this.openErrorModal(error['message'],"Error Message");
    throw error;
  } 
  }

  async getAuthorizers() {
    var count_issuers = 0;
    var index = 1;
    try{
      this.blockUserInterface();
    const res = await this.superadminService.getAuthorizers(5,0,'');
    console.log(res);
    console.log(res["total"]);
    let total = res["total"];
    var total_issuers = total;
    count_issuers = total_issuers;    
    this.authorizerresponse = res["authorizers"];   
    this.unblockUserInterface();
  }
  catch(error)
  {
    this.unblockUserInterface();
    this.openErrorModal(error['message'],"Error Message");
    throw error;
  }    
  }

async issuerremove(iid)
{
  try{
    this.blockUserInterface();
  let promise = await this.superadminService.removeIssuer(iid);
  if(promise['isSuccess']===true){
  this.openAlertodal("Success!","Issuer removed Successfully")  
    await this.getIssuers();
  }
  else{
    this.openAlertodal("Error!",promise['message'])
  }
  this.unblockUserInterface();
}
catch(error)
      {
        this.unblockUserInterface();
        this.openErrorModal(error['message'],"Error Message");
      }
}

async authremove1(aid)
{  
  try{
    this.blockUserInterface();
  let promise = await this.superadminService.removeAuthorizer(aid);
  if(promise['isSuccess']===true){
    this.openAlertodal("Success!","Authoizer removed Successfully")
    await this.getAuthorizers();
  }
  else{
    this.openAlertodal("Error!",promise['message'])
  }
  this.unblockUserInterface();
}
catch(error)
      {
        this.openErrorModal(error['message'],"Error Message");
      }
}

//open error pop up
openErrorModal(message,heading)
{
  this.alertHead=heading;
  this.errorMessage=message;
  this.ErrorModal=true;
}
//close error pop up
closeErrorModal()
{
  this.ErrorModal=false;
}
  /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }
}


