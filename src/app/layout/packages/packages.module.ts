import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PackagesComponent } from './packages.component';
import { PackagesRoutingModule } from './packages-routing.module';
import { ShareModule } from '../../share.module';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [PackagesComponent],
  imports: [
    CommonModule,
    PackagesRoutingModule,
    ShareModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PackagesModule { }
