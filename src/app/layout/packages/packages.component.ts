import { Component, OnInit } from '@angular/core';
import { PackagesService } from 'src/app/services/packages/packages.service';
import { $ } from 'protractor';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
packagesList:any=[];
couponsList:any=[];
asset:string='certs';
countrycode:string='';
packageName:string='';
couponCode:string='';
couponapplied:boolean=false;
finalprice:string='';
currency:string='';
chosecurrency:string='';
chosentype:string='';
data:string='';
username:string='';
useremail:string='';
usercontact:string='';
  showipayform: string="none";
  link: any;
  responseurl: any;
  paymetid: any;
  couponapplieddiv: boolean;
  contact: any;
  gateway: any;
  address:string;
  bel: any;
  belToken: any;
  kycStatus: any;
  role: any;
  secret: any;
  bkvsdm_token: any;
  isLoggedIn: string;
  AlertModal: boolean=false;
  heading: any;
  alertmsg: any;
  alertHead: any;
  errorMessage: any;
  ErrorModal: boolean;

constructor(private packages:PackagesService,private local:LocalstorageService) { }

async ngOnInit() 
{
  this.blockUserInterface();
  try
  {
    await this.getpackages();
     this.address=this.local.getWalletAddress();
    this.useremail=this.local.getEmail();
    this.countrycode=this.address.substring(this.address.length-2,this.address.length);
    // this.countrycode="MY";
    this.unblockUserInterface();
  }
  catch(error)    
  {
    this.unblockUserInterface();
    this.openErrorModal(error['message'],'error message');    
  }
}

async getpackages(){
  this.blockUserInterface();
  try
  {
    const res=await this.packages.getPackageList()
    console.log(res);
    if(res['isSuccess']){
    this.packagesList=res['result'];
    }
    this.unblockUserInterface();
  }
  catch(error)    
  {
    this.unblockUserInterface();
    this.openErrorModal(error['message'],"error message");
    throw error;
  }
// this.unblockUserInterface();
}

async getcoupons(packagename)
{
  this.packageName=packagename;  
  this.blockUserInterface();
  try
  {
    const res=await this.packages.getCouponList(this.asset,this.countrycode);
    console.log(res);
    if(res['isSuccess']){
      var response=res['response'];
      console.log(response.length)
      this.couponsList=[];
      for(var i=0;i<response.length;i++){
        if(response[i]!=null)
        {
          var data=JSON.parse(response[i]['coupon_data'])
          console.log(data);
          var params={
            name:response[i]['name'],
            type:data['type'],
            percent:data['percent'],
            enddate:data['enddate']
          };
        this.couponsList.push(params);
        }
      }
      console.log(this.couponsList);
    }
    this.unblockUserInterface();
  }
  catch(error)
  {
    this.unblockUserInterface();
    this.openErrorModal(error['message'],"error message");   
    throw error; 
  }
}

applyCoupon(couponname){
  // alert(couponname)
  this.couponCode=couponname;
  this.couponapplied=true;
  console.log(this.packageName)
}

async proceedToPay(){
  console.log(this.couponCode);
  console.log(this.packageName);
  console.log(this.countrycode);
  this.blockUserInterface();
  try
  {
    const res=await this.packages.getFinalPrice(this.packageName,this.couponCode,this.countrycode);
    console.log(res);    
    if(res['isSuccess']){
      this.finalprice=res['data']['price'].toFixed(2);
      // this.finalprice='1000'
      this.paymetid=res['id'];
      this.responseurl=res['responseurl'];
      this.link=res['link'];
      this.currency=res['data']['currency'];
      this.unblockUserInterface();
      await this.redirectTo();
    }
    else{      
      this.openErrorModal(res['message'],"error message");    
    }
  }
  catch(error){    
    this.unblockUserInterface();
    this.openErrorModal(error['message'],"error message");
    throw error;
  }
}

async redirectTo(){
  this.blockUserInterface();
  try
  {
    // if(this.chosentype=='currency'){
      const pg=await this.packages.getPaymentGateway(this.currency);
      console.log(pg);
      this.gateway=pg['pg']
      // var gateway='paypal'
      if(pg['isSuccess'])
      {
        if(this.gateway!='ipay')
        {
          await this.navigateToPaypal();
          // alert("Sorry!! We are not acccepting payments from your country")
          // this.openErrorModal("Sorry!! We are not accepting payments from your country","error message");
        }
        else{
          // alert(this.currency+this.finalprice)
          const response=await this.packages.getsignature(this.currency,this.finalprice)
          console.log(response);
          //  if(response['isSuccess']){
          this.data=response['data'];
          // var e=document.getElementById('finalprice')
          // alert("finalprice"+this.finalprice+"data"+this.data+"currency"+this.currency);
          // document.getElementById('ipay').click();
          //  }          
          this.showipayform="block";
        }
      }      
    // }
    // else if(this.chosentype='crypto')
    // {
    //   console.log(this.chosentype);
    // }
    this.unblockUserInterface();
  }
  catch(error){    
    this.unblockUserInterface();
    this.openErrorModal(error['message'],"error message");    
  }
}

getcurrencytype(e){
  console.log(e);
  console.log(e.target);
  console.log(e.target.checked);
  this.chosentype=e.target.value;
  console.log(this.chosentype)
}

 removecoupon(){   
   this.couponapplied=false;
  // document.getElementById('couponapplied').style.display='none';
  this.couponCode='';
}

async navigate(){
  this.local.setPackage(this.packageName);

  // if(this.gateway=='ipay'){
this.kycStatus=this.local.getKycStatus();
this.role=this.local.getUserRole();
this.bkvsdm_token=this.local.getBKVSToken();
this.belToken=this.local.getBKVSToken();
this.bel=this.local.getBalance();
this.isLoggedIn=this.local.getLoggedIn();
var local={
  package:this.packageName,
            address:this.address,
            bel:this.bel,
            belToken:this.belToken,
            kycStatus:this.kycStatus,
            roleId:this.role,
            email:this.useremail, 
            bkvsdm_token:this.bkvsdm_token,
            isLoggedIn:this.isLoggedIn 
}
var data={
  type:"ipay",
  finalprice:this.finalprice,
  currency:this.currency,
  username:this.username,
  email:this.useremail,
  contact:this.contact,
  signature:this.data,
  url:this.responseurl
}
    var params={
      id:this.paymetid,
      local:local,
      data:data
  }
// }
this.blockUserInterface(); 
  const res=await this.packages.sendbody(params);
  console.log(res);
  if(res['isSuccess']){
    this.unblockUserInterface();

    this.local.setPackage(this.packageName);
    // localStorage.setItem('package',this.packageName);
    if(res['message']=="Payment data saved"){
      window.location.href=this.link;
    }
  }
  this.unblockUserInterface();

}

closeform(){
  this.showipayform="none";
}

//open error pop up
openErrorModal(message,heading)
{
  this.alertHead=heading;
  this.errorMessage=message;
  this.ErrorModal=true;
}

//close error pop up
closeErrorModal()
{
  this.ErrorModal=false;
}

 /* It blocks UI. */
 public blockUserInterface() {
  this.blockUI.start("Wait...");
}

/* It unblocks UI.*/
public unblockUserInterface() {
  this.blockUI.stop();
}





async navigateToPaypal(){
  this.local.setPackage(this.packageName);

  // if(this.gateway=='ipay'){
this.kycStatus=this.local.getKycStatus();
this.role=this.local.getUserRole();
this.bkvsdm_token=this.local.getBKVSToken();
this.belToken=this.local.getBKVSToken();
this.bel=this.local.getBalance();
this.isLoggedIn=this.local.getLoggedIn();
var local={
  package:this.packageName,
            address:this.address,
            bel:this.bel,
            belToken:this.belToken,
            kycStatus:this.kycStatus,
            roleId:this.role,
            email:this.useremail, 
            bkvsdm_token:this.bkvsdm_token,
            isLoggedIn:this.isLoggedIn 
}
var data={
  type:"paypal",
}
    var params={
      id:this.paymetid,
      local:local,
      data:data
  }
// }
this.blockUserInterface();
 
  const res=await this.packages.sendbody(params);
  console.log(res);
  if(res['isSuccess']){
    this.unblockUserInterface();
    this.local.setPackage(this.packageName);
    var newurl = this.responseurl.replace("post", "pay");
    // alert(newurl)
    // localStorage.setItem('package',this.packageName);
    if(res['message']=="Payment data saved"){
this.blockUserInterface();

     const result= await this.packages.payUsingPaypal('1.00',this.currency,newurl);
     this.unblockUserInterface();

     window.location.href=result['url'];

    }
  }
  this.unblockUserInterface();

}
   
}



