import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthorizerDashboardComponent} from './authorizer-dashboard.component' ;

const routes: Routes = [{
  path:'',component:AuthorizerDashboardComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorizerDashboardRoutingModule { }
