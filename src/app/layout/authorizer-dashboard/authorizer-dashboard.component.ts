import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { SuperadminService } from 'src/app/services/superadmin/superadmin.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-authorizer-dashboard',
  templateUrl: './authorizer-dashboard.component.html',
  styleUrls: ['./authorizer-dashboard.component.scss']
})
export class AuthorizerDashboardComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild('chart') canvas: ElementRef;
  aid: string;
  certssigned: string;
  certsrejeted: string;
  studentscount: string;
  chartColors1;
  chartColors;
  year = new Date().getFullYear();
  month = new Date().getMonth() + 1;
  filteryear = new Date().getFullYear();
  authemail: string;
  initialauthemail: string;
  chartDataArr1: Array<any> = [];
  barChartLabels1: Array<any> = [];
piechartData:Array<any>=[];

  barChartData1: any = [
    {
      data: [],
      label: 'Month'
    }
  ];
  public barChartType1 = 'bar';
  public barChartLegend1 = true;

  public barChartOptions1 = {
    scaleShowVerticalLines: true,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        },
        barThickness: 15,
      }]
    }
  }

  chartDataArr: Array<any> = [];

  barChartLabels: Array<any> = [];
  barChartData: any = [
    {
      data: [],
      label: 'Department'
    }
  ];
  public barChartType = 'horizontalBar';
  public barChartLegend = true;

  public barChartOptions = {
    scaleShowVerticalLines: true,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        },
        barThickness: 15,
      }]
    }
  }
  errorMessage: any;
  ErrorModal: boolean;
  alertHead: any;

  // Doughnut
 public doughnutChartLabels: Array<any>=[];
 public doughnutChartData: any = [
  { 
    data: [],
    label:'Reason'
}
  ];
 public doughnutChartType= 'doughnut';

 public pieChartOptions={
 
  plugins: {
    datalabels: {
      backgroundColor: function(context) {
        return context.dataset.backgroundColor;
      },
      borderColor: 'white',
      borderRadius: 25,
      borderWidth: 2,
      color: 'white',
      display: function(context) {
        var dataset = context.dataset;
        var count = dataset.data.length;
        var value = dataset.data[context.dataIndex];
        return value > count * 1.5;
      },
      font: {
        weight: 'bold'
      },
      formatter: Math.round
    }
  }
}

  constructor(private dashboardService: DashboardService, private local: LocalstorageService, private superadminservice: SuperadminService) { }

  async ngOnInit() {    
    this.aid = this.local.getAuthorizerId();
    this.authemail = this.local.getEmail();

    try
    {
      await this.rejectReasonStats();
    const authdata = await this.dashboardService.getAuthData(this.aid);
    console.log(authdata);
    if (authdata['isSuccess']) {
      this.certsrejeted = authdata['rejectedCount'];
      this.certssigned = authdata['signCount'];
    }
 
    const studres = await this.dashboardService.getSuperuserDashboardCounts();
    console.log(studres);
    if (studres['isSuccess']) {
      this.studentscount = studres['recipientsCount'];
    }

    await this.gettopdep();

    await this.getmonthlycount(this.authemail)
    const gradient = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
    gradient.addColorStop(0, 'blue');
    gradient.addColorStop(1, 'white');

    this.chartColors1 = [
      {
        backgroundColor: gradient
      }
    ];

    const gradient1 = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
    gradient1.addColorStop(0, 'green');
    gradient1.addColorStop(1, 'white');

    this.chartColors1 = [
      {
        backgroundColor: gradient1
      }
    ];
    this.unblockUserInterface();
  }
  catch(error)
  {
    this.openErrorModal(error['message'],"error message");
  }
  }

  async getmonthlycount(email) {
    try{
    const response = await this.dashboardService.getRoleBasedStatistics(this.year, 'authorizer', email);
    console.log(response);
    this.chartDataArr1 = [];
    this.barChartLabels1 = [];
    this.barChartData1 = [];
    var months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
    for (var i = 0; i < 12; i++) {
      this.chartDataArr1.push(Number(response['monthsCount'][i]));
      this.barChartLabels1.push(months[i])
    }
    console.log("deplabelarr" + JSON.stringify(this.barChartLabels1));
    console.log("depdataarr" + JSON.stringify(this.chartDataArr1));
    this.barChartData1.push({ data: this.chartDataArr1, label: 'Month' });
  }
  catch(error)
  {
    throw error;
  }
  }

  async  gettopdep() {
    try{
    const response = await this.dashboardService.getAuthDepRanks(this.aid, 5, this.month, this.filteryear);
    console.log(response);

    if(response['isSuccess']==true){
      const data=response['result'];
      this.chartDataArr=[];
      this.barChartLabels=[];
      this.barChartData=[];
       for(var i=0;i<data.length;i++){    
       this.chartDataArr.push(Number(data[i]['count'])); 
       this.barChartLabels.push(data[i]['name']);
      // console.log("deplabelarr" + JSON.stringify(this.barChartLabels));
      // console.log("depdataarr" + JSON.stringify(this.chartDataArr));
       }
       this.barChartData.push({data:this.chartDataArr, label:'Department'}); 
     }
    }
    catch(error)
    {
      throw error;
    }
    }


async rejectReasonStats(){
  try
  {
  const res=await this.dashboardService.rejectReasonStatistics();
  
  if(res['isSuccess']){
    console.log(res['reasons']);
    const data=res['reasons']
    this.doughnutChartData=[];
    this.doughnutChartLabels=[];
    this.piechartData=[];
    for(var i=0;i<data.length;i++){
      this.piechartData.push(data[i]['count']);
      this.doughnutChartLabels.push(data[i]['reason']);

    }
    this.doughnutChartData.push({data:this.piechartData,label:'Reason'});
    console.log(this.doughnutChartData);
  }
}
catch(error)
{
  throw error;
}
}

    //open error pop up
  openErrorModal(message,heading)
  {
    this.alertHead=heading;
    this.errorMessage=message;
    this.ErrorModal=true;
  }
  //close error pop up
  closeErrorModal()
  {
    this.ErrorModal=false;
  }

    /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }

}
