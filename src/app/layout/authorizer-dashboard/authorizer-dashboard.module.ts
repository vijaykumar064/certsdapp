import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthorizerDashboardComponent} from './authorizer-dashboard.component' ;
import { AuthorizerDashboardRoutingModule } from './authorizer-dashboard-routing.module';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { ShareModule } from '../../share.module'; 


@NgModule({
  declarations: [AuthorizerDashboardComponent],
  imports: [
    CommonModule,
    AuthorizerDashboardRoutingModule,
    Ng2GoogleChartsModule,
    FormsModule,
    ChartsModule,
    ShareModule
  ]
})
export class AuthorizerDashboardModule { }
