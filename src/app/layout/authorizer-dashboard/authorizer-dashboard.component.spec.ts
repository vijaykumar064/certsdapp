import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizerDashboardComponent } from './authorizer-dashboard.component';

describe('AuthorizerDashboardComponent', () => {
  let component: AuthorizerDashboardComponent;
  let fixture: ComponentFixture<AuthorizerDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizerDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
