import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  email:string;
  role: string;
  balance: string;
  organization: string;
  selectedItem: string;
  menuItems: any[] = [];
  isActive: boolean;
  collapsed: boolean;
  showMenu: string;
  pushRightClass: string;
  kycstatus: boolean;
  superuserflag: boolean;
  issuerflag: boolean;
  authflag: boolean;
  useremail: string;
  displayorganisation: boolean = true;
  @Output() collapsedEvent = new EventEmitter<boolean>();


  constructor(private route: Router, private local: LocalstorageService) {
    this.route.events.subscribe(val => {
      if (
        val instanceof NavigationEnd &&
        window.innerWidth <= 992 &&
        this.isToggled()
      ) {
        this.toggleSidebar();
      }
    });

  }

  ngOnInit() {
    this.balance = this.local.getBalance();
    this.organization = this.local.getOrganization();
    this.role = this.local.getUserRole();
    this.useremail = this.local.getEmail();
    this.kycstatus = this.local.getKycStatus(); // change by priyanka for testing
    this.loadRoute();
  }
  eventCalled() {
    this.isActive = !this.isActive;
  }

  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }
  toggleCollapsed() {
    this.collapsed = !this.collapsed;
    this.collapsedEvent.emit(this.collapsed);
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.pushRightClass);
  }

  loadRoute() {

    if (this.role === "superuser") {
      if ((this.useremail != "vino9@yopmail.com") && (this.kycstatus == false)) {
        if(this.useremail !="cino1@yopmail.com"){
          this.superuserflag = true;
        }
      }
      //  if(this.kycstatus == false){
      //   this.superuserflag=true;
      // }   
      this.organization = this.local.getOrganization();

      this.menuItems.push({ displayName: "Home",displayId: 1, iconName: "Mask Group 1.png", link:"/superadmin",disabled:this.superuserflag});
      this.menuItems.push({ displayName: "Dashboard",displayId: 1, iconName: "Mask Group 2.png", link:"/dashboard",disabled:this.superuserflag});
      this.menuItems.push({ displayName: "Certificate",displayId: 1, iconName: "icons8-diploma-100-3.png", link:"/certificate",disabled:this.superuserflag});
      this.menuItems.push({ displayName: "My Wallet",displayId: 1, iconName: "icons8-wallet-100-3.png", link:"/wallet",disabled:false});
      this.menuItems.push({ displayName: "KYC",displayId: 1, iconName: "Mask Group 4.png", link:"/kyc",disabled:false});
      this.menuItems.push({ displayName: "Settings",displayId: 1, iconName: "Mask Group 5.png", link:"/settings",disabled:this.superuserflag});
      this.menuItems.push({ displayName: "Members",displayId: 1, iconName: "employee1.png", link:"/members",disabled:this.superuserflag});
    }
    else if(this.role==="issuer")
    {  
      if(this.kycstatus == false){
        if(this.useremail!=="vino10@yopmail.com") {
          if(this.useremail!=="cino2@yopmail.com")
        {
          this.issuerflag=true;
        }
        }  
      }
     
    //  if(this.kycstatus== false){
    //    this.issuerflag=true;
    //  }
      this.organization=this.local.getOrganization();

      this.menuItems.push({ displayName: "Home",displayId: 1, iconName: "Mask Group 1.png", link:"/issuer",disabled:this.issuerflag});
      this.menuItems.push({ displayName: "Dashboard",displayId: 1, iconName: "Mask Group 2.png", link:"/issuerdashboard",disabled:this.issuerflag});
      this.menuItems.push({ displayName: "Certificate",displayId: 1, iconName: "icons8-diploma-100-3.png", link:"/certificate",disabled:this.issuerflag});
      this.menuItems.push({ displayName: "My Wallet",displayId: 1, iconName: "icons8-wallet-100-3.png", link:"/wallet",disabled:false});
      this.menuItems.push({ displayName: "KYC",displayId: 1, iconName: "Mask Group 4.png", link:"/kyc",disabled:false});
      this.menuItems.push({ displayName: "Members",displayId: 1, iconName: "employee1.png", link:"/members",disabled:this.issuerflag});
      // this.menuItems.push({ displayName: "Settings",displayId: 1, iconName: "Mask Group 5.png", link:"/settings",disabled:"true"});
      // this.route.navigateByUrl('/issuer');
    }
    else if (this.role === "authorizer") {
      if (this.kycstatus == false) {

        if (this.useremail != "vino11@yopmail.com") {
          if (this.useremail != "cino3@yopmail.com") {
          //   if (this.useremail != "cd_auth3@yopmail.com") {
              this.authflag = true;
          //   }
          }
        }
      }
      // if(this.kycstatus== false){
      //   this.authflag=true;
      // }
    this.organization=this.local.getOrganization();

     // this.route.navigateByUrl('/authorizer');
      this.menuItems.push({ displayName: "Home",displayId: 1, iconName: "Mask Group 1.png", link:"/authorizer",disabled:this.authflag});
      this.menuItems.push({ displayName: "Dashboard",displayId: 1, iconName: "Mask Group 2.png", link:"/authorizer-dashboard",disabled:this.authflag});
      this.menuItems.push({ displayName: "Certificate",displayId: 1, iconName: "icons8-diploma-100-3.png", link:"/certificate",disabled:this.authflag});
      this.menuItems.push({ displayName: "My Wallet",displayId: 1, iconName: "icons8-wallet-100-3.png", link:"/wallet",disabled:false});
      this.menuItems.push({ displayName: "KYC",displayId: 1, iconName: "Mask Group 4.png", link:"/kyc",disabled:false});
      this.menuItems.push({ displayName: "Members",displayId: 1, iconName: "employee1.png", link:"/members",disabled:this.authflag});
      // this.menuItems.push({ displayName: "Settings",displayId: 1, iconName: "Mask Group 5.png", link:"/settings",disabled:"false"});   
    }
    else if (this.role === "new user") {
      this.displayorganisation = false;
      
      // this.route.navigateByUrl('/authorizer');
      this.menuItems.push({ displayName: "My Wallet", displayId: 1, iconName: "icons8-wallet-100-3.png", link: "/wallet", disabled: false });
      this.menuItems.push({ displayName: "KYC", displayId: 1, iconName: "Mask Group 4.png", link: "/kyc", disabled: false });
      // this.menuItems.push({ displayName: "Settings",displayId: 1, iconName: "Mask Group 5.png", link:"/settings",disabled:"false"});   
    }

  }

  logout() {
    localStorage.clear()
    this.route.navigateByUrl('/home');
  }

}
