import { Component, OnInit } from '@angular/core';
import { WindowRefService } from '../../services/WindowRef/window-ref.service';
import {WalletService} from '../../services/wallet/wallet.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalstorageService } from 'src/app/services/localstorage/localstorage.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { PackagesService } from 'src/app/services/packages/packages.service';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  RechargeSuccess:boolean=false;
  dappregerror:string='';
  KycNotVerifiedUser:boolean=false;
  KycVerifiedNewUser:boolean=false;
  KycVerifiedSuperUser:boolean=false;
  walletadress:string;
  amount:string;
  walletaddress: string;
  Passphrase:string;
  passphrase:string;
  dappname:string;
  description:string;
  company1:string;
  display1:string='none';
  AlertModal:boolean=false;
  heading:string;
  alertmsg:string;
  kycMandatoryModal:boolean=false;
  dappRegMandatoryModal:boolean=false;
  kycstatus:boolean;
  email:string;
  role:string;
  balance:string;
  rechargebtn:string='none';
  dappregbtn:string='none';
  kycverifybtn: string='none';
  params:string='';
  id:string='';
  selectedPackage:string='';
  countrycode: any;
  dappregsteps: boolean;
  constructor(private winRef: WindowRefService,private actRoute: ActivatedRoute, private walletService:WalletService,private route:Router,private local:LocalstorageService,private packages:PackagesService) { 
    console.log('Native window obj', winRef.nativeWindow);
    this.local.setLoggedIn("true");
   
  }

 async ngOnInit() {
  this.local.setLoggedIn("true");

  this.actRoute.queryParams.subscribe(params => {
    this.params = this.actRoute.snapshot.params.result;  
    // alert(this.params); 
    this.id=this.actRoute.snapshot.params.id;
    // alert(this.id);
    console.log(this.id)
  });
// alert(this.id)
if(this.id){
  // alert(this.id);
 const res=await this.packages.getdata(this.id);
//  alert("here");
 if(res['isSuccess']){
   this.local.setLoggedIn(res['data']['local']['isLoggedIn']);
  this.local.setPackage(res['data']['local']['package'])
  // alert(res['data']['local']['package']);
  this.local.setWalletAddress(res['data']['local']['address'])
  this.local.setBalance(res['data']['local']['bel'])
  this.local.setBelriumToken(res['data']['local']['belToken'])
  this.local.setKycStatus(res['data']['local']['kycStatus'])
  this.local.setUserRole(res['data']['local']['roleId'])
  this.local.setEmail(res['data']['local']['email'])
  this.local.setBKVSToken(res['data']['local']['bkvsdm_token'])
  this.balance=this.local.getBalance();
  this.role=this.local.getUserRole();
this.selectedPackage=this.local.getPackage();
this.kycstatus=this.local.getKycStatus();
this.selectedPackage=this.local.getPackage();
// alert(this.selectedPackage);
}
}
this.email=this.local.getEmail();
this.walletaddress=this.local.getWalletAddress();
    this.balance=this.local.getBalance();
    this.role=this.local.getUserRole();

 this.kycstatus=this.local.getKycStatus();
 if(this.kycstatus== true){
   if(this.role == "new user")
  {

    var flag=localStorage.getItem('flag');
    if(this.params=='success'){
            if(flag!="true"){
              // alert('hey');
              this.countrycode=this.walletaddress.substr(this.walletaddress.length -2);
                const res=await this.packages.sendMoney(this.countrycode,this.walletaddress);
                if(res['isSuccess'])
                {
                   localStorage.setItem('flag','true')
                   this.RechargeSuccess=true;
                  //  alert(this.selectedPackage);
                   console.log(this.selectedPackage);
                const response=await this.packages.savePackage(this.selectedPackage,this.email);
                console.log(response);
                }
            }
            else{
              this.RechargeSuccess=true;
            }
    }
    else{
      // alert("dappsteps");
this.dappRegMandatoryModal=true;
this.KycVerifiedNewUser=true;
    }
    
  }
  else if(this.role =="superuser"){
    this.KycVerifiedSuperUser=true;
  }
 }
else{
  this.KycNotVerifiedUser=true;
}
}

  // closekycMandatoryModal(){
  //   this.kycMandatoryModal=false;
  // }
  closeRechargeModal(){
this.RechargeSuccess=false;
this.dappregsteps=true;
  }

  closedappStepsModal(){
    this.dappregsteps=false;
    this.display1='block';
  }

  closeMandatoryModal()
  {
    this.dappRegMandatoryModal=false;
  }

openAlertModal(heading,msg)
{
  this.AlertModal=true;
  this.heading=heading;
  this.alertmsg=msg;
}

openModal()
{
  this.display1="block";
}

onCloseHandled()
{
this.display1='none';
}

async registerdapp(){
 const data = await this.register();
 console.log(JSON.stringify(data));
const regResponse=JSON.parse(data['_body'])
if(regResponse['isSuccess'])
  {
 console.log(regResponse);
 this.display1='none'
 this.openAlertModal("Success!","Successfully Installed.Please login again");
}
else{
this.dappregerror=regResponse['message'];
  // this.openAlertModal("Error!",regResponse['message']);
 }
}

logout(){
  localStorage.clear()
  this.route.navigateByUrl('/home');
}
async register()
{
  var pp=this.passphrase;
  var dapp=this.dappname;
  var desc=this.description;
  var com=this.company1;

  var params:any={
    secret: pp,
    des: desc,
   email:localStorage.getItem("email"),
    // email: "pr.superuser@yopmail.com",  
    company: com,
    // country:localStorage.getItem("country")
    country: "India",
    name: dapp,
    assetType: "certificate"
  }
  console.log(params);
  this.blockUserInterface();
  try{
  let promise = await this.walletService.onregister(params);
  this.unblockUserInterface();
  return promise;
  }
  catch(error)
  {
    this.unblockUserInterface();
    this.openAlertModal('error message',error['message']);
    throw error;
  }
}

rzp1:any;
    options:any={
            "key": "rzp_test_gUZDy2ThhDous7",
            "amount": "200000", // 2000 paise = INR 20
            "name": "Belfrics",
            "description": "Dapp Register",
            "image": "../../lib/img/belbooks-logo.svg",
            "handler": function (response) {
              this.openAlertModal("Success Message","Payment successful");
              // console.log(response.razorpay_payment_id);
              alert("Payment successful");
            },
            "prefill": {
                "name": "Vinodh Kumar",
                "email": "vinodh.p@belfricsbt.com"
            },
            "notes": {
                "address": "Yo!Chick"
            },
            "theme": {
                "color": "#528ff0"
            }
    
    }

public recharge(): void{
  //  console.log("first");
   var role = localStorage.getItem("roleId");
   console.log(role);
   this.rzp1 = new this.winRef.nativeWindow.Razorpay(this.options);
   this.rzp1.open();
   event.preventDefault();
}


  async update(){
     var amount=this.amount;
     var passphrase=this.Passphrase;
     var did=localStorage.getItem('dappid');
     var params:any={
      secret:passphrase,
      dappId:did,
      amount:Number(amount),
      countryCode:"IN"
     }
     try{
     let promise = await this.walletService.onupdate(params);
     return promise;
     }
     catch(error)
  {
    this.openAlertModal('error message',error['message']);
    throw error;
  }
  }


  closeAlertModal(){
    this.AlertModal=false;
  }
  closekycMandatoryModal(){
    this.kycMandatoryModal=false;
  }
    /* It blocks UI. */
    public blockUserInterface() {
      this.blockUI.start("Wait...");
    }
  
    /* It unblocks UI.*/
    public unblockUserInterface() {
      this.blockUI.stop();
    }
}
