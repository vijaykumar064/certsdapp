import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WalletComponent } from './wallet.component';
import { WalletRoutingModule } from './wallet-routing.module';
import { FormsModule } from '@angular/forms';
import { WindowRefService } from '../../services/WindowRef/window-ref.service';
import { ShareModule } from '../../share.module';
// import { DialogComponent } from '../../dialogs/dialog/dialog.component';

@NgModule({
  declarations: [WalletComponent],
  imports: [
    CommonModule,
    WalletRoutingModule,
    FormsModule,
    ShareModule
  ],
  providers: [WindowRefService],
})
export class WalletModule { }
