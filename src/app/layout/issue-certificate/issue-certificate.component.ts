import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { CommonService } from '../../services/common/common.service';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { CertificateService } from '../../services/certificate/certificate.service';
import { SelectorMatcher } from '@angular/compiler';
import { ActivatedRoute } from '@angular/router';
import { ValidationService } from 'src/app/services/Validations/validation.service';
import { validateBasis } from '@angular/flex-layout';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-issue-certificate',
  templateUrl: './issue-certificate.component.html',
  styleUrls: ['./issue-certificate.component.scss']
})
export class IssueCertificateComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  findemail: string;
  certform: FormGroup;
  userregform: FormGroup;
  reguser: string = 'none';
  countries: {};
  _timeout: any = null;
  prevTextBoxText: string = '';
  // issuerdep:string[] = new Array();
  issuerdepartments: any;
  countrycode: string;
  countryid: string;
  employeedata: string;
  getuserid = "";
  iid: string;
  usernotexists: string = 'none';
  passphrase: string;
  userid1: string;
  getuserdep: string;
  getusername: string;
  university: string;
  getdegree: string;
  getyear: string;
  empregresponse: {};
  params: string;
  searchemail: string;
  setemail: string;
  ErrorModal: boolean = false;
  errorMessage: string;
  heading: string;
  alertmsg: string;
  alertHead: string;
  AlertModal: boolean = false;
  date: any;
  constructor(private common: CommonService, private actRoute: ActivatedRoute, private local: LocalstorageService, private certificateservice: CertificateService) {
    this.actRoute.queryParams.subscribe(params => {
      this.params = this.actRoute.snapshot.params.email;        
    });
  }

  async ngOnInit() {
    this.setemail=this.params;
    this.userregform = new FormGroup({
      countrycode: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required,Validators.email]),
      fname: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
      lname: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
      empid1: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z0-9]{1,10}$/)]),
      adhaar: new FormControl('', [Validators.required,Validators.pattern(/^[a-zA-Z0-9]{1,20}$/)]),
      department: new FormControl('', [Validators.required]),
    })

    this.certform=new FormGroup({
      email1:new FormControl('',[Validators.required,Validators.email]),
      username:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
      userid:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z0-9]{1,10}$/)]),
      gradyear:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z0-9]{4}$/)]),
      degree:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z.]{2,15}$/)]),
      userdep:new FormControl('',[Validators.required,Validators.pattern(/^[a-zA-Z]{2,15}$/)]),
  })
    this.iid=this.local.getIssuerId();
    this.university=this.local.getOrganization();
  var departments=this.local.getIssuerDept();
  this.issuerdepartments=departments.split(',');
  // this.issuerdepartments=departments.split(',');
  // console.log(typeof(JSON.stringify(this.issuerdepartments)))
  // console.log(JSON.stringify(this.issuerdepartments))


  // var issuerdep:any
  // issuerdep=departments.split(',');
  // for(var i=0;i<issuerdep.length;i++){
  //   var val:any
  //   val={
  //     dep:issuerdep[i]
  //   }
  //   this.issuerdepartments.push(any)

  // }
  console.log(this.issuerdepartments);
  try
  {
  const response=await this.common.getCountries();
   this.countries = response;
        console.log(response);

    var d = new Date().toLocaleDateString();
    this.date = d;
    console.log(this.date);
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}


  openAlertModal(heading, msg) {
    this.AlertModal = true;
    this.heading = heading;
    this.alertmsg = msg;
  }

  closeAlertModal() {
    this.AlertModal = false;

  }

  openregmodal() {    
    this.reguser = 'block';
  }
  onCloseHandled() {
    this.reguser = 'none';
  }

  async finduser() {    
    this.findemail = this.certform.value.email1

    var params = {
      email: this.certform.value.email1
    }
    try {
      this.blockUserInterface();
      const result = await this.common.userregisterd(params);
      this.unblockUserInterface();
      console.log(result);
      if (result['success'] === true) {  
        if (result['exists'] === false) {          
          this.usernotexists = 'block';
          // this.reguser="block";
        }
        else if (result['exists'] === true) {
          // console.log(result['data']);
          this.employeedata = result['data'];
          this.getuserid = this.employeedata['empid'];
          this.getuserdep = this.employeedata['department'];
          this.getusername = this.employeedata['name'];
          // this.certform.setValue(this.employeedata['userid']:{ this.employeedata['empid']})
          // this.certform.value.userid=this.employeedata['empid'];
        }
      }
    }
    catch (error) {
      this.openErrorModal(error['message'], "Error Message");
    }
  }

  closeexistModal() {
    this.usernotexists = 'none';
  }
  async autofill() {
    var params = {
      email: this.searchemail
    }
    try {
      this.blockUserInterface();
      const result = await this.common.userregisterd(params);
      this.unblockUserInterface();
      console.log(result);

      if (result['success'] === true) {
        if (result['exists'] === false) {
          this.reguser = "block";
        }
        else if (result['exists'] === true) {
          console.log(result['data']);
          this.employeedata = result['data'];
          this.setemail = this.params;
          this.getuserid = this.employeedata['empid'];
          this.getuserdep = this.employeedata['department'];
          this.getusername = this.employeedata['name'];
        }
      }
    }
    catch (error) {
      this.openErrorModal(error['message'], "Error Message");
    }
  }

  async registeruser() {
    var e = (document.getElementById("country")) as HTMLSelectElement;
    var sel = e.selectedIndex - 1;
    this.countrycode = this.countries[sel]['countryCode'];
    this.countrycode = this.countrycode ? this.countrycode : "undefined";
    this.countryid = this.countries[sel]['countryID'];
    console.log(this.countryid);
    console.log(this.iid);

    console.log(this.userregform.value.department);


    var dep = (document.getElementById("dep")) as HTMLSelectElement;
    var department = dep.options[dep.selectedIndex].value;
    console.log(department);
    var params = {
      countryCode: this.countrycode,
      email: this.userregform.value.email,
      empid: this.userregform.value.empid1,
      lastName: this.userregform.value.lname,
      name: this.userregform.value.fname,
      identity: {
        Aadhaar: this.userregform.value.adhaar
      },
      extra: {
      },
      groupName: "Dapps",
      iid: this.iid,
      department: department
    }
    console.log(params);
    try {
      this.blockUserInterface();
      const res = await this.certificateservice.registeremployee(params);
      this.unblockUserInterface();
      console.log(res);
      if (res['isSuccess'] === true) {
        if (res['message'] === "Awaiting wallet address")
        {
          var msg = "User Registered Successfully," + res['message'];
          this.openAlertModal("Success!", msg)
        }      
        else {
          this.openAlertModal("Success!", "User Registered Successfully");        
          this.empregresponse = res['employee'];
          await this.fillempdetails()
        }
      }
      else {
        this.openAlertModal("Error!", res['message'])        
      }
    }
    catch (error) {      
      this.openErrorModal(error['message'], "Error Message");
    }
  }
  async fillempdetails() {
    console.log(this.empregresponse);
    this.getuserid = this.empregresponse['empid'];
    this.getuserdep = this.empregresponse['department'];
    this.getusername = this.empregresponse['name'];
  }
  async initiatecert() {
    var temp=document.getElementById('certtemplate').innerHTML;
    var params = {
      empid: this.certform.value.userid,
      issuerid: this.iid,
      secret: this.passphrase,
      data: {
        name: this.certform.value.username,
        degree: this.certform.value.degree,
        department: this.certform.value.userdep,
        gradyear: this.certform.value.gradyear
      },
      template:temp
    }
    console.log(document.getElementById('certtemplate').innerHTML)

    console.log(params);
    try {
      this.blockUserInterface();
      const res = await this.certificateservice.PayslipInitialissue(params);
      this.unblockUserInterface();
      console.log(res);
      if (res['isSuccess'] === true) {
        this.openAlertModal("Success!", "Certificate Initiated");
      }
      else {
        this.openAlertModal("Error!", res['message']);        
      }
    }
    catch (error) {
      this.openErrorModal(error['message'], "Error Message");
    }
  }

  //open error pop up
  openErrorModal(message, heading) {
    this.alertHead = heading;
    this.errorMessage = message;
    this.ErrorModal = true;
  }
  //close error pop up
  closeErrorModal() {
    this.ErrorModal = false;
  }
  onSearch() {
    if (this._timeout) { //if there is already a timeout in process cancel it
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
      var se1 = '' + this.setemail;
      this._timeout = null;
      if (se1 != this.prevTextBoxText) {
        this.prevTextBoxText = se1;
        if ((se1 != 'undefined') && (se1 != '') && (se1.match(/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/))) {          
          this.finduser()
        }
      }
    }, 1000);


  }
  
  /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }
}



