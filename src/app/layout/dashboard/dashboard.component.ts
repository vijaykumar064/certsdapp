import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';
import {SuperadminService} from 'src/app/services/superadmin/superadmin.service';
import { ChartsModule } from 'ng2-charts';
import { MultiDataSet, Label } from 'ng2-charts';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { IssuerService } from 'src/app/services/issuer/issuer.service';

var google: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  @ViewChild('chart') canvas: ElementRef;
  // @ViewChild('chart1')canvas:ElementRef;
  dataavailable:boolean=false;
  nodata:boolean=false;
  initialissueremail:string;
  topdep:any[][]=new Array;
  monthlycount:any[][]=new Array;
  authemail:string;
  issuers:any=[];
  authorizers:[];
  topdepartments:[];
  issueremail:string;
  chartDataArr:Array<any>=[];
  chartDataArr1:Array<any>=[];

  chartColors;
  chartColors1;
  issuedCount:string;
  pendingCount:string;
  recipientsCount:string;
  filteryear=new Date().getFullYear();
  year=new Date().getFullYear();
  month= new Date().getMonth()+1;
  
  
  barChartLabels: Array<any>=[];
  barChartData:any = [
    { 
        data: [],
        label:'Department'
    }
];
public barChartType = 'horizontalBar';
public barChartLegend = true;


public barChartOptions = {
  scaleShowVerticalLines: false,
  responsive: true,
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      },
        barThickness : 15
    }]
}

};

barChartLabels1: Array<any>=[];
  barChartData1:any = [
    { 
        data: [],
        label:'Month'
    }
];
public barChartType1 = 'bar';
public barChartLegend1 = true;

public barChartOptions1 = {
  scaleShowVerticalLines: true,
  responsive: true,
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      },
        barThickness : 15,
    }]
}

};



// public pieChartOptions = {
//   tooltips: {
//     enabled: false
//   },
//   plugins: {
//     datalabels: {

//       formatter: (value, ctx) => {

//         let datasets = ctx.chart.data.datasets;
//         let percentage;

//         if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
//           let sum = datasets[0].data.reduce((a, b) => a + b, 0);
//          percentage = Math.round((value / sum) * 100) + '%';
//           return percentage;
//         } else {
//           return percentage;
//         }
//       },
//       color: '#fff',
//     }
//   }
// };


  public SystemName: string = "MF1";
  firstCopy = false;
  alertHead: any;
  errorMessage: any;
  ErrorModal: boolean;
  constructor(private dashboardservice:DashboardService,private superadminservice:SuperadminService)  { }

 
 async ngOnInit() {
  try
  {
   var depresult=await this.dashboardservice.getTopFiveDepartments(this.month,this.year,5) ;
   if(depresult['isSuccess']){
     var len=depresult['result'].length;
   }
   if(len==0){
     this.nodata=true;
   }
   else{
    this.gettopdep()
    console.log(len);
    this.dataavailable=true;
    const issuerres=await this.superadminservice.getIssuers(100,0,'');
    console.log(issuerres);
    // alert(issuerres['isSuccess']);
    console.log(issuerres['issuers'][0]['email'])
    if(issuerres['success']){
      this.issuers=issuerres['issuers'];
      if(IssuerService.length>0)
        this.initialissueremail=this.issuers[0]['email']
    }

    const authres=await this.superadminservice.getAuthorizers(100,0,'');
    //  console.log(authres);
    this.authorizers=authres['authorizers'];    
   await this.getrolebased('issuer',this.initialissueremail)
   const countresponse=await this.dashboardservice.getSuperuserDashboardCounts();
   //  console.log(countresponse);
   this.issuedCount=countresponse['issuedCount'];
   this.pendingCount=countresponse['pendingCount'];
   this.recipientsCount=countresponse['recipientsCount'];
  const gradient = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
  gradient.addColorStop(0, 'blue');
  gradient.addColorStop(1, 'white');
  this.chartColors = [
    {
        backgroundColor: gradient
    }
];
  const gradient1 = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 200);
  gradient1.addColorStop(0, 'green');
  // gradient1.addColorStop(1, 'white');

  this.chartColors1 = [
    {
        backgroundColor: gradient1
    }
];
   }
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
  }
  }

async getsuperuserstats(){
  try
  {
    const response=await this.dashboardservice.getSuperuserData();  
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}

async getissuerbaseddata(){
  try
  {
    const response=await this.dashboardservice.getIssuerData('1');
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }  
}

async getauthbasedata(){
  try
  {
  const response=await this.dashboardservice.getAuthData('1');
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}

async getissuerbased(){  
  try
  {
  await this.getrolebased('issuer',this.issueremail)
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}

async getauthorizerbased(){
  try
  {  
  await this.getrolebased('authorizer',this.authemail)
  }
  catch(error)
  {
    this.openErrorModal(error['message'],'error message');
    throw error;
  }
}

async gettopdep(){
  try
  {
    const response=await this.dashboardservice.getTopFiveDepartments(this.month,this.year,5);
    // console.log("depresponse"+response);
    if(response['isSuccess']==true){
      const data=response['result'];
      this.topdepartments=response['result'];
      if(this.topdepartments.length!=0){
        this.chartDataArr=[];
      this.barChartLabels=[];
      this.barChartData=[];
   
      for(var i=0;i<data.length;i++){    
      this.chartDataArr.push(Number(data[i]['count'])); 
      this.barChartLabels.push(data[i]['department']);
      // console.log("deparr" + JSON.stringify(this.topdep));
      // console.log("deplabelarr" + JSON.stringify(this.barChartLabels));
      // console.log("depdataarr" + JSON.stringify(this.chartDataArr));
      }
      this.barChartData.push({data:this.chartDataArr, label:'Department'}); 
      // console.log("depdataarr" + JSON.stringify(this.barChartData));
      
      }
     }
    }
     catch(error)
     {
       this.openErrorModal(error['message'],'error message');
       throw error;
     }
  }
  


async getrolebased(role,email){
  try
  {
  const response=await this.dashboardservice.getRoleBasedStatistics(this.filteryear,role,email);
  console.log(response);  
  this.chartDataArr1=[];
  this.barChartLabels1=[];
  this.barChartData1=[];
  var months=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
  for(var i=0;i<12;i++){
    this.chartDataArr1.push(Number(response['monthsCount'][i]));
    this.barChartLabels1.push(months[i])
     
      }
      console.log("deplabelarr" + JSON.stringify(this.barChartLabels1));
      console.log("depdataarr" + JSON.stringify(this.chartDataArr1));
  this.barChartData1.push({data:this.chartDataArr1, label:'Month'}); 
    }
    catch(error)
    {
      this.openErrorModal(error['message'],'error message');
      throw error;
    }
} 

public chartClicked(e: any): void {
  console.log(e);
}
public chartHovered(e: any): void {
  console.log(e);
}

//open error pop up
openErrorModal(message,heading)
{
  this.alertHead=heading;
  this.errorMessage=message;
  this.ErrorModal=true;
}
//close error pop up
closeErrorModal()
{
  this.ErrorModal=false;
}

 
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }
  public unblockUserInterface() {
    this.blockUI.stop();
  }

}

