import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthorizerRoutingModule } from './authorizer-routing.module';
import { AuthorizerComponent } from './authorizer.component';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../share.module'; 
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
  declarations: [AuthorizerComponent],
  imports: [
    CommonModule,
    AuthorizerRoutingModule,
    FormsModule,
    ShareModule,
    InfiniteScrollModule
  ]
})
export class AuthorizerModule { }
