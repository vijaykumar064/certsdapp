import { Component, OnInit } from '@angular/core';
import { AuthorizerService } from '../../services/authorizer/authorizer.service';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { CommonService } from '../../services/common/common.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';

@Component({
  selector: 'app-authorizer',
  templateUrl: './authorizer.component.html',
  styleUrls: ['./authorizer.component.scss']
})
export class AuthorizerComponent implements OnInit {
  @BlockUI() blockUI: NgBlockUI;
  authdepartments:{};
  selectedAll:any;
  pendingList=[];
  AuthSigned=[];
  display_pending_authorizations:boolean=false;
  display_rejected:boolean=false;
  display_authorized:boolean=false;
  activeFlag:number=0;
  aid:string;
  offset:number=0;
  limit: number=4;
  rejectedlimit:number=6;
  rejectedoffset:number=0;
  // rejecteddiff:number=this.rejectedlimit-this.rejectedoffset;
  authorizedoffset:number=0;
  authorizedlimit:number=6;
  // authorizeddiff:number=this.authorizedlimit-this.authorizedoffset;
  // diff:number=this.limit-this.offset;
  pending_authorization_count:number=0;
  rejected_count:number=0;
  signCount:number=0;
  totalPendingAuths:{};
  totalRejectedAuths:{};
  totalSignedAuths:{};
  secretPhrase:string;
  rejectFlag:boolean=false;
  certid:string;
  comments:string;
  ErrorModal: boolean=false;
  errorMessage: any;
  alertHead: any;
  authorizeModal: boolean;
  bulkpassphrase:string;
  bulkAuthorizeModal:boolean=false;
  rejectModal: boolean=false;
  passphrase: string;
  reason: string;
  rejectReasonOther: boolean;
  showimage:boolean=false;
  rejComments: string;
  reasonList: [];
  certsauthorizedfilter:string='';
  certsrejectedfilter:string='';
  pendingauthfilter:string='';
  authStatus=[];
  authRejection=[];
  imgMessage: string;
  bulkpid=[];
  constructor(private authserv: AuthorizerService, private local: LocalstorageService, private dashboardserv: DashboardService, private commonServ:CommonService) { 
    this.aid=this.local.getAuthorizerId();
  }

  async ngOnInit() {
  try{
   this.blockUserInterface();
   await this.getauthdata();
   await this.getPendingAuthCerts();
   this.unblockUserInterface();
  }
  catch(error)
  {
    this.openErrorModal(error['message'],"Error Message");
  }
   var dep=this.local.getAuthorizerDept();
   this.authdepartments=dep.split(',');
  }

  async getauthdata(){
    try{
      this.blockUserInterface();
      const response = await this.dashboardserv.getAuthData(this.aid);
      console.log(response);   
      this.pending_authorization_count=response['pendingCount'];      
      this.rejected_count=response['rejectedCount'];      
      this.signCount=response['signCount'];
      this.unblockUserInterface();
    }    
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
      throw error;
    }
  }

  onUp(){

//   this.limit=this.limit-this.diff
//   this.offset=this.offset-this.diff

  }
  async onPendingScrollDown(){    
    this.offset = this.offset + this.limit;
    // this.limit=this.limit+this.diff;
    try{
      await this.getPendingAuthCerts();  
    }    
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
    }
  }

  async applyPendingAuthfilter(){
    if(this.pendingauthfilter=='none'){
      this.pendingauthfilter='';
    }
      this.pendingList=[];
      this.limit=4;
      this.offset=0;
      try{
        await this.getPendingAuthCerts();
      }
      catch(error)
      {
        this.openErrorModal(error['message'],"error message");
      }      
  }
  async getPendingAuthCerts()
  {
    this.activeFlag=1;
    this.showimage=false;
    this.display_rejected=false;
    this.display_authorized=false;
    this.display_pending_authorizations=false;
    if(this.pending_authorization_count===0){
      this.showimage=true;
      this.imgMessage="Congrats! you have no pending certificates";
    }
    else
    {
      this.display_pending_authorizations=true;     
    try
    {
      if(this.pendingList.length<=this.offset){
      this.blockUserInterface();
      const pendingSignsResp = await this.authserv.pendingSigns(this.aid,this.limit,this.offset,this.pendingauthfilter);    
      this.totalPendingAuths=pendingSignsResp['result']; 
       
      for(var key in this.totalPendingAuths)
      {
        console.log(this.totalPendingAuths[key]['totalLevels']); 
        this.authStatus=[];   
        for(var j=0;j<this.totalPendingAuths[key]['totalLevels'];j++)
        {
          if(this.totalPendingAuths[key]['authLevel']>j+1)
          {
            this.authStatus.push({level:'Auth '+j,status: 'signed'});
          }
          else
          {
            this.authStatus.push({level:'Auth '+j,status: 'unsigned'});
          }
        }
        this.pendingList.push({departmentName:this.totalPendingAuths[key]['departmentName'],pid:this.totalPendingAuths[key]['pid'], iid:this.totalPendingAuths[key]['iid'], timestamp: this.totalPendingAuths[key]['timestamp'],receipientName:this.totalPendingAuths[key]['receipientName'],selected:false });         
      }
      this.unblockUserInterface();
    }
   }
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
      throw error;
    }
  }
  }
async onRejectedScrollDown(){
  this.rejectedoffset = this.rejectedoffset + this.rejectedlimit;
  // this.rejectedlimit=this.rejectedlimit+this.rejecteddiff;;
  await this.getRejectedCerts();
}


async applyRejectedfilter(){
  if(this.certsrejectedfilter=='none'){
    this.certsrejectedfilter='';
  }
    this.authRejection=[];
    this.rejectedlimit=6;
    this.rejectedoffset=0;
    await this.getRejectedCerts();
}
  async getRejectedCerts()
  {
    this.activeFlag=2;
    this.showimage=false;
    this.display_pending_authorizations=false;   
    this.display_authorized=false;
    if(this.rejected_count===0)
    {
        this.showimage=true;
        this.imgMessage="You haven't rejected any certificates yet!";
    }
    else{
      this.display_rejected=true;
            
  
   
    try
    {
      if( this.authRejection.length<=(this.rejectedoffset)){
      this.blockUserInterface();
      const rejectedSignResp = await this.authserv.rejectedPayslips(this.aid,this.rejectedlimit,this.rejectedoffset,this.certsrejectedfilter);    
      this.totalRejectedAuths=rejectedSignResp['result'];
  for(var key in this.totalRejectedAuths)
  {   
    this.authStatus=[];       
    for(var j=0;j<this.totalRejectedAuths[key]['totalLevels'];j++)
    {                  
      if(j+1<this.totalRejectedAuths[key]['authLevel'])
      {
        this.authStatus.push({level:'Auth '+j,status: 'signed'});
      }
      else if(this.totalRejectedAuths[key]['authLevel']==j+1)
      {        
        this.authStatus.push({level:'Auth '+j,status: 'rejected'});
      }
      else
      {
        this.authStatus.push({level:'Auth '+j,status: 'unsigned'});
      }
    }    
    this.authRejection.push({departmentName: this.totalRejectedAuths[key]['departmentName'],pid: this.totalRejectedAuths[key]['pid'],iid: this.totalRejectedAuths[key]['iid'],receipientName:this.totalRejectedAuths[key]['receipientName'],timestamp: this.totalRejectedAuths[key]['timestamp'],totalLevels: this.totalRejectedAuths[key]['totalLevels'],authStatus:this.authStatus});
  }
  this.unblockUserInterface();
  }
}
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
    }
  }
  }

  async onAuthorizedScrollDown(){
    this.authorizedoffset = this.authorizedoffset+this.authorizedlimit;
    // this.authorizedlimit=this.authorizedlimit+this.authorizeddiff;
    await this.getAuthorizedCerts();
  }
  
  
  async applyAuthorizedfilter(){
    if(this.certsauthorizedfilter=='none' || this.certsauthorizedfilter=='Departments'){
    
      this.certsauthorizedfilter='';
    }
      this.AuthSigned=[];
      this.authorizedlimit=6;
      this.authorizedoffset=0;
      await this.getAuthorizedCerts();
  }
  async getAuthorizedCerts()
  {
    this.activeFlag=3;
    this.showimage=false;
    this.display_pending_authorizations=false;
    this.display_rejected=false;    
    if(this.signCount==0)
    {
        this.showimage=true;
        this.imgMessage="You haven't signed any certificates yet!";
    }
    else{
      this.display_authorized=true;
    
   
    try
    {
       if(this.AuthSigned.length<=this.authorizedoffset){
         alert(this.certsauthorizedfilter)
        //  alert("length"+this.AuthSigned.length+"limit"+this.authorizedlimit)
      this.blockUserInterface();
      const signedAuthResp = await this.authserv.authorizedPayslips(this.aid,this.authorizedlimit,this.authorizedoffset,this.certsauthorizedfilter);
      this.totalSignedAuths=signedAuthResp['result'];
      for(var key in this.totalSignedAuths)
      {
    this.authStatus=[];       

        console.log(this.totalSignedAuths[key]['totalLevels']);  
        for(var j=0;j<this.totalSignedAuths[key]['totalLevels'];j++)
        {
          if(this.totalSignedAuths[key]['authLevel']>=j+1)
          {
            this.authStatus.push({level:'Auth '+j,status: 'signed'});
          }
          else
          {
            this.authStatus.push({level:'Auth '+j,status: 'unsigned'});
          }
        }
    this.AuthSigned.push({departmentName: this.totalSignedAuths[key]['departmentName'],pid: this.totalSignedAuths[key]['pid'],iid: this.totalSignedAuths[key]['iid'],receipientName:this.totalSignedAuths[key]['receipientName'],timestamp: this.totalSignedAuths[key]['timestamp'],totalLevels: this.totalSignedAuths[key]['totalLevels'],authStatus:this.authStatus});

      }
      this.unblockUserInterface();
    }
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
    }
  }
  }

  async authorize()
  {
    this.closeAuthorizeModal();
    try
    {
      this.blockUserInterface();
      const response = await this.authserv.authorize(this.certid,this.aid,this.passphrase);            
      this.unblockUserInterface();
      if(response['isSuccess'])
      {
        this.openErrorModal("Successfully Authorized","success message");
        await this.getauthdata();
        this.offset=0;
        this.limit=4;
        this.pendingList=[];
        await this.getPendingAuthCerts();
      }      
      else{
        this.openErrorModal("Failed to Authorize","error message");
      }
    }
    catch(error)
    {      
      this.openErrorModal(error['message'],"error message");
    }
  }

  setRejectReason()
  {
    if(this.reason=="other")
    {
      this.rejectReasonOther=true;
    }
    else
    {
      this.rejectReasonOther=false;
      this.rejComments=this.reason;
    }
  }

  async reject()
  {
    this.closeRejectModal();
    if(this.rejectReasonOther)
    {
      this.rejComments=this.comments;
    }
    try
    {
      this.blockUserInterface();
      const response = await this.authserv.rejectPayslip(this.certid,this.aid,this.rejComments);      
      this.unblockUserInterface();
      if(response['success'])
      {        
        this.openErrorModal("Rejected","success message");
        await this.getauthdata();
        this.offset=0;
        this.limit=4;
        this.pendingList=[];
        await this.getPendingAuthCerts();
      }
      else{        
        this.openErrorModal("Failed to Reject","error message");
      }
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
    }
  }

   //open error pop up
   openErrorModal(message,heading)
   {
     this.alertHead=heading;
     this.errorMessage=message;
     this.ErrorModal=true;
   }
   //close error pop up
   closeErrorModal()
   {
     this.ErrorModal=false;
   }

   openAuthorizeModal(certid)
   {
      this.authorizeModal=true;
      this.certid=certid;
   }

   closeAuthorizeModal()
   {
    this.authorizeModal=false;
   }

   async openRejectModal(certid)
   {
     this.rejectModal=true;
     this.certid=certid;
     const reasonRes = await this.authserv.getRejectReasons();
     if(reasonRes['isSuccess']==true)
     {
        this.reasonList = reasonRes['reasons'];
     }
     console.log();
   }

   closeRejectModal()
   {
     this.rejectModal=false;
   }

   selectAll(tabtype) {
    if(tabtype==="pending"){
       for (var i = 0; i < this.pendingList.length; i++) {
         this.pendingList[i].selected = this.selectedAll;
       }
    }
  
   }
   checkIfAllSelected(tabtype) {
     if(tabtype==="pending"){
     this.selectedAll = this.pendingList.every(function(item:any) {
         return item.selected == true;
       })
     }
   }

   openBulkAuthorize(){
    this.bulkAuthorizeModal=true;
     console.log(this.pendingList.length);
     this.bulkpid=[];
     for(var i=0;i<this.pendingList.length;i++){       
       if(this.pendingList[i]['selected']==true){
        this.bulkpid.push(this.pendingList[i]['pid'])
       }
     }
     console.log(this.bulkpid)
   }
   async bulkAuthorize(){
    try
    {
      this.blockUserInterface();
      const response = await this.authserv.bulkAuthorization(this.bulkpid,this.aid,this.bulkpassphrase);
      this.unblockUserInterface();
      if(response['isSuccess'])
      {        
        this.openErrorModal(JSON.stringify(response['results']),"Success Message");
        await this.getauthdata();
        this.offset=0;
        this.limit=4;
        this.pendingList=[];
        await this.getPendingAuthCerts();
      }      
      else{
        this.openErrorModal("Failed to Authorize","error message");
      }
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");
    }
    
   }

   
   closeBulkAuthorizeModal(){
    this.bulkAuthorizeModal=false;
   }
  /* It blocks UI. */
  public blockUserInterface() {
    this.blockUI.start("Wait...");
  }

  /* It unblocks UI.*/
  public unblockUserInterface() {
    this.blockUI.stop();
  }
}

