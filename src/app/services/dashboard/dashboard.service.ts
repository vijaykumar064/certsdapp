import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../../constants';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { RequestOptionsService } from '../../services/requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'

})

export class DashboardService {
dappid:string;
dappRequestUrl:string;

  constructor(private http: Http, private local: LocalstorageService, private requestOptions: RequestOptionsService) 
  { 
    this.dappRequestUrl=Constants.HOST_URL;            
  }
  async getSuperuserData():Promise<object>
  {
    this.dappid = this.local.getDappId();    
    var data={}
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/superuser/statistic/counts',data,options).toPromise();
    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }

  async getIssuerData(iid:string):Promise<Object>
  {   
    this.dappid = this.local.getDappId(); 
    var data={iid: iid}
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/issuer/statistic/counts',data,options).toPromise();
    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async getAuthData(authid:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var data=
    { 
      aid: authid 
    };
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/authorizer/statistic/counts',data,options).toPromise()
    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async getTopFiveDepartments(month,year,limit):Promise<Object>  
  {
    this.dappid = this.local.getDappId();
    var data=
    {
      month: month,
      year: year,
      limit: limit
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl + this.dappid + '/query/departments/topIssued',data,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async getRoleBasedStatistics(year,role,email):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var data=
    {
      year:year,
      role:role,
      email:email
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl + this.dappid + '/statistics/monthlyCounts',data,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }
  

  async getSuperuserDashboardCounts():Promise<Object>  
  {
    this.dappid = this.local.getDappId();
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.get(this.dappRequestUrl + this.dappid + '/totals1',options).toPromise();
      console.log(res);
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async getIssuerDepRanks(iid:string,limit:number,month:number,year:number):Promise<Object>  
  {
    var data={

      iid: iid,
      limit: limit,
      month: month,
      year: year
    }
    this.dappid = this.local.getDappId();
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl + this.dappid + '/query/issuer/departments/ranks',data,options).toPromise();
      console.log(res);
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }
  async rejectReasonStatistics():Promise<Object>
  {
    this.dappid = this.local.getDappId();
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/rejecteds/reasons/count',{},options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }
 
  async getAuthDepRanks(aid:string,limit:number,month:number,year:number):Promise<Object>
  {
    var data={
      aid: aid,
      limit: limit,
      month: month,
      year: year
    }
    this.dappid = this.local.getDappId();
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/query/authorizer/departments/ranks',data,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }
  


  private handleError(error: Response) 
  {
    console.error(error['message']);
    return Observable.throw(error['message']);
  }



}
