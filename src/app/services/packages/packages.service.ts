import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../../constants';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { RequestOptionsService } from '../../services/requestoptions/request-options.service';
import { count } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PackagesService {
  dappid:string;
RequestUrl:string;

  constructor(private http: Http, private local: LocalstorageService, private requestOptions: RequestOptionsService) { 
    this.RequestUrl=Constants.SERVERURL;            
  }
  async getPackageList():Promise<object>
  {
    this.dappid = this.local.getDappId();    
    var data={}
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'getpackages',data,options).toPromise();
    // const res=await this.http.post('http://192.168.0.181:8080/getpackages',data,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }

  async getCouponList(asset:string,code:string):Promise<object>
  {
    var data={
      asset:asset,
      country:code
    }
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'couponsonly',data,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/couponsonly',data,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }

  async getFinalPrice(packagename:string,couponcode:string,countrycode:string):Promise<object>
  {
    var data={
      pid:packagename,
    cid:couponcode,
      country:countrycode,
      type:"certificate"
    }
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'getprice',data,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/getprice',data,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }
  
  async getPaymentGateway(currency:string):Promise<object>
  {
    var data={
      cc:currency
    }
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'pg',data,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/pg',data,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }


  async sendbody(data:{}):Promise<object>
  {

    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'bodysend',data,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/bodysend',data,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }

  async getdata(id:string):Promise<object>
  {

    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.get(this.RequestUrl + 'getdata/'+id,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/getdata/'+id,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }


  async sendMoney(countrycode:string,address:string):Promise<object>
  {

    var params={
      amount:2,
      countrycode:countrycode,
      address:address
      }
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'onlybel',params,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/onlybel',params,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }

  async savePackage(selectedpackage:string,email:string):Promise<object>
  {
    var params={
      id:selectedpackage,
  email:email
      }

    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'apply',params,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/apply',params,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }



  async payUsingPaypal(price:string,currency:string,responseurl:string):Promise<object>
  {
    var params={
      cost:price,
      currency:currency,
      url:responseurl
      }

    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'paywithpaypal',params,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/paywithpaypal',params,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }
  async getsignature(currency:string,amount:string):Promise<object>
  {
    var data={
      MerchantKey:"mGgn9LrAtE",
      MerchantCode:"M03226",
      RefNo:"A00000001",
      currency:currency,
      amount:amount
    }
    const options=this.requestOptions.getDappRequestOptions();
    try{
    const res=await this.http.post(this.RequestUrl + 'ipay',data,options).toPromise();
    //  const res=await this.http.post('http://192.168.0.181:8080/ipay',data,options).toPromise();

    return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;

    }
  }
  private handleError(error: Response) 
  {
    console.error(error['message']);
    return Observable.throw(error['message']);
  }

}
