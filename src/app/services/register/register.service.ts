import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs';
import { Constants } from '../../constants';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { RequestOptionsService } from '../requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  regUrl: string;

  constructor(private http: Http, private requestOptions: RequestOptionsService) 
  { 
    this.regUrl=Constants.HOME_URL + 'user/signup';
  }

  async onReg(data:{}): Promise<Object>
  {          
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const resp= await this.http.post(this.regUrl,data,options).toPromise();
      return resp.json();   
    }    
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }
  
  private handleError(error:Response)
  {
    console.error(error);
    return Observable.throw(error.json().message)
  }

}