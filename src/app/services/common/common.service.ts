import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../../constants';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { RequestOptionsService } from '../requestoptions/request-options.service'

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  KYCLoginUrl:string;
  getAddssUrl:string;
  getBalUrl:string;
  getRoleUrl:string;
  getCountriesUrl:string;
  token:string;
  bkvsdmToken:string;  
  dappid:string;
  dappRequestUrl:string;

  constructor(private http: Http, private local:LocalstorageService,private requestOptions:RequestOptionsService) 
  {  
    this.KYCLoginUrl= Constants.SWAGGERAPI+'user/countries/kyc';
    this.getCountriesUrl=Constants.SWAGGERAPI+'countries';
    this.getAddssUrl=Constants.HOME_URL + 'user/wallet';
    this.getBalUrl=Constants.HOME_URL + 'user/balance';
    this.getRoleUrl=Constants.HOME_URL + 'user/dappid';  
    this.dappRequestUrl=Constants.HOST_URL;    
   }

  async onCheckYCStatus(token:string):Promise<object>
  {        
    const options = this.requestOptions.getRequestOptions(token);
    try
    {
      const res= await this.http.get(this.KYCLoginUrl,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async getCountries():Promise<object>
  {
    const options=this.requestOptions.getDappRequestOptions();
    try
    {
      const res=await this.http.get(this.getCountriesUrl,options).toPromise();   
      let body=res['_body'];             
      let country1=JSON.parse(body);
      let countries=country1['data'];      
      return countries;     
    }
    catch(error)
    {
      if(error)
      {
        var err=JSON.parse(error._body);
        await this.handleError(err);
        throw err;
      }
      console.log("OOPS THERE IS A PROBLEM");
    }
  }

  async getDepartmentsApi():Promise<object>
  {
    this.dappid = this.local.getDappId();
    try
    {
      const res=await this.http.post(this.dappRequestUrl + this.dappid + '/department/get',{}).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);          
      await this.handleError(err);
      throw err;
    }
  }

  async ongetAddress():Promise<object>
  {
    let token=this.local.getBelriumToken();
    var params=
      {
        token:token
      };       
    const options = this.requestOptions.getDappRequestOptions();
    try{
      const res= await this.http.post(this.getAddssUrl,params,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }
    
  async ongetBal(address,token):Promise<object>
  {
    var param=
      {
        address:address,
        belriumtoken:token
      };        
      const options = this.requestOptions.getDappRequestOptions();
      try
      {
        const res= await this.http.post(this.getBalUrl,param,options).toPromise();
        // var data=JSON.parse(getBalRes['data']);
        // var bal=(data.balance)/10000000000;
        return res.json();
      }
      catch(error)
      {
        var err=JSON.parse(error._body);
        await this.handleError(err);
        throw err;
      }
  }

  async oncheckRole():Promise<object>
  {
    let email=this.local.getEmail();
    var params=
      {
        email:email
      };  
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res= await this.http.post(Constants.HOME_URL+ 'user/dappid2',params,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async ongetIssuer():Promise<object>
  {
    this.dappid=this.local.getDappId();
    let email=this.local.getEmail();
    var params=
      {
        email:email
      };    
      const options = this.requestOptions.getDappRequestOptions();
      try
      {
        const res= await this.http.post(this.dappRequestUrl + this.dappid + '/issuers/getId',params,options).toPromise();
        return res.json();
      }
      catch(error)
      {
        var err=JSON.parse(error._body);
        await this.handleError(error._body);
        throw err;
      }
  }

  async oncheckIssuerData(iid):Promise<object>
  {
    this.dappid = this.local.getDappId();
    var param=
    {
      iid:iid
    }; 
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res= await this.http.post(this.dappRequestUrl + this.dappid + '/issuer/data',JSON.stringify(param),options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async ongetauth():Promise<object>
  {
    this.dappid = this.local.getDappId();
    let email=this.local.getEmail();
    var params=
      {
        email:email
      };             
      const options = this.requestOptions.getDappRequestOptions();
      try
      {
        const res= await this.http.post(this.dappRequestUrl + this.dappid + '/authorizers/getId',params,options).toPromise();
        return res.json();
      }
      catch(error)
      {
        if(error)
        {
          var err=JSON.parse(error._body);
          await this.handleError(err);
          throw err;
        }
      }
  }

  async getCertDetails(certid:number):Promise<Object>    
  {
    this.dappid = this.local.getDappId();
    var body=
      {
        pid: certid
      }
    this.dappid=this.local.getDappId();
    const options=this.requestOptions.getDappRequestOptions();
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/payslip/statistic2',body,options).toPromise();
    return res.json();
  }

  async ongetauthdata(aid):Promise<object>
  {
    this.dappid = this.local.getDappId();
    var params=
      {
        aid:aid
      }
      const options = this.requestOptions.getDappRequestOptions();
      try
      {
        const res= await this.http.post(this.dappRequestUrl + this.dappid + '/authorizer/data',params,options).toPromise()
        return res.json();
      }
      catch(error)
      {
        var err=JSON.parse(error._body);
        await this.handleError(err);
        throw err;
      }
  } 

  async userregisterd(params):Promise<object>
  {
    this.dappid = this.local.getDappId();
    const options=this.requestOptions.getDappRequestOptions();
    try
    {
      const res= await this.http.post(this.dappRequestUrl + this.dappid + '/receipient/email/exists',params,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  private handleError(error: Response)
  {
    console.error(error['message']);
    return Observable.throw(error['message']);
  }  
}

