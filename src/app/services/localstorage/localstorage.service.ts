import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

  public setEmail(email)
  {
    localStorage.setItem('email',email);
  }

  public getEmail()
  {
    return localStorage.getItem('email');
  }

  public setLoggedIn(login)
  {
    localStorage.setItem('isLoggedIn',login);
  }

  public getLoggedIn()
  {
    return localStorage.getItem('isLoggedIn');
  }

  // sets belrium token
  public setBelriumToken(token){
		localStorage.setItem('token',token);
	}

	//gets belrium token
	public getBelriumToken(){
		return localStorage.getItem('token');
  }
  
  // sets bkvs-dm token
  public setBKVSToken(bkvstoken){
		localStorage.setItem('bkvs-token',bkvstoken);
	}

	//gets bkvs-dm token
	public getBKVSToken(){
		return localStorage.getItem('bkvs-token');
  }
  
   // sets role of an user
   public setUserRole(role){
		localStorage.setItem('role',role);
	}

	//gets role of an user
	public getUserRole(){
		return localStorage.getItem('role');
  }
  
    // sets wallet balance
    public setBalance(balance){
      localStorage.setItem('balance',balance);
    }
  
    //gets wallet balance
    public getBalance(){
      return localStorage.getItem('balance');
    }

    // sets country of an user
    public setCountryName(countryName){
      localStorage.setItem('countryName',countryName);
    }
  
    //gets country of an user
    public getCountryName(){
      return localStorage.getItem('countryName');
    }

    //sets dapp id for the user
    public setDappId(dappid)
    {
      localStorage.setItem('dappid',dappid);
    }

    //gets dapp id for the suer
    public getDappId()
    {
      return localStorage.getItem('dappid');
    }

    public setKycStatus(kycstatus)
    {    
      localStorage.setItem('kycstatus',JSON.stringify(kycstatus));
    }

    public getKycStatus()
    {      
      return Boolean(JSON.parse(localStorage.getItem('kycstatus')))      
    }

    public setWalletAddress(address)
    {
      localStorage.setItem('address',address);
    }

    public getWalletAddress()
    {
      return localStorage.getItem('address');
    }

    public setOrganization(organization)
    {
      localStorage.setItem('organization',organization);
    }

    public getOrganization()
    {
      return localStorage.getItem('organization');
    }

    public setUserName(name)
    {
      localStorage.setItem('name',name);
    }

    public getUserName()
    {
      return localStorage.getItem('name');
    }

    public setIssuerId(id)
    {      
      localStorage.setItem('issuerid',id);
    }

    public getIssuerId()
    {    
      return localStorage.getItem('issuerid');
    }

    public setIssuerDept(dept)
    {
      localStorage.setItem('issuerDept',dept);
    }

    public getIssuerDept()
    {
      return localStorage.getItem('issuerDept');
    }

    public setAuthorizerId(id)
    {
      localStorage.setItem('Authorizerid',id);
    }

    public getAuthorizerId()
    {
      return localStorage.getItem('Authorizerid');
    }

    public setAuthorizerDept(dept){
      return localStorage.setItem('authDept',dept);
    }
    public getAuthorizerDept(){
      return localStorage.getItem('authDept');
    }

    public setPackage(packagename){
      return localStorage.setItem('package',packagename);
    }
    public getPackage(){
      return localStorage.getItem('package');
    }
}
