import { Injectable } from '@angular/core';
import { RequestOptionsService } from '../../services/requestoptions/request-options.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { Constants } from 'src/app/constants';
import { Observable } from 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  dappRequestUrl:string;  
  dappid:string;
  
  constructor(private http : Http,private requestOptions: RequestOptionsService,private local:LocalstorageService) 
  {     
    this.dappRequestUrl = Constants.HOST_URL;    
  }

  async definecustomfields(data:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    const options=this.requestOptions.getDappRequestOptions();
    try
    {
      const res=await this.http.post(this.dappRequestUrl + this.dappid + '/customFields/define',data,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async getcustomfields():Promise<Object>
  {
    this.dappid = this.local.getDappId();
    const options=this.requestOptions.getDappRequestOptions();
    try
    {
      const res=await this.http.post(this.dappRequestUrl + this.dappid + '/customFields/get',options).toPromise()
      return res.json();
    }
     catch(error)
     {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

 async assigndepartments(data):Promise<Object>
 {
  this.dappid = this.local.getDappId();
  const options=this.requestOptions.getDappRequestOptions(); 
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/department/assignAuthorizers',data,options).toPromise()
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }  
 }

 async getdepwithcount():Promise<Object>
 {
  this.dappid = this.local.getDappId();
  var data={};
  const options=this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/department/get',data,options).toPromise()
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
 }

 async authdep(data:{}):Promise<Object>
 {
  this.dappid = this.local.getDappId();
  const options=this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/getDepartment/authorizers',data,options).toPromise()
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
 }



 async setServiceFee(data:{}):Promise<Object>
 {
  const options=this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(Constants.HOME_URL + '/setVerificationFee',data,options).toPromise()
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
 }


 async getServiceFee(data:{}):Promise<Object>
 {
  const options=this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(Constants.HOME_URL + '/getVerificationFee',data,options).toPromise()
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
 }
 private handleError(error: Response) 
  {
    console.error(error['message']);
    return Observable.throw(error['message']);
  }
}



