import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../../constants';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { RequestOptionsService } from '../../services/requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'
})
export class SuperadminService {
  dappid:string;
  dappRequestUrl:string;
  getCountriesUrl : string;
  pendingIssuesUrl:string;
  addUserUrl:string;
  pendingAuthorizationUrl:string;
  rejectedIssuesUrl:string;
  constructor(private http: Http,private requestOptions: RequestOptionsService, private local:LocalstorageService) 
  { 
    this.dappRequestUrl=Constants.HOST_URL;
    this.getCountriesUrl = Constants.SWAGGERAPI + 'countries';
    this.addUserUrl = Constants.HOST_URL + this.dappid + '/registerUser/';
    this.pendingIssuesUrl=Constants.HOST_URL+this.dappid+'/query/superuser/statistic/pendingIssues2';
    this.pendingAuthorizationUrl=Constants.HOST_URL+this.dappid+'/query/superuser/statistic/pendingAuthorization2';
    this.rejectedIssuesUrl=Constants.HOST_URL+this.dappid+'/query/superuser/statistic/rejectedIssues2'
  }
  
  async getDepartments():Promise<Object>
  {    
    this.dappid = this.local.getDappId();
    const options = this.requestOptions.getDappRequestOptions();
    const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/department/get',options).toPromise();
    return res.json;
  }

async getpendingauthresult(limit,offset,dep):Promise<Object>
{
  var data={
    limit:limit,
    offset:offset,
    department:dep
  };
  const options=this.requestOptions.getDappRequestOptions();
  try{
  const res=await this.http.post(this.dappRequestUrl+this.dappid+'/query/superuser/statistic/pendingAuthorization2',data,options).toPromise()
  return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
}
async getpendingissueresult(limit,offset,dep):Promise<Object>
{
  var data={
    limit:limit,
    offset:offset,
    department:dep
  }
 const options=this.requestOptions.getDappRequestOptions();
 try{
 const res=await this.http.post(this.dappRequestUrl + this.dappid+'/query/superuser/statistic/pendingIssues2',data,options).toPromise()
 return res.json();
 }
 catch(error){
  var err=JSON.parse(error._body);  
  await this.handleError(err);
  throw err;
}
}

async getcertificaterejresult(limit,offset,dep):Promise<Object>
{
  var data={
    limit:limit,
    offset:offset,
    department:dep
  }
 const options=this.requestOptions.getDappRequestOptions();
 try{
 const res=await this.http.post(this.dappRequestUrl + this.dappid+'/query/superuser/statistic/rejectedIssues2',data,options).toPromise()
 return res.json();
 }
 catch(error){
  var err=JSON.parse(error._body);
  await this.handleError(err);
  throw err;
}
}

async getIssuedPayslips(iid:string,limit:number,offset:number):Promise<object>
{
  this.dappid = this.local.getDappId();
  var data=
  {
    iid: iid,
    limit: limit,
    offset: offset
  };
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/issuer/data/issuedPayslips',data,options).toPromise();
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async getSignedPayslips(aid:string,limit:number,offset:number):Promise<Object>
{
  this.dappid = this.local.getDappId();
  var data={
    aid: aid,
    limit: limit,
    offset: offset
  };  
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/authorizer/signedPayslips',data,options).toPromise();
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async getIssuers(limit:number,offset:number,filterdep:string):Promise<Object>
{
  this.dappid = this.local.getDappId();
  var data={
    limit: limit,
    offset: offset,
    department: filterdep
  };
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/issuers',data,options).toPromise();
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async getAuthorizers(limit:number,offset:number,filterdep:string):Promise<Object>
{
  this.dappid = this.local.getDappId();
  var data=
  {
    limit: limit,
    offset: offset,
    department: filterdep
  };
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/authorizers',data,options).toPromise();
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async removeIssuer(iid:string):Promise<Object>
{
  this.dappid = this.local.getDappId();
  var data={
    iid: iid
  };  
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/issuers/remove',data,options).toPromise();
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async removeAuthorizer(aid:string):Promise<Object>
{
  this.dappid = this.local.getDappId();
  var data={
    aid: aid
  };
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res = await this.http.post(this.dappRequestUrl + this.dappid + '/authorizers/remove',data,options).toPromise();
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async addUser(data:string):Promise<Object>
{
  this.dappid = this.local.getDappId();
  const options = this.requestOptions.getDappRequestOptions();
  try{
    const res= await this.http.post(this.dappRequestUrl + this.dappid + '/registerUser/',JSON.stringify(data),options).toPromise()
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);  
    await this.handleError(err);
    throw err;
  }
}

async addAuthorizer(data:string):Promise<Object>
{
  this.dappid = this.local.getDappId();
  const options = this.requestOptions.getDappRequestOptions();
  try{
    const res= await this.http.post(this.dappRequestUrl + this.dappid + '/registerUser/',JSON.stringify(data),options).toPromise()
    return res.json();
  }
  catch(error){
    var err=JSON.parse(error._body);    
    await this.handleError(err);
    throw err;
  }
}

private handleError(error: Response) {
  console.error(error['message']);
 return Observable.throw(error['message']);
}
}
