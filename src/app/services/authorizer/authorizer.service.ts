import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
 import { Http, Response } from '@angular/http';
 import 'rxjs/add/operator/map';
 import 'rxjs/add/observable/throw';
 import 'rxjs/add/operator/timeoutWith';
 import 'rxjs/add/operator/timeout';
 import 'rxjs/add/operator/toPromise';
 import { Constants } from '../../constants';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { RequestOptionsService } from '../requestoptions/request-options.service';

//displaypayslipdata pop up API is pending. It has to be added in common

@Injectable({
  providedIn: 'root'
})
export class AuthorizerService {
  
  dappid:string;
  dappRequestUrl: string;
  penidngSignsUrl : string;
  authorizeUrl : string;
  rejectPayslipUrl : string;
  authorizedPayslipsUrl : string;
  rejectedPayslipsUrl : string;
  rejectReasonsUrl:string;

  constructor(private http: Http, private requestOptions: RequestOptionsService,private local: LocalstorageService) 
  { 
    this.dappRequestUrl = Constants.HOST_URL;    
  }

  async pendingSigns(aid:string,limit:number,offset:number,dep:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var params=
    {
      aid: aid,
      limit: limit,
      offset: offset,
      department:dep
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/query/authorizers/pendingSigns',params,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }
  }

  async authorize(pid:string,aid:string,passphrase:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var params=
    {
      pid: pid,
      aid: aid,
      secret: passphrase
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/authorizer/authorize',params,options).toPromise()    
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }   
  }

  async bulkAuthorization(pid:any[],aid:string,passphrase:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var params=
    {
      pids: pid,
      aid: aid,
      secret: passphrase
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/authorizer/authorizeMultiple',params,options).toPromise()    
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }   
  }




  async rejectPayslip(pid:string,aid:string,comments:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var params=
    {
      pid: pid,
      aid: aid,
      message: comments
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/authorizer/reject',params,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }    
  }

  async rejectMultiplePayslips(pid:string[],aid:string,comments:string):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    var params=
    {
      pid: pid,
      aid: aid,
      message: comments
    };
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/authorizer/rejectMultiple',params,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }    
  }

  async authorizedPayslips(aid:string,limit:number,offset:number,dep:string):Promise<object>
  {
    this.dappid = this.local.getDappId();
    let params={
      aid: aid,
      limit:limit,
      offset:offset,
      department:dep

    }
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/query/authorizer/statistic/signedIssues',params,options).toPromise();
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }    
  }

  async rejectedPayslips(aid:string,limit:number,offset:number,dep):Promise<Object>
  {
    this.dappid = this.local.getDappId();
    let params={
      aid:aid,
      limit:limit,
      offset:offset,
      department:dep
    }
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/query/authorizer/statistic/rejectedIssues',params,options).toPromise();    
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }    
  }

  async getRejectReasons():Promise<Object>
  {    
    this.dappid = this.local.getDappId();
    const options = this.requestOptions.getDappRequestOptions();
    try
    {
      const res = await this.http.post(this.dappRequestUrl+ this.dappid + '/issues/rejected/reasons',{},options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);    
      await this.handleError(err);
      throw err;
    }
  }

  private handleError(error: Response) {
    console.error(error['message']);
   return Observable.throw(error['message']);
  }
}

