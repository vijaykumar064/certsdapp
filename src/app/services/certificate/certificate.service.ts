import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
 import 'rxjs/add/observable/throw';
 import 'rxjs/add/operator/timeoutWith';
 import 'rxjs/add/operator/timeout';
 import 'rxjs/add/operator/toPromise';
 import { Constants } from '../../constants';
import { RequestOptionsService } from '../requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'
})
export class CertificateService {
  dappid=localStorage.getItem('dappid');
  PayslipmonthStatsUrl:string;
  EmployeeDataUrl:string;
  payslipstatisticUrl:string;
  PayslipInitialissueUrl:string;
  DepartmentUrl:string;
  EmpDesignationsUrl:string;
  getbanksUrl:string;
  registeremployeeUrl:string;
  issueTransactionUrl:string;
  customfieldsUrl:string;
  EmployeeDesignationsUrl:string;
  assetDataUrl: string;
  getIssuersUrl : string;
  getAuthorizersUrl : string;




  constructor(private http: Http,private requestOptions:RequestOptionsService) { 
    this. PayslipmonthStatsUrl=Constants.HOST_URL+this.dappid+"/employee/payslip/month/status";
    this.EmployeeDataUrl=Constants.HOST_URL+this.dappid+"/employeeData";
    this.payslipstatisticUrl=Constants.HOST_URL+this.dappid+"/payslip/statistic";
    this.PayslipInitialissueUrl=Constants.HOST_URL+this.dappid+"/payslip/initialIssue";
    this.DepartmentUrl=Constants.HOST_URL+this.dappid+"/department/get";
    this.EmpDesignationsUrl=Constants.HOST_URL+this.dappid+"/employees/getDesignations";
    this.getbanksUrl=Constants.HOST_URL+this.dappid+"/getBanks";
    this.registeremployeeUrl=Constants.HOST_URL+this.dappid+"/registerEmployee";
    this.issueTransactionUrl=Constants.HOST_URL+this.dappid+"/issueTransactionCall";
    this.customfieldsUrl=Constants.HOST_URL+this.dappid+"/customFields/get";
    this.EmployeeDesignationsUrl=Constants.HOST_URL+this.dappid+"/employees/getDesignations";
    this.assetDataUrl =Constants.HOST_URL+this.dappid+"/query/department/assets";
    this.getIssuersUrl = Constants.HOST_URL + this.dappid + '/issuers';
    this.getAuthorizersUrl = Constants.HOST_URL + this.dappid + '/authorizers';
  }
  getBlockchaindata(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.PayslipmonthStatsUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }
  getEmployeeDetails(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.EmployeeDataUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }

 async payslipstatistic(certId){
    var data={
      "pid": certId
    }
    const options=this.requestOptions.getDappRequestOptions();
    const res = await this.http.post(this.payslipstatisticUrl,data,options).toPromise();
    console.log("viewdetails",res.json());
    return res.json();    
  }

  async PayslipInitialissue(data:{}):Promise<Object>
  {
    const options=this.requestOptions.getDappRequestOptions();
    const res= await this.http.post(this.PayslipInitialissueUrl,data,options).toPromise();
    return res.json();
  }


async getIssuers(limit,offset,filterdep) :Promise<Object>
{
  var data={
    limit: limit,
    offset: offset,
    department: filterdep
  };

  const options=this.requestOptions.getDappRequestOptions();
  const res = await this.http.post(this.getIssuersUrl,data,options).toPromise();
  console.log("issuedata",res.json());
  return res.json();
}

async getAuthorizers(limit,offset,filterdep):Promise<Object>
{
  var data={
    limit: limit,
    offset: offset,
    department: filterdep
  };

  const options=this.requestOptions.getDappRequestOptions();
  const res = await this.http.post(this.getAuthorizersUrl,data,options).toPromise();
  console.log("authdata",res.json());
  return res.json();
}

  async getCertifcates(certSearchData):Promise<Object>{
    console.log("searchCertdata"+ JSON.stringify(certSearchData));
    var data={
      "did": certSearchData['did'],
       "limit": certSearchData['limit'],    
       "offset": certSearchData['offset'],  
       "iid": certSearchData['iid'],  
      "aid": certSearchData['authorizer'],    
      "status": certSearchData['status'],    
      "year": certSearchData['year'],    
      "month": certSearchData['month'],

    }
    console.log("asseturl"+ JSON.stringify(data));
    // const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    // const options=new RequestOptions({headers:headers,method:'post'});
    const options=this.requestOptions.getDappRequestOptions();
    const res = await this.http.post(this.assetDataUrl,data,options).toPromise();
    console.log("certdata",res.json());
    return res.json();
  }

   async Department():Promise<Object>{
    var data={
      "limit": 5,    
      "offset": 0   
    
    }
    console.log("depurl"+ this.DepartmentUrl);
    try{
      const options=this.requestOptions.getDappRequestOptions();
      const res = await this.http.post(this.DepartmentUrl,data,options).toPromise();
      console.log("depdata" + res);
      return res.json();  
    }
    catch(error)
    {      
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }   
    
  }
  EmpDesignations(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.EmpDesignationsUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }
  getbanks(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.getbanksUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }
  async registeremployee(data:{}):Promise<Object>
  {
    const options=this.requestOptions.getDappRequestOptions();
    const res= await this.http.post(this.registeremployeeUrl,data,options).toPromise();
    return res.json();
  }
  issueTransaction(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.issueTransactionUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }
  customfields(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.customfieldsUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }
  EmployeeDesignations(){
    var data={}
    const headers=new Headers({'content-Type':'application/json','magic':'594fe0f3'});
    const options=new RequestOptions({headers:headers,method:'post'});
    return this.http.post(this.EmployeeDesignationsUrl,data,options).timeout(1000)
    .map((res:Response)=>{
      console.log("data",res.json());
      return res.json();
    })
    .catch(this.handleError);
  }
  private handleError(error: Response) {
    console.error(error);
   return Observable.throw(error.json().message);
  }
}
