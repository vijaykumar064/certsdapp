import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
 import { Http, Response } from '@angular/http';
 import 'rxjs/add/operator/map';
 import 'rxjs/add/observable/throw';
 import 'rxjs/add/operator/timeoutWith';
 import 'rxjs/add/operator/timeout';
 import 'rxjs/add/operator/toPromise';
 import { Constants } from '../../constants';
 import { RequestOptionsService } from '../../services/requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  existUrl: string;
  url: string;
  hyperledgerLogin:string;

  constructor(private http: Http,private requestOptions:RequestOptionsService) 
  {
    this.existUrl = Constants.HOME_URL + 'user/exists';
    this.url = Constants.HOME_URL + 'user/login/';
    this.hyperledgerLogin=Constants.HOME_URL + 'user/hllogin/';
  }
  
  userExist(email){
    const body = 
    {
        email: email          
    };        
    const options=this.requestOptions.getDappRequestOptions();
    return this.http.post(this.existUrl, body, options).timeout(300000)
        .map((res: Response) => {
            console.log("data",res.json());
            return res.json();
        })
        .catch(this.handleError);
    }

    async doLogin(data: {}): Promise<Object>
    {           
        const options=this.requestOptions.getDappRequestOptions();
        try
        {
            const res= await this.http.post(this.url,data,options).toPromise()
            return res.json();
        }
        catch(error)
        {
            if(error)
            {
                var err=JSON.parse(error._body);
                await this.handleError(err);
                throw err;
            }
            else
            {
                console.log("OOPS! FAILED TO PROCESS YOUR REQUEST");
            }
        }
    }

    async onCheckHyperLedger(secret:string,token:string):Promise<object>
    {
        var data=
        {
            secret: secret,
            token:token
        }
        const options=this.requestOptions.getDappRequestOptions();
        try
        {
            const res= await this.http.post(this.hyperledgerLogin,data,options).toPromise()
            return res.json();
        }
        catch(error)
        {
            if(error)
            {
                var err=JSON.parse(error._body);
                await this.handleError(err);
                throw err;
            }
            else
            {
                console.log("OOPS! FAILED TO PROCESS YOUR REQUEST");
            }
        }
    }
      
	private handleError(error: Response) {
         console.error(error['message']);
        return Observable.throw(error['message']);
    }
  }
