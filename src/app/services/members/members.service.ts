import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../../constants';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { RequestOptionsService } from '../requestoptions/request-options.service'

@Injectable({
  providedIn: 'root'
})
export class MembersService {
  dappRequestUrl: string;
  dappid:string;

  constructor(private http: Http, private local:LocalstorageService,private requestOptions:RequestOptionsService)
  { 
    this.dappRequestUrl=Constants.HOST_URL;  
  }

  async getAllMembers(limit,offset):Promise<object>
  {
    this.dappid = this.local.getDappId();
    var data=
    {
      limit: limit,
      offset: offset
    };
    const options=this.requestOptions.getDappRequestOptions();
    try
    {
      const res= await this.http.post(this.dappRequestUrl + this.dappid + '/query/employees2',data,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  async searchMembers(text:string,name:string,limit:number,offset:number):Promise<object>
  {
    this.dappid = this.local.getDappId();    
    var data=
    {
      text: text,
      searchBy:name,
      limit:limit,
      offset:offset
    };
    const options=this.requestOptions.getDappRequestOptions();
    try
    {
      const res= await this.http.post(this.dappRequestUrl + this.dappid + '/searchEmployee',data,options).toPromise()
      return res.json();
    }
    catch(error)
    {
      var err=JSON.parse(error._body);
      await this.handleError(err);
      throw err;
    }
  }

  private handleError(error: Response)
  {
    console.error(error['message']);
    return Observable.throw(error['message']);
  }  
}
