import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn ,FormGroup} from '@angular/forms';
import { AppModule } from 'src/app/app.module';
import { AotCompiler } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  static cpasswordvalidate(regForm: FormGroup) {
    let password = regForm.controls.password.value;
    let confirmpassword = regForm.controls.confirmpassword.value;

    if (confirmpassword.length <= 0) {
        return null;
    }

    if (confirmpassword !== password) {
        return {
            doesMatchPassword: true
        };
    }

    return null;

}

static MatchPassword(pass: AbstractControl){
  
   // alert("formGroup "+ controlName + ":" + matchingControlName);
    const password = pass.get('password').value;
    const confirmpassword = pass.get('confirmpassword').value;
    // alert("formGroup "+ password + ":" + confirmpassword);
    if(password != confirmpassword){
      pass.get('confirmpassword').setErrors({MatchPassword: true})
    }else{
      return null;
    }

  }
static match(controlName: string, matchingControlName: string){
  return (formGroup: FormGroup) => {
    alert("formGroup "+ controlName + ":" + matchingControlName);
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    // if(matchingControl.errors && !matchingControl.errors.invalidMatch){
    //   return;
    // }

    // if(control.value !== matchingControl.value){
    //   if(control.value != matchingControl.value){
    //     matchingControl.setErrors({invalidMatch: true});

    //   }else{
    //     matchingControl.setErrors(null);
    //   }
    // }
  }
}

static confirmPassword(control: AbstractControl, group: FormGroup, matchPassword: string) {
  alert("value"+group.controls[matchPassword].value);
  alert("value1"+control.value);
  if (!control.value || group.controls[matchPassword].value !== null || group.controls[matchPassword].value === control.value) {
      return null;
  }
  return { 'mismatch': true }
}

static emailValidator1(control: AbstractControl) {

  if (control && (control.value !== null || control.value !== undefined)) {
      const regex = new RegExp(/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/);

      if (!regex.test(control.value)) {
          return {
              errors: true
          };
      }
  }

  return null;
}

static departnameValidator(control:AbstractControl)
{
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp(/^[a-zA-Z]*$/);

    if (!regex.test(control.value)) {
        return {
            isError: true
        };
    }
}
return null;
}

static Idvalidator(control:AbstractControl)
{

  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp(/^[0-9]*$/);

    if (!regex.test(control.value)) {
        return {
            isError: true
        };
    }
}
return null;

}


static yearValidator(control:AbstractControl)
{
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp(/^[0-9]{4}$/);

    if (!regex.test(control.value)) {
        return {
            isError: true
        };
    }
}
return null;
}




static countryvalidate(control:AbstractControl){
const country:string=control.value;
if(country === '' || country === 'Select country'){
  return {
    isError: true
};
}
return null;
}

static passwordvalidate(control:AbstractControl){
  const password:string=control.value;
  if (control && (control.value !== null || control.value !== undefined)) {
  const regex= new RegExp(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,14}$/);
  if(!regex.test(control.value)){
    return {
      isError: true
  };
  }
return null;
}
}









static emailvalidator(control:AbstractControl)
{
  const regex = new RegExp(/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/);

const email:string = control.value;

if(email ===''|| !regex.test(control.value) )
{
return { emailerror:true}
}  
else{
  return null;
}
}



static aadharcardValidator(control:AbstractControl)
{
  if (control && (control.value !== null || control.value !== undefined)) {
    const regex = new RegExp(/^(\d{12}|\d{16})$/);

    if (!regex.test(control.value)) {
        return {
            isError: true
        };
    }
}
return null;
}

// static emailValidator(control)
//  {
//   // RFC 2822 compliant regex
//   if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
//     return null;
//   } else {
//     return { 'invalidEmailAddress': true };
//   }
// }
static passwordValidator(control) {
  // {6,100}           - Assert password is between 6 and 100 characters
  // (?=.*[0-9])       - Assert a string has at least one number
  if (control.value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,14}$/)) {
    return null;
  } else {
    return { 'invalidPassword': true };
  }
}

}