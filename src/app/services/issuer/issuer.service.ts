import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/timeoutWith';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';
import { Constants } from '../../constants';
import { LocalstorageService } from '../../services/localstorage/localstorage.service';
import { RequestOptionsService } from '../../services/requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'
})
export class IssuerService {
  dappid:string;
  dappRequestUrl:string;
  ConfirmedPayslipUrl:string;
  IssuerPayslipsUrl:string;
  rejectedCertsUrl:string;
  finalIssueUrl:string;
  constructor(private http:Http, private requestOptions:RequestOptionsService,private local: LocalstorageService) 
  {
    this.dappRequestUrl=Constants.HOST_URL;
    this.dappid=this.local.getDappId();
    this.ConfirmedPayslipUrl=Constants.HOST_URL+this.dappid+'/query/issuer/statistic/pendingIssues';
    this.IssuerPayslipsUrl=Constants.HOST_URL+this.dappid+'/query/issuer/statistic/issuedIssues';
    this.rejectedCertsUrl=Constants.HOST_URL+this.dappid+'/query/issuer/statistic/rejectedIssues';
    this.finalIssueUrl=Constants.HOST_URL+this.dappid+'/issueTransactionCall';
  }

async pendingissues(iid:string,limit:number,offset:number,dep:string):Promise<Object>
{
  var data={

	limit:limit,
	offset:offset,
	department:dep,
    iid:iid
  }
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/query/issuer/statistic/pendingIssues',data,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);      
    await this.handleError(err);
    throw err;
  }
}

async totalIssuerpayslips(iid:string,limit:number,offset:number,dep:string):Promise<Object>
{
  var data={
   
	limit:limit,
	offset:offset,
	department:dep,
    iid:iid
  }
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/query/issuer/statistic/issuedIssues',data,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
}

async rejectedCerts(iid:string,limit:number,offset:number,dep:string):Promise<Object>
{
  var data={
   
	limit:limit,
	offset:offset,
	department:dep,
    iid:iid
  }
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/query/issuer/statistic/rejectedIssues',data,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);    
    await this.handleError(err);
    throw err;
  }
}

async finalIssue(data:{}):Promise<Object>
{
  this.dappid = this.local.getDappId();
  const options = this.requestOptions.getDappRequestOptions();
  try
  {
    const res=await this.http.post(this.dappRequestUrl + this.dappid + '/issueTransactionCall',data,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
}

async bulkfinalIssue(data:{}):Promise<Object>
{
  this.dappid=this.local.getDappId();
  const options=this.requestOptions.getDappRequestOptions();
  try{
    const res=await this.http.post(this.dappRequestUrl+this.dappid+'/issueTransactionCallMultiple',data,options).toPromise();
    return res.json();

  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
}

private handleError(error: Response) {
  console.error(error['message']);
 return Observable.throw(error['message']);
}
}



