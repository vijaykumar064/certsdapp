import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
 import { Http, Response } from '@angular/http';
 import 'rxjs/add/operator/map';
 import 'rxjs/add/observable/throw';
 import 'rxjs/add/operator/timeoutWith';
 import 'rxjs/add/operator/timeout';
 import 'rxjs/add/operator/toPromise';
 import { Constants } from '../../constants';
import { LocalstorageService } from '../localstorage/localstorage.service';
import { RequestOptionsService } from '../requestoptions/request-options.service';

@Injectable({
  providedIn: 'root'
})
export class KycService {
  FetchKycDocUrl: string;
  MetaDataFormFieldsUrl:string;
  KycPaymentUrl:string;
  SendVerificationUrl:string;
  GetPublikeyUrl:string;
  EnableKycUrl:string;
  kycUploadUrl:string;
  token:string;
  bkvsdm_token:string;

  constructor(private http: Http,private local: LocalstorageService, private requestOptions: RequestOptionsService) { 
    this.FetchKycDocUrl = Constants.SWAGGERAPI + 'users/document';
    this.MetaDataFormFieldsUrl = Constants.SWAGGERAPI+'kycdocs/kycdocformfieldmetas';
    this.KycPaymentUrl = Constants.SWAGGERAPI+'kycuserdocuments/payment';
    this.SendVerificationUrl = Constants.SWAGGERAPI+'kycuserdocuments/send/verification';
    this.GetPublikeyUrl = 'https://node1.belrium.io/api/accounts/open';
    this.EnableKycUrl = Constants.SWAGGERAPI+'transactions/enable/account';    
    this.kycUploadUrl = Constants.SWAGGERAPI + 'kycuserdocuments/upload';
  }
  
 async fetchKycDocument(token:string):Promise<Object>
 {
    const options = this.requestOptions.getRequestOptions(token);
    try
    {
      const res = await this.http.get(this.FetchKycDocUrl,options).toPromise();
      console.log(res);
      return res.json();
    }
    catch(error)
    {     
      var err=JSON.parse(error._body);  
      await this.handleError(err);    
      throw err;
    }
  } 

async kycUplaodFile(formdata:FormData):Promise<Object>
{
  var token = this.local.getBelriumToken();
  var bkvsdmToken = this.local.getBKVSToken();
  const options = this.requestOptions.getRequestBKVSOptions(token,bkvsdmToken);
  try
  {
    const res = await this.http.post(this.kycUploadUrl,formdata,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);  
    await this.handleError(err);         
    throw err;
  }
}

async getMetaDataFormFields(metaData:[]):Promise<Object>
{
  var token = this.local.getBelriumToken();
  var countryCode=localStorage.getItem('countryCode');
  const options = this.requestOptions.getRequestOptions(token);
  try
  {
    const res = await this.http.get(this.MetaDataFormFieldsUrl +'?kycDocumentMetaId='+ metaData['kycDocumentMetaId'] + '&kycDocumentTypeId=' + metaData['kycDocumentTypeId'] + '&countryCode=' + metaData['countryCode'],options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);    
    await this.handleError(err);
    throw err;
  }
}

async kycPayment(payData:{}):Promise<Object>
{
  var token=this.local.getBelriumToken();
  var data={
    kycUserDocumentID: payData['kycUserDocumentID'],
    publicKey: payData['publicKey'],
    secondSecret:" ",
    secret: payData['passphrase'],
    senderCountryCode: payData['CountryCode']
  };  
  const options = this.requestOptions.getRequestOptions(token);
  try
  {
    const res = await this.http.put(this.KycPaymentUrl,data,options).timeout(1000).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);    
    await this.handleError(err);
    throw err;
  }
}

async sendVerification(kycUserDocumentID):Promise<Object>
{ 
  var data={};
  const token = this.local.getBelriumToken();
  const bkvsdmToken = this.local.getBKVSToken();
  const options = this.requestOptions.getRequestBKVSOptions(token,bkvsdmToken);
  try
  {
    const res = await this.http.post(this.SendVerificationUrl+'?kycUserDocumentID=' + kycUserDocumentID,data,options).timeout(1000).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);
    await this.handleError(err);
    throw err;
  }
}

async getPublicKey(passPhrase) {
  var token = this.local.getBelriumToken();
  var countryCode=this.local.getCountryName();
  var data={
    secret:passPhrase,
    countryCode:countryCode
  };
  const options = this.requestOptions.getRequestOptions(token);
  try
  {
    const res = await this.http.post(this.GetPublikeyUrl,data,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);    
    await this.handleError(err);
    throw err;
  }
}

async doEnableKYC(enableCriteria:{}):Promise<Object>
{
  var token = this.local.getBelriumToken();
  var data = {
      countryCode: enableCriteria['countryCode'],
      publicKey: enableCriteria['publicKey'],
      secondSecret: enableCriteria['secondSecret'],
      secret: enableCriteria['passphrase']
  };  
  const options = this.requestOptions.getRequestOptions(token);
  try
  {
    const res = await this.http.put(this.EnableKycUrl,data,options).toPromise();
    return res.json();
  }
  catch(error)
  {
    var err=JSON.parse(error._body);    
    await this.handleError(err);
    throw err;
  }
}

private handleError(error: Response) {
  console.error(error['message']);  
 return Observable.throw(error);
}
}
