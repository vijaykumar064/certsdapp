import { Injectable } from '@angular/core';
import { Headers }  from '@angular/http';
// import { HttpHeaders } from '@angular/common/http'

@Injectable()
export class RequestOptionsService {

  constructor() { 
  }

  public getDappRequestOptions(){
    const httpOptions = {
      headers: new Headers({
        'Content-Type': 'application/json',    
        'Accept':  'application/json',
   
      })
    };
   
    return httpOptions;
  }


  public paypalRequestOptions(){
    const httpsOptions={
      headers:new Headers({
        'Content-Type': 'application/json',    
        'Accept':  'application/json',
        // 'Access-Control-Allow-Origin':'*',
        // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        // 'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
        // 'Access-Control-Allow-Credentials': true

        "Access-Control-Allow-Origin": "https://www.sandbox.paypal.com",
        "Access-Control-Allow-Methods":"GET",
            "Access-Control-Allow-Credentials": true
      })
    };
    return httpsOptions;
  }

  public getRequestOptions(token){
    const httpOptions = {
      headers: new Headers({
        'Accept':  'application/json',
        'belrium-token': token
      })
    };
    return httpOptions;
  }

  public getRequestBKVSOptions(token, bkvsToken){    
    const httpOptions = {
      headers: new Headers({
        'Accept':  'application/json',
        'belrium-token': token,
        'bkvsdm-token': bkvsToken
      })
    };
    return httpOptions;
  }
}
