import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { LocalstorageService } from '../localstorage/localstorage.service';

@Injectable({
 providedIn: 'root'
})
export class AuthguardService implements CanActivate{

constructor(private router: Router,private local: LocalstorageService) { }

canActivate() 
{
   if (this.local.getLoggedIn()) 
   {
       return true;
   }
   this.router.navigate(['/home']);
   return false;
 }
}