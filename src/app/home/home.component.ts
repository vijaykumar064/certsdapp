import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../services/user/user.service';
import { LocalstorageService } from '../services/localstorage/localstorage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [ UserService, LocalstorageService ]
})
export class HomeComponent implements OnInit {

  userinfo;
	errorMessage: string;
	email: string;
	warning:string;

	constructor(private userService: UserService,private local:LocalstorageService,private router: Router) { 
	}

	// Check if user already logged in
	ngOnInit() {
  let text1 = ['Decentralized', 'Digital Certification'];
  var i = 0;
  var doit = true;
  var interval;
  var interval2;
  var scrollElement = document.getElementById('titleroll');
  function printit(printedText) {  
    var size = 0;
    function close() {
      clearInterval(interval);
    }
    interval = setInterval(() => {
      if (doit) {
        if (size == printedText.length) {
          doit = false;
          interval2 = setInterval(() => {
            if (scrollElement.innerText.length != 1) {
              scrollElement.innerText = scrollElement.innerText.slice(0, -1);
            } else {
              doit = true;
              caller();
              close();
              close1();
              return;
            }
            function close1() {
              clearInterval(interval2);
            }
          }
            , 200);
        } else {
          scrollElement.innerText += printedText[size];
        }
        size += 1;
      }
    }, 200);
  }
  function caller() {
    setTimeout(() => {
      if (i == text1.length) {
        i = 0;
      }
      printit(text1[i]);
      i += 1;

    }, 200);

  }
  caller();
	}

	// Initicate login
	checkExists(){		
		this.userService.userExist(this.email).subscribe(users => {
			if((users==='0')||(users==='00')){
				this.router.navigate(['/login/email/' + this.email]);
			}
			if(users==='-1'){
				this.router.navigate(['/register/email/'+this.email]);
			}
        },
            error => {
                this.errorMessage = <any>error;
                if (this.errorMessage === 'Invalid Username') {
					console.log(this.errorMessage);
                   
                } else {
					console.log(this.errorMessage);
                }
            });
	}
}
