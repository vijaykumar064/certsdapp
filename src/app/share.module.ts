import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DialogComponent } from './dialogs/dialog/dialog.component';
@NgModule({
    imports:[CommonModule, FormsModule], 
    declarations:[
        DialogComponent
    ],
    exports:[
        DialogComponent
    ]
})
export class ShareModule {}