import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetsSelectComponent } from './assets-select.component';

const routes: Routes = [
  {
    path: '', component: AssetsSelectComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssetSelectRoutingModule { }
