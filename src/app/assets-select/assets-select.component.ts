import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../services/common/common.service';
import { LocalstorageService } from '../services/localstorage/localstorage.service';

@Component({
  selector: 'app-assets-select',
  templateUrl: './assets-select.component.html',
  styleUrls: ['./assets-select.component.scss']
})
export class AssetsSelectComponent implements OnInit {
dappid:string;
role:string;
company:string;
name:string;
assettype:string;
assets:[];
useremail:string;
kycstatus:boolean;
ErrorModal:boolean=false;
alertHead:string='';
errorMessage:string='';
  constructor(private commonService:CommonService,private local:LocalstorageService,private router:Router) { }
  async ngOnInit() {
    try
    {
      await this.getRole();
    }
    catch(error)
    {
      this.openErrorModal(error['message'],"error message");      
    }   
  }
  async getRole() {
    try
    {
      const data = await this.commonService.oncheckRole();
      console.log(data['dapps']);
      this.assets=data['dapps'];
    }
    catch(error)
    {      
      throw error;
    }
  }

  async getdappid(dapp)
  {
    console.log(dapp)
    this.local.setOrganization(dapp["company"]);
    this.local.setUserName(dapp["name"]);
    this.local.setUserRole(dapp['role']);
    this.local.setDappId(dapp['dappid']);
    this.kycstatus=this.local.getKycStatus();
    this.useremail = this.local.getEmail();

    var role=dapp['role'];
    if (role === "new user") {
          console.log("wallet page");
          this.router.navigateByUrl('/wallet');
        }
    else if (role === "superuser") {
      if ((this.useremail != "cd_superuser@yopmail.com") && (this.kycstatus == false)) {            
            this.router.navigateByUrl('/wallet');    
      }
      else {
        console.log("superadmin page");
        this.router.navigateByUrl('/superadmin');
      }
    }
    else if (role === "issuer") {      
      try{
        const res = await this.commonService.ongetIssuer();
        console.log(res);        
        if(res['isSuccess'])
        { 
          let a = res["result"];
          let id = a.iid;           
          this.local.setIssuerId(id);
          const response = await this.commonService.oncheckIssuerData(id);
          console.log(response);
          let b = response["issuer"];
          console.log(b);          
          let isdept = b.departments;
          this.local.setIssuerDept(isdept);
          if ((this.kycstatus == false) && (this.useremail != "cd_issuer1@yopmail.com")) {    
            this.router.navigateByUrl('/wallet');
          }
          else {
            this.router.navigateByUrl('/issuer');
          }
        }
        else{
            this.openErrorModal(res['message'],'error message');            
        }
      }
      catch(error)
      {
        this.openErrorModal(error['message'],'error message');
      }
      }
      else if (role === "authorizer") {
        try
        {
        const res = await this.commonService.ongetauth();
        console.log(res);        
          if(res['isSuccess']){ 
            let a = res["result"];          
            let id = a.aid;
            console.log(id);
            this.local.setAuthorizerId(id);
            this.local.setAuthorizerDept(a.departments);
            if (this.kycstatus == false) {
              var flag='false';
              // if (this.useremail == "cd_auth1@yopmail.com") {
              //   this.router.navigateByUrl('/authorizer');
              // }
              // if (this.useremail == "cd_auth2@yopmail.com") {
              //   this.router.navigateByUrl('/authorizer');
              // }
              // if (this.useremail == "cd_auth3@yopmail.com") {
              //   this.router.navigateByUrl('/authorizer');
              // }    
              // if(flag=='true'){
                this.router.navigateByUrl('/authorizer');
              // }
          }
          else {
            this.router.navigateByUrl('/authorizer');
          }
        }
        else{
          this.openErrorModal(res['message'],'error message');         
          }
        }
        catch(error)
      {
        this.openErrorModal(error['message'],'error message');
      }
      }            
      }

       //open error pop up
   openErrorModal(message,heading)
   {
     this.alertHead=heading;
     this.errorMessage=message;
     this.ErrorModal=true;
   }
   //close error pop up
   closeErrorModal()
   {
     this.ErrorModal=false;
   }
     
}