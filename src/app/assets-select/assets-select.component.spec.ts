import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsSelectComponent } from './assets-select.component';

describe('AssetsSelectComponent', () => {
  let component: AssetsSelectComponent;
  let fixture: ComponentFixture<AssetsSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
