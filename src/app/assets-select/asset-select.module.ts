import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetsSelectComponent } from './assets-select.component';
import { AssetSelectRoutingModule } from './asset-select-routing.module';
import { ShareModule } from '../share.module';


@NgModule({
  declarations: [AssetsSelectComponent],
  imports: [
    CommonModule,
    AssetSelectRoutingModule,
    ShareModule
  ]
})
export class AssetSelectModule { }
