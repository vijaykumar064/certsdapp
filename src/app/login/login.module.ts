import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {CommonModule}from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
//import { ShareModule } from '../share.module';
 import { ShareModule } from '../share.module';
 import { NgxCaptchaModule } from 'ngx-captcha';
 import { RecaptchaModule } from 'ng-recaptcha';
@NgModule({
  declarations: [LoginComponent],
  imports: [
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ShareModule,
    NgxCaptchaModule,
    RecaptchaModule.forRoot()
    
  ]
})
export class LoginModule { }
